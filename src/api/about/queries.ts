import { axiosInstance } from 'api/axios';
import { IAboutCategory, IPaginationRequest } from 'api/types';
import { useQuery, UseQueryOptions } from 'react-query';
import { IAboutResponse, IAboutsResponse } from './types';

export function useAboutQuery(request: IPaginationRequest, options?: UseQueryOptions<IAboutsResponse>) {
  return useQuery<IAboutsResponse>(
    ['/articles/paging_filter_about', request],
    async () => {
      const { data } = await axiosInstance.post('/articles/paging_filter_about', request);
      return data;
    },
    options
  );
}

export function useGetDetailAbout(id: string, options?: UseQueryOptions<IAboutResponse>) {
  return useQuery<IAboutResponse>(
    ['/articles/item-about', id],
    async () => {
      const { data } = await axiosInstance.get(`/articles/item-about/${id}`);
      return data;
    },
    options
  );
}

export function useGetCategoryAbout(params: IAboutCategory, options?: UseQueryOptions<any>) {
  return useQuery<any>(
    ['/articles/select-category-about', params],
    async () => {
      const { data } = await axiosInstance.get(`/articles/select-category-about`, { params });
      return data;
    },
    options
  );
}

export function useGetDetailCategoryAbout(id: string, options?: UseQueryOptions<any>) {
  return useQuery<any>(
    ['/articles/item-category-about'],
    async () => {
      const { data } = await axiosInstance.get(`/articles/item-category-about/${id}`);
      return data;
    },
    options
  );
}
