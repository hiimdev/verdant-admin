import { axiosInstance, axiosDirectly } from 'api/axios';

export const createAbout = async (params: FormData): Promise<any> => {
  const { data } = await axiosDirectly.post(`/articles/create-about`, params);
  return data;
};

export const updateAbout = async (params: FormData): Promise<any> => {
  const { data } = await axiosDirectly.post(`/articles/update-about`, params);
  return data;
};

export const deleteAbout = async (id: string): Promise<any> => {
  const { data } = await axiosInstance.delete(`/articles/delete-about/${id}`);
  return data;
};

export const createAboutCategory = async (request: any): Promise<any> => {
  const { data } = await axiosInstance.post(`/articles/create-category-about`, request);
  return data;
};

export const updateAboutCategory = async (request: any): Promise<any> => {
  const { data } = await axiosInstance.post(`/articles/update-category-about`, request);
  return data;
};
