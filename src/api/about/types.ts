import { IPaginationResponse } from 'api/types';

export interface IAboutResponse {
  id: number;
  title: string;
  description: string;
  create_at: string;
  update_at: string;
  image: string;
  logoUrl: string;
  categoryAboutId: string | number;
  status: string;
  section: string;
  logoBlack: string;
}

export interface IAboutsResponse {
  list: IAboutResponse[];
  pagination: IPaginationResponse;
}
