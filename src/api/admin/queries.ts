import { axiosInstance } from 'api/axios';
import { IPaginationRequest } from 'api/types';
import { useQuery, UseQueryOptions } from 'react-query';
import {
  IAdminListResponse,
  IAdminResponse,
  IGasResponse,
  ILimitTransactionResponse,
  IMaiboxResponse,
  IScoreWalletResponse,
  IVMTStatisticalResponse,
  IWeightResponse,
  IGasTankResponse,
} from './types';

export function useAdminsQuery(request: IPaginationRequest, options?: UseQueryOptions<IAdminListResponse>) {
  return useQuery<IAdminListResponse>(
    ['/admin/paging_filter', request],
    async () => {
      const { data } = await axiosInstance.post('/admin/paging_filter', request);
      return data;
    },
    options
  );
}

export function useAdminQuery(id: string, options?: UseQueryOptions<IAdminResponse>) {
  return useQuery<IAdminResponse>(
    ['/admin/info-admin', id],
    async () => {
      const { data } = await axiosInstance.get(`/admin/info-admin/${id}`);
      return data;
    },
    options
  );
}

export function useGetLimitTransaction(params?: any, options?: UseQueryOptions<ILimitTransactionResponse>) {
  return useQuery<ILimitTransactionResponse>(
    ['/admin/get-limit-transaction'],
    async () => {
      const { data } = await axiosInstance.get('/admin/get-limit-transaction');
      return data;
    },
    options
  );
}
export function useGetGas(options?: UseQueryOptions<IGasResponse>) {
  return useQuery<IGasResponse>(
    ['/admin/get-gas'],
    async () => {
      const { data } = await axiosInstance.get('/admin/get-gas');
      return data;
    },
    options
  );
}

export function useGetScoreWallet(options?: UseQueryOptions<IScoreWalletResponse>) {
  return useQuery<IScoreWalletResponse>(
    ['/admin/get-score-wallet'],
    async () => {
      const { data } = await axiosInstance.get('/admin/get-score-wallet');
      return data;
    },
    options
  );
}

export function useGetGasTank(options?: UseQueryOptions<IGasTankResponse>) {
  return useQuery<IGasTankResponse>(
    ['/admin/get-seed-amount'],
    async () => {
      const { data } = await axiosInstance.get('/admin/get-seed-amount');
      return data;
    },
    options
  );
}

export function useGetVMTStatistical(options?: UseQueryOptions<IVMTStatisticalResponse>) {
  return useQuery<IVMTStatisticalResponse>(
    ['/transactions/statistical-vmt'],
    async () => {
      const { data } = await axiosInstance.get('/transactions/statistical-vmt');
      return data;
    },
    options
  );
}

export function useGetMailbox(options?: UseQueryOptions<IMaiboxResponse>) {
  return useQuery<IMaiboxResponse>(
    ['/admin/get-mailbox-contactus'],
    async () => {
      const { data } = await axiosInstance.get('/admin/get-mailbox-contactus');
      return data;
    },
    options
  );
}

export function useGetConfigListWeight(params: IPaginationRequest, options?: UseQueryOptions<IWeightResponse[]>) {
  return useQuery<IWeightResponse[]>(
    ['/config/admin/list-weight-data', params.toString()],
    async () => {
      const { data } = await axiosInstance.get('/config/admin/list-weight-data', { params });
      return data;
    },
    options
  );
}

export function useGetWeightDetail(id: string, options?: UseQueryOptions<IWeightResponse>) {
  return useQuery<IWeightResponse>(
    ['/config/item-weight-data/', id],
    async () => {
      const { data } = await axiosInstance.get(`/config/item-weight-data/${id}`);
      return data;
    },
    options
  );
}

export function useGetListChainLink(params: IPaginationRequest, options?: UseQueryOptions<IWeightResponse[]>) {
  return useQuery<IWeightResponse[]>(
    ['/config/list-chain-link', params.toString()],
    async () => {
      const { data } = await axiosInstance.get('/config/list-chain-link', { params });
      return data;
    },
    options
  );
}

export function useGetChainLinkDetail(id: string, options?: UseQueryOptions<IWeightResponse[]>) {
  return useQuery<IWeightResponse[]>(
    ['/config/get-chain-link/', id],
    async () => {
      const { data } = await axiosInstance.get(`/config/get-chain-link/${id}`);
      return data;
    },
    options
  );
}

export function useGetListFloatPrice(params: IPaginationRequest, options?: UseQueryOptions<IWeightResponse[]>) {
  return useQuery<IWeightResponse[]>(
    ['/chainlink/list-float-price', params.toString()],
    async () => {
      const { data } = await axiosInstance.get('/chainlink/list-float-price', { params });
      return data;
    },
    options
  );
}

export function useGetFloatPriceDetail(id: string, options?: UseQueryOptions<IWeightResponse[]>) {
  return useQuery<IWeightResponse[]>(
    ['/chainlink/item-float-price/', id],
    async () => {
      const { data } = await axiosInstance.get(`/chainlink/item-float-price/${id}`);
      return data;
    },
    options
  );
}
