import { axiosInstance } from 'api/axios';
import {
  IChainLinkRequest,
  ICreateWeightRequest,
  IFloatPriceRequest,
  IPercentRequest,
  IUpdateWeightRequest,
} from './types';

export const updateAdministrator = async (params: any): Promise<any> => {
  const { data } = await axiosInstance.post(`/admin/update-admin`, params);
  return data;
};

export const createAdministrator = async (params: any): Promise<any> => {
  const { data } = await axiosInstance.post(`/admin/register`, params);
  return data;
};

export const deleteAdministrator = async (id: string): Promise<any> => {
  const { data } = await axiosInstance.delete(`/admin/delete-admin/${id}`);
  return data;
};

export const setLimitTransaction = async (params: { limit: number }) => {
  const { data } = await axiosInstance.post(`/admin/set-limit-transaction`, params);
  return data;
};
export const setGas = async (params: { gas: number }) => {
  const { data } = await axiosInstance.post(`/admin/set-gas`, params);
  return data;
};
export const setScoreWallet = async (params: { score: number }) => {
  const { data } = await axiosInstance.post(`/admin/set-score-wallet`, params);
  return data;
};

export const setGasTank = async (params: { amount: number }) => {
  const { data } = await axiosInstance.post(`/admin/set-seed-amount`, params);
  return data;
};

export const setMailboxContactUs = async (params: { email: string }) => {
  const { data } = await axiosInstance.post(`/admin/set-mailbox-contactus`, params);
  return data;
};

export const setAllownacePercent = async (request: IPercentRequest) => {
  const { data } = await axiosInstance.post(`/nft-admin/create-purchase-rule`, request);
  return data;
};

export const addWeight = async (request: ICreateWeightRequest) => {
  const { data } = await axiosInstance.post(`/config/create-weight-data`, request);
  return data;
};

export const updateWeight = async (request: IUpdateWeightRequest) => {
  const { data } = await axiosInstance.post(`/config/update-weight-data`, request);
  return data;
};

export const setChainLink = async (request: IChainLinkRequest) => {
  const { data } = await axiosInstance.post(`/config/set-chain-link`, request);
  return data;
};

export const setFloatPrice = async (request: IFloatPriceRequest) => {
  const { data } = await axiosInstance.post(`/chainlink/set-float-price`, request);
  return data;
};
