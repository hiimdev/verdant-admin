import { IPaginationResponse } from 'api/types';

export interface IAdminResponse {
  avatar_url: string | null;
  email: string;
  fullName: string;
  group: number;
  id: number;
  isActive: number;
  status: string;
  updatedAt: string;
  username: string;
}

export interface IAdminListResponse {
  list: IAdminResponse[];
  pagination: IPaginationResponse;
}

export interface ILimitTransactionResponse {
  configValue: string;
}

export interface IGasResponse {
  configValue: string;
  id: number;
  configKey: string;
  createdAt: string;
  updatedAt: string;
  createdBy: string;
  updatedBy: string;
}
export interface IScoreWalletResponse {
  id: number;
  configKey: string;
  configValue: string;
  createdAt: string;
  updatedAt: string;
  createdBy: string;
  updatedBy: string;
}

export interface IGasTankResponse {
  id: number;
  configKey: string;
  configValue: string;
  createdAt: string;
  updatedAt: string;
  createdBy: string;
  updatedBy: string;
  amount: string;
}

export interface IVMTStatisticalResponse {
  totalVMTsCreatedFormat: string;
  totalVMTsVendorIssuedFormat: string;
  totalVMTsRemainingFormat: string;
  totalVMTsVendorUserMint: string;
}
export interface IMaiboxResponse {
  id: number;
  configKey: string;
  configValue: string;
  createdAt: string;
  updatedAt: string;
  createdBy: string;
  updatedBy: string;
}

export interface IPercentRequest {
  sellerOffer: number;
  buyerBid: number;
}

export interface IPercentResponse {
  id: number;
  sellerOffer: number;
  buyerBid: number;
  status: boolean;
  createdAt: string;
  updatedAt: string;
  createdBy: any;
  updatedBy: any;
}

export interface ICreateWeightRequest {
  gram: number;
  ounce: number;
  status: number;
}

export interface IUpdateWeightRequest {
  id: number;
  gram: number;
  ounce: number;
  status: number;
}

export interface IWeightResponse {
  id: number;
  gram: string;
  ounce: string;
  status: number;
  createdAt: string;
  updatedAt: string;
}

export interface IChainLinkRequest {
  assetName: string;
  aggregatorAddress: string;
  paymentToken: number;
}

export interface IFloatPriceRequest {
  deviationPercentage: string;
}
