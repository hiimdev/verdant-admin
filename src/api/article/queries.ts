import { axiosInstance } from 'api/axios';
import { IPaginationRequest } from 'api/types';
import { useQuery, UseQueryOptions } from 'react-query';
import { IArticleResponse, IArticlesResponse } from './types';

export function useArticlesQuery(request: IPaginationRequest, options?: UseQueryOptions<IArticlesResponse>) {
  return useQuery<IArticlesResponse>(
    ['/articles/paging_filter', request],
    async () => {
      const { data } = await axiosInstance.post('/articles/paging_filter', request);
      return data;
    },
    options
  );
}

export function useArticleQuery(id: string, options?: UseQueryOptions<IArticleResponse>) {
  return useQuery<IArticleResponse>(
    ['/articles/item', id],
    async () => {
      const { data } = await axiosInstance.get(`/articles/item/${id}`);
      return data;
    },
    options
  );
}
