import { axiosDirectly, axiosInstance } from 'api/axios';

export const updateArticles = async (params: any): Promise<any> => {
  const { data } = await axiosDirectly.post(`/articles/update`, params);
  return data;
};

export const createArticle = async (params: any): Promise<any> => {
  const { data } = await axiosDirectly.post(`/articles/create`, params);
  return data;
};

export const deleteArticle = async (id: any): Promise<any> => {
  const { data } = await axiosDirectly.delete(`/articles/delete-articles/${id}`);
  return data;
};

export const deleteBrand = async (id: string): Promise<any> => {
  const { data } = await axiosInstance.delete(`/brand/delete-brand/${id}`);
  return data;
};

export const uploadImage = async (request: FormData): Promise<any> => {
  const { data } = await axiosInstance.post(`/articles/upload-image`, request);
  return data;
};
