import { IPaginationResponse } from 'api/types';

export interface IArticleResponse {
  id: number;
  image: string;
  author: any;
  summary: string;
  imageType: string;
  publishDate: string;
  title: string;
  content: string;
  status: number;
  createdAt: string;
  updatedAt: string;
  createdBy: string;
  updatedBy: string;
  categoryName: string;
  categoryArticleId: number;
}
export interface IArticlesResponse {
  list: IArticleResponse[];
  pagination: IPaginationResponse;
}
