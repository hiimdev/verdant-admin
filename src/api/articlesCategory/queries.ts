import { axiosInstance } from 'api/axios';
import { useQuery, UseQueryOptions } from 'react-query';
import { ICategoryArticlesResponse, IDetailArticlesCategoryResponse } from './types';

export function useGetArticlesListCategoryArticles(
  params: { page: number; limit: number },
  options?: UseQueryOptions<ICategoryArticlesResponse>
) {
  return useQuery<ICategoryArticlesResponse>(
    ['/articles/list-category-article', params],
    async () => {
      const { data } = await axiosInstance.get('/articles/list-category-article', { params });
      return data;
    },
    options
  );
}
export function useArticlesCategoryQuery(id: string, options?: UseQueryOptions<IDetailArticlesCategoryResponse>) {
  return useQuery<IDetailArticlesCategoryResponse>(
    ['/articles/item-category-article', id],
    async () => {
      const { data } = await axiosInstance.get(`/articles/item-category-article/${id}`);
      return data;
    },
    options
  );
}
