import { axiosInstance } from 'api/axios';
import { IParamsArticlesCategory } from './types';

export const createArticlesCategory = async (params: IParamsArticlesCategory): Promise<any> => {
  const { data } = await axiosInstance.post(`/articles/create-category-article`, params);
  return data;
};
export const updateArticlesCategory = async (params: IParamsArticlesCategory): Promise<any> => {
  const { data } = await axiosInstance.post(`/articles/update-category-article`, params);
  return data;
};
export const deleteArticlesCategory = async (id: string): Promise<any> => {
  const { data } = await axiosInstance.delete(`/articles/delete-category-article/${id}`);
  return data;
};
