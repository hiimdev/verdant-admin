import { IPaginationResponse } from 'api/types';

export interface ICategoryArticles {
  id: string;
  categoryName: string;
  categoryDescription: string;
}

export interface ICategoryArticlesResponse {
  list: ICategoryArticles[];
  pagination: IPaginationResponse;
}

export interface IParamsArticlesCategory {
  categoryName: string;
  categoryDescription: string;
  id?: number;
}
export interface IDetailArticlesCategoryResponse {
  id: string;
  categoryName: string;
  categoryDescription: string;
  isDeleted: number;
  createdBy: string;
  updatedBy: string;
  createdAt: string;
  updatedAt: string;
}
