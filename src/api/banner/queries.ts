import { axiosInstance } from 'api/axios';
import { IPaginationRequest } from 'api/types';
import { useQuery, UseQueryOptions } from 'react-query';
import { IFaqsResponse } from './types';

export function useGetListBanner(params: IPaginationRequest, options?: UseQueryOptions<IFaqsResponse>) {
  return useQuery<IFaqsResponse>(
    ['/articles/list-banner', params],
    async () => {
      const { data } = await axiosInstance.get('/articles/list-banner', { params });
      return data;
    },
    options
  );
}

export function useGetDetailBanner(id: string, options?: UseQueryOptions<any>) {
  return useQuery<any>(
    ['/articles/item-banner', id],
    async () => {
      const { data } = await axiosInstance.get(`/articles/item-banner/${id}`);
      return data;
    },
    options
  );
}
