import { axiosInstance, axiosDirectly } from 'api/axios';

export const updateBanner = async (params: FormData): Promise<any> => {
  const { data } = await axiosDirectly.post(`/articles/update-banner`, params);
  return data;
};

export const deleteBanner = async (id: string): Promise<any> => {
  const { data } = await axiosInstance.delete(`/articles/delete-banner/${id}`);
  return data;
};
