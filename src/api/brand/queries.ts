import { axiosInstance } from 'api/axios';
import { ICountResponse, IPaginationRequest } from 'api/types';
import { useQuery, UseQueryOptions } from 'react-query';
import { IBrandResponse, IBrandsResponse } from './types';

export function useBrandsQuery(request: IPaginationRequest, options?: UseQueryOptions<IBrandsResponse>) {
  return useQuery<IBrandsResponse>(
    ['/brand/paging_filter', request],
    async () => {
      const { data } = await axiosInstance.post('/brand/paging_filter', request);
      return data;
    },
    options
  );
}

export function useBrandQuery(id: string, options?: UseQueryOptions<IBrandResponse>) {
  return useQuery<IBrandResponse>(
    ['/brand/item', id],
    async () => {
      const { data } = await axiosInstance.get(`/brand/item/${id}`);
      return data;
    },
    options
  );
}

export function useBrandsListVendor(
  params: { page: number; limit: number; userId: number; vendorId: number },
  options?: UseQueryOptions<IBrandsResponse>
) {
  return useQuery<IBrandsResponse>(
    ['/brand/list-brand-user', params],
    async () => {
      const { data } = await axiosInstance.get('/brand/list-brand-user', { params });
      return data;
    },
    options
  );
}

export function useBrandCountNtfs(id: string, options?: UseQueryOptions<ICountResponse>) {
  return useQuery<ICountResponse>(
    ['/brand/count-nfts', id],
    async () => {
      const { data } = await axiosInstance.get(`/brand/count-nfts/${id}`);
      return data;
    },
    options
  );
}
