import { axiosInstance, axiosDirectly } from 'api/axios';

export const updateBrand = async (params: any): Promise<any> => {
  const { data } = await axiosDirectly.post(`/brand/update`, params);
  return data;
};

export const createBrand = async (params: any): Promise<any> => {
  const { data } = await axiosDirectly.post(`/brand/create`, params);
  return data;
};

export const deleteBrand = async (id: string): Promise<any> => {
  const { data } = await axiosInstance.delete(`/brand/delete-brand/${id}`);
  return data;
};
