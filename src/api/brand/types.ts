import { IPaginationResponse } from 'api/types';

export interface IBrandResponse {
  brandName: string;
  createdAt: string;
  description: string;
  descriptionType: string;
  descriptionUrl: string;
  logotype: string;
  logoUrl: string;
  status: string | null;
  updatedAt: string;
  id: number;
  logoBlackUrl: string;
  avatar: string;
}

export interface IBrandsResponse {
  list: IBrandResponse[];
  pagination: IPaginationResponse;
}
