import { axiosInstance } from 'api/axios';
import { ICountResponse, IPaginationRequest } from 'api/types';
import { useQuery, UseQueryOptions } from 'react-query';
import { ICategoryResponse, ICategoriesResponse } from './types';

export function useCategoriesQuery(request?: IPaginationRequest, options?: UseQueryOptions<ICategoriesResponse>) {
  return useQuery<ICategoriesResponse>(
    ['/category/paging_filter', request],
    async () => {
      const { data } = await axiosInstance.post('/category/paging_filter', request);
      return data;
    },
    options
  );
}

export function useCategoryQuery(id: string, options?: UseQueryOptions<ICategoryResponse>) {
  return useQuery<ICategoryResponse>(
    ['/category/item', id],
    async () => {
      const { data } = await axiosInstance.get(`/category/item/${id}`);
      return data;
    },
    options
  );
}
export function useCategoryCountNtfs(id: string, options?: UseQueryOptions<ICountResponse>) {
  return useQuery<ICountResponse>(
    ['/category/count-nfts', id],
    async () => {
      const { data } = await axiosInstance.get(`/category/count-nfts/${id}`);
      return data;
    },
    options
  );
}
