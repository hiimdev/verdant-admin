import { axiosDirectly, axiosInstance } from 'api/axios';

export const updateCategory = async (params: any): Promise<any> => {
  const { data } = await axiosDirectly.post(`/category/update`, params);
  return data;
};

export const createCategory = async (params: any): Promise<any> => {
  const { data } = await axiosDirectly.post(`/category/create`, params);
  return data;
};

export const deleteCategory = async (id: string): Promise<any> => {
  const { data } = await axiosInstance.delete(`/category/delete/${id}`);
  return data;
};
