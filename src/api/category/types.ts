import { IPaginationResponse } from 'api/types';

export interface ICategoryResponse {
  categoryName: string;
  id: number;
  createdAt: string;
  updatedAt: string;
  categoryDescription: string;
  logoUrl: string;
  descriptionUrl: string;
}

export interface ICategoriesResponse {
  list: ICategoryResponse[];
  pagination: IPaginationResponse;
}
