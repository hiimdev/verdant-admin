import { axiosInstance } from 'api/axios';
import { ICountResponse, IPaginationRequest } from 'api/types';
import { useQuery, UseQueryOptions } from 'react-query';
import { ICollectionResponse, ICollectionsResponse } from './types';

export function useCollectionsQuery(request: IPaginationRequest, options?: UseQueryOptions<ICollectionsResponse>) {
  return useQuery<ICollectionsResponse>(
    ['/collection-admin/paging_filter', request],
    async () => {
      const { data } = await axiosInstance.post('/collection-admin/paging_filter', request);
      return data;
    },
    options
  );
}

export function useCollectionQuery(id: string, options?: UseQueryOptions<ICollectionResponse>) {
  return useQuery<ICollectionResponse>(
    ['/collection-admin/item', id],
    async () => {
      const { data } = await axiosInstance.get(`/collection-admin/item/${id}`);
      return data.length > 0 ? data[0] : data;
    },
    options
  );
}

export function useHotCollectionsQueryDB(params: IPaginationRequest, options?: UseQueryOptions<ICollectionsResponse>) {
  return useQuery<ICollectionsResponse>(
    ['/collection-admin/list-collection-hot-db', params],
    async () => {
      const { data } = await axiosInstance.get('/collection-admin/list-collection-hot-db', { params });
      return data;
    },
    options
  );
}

export function useHotCollectionsQueryAdmin(
  params: IPaginationRequest,
  options?: UseQueryOptions<ICollectionsResponse>
) {
  return useQuery<ICollectionsResponse>(
    ['/collection-admin/list-collection-hot-admin', params],
    async () => {
      const { data } = await axiosInstance.get('/collection-admin/list-collection-hot-admin', { params });
      return data;
    },
    options
  );
}

export function useListChooseCollection(params: IPaginationRequest, options?: UseQueryOptions<ICollectionsResponse>) {
  return useQuery<ICollectionsResponse>(
    ['/collection-admin/list-collection-choose-hot-admin', params],
    async () => {
      const { data } = await axiosInstance.get('/collection-admin/list-collection-choose-hot-admin', { params });
      return data;
    },
    options
  );
}

export function useGetIsShow(options?: UseQueryOptions<number>) {
  return useQuery<number>(
    '/collection-admin/is-show',
    async () => {
      const { data } = await axiosInstance.get('/collection-admin/is-show');
      return data;
    },
    options
  );
}

export function useCollectionCountNtfs(id: string, options?: UseQueryOptions<ICountResponse>) {
  return useQuery<ICountResponse>(
    ['/collection-admin/count-nfts', id],
    async () => {
      const { data } = await axiosInstance.get(`/collection-admin/count-nfts/${id}`);
      return data;
    },
    options
  );
}
