import { axiosInstance } from 'api/axios';
import { IAddListAdminRequest, IHotShowRequest } from './types';

export const removeCollectionFromHotList = async (id: string): Promise<any> => {
  const { data } = await axiosInstance.get(`/collection-admin/remove-list-hot/${id}`);
  return data;
};

export const moveUpCollection = async (id: string): Promise<any> => {
  const { data } = await axiosInstance.get(`/collection-admin/move-up-collection/${id}`);
  return data;
};

export const moveDownCollection = async (id: string): Promise<any> => {
  const { data } = await axiosInstance.get(`/collection-admin/move-down-collection/${id}`);
  return data;
};

export const updateShowCollection = async (params: IHotShowRequest): Promise<any> => {
  const { data } = await axiosInstance.post(`/collection-admin/update-is-show`, params);
  return data;
};

export const collectionAddListAdmin = async (params: IAddListAdminRequest): Promise<any> => {
  const { data } = await axiosInstance.post(`/collection-admin/add-list-admin`, params);
  return data;
};

export const addCollection = async (request: FormData): Promise<any> => {
  const { data } = await axiosInstance.post(`/collection-admin/create`, request);
  return data;
};

export const updateCollection = async (request: FormData): Promise<any> => {
  const { data } = await axiosInstance.post(`/collection-admin/update`, request);
  return data;
};

export const deleteCollection = async (id: string): Promise<any> => {
  const { data } = await axiosInstance.delete(`/collection-admin/delete-collection/${id}`);
  return data;
};
