import { IPaginationResponse } from 'api/types';

export interface ICollectionResponse {
  createdAt: string;
  description: string;
  id: number;
  imageUrl: string;
  name: string;
  ownerId: number;
  ownerName: string;
  owner_email: string;
  owner_first_name: string;
  owner_last_name: string;
  owner_name: string;
  paymentToken: string;
  slug: string;
  status: null;
  type: null;
  updatedAt: string;
  id_hot_collection: string;
  minPrice: string;
  maxPrice: string;
  numberNft: string;
  brandName: string;
}

export interface ICollectionsResponse {
  list: ICollectionResponse[];
  pagination: IPaginationResponse;
}

export interface IHotCollectionResponse {
  created_at: string;
  data: null;
  description: string;
  id: number;
  image_url: string;
  index_show: number;
  is_show: number;
  name: string;
  owner_id: number;
  owner_name: string;
  payment_token: string;
  slug: string;
  status: null;
  type: null;
  updated_at: string;
}

export interface IHotCollectionsResponse {
  list: IHotCollectionResponse[];
  pagination: IPaginationResponse;
}

export interface IHotShowRequest {
  isShow: boolean;
}

export interface IAddListAdminRequest {
  collectionId: number;
  indexShow: number | string;
}
