import { axiosInstance } from 'api/axios';
import { IPaginationRequest } from 'api/types';
import { useQuery, UseQueryOptions } from 'react-query';
import {
  IBrandsCommissionResponse,
  ICategoriesCommissionResponse,
  ICollectionsCommissionResponse,
  ICommissionResponse,
  ICommissionsResponse,
  IHistoryCommissionRequest,
} from './types';

export function useGetCommission(params: IPaginationRequest, options?: UseQueryOptions<ICommissionsResponse>) {
  return useQuery<ICommissionsResponse>(
    ['/commission/list', params],
    async () => {
      const { data } = await axiosInstance.get('/commission/list', { params });
      const temp = data?.list?.map((item: ICommissionResponse) => {
        const request = { ...item };
        if (!request.category_commission_id) {
          request.category_name = 'Default';
        } else if (!request.brand_id) {
          request.brand_name = 'Default';
        } else {
          if (!request.collection_id) {
            request.collection_name = 'Default';
          }
        }
        return { ...request };
      });
      data.list = temp;
      return data;
    },
    options
  );
}

export function useGetCommissionById(id: string | number | null, options?: UseQueryOptions<any>) {
  return useQuery<any>(
    ['/commission/item', id],
    async () => {
      const { data } = await axiosInstance.get(`/commission/item/${id}`);
      return data;
    },
    options
  );
}

export function useGetListCategoryOfCommission(params: IPaginationRequest, options?: UseQueryOptions<any>) {
  return useQuery<any>(
    ['/category/list-category-of-commission', params],
    async () => {
      const { data } = await axiosInstance.get('/category/list-category-of-commission', { params });
      return data;
    },
    options
  );
}

export function useGetHistoryCommission(
  params: IHistoryCommissionRequest,
  options?: UseQueryOptions<ICommissionsResponse>
) {
  return useQuery<ICommissionsResponse>(
    ['/commission/list-history', params],
    async () => {
      const { data } = await axiosInstance.get(`/commission/list-history`, { params });
      return data;
    },
    options
  );
}

export function useGetCategoryCommission(
  params: IPaginationRequest,
  options?: UseQueryOptions<ICategoriesCommissionResponse>
) {
  return useQuery<ICategoriesCommissionResponse>(
    ['/category/list-category-commission', params],
    async () => {
      const { data } = await axiosInstance.get('/category/list-category-commission', { params });
      return data;
    },
    options
  );
}

export function useGetBrandCommission(
  params: IPaginationRequest,
  options?: UseQueryOptions<IBrandsCommissionResponse>
) {
  return useQuery<IBrandsCommissionResponse>(
    ['/brand/list-brand-commission', params],
    async () => {
      const { data } = await axiosInstance.get('/brand/list-brand-commission', { params });
      return data;
    },
    options
  );
}

export function useGetCollectionCommission(
  params: IPaginationRequest,
  options?: UseQueryOptions<ICollectionsCommissionResponse>
) {
  return useQuery<ICollectionsCommissionResponse>(
    ['/collection-admin/list-collection-commission', params],
    async () => {
      const { data } = await axiosInstance.get('/collection-admin/list-collection-commission', { params });
      return data;
    },
    options
  );
}
