import { axiosInstance } from 'api/axios';
import { ICommissionRequest } from './types';

export const updateCommission = async (params: ICommissionRequest): Promise<any> => {
  const { data } = await axiosInstance.post(`/commission/update`, params);
  return data;
};

export const createCommission = async (params: ICommissionRequest): Promise<any> => {
  const { data } = await axiosInstance.post(`/commission/create`, params);
  return data;
};

export const deleteCommission = async (id: string): Promise<any> => {
  const { data } = await axiosInstance.delete(`/commission/delete-commission/${id}`);
  return data;
};
