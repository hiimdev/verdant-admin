import { IPaginationResponse } from 'api/types';

export interface ICommissionResponse {
  name_admin: string;
  commission: string;
  updated_at: string;
  status: number;
  categoryCommissionId?: number | string;
  description?: string;
  id: number;
  id_admin: number;
  category_commission_id: number;
  brand_id: number;
  collection_id: number;
  seller_commission: number;
  buyer_commission: number;
  created_at: string;
  category_name: string;
  collection_name: string;
  brand_name: string;
}

export interface ICommissionsResponse {
  list: ICommissionResponse[];
  pagination: IPaginationResponse;
}

export interface ICommissionRequest {
  buyerCommission: string | number;
  sellerCommission: string | number;
  categoryCommissionId: string | number;
  collectionId: string | number;
  brandId: string | number;
  description?: string | number;
  action?: 'add' | 'edit';
  vendorId: string;
}

export interface IHistoryCommissionRequest {
  page: number;
  limit: number;
  category_commission_id?: number | string | null;
  brand_id?: number | string | null;
  collection_id?: number | string | null;
}

export interface ICategoryCommissionResponse {
  categoryId: string | number;
  categoryName: string;
}

export interface IBrandCommissionResponse {
  categoryId: string | number;
  categoryName: string;
  brandId: number;
  brandName: string;
}

export interface ICollectionCommissionResponse {
  categoryId: string | number;
  categoryName: string;
  collectionId: number;
  collectionName: string;
  brandName: string;
  brandId: number;
}

export interface ICategoriesCommissionResponse {
  list: ICategoryCommissionResponse[];
  pagination: IPaginationResponse;
}

export interface IBrandsCommissionResponse {
  list: IBrandCommissionResponse[];
  pagination: IPaginationResponse;
}

export interface ICollectionsCommissionResponse {
  list: ICollectionCommissionResponse[];
  pagination: IPaginationResponse;
}
