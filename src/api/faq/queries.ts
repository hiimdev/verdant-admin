import { axiosInstance } from 'api/axios';
import { IPaginationRequest } from 'api/types';
import { useQuery, UseQueryOptions } from 'react-query';
import { IFaqResponse, IFaqsResponse } from './types';

export function useFaqsQuery(request: IPaginationRequest, options?: UseQueryOptions<IFaqsResponse>) {
  return useQuery<IFaqsResponse>(
    ['/articles/paging_filter_faq', request],
    async () => {
      const { data } = await axiosInstance.post('/articles/paging_filter_faq', request);
      return data;
    },
    options
  );
}

export function useFaqQuery(id: string, options?: UseQueryOptions<IFaqResponse>) {
  return useQuery<IFaqResponse>(
    ['/articles/item-faq', id],
    async () => {
      const { data } = await axiosInstance.get(`/articles/item-faq/${id}`);
      return data;
    },
    options
  );
}

export function useFaqQueryHome(request: IPaginationRequest, options?: UseQueryOptions<IFaqsResponse>) {
  return useQuery<IFaqsResponse>(
    ['/articles/paging-filter-faq-priority', request],
    async () => {
      const { data } = await axiosInstance.post('/articles/paging-filter-faq-priority', request);
      return data;
    },
    options
  );
}
