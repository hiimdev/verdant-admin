import { axiosInstance, axiosDirectly } from 'api/axios';

export const updateFaq = async (params: FormData): Promise<any> => {
  const { data } = await axiosDirectly.post(`/articles/update-faq`, params);
  return data;
};

export const createFaq = async (params: FormData): Promise<any> => {
  const { data } = await axiosDirectly.post(`/articles/create-faq`, params);
  return data;
};

export const deleteFaq = async (id: string): Promise<any> => {
  const { data } = await axiosInstance.delete(`/articles/delete-faq/${id}`);
  return data;
};

export const deleteFaqHome = async (id: string): Promise<any> => {
  const { data } = await axiosInstance.post(`/articles/remove-priority-faq/${id}`);
  return data;
};

export const sortFAQHome = async (request: any): Promise<any> => {
  const { data } = await axiosInstance.post(`/articles/sort-priority-faq/${request.id}`, request);
  return data;
};

export const addFAQHome = async (id: string): Promise<any> => {
  const { data } = await axiosInstance.post(`/articles/add-priority-faq/${id}`);
  return data;
};
