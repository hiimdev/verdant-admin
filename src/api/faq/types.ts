import { IPaginationResponse } from 'api/types';

export interface IFaqResponse {
  answer: string | null;
  categoryId: string | null;
  createdAt: string;
  id: number;
  question: string | null;
  updated_at: string;
  logoUrl: string;
}

export interface IFaqsResponse {
  list: IFaqResponse[];
  pagination: IPaginationResponse;
}
