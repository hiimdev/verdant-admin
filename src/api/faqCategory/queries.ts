import { axiosInstance } from 'api/axios';
import { IPaginationRequest } from 'api/types';
import { useQuery, UseQueryOptions } from 'react-query';
import { ICategoryResponse, ICategoriesResponse } from './types';

export function useFaqCategoriesQuery(request?: IPaginationRequest, options?: UseQueryOptions<ICategoriesResponse>) {
  return useQuery<ICategoriesResponse>(
    ['/articles/paging_filter_category', request],
    async () => {
      const { data } = await axiosInstance.post('/articles/paging_filter_category', request);
      return data;
    },
    options
  );
}

export function useFaqCategoryQuery(id: string, options?: UseQueryOptions<ICategoryResponse>) {
  return useQuery<ICategoryResponse>(
    ['/articles/item-category', id],
    async () => {
      const { data } = await axiosInstance.get(`/articles/item-category/${id}`);
      return data;
    },
    options
  );
}
