import { axiosInstance } from 'api/axios';

export const updateFaqCategory = async (params: any): Promise<any> => {
  const { data } = await axiosInstance.post(`/articles/update-category`, params);
  return data;
};

export const createFaqCategory = async (params: any): Promise<any> => {
  const { data } = await axiosInstance.post(`/articles/create-category`, params);
  return data;
};

export const deleteFaqCategory = async (id: string): Promise<any> => {
  const { data } = await axiosInstance.delete(`/articles/delete-category/${id}`);
  return data;
};
