import { IPaginationResponse } from 'api/types';

export interface ICategoryResponse {
  categoryName: string;
  id: number;
  createdAt: string;
  updatedAt: string;
}

export interface ICategoriesResponse {
  list: ICategoryResponse[];
  pagination: IPaginationResponse;
}
