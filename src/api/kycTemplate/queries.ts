import { axiosInstance } from 'api/axios';
import { IPaginationRequest } from 'api/types';
import { useQuery, UseQueryOptions } from 'react-query';
import { IKYCTemplate, IListKYCTemplate } from './types';

export function useGetKYCTemplate(params?: IPaginationRequest, options?: UseQueryOptions<IListKYCTemplate[]>) {
  return useQuery<IListKYCTemplate[]>(
    ['/user-admin/list-templates-kyc', params],
    async () => {
      const { data } = await axiosInstance.get('/user-admin/list-templates-kyc');
      const { data: currentData } = await axiosInstance.get(`/user-admin/templates-kyc`);

      return {
        list: data.map((d: IKYCTemplate) => {
          if (currentData?.templateId && currentData.templateId === d.templateId) {
            return {
              ...d,
              isUsing: true,
            };
          } else {
            return {
              ...d,
              isUsing: false,
            };
          }
        }),
      } as any;
    },
    options
  );
}

export function useGetCurrentKYCTemplate(options?: UseQueryOptions<IKYCTemplate>) {
  return useQuery<IKYCTemplate>(
    ['/user-admin/template-kyc'],
    async () => {
      const { data } = await axiosInstance.get(`/user-admin/templates-kyc`);
      return data;
    },
    options
  );
}
