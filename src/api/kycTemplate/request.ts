import { axiosInstance } from 'api/axios';
import { IKYCTemplate } from './types';

export const setTemplateKYC = async (payload: IKYCTemplate): Promise<any> => {
  const { data } = await axiosInstance.post(`/user-admin/set-templates-kyc`, payload);
  return data;
};
