import { IPaginationResponse } from 'api/types';

export interface IKYCTemplate {
  description: string;
  isSignUpTemplate: boolean;
  name: string;
  signUpMethodEmail: boolean;
  signUpMethodWallet: boolean;
  templateId: string;
  type: string;
  isUsing?: boolean;
}

export interface IListKYCTemplate {
  list: IKYCTemplate[];
  pagination: IPaginationResponse;
}
