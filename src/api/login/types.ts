export interface IUser {
  id: number;
  email: string;
  username: string;
}

export interface ILoginRequest {
  email: string;
  password: string;
  twofa: string;
}

export interface ILoginResponse {
  email: string;
  password: string;
}

export interface IGen2FARequest {
  email: string;
  password: string;
}

export interface IGen2FAResponse {
  isActive2fa: number;
  twoFactorAuthenticationSecret: string;
}
