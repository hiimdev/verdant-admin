import { axiosInstance } from 'api/axios';
import { IPaginationRequest } from 'api/types';
import { useQuery, UseQueryOptions } from 'react-query';
import { IMuslimResponse } from './types';

interface IMuslimPaginationRequest extends IPaginationRequest {
  countriesName?: string;
}

export function useGetListMuslim(params: IMuslimPaginationRequest, options?: UseQueryOptions<IMuslimResponse>) {
  return useQuery<IMuslimResponse>(
    ['/config/list', params],
    async () => {
      const { data } = await axiosInstance.get('/config/list', { params });
      return data;
    },
    options
  );
}

export function useGetDetailMuslim(id: string, options?: UseQueryOptions<IMuslimResponse>) {
  return useQuery<IMuslimResponse>(
    ['/config/item', id],
    async () => {
      const { data } = await axiosInstance.get(`/config/item/${id}`);
      return data;
    },
    options
  );
}
