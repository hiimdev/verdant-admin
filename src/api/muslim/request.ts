import { axiosInstance } from 'api/axios';
import { IMuslimRequest } from './types';

export const configMuslim = async (request: IMuslimRequest): Promise<any> => {
  const { data } = await axiosInstance.post(`/config/set-is-muslim`, request);
  return data;
};
