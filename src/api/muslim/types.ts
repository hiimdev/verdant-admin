import { IPaginationResponse } from 'api/types';

export interface IMuslimResponse {
  list: any[];
  pagination: IPaginationResponse;
}

export interface IMuslimRequest {
  id: number;
  isMuslim: number;
  legalDrinkingAge: number;
}
