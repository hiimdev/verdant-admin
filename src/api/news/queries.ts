import { axiosInstance } from 'api/axios';
import { IPaginationRequest } from 'api/types';
import { useQuery, UseQueryOptions } from 'react-query';
import { INewResponse, INewsResponse } from './types';

export function useGetNews(request?: IPaginationRequest, options?: UseQueryOptions<INewsResponse>) {
  return useQuery<INewsResponse>(
    ['/articles/paging-filter-news', request],
    async () => {
      const { data } = await axiosInstance.post('/articles/paging-filter-news', request);
      return data;
    },
    options
  );
}

export function useGetNewsDetail(id: string, options?: UseQueryOptions<INewResponse>) {
  return useQuery<INewResponse>(
    ['/articles/item-news', id],
    async () => {
      const { data } = await axiosInstance.get(`/articles/item-news/${id}`);
      return data;
    },
    options
  );
}
