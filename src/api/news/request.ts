import { axiosDirectly, axiosInstance } from 'api/axios';

export const updateNews = async (params: FormData): Promise<any> => {
  const { data } = await axiosDirectly.post(`/articles/update-news`, params);
  return data;
};

export const createNews = async (params: FormData): Promise<any> => {
  const { data } = await axiosDirectly.post(`/articles/create-news`, params);
  return data;
};

export const deleteNews = async (id: string): Promise<any> => {
  const { data } = await axiosInstance.delete(`/articles/delete-news/${id}`);
  return data;
};
