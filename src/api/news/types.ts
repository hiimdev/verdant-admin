import { IPaginationResponse } from 'api/types';

export interface INewResponse {
  id: number;
  title: string;
  summary: string;
  image: string;
  imageType: string;
  status: number;
  createdAt: string;
  updatedAt: string;
  createdBy: string;
  updatedBy: string;
  publishDate: string;
  categoryName: string;
  categoryArticleId: number;
  link: string;
  author: string;
}

export interface INewsResponse {
  list: INewResponse[];
  pagination: IPaginationResponse;
}
