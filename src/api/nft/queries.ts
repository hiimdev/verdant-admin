import { axiosInstance } from 'api/axios';
import { IPaginationRequest } from 'api/types';
import { useQuery, UseQueryOptions } from 'react-query';
import {
  INFTResponse,
  INFTsResponse,
  ITop10sResponse,
  INFTResponseV2,
  ISellerResponse,
  IAllPurchaseRuleDefaultRequest,
  IHistoryPurchaseRuleRequest,
} from './types';

export function useNFTsQuery(request: IPaginationRequest, options?: UseQueryOptions<INFTsResponse>) {
  return useQuery<INFTsResponse>(
    ['/nft-admin/paging_filter', request],
    async () => {
      const { data } = await axiosInstance.post('/nft-admin/paging_filter', request);
      return data;
    },
    options
  );
}

export function useNFTsQueryByCollection(params: IPaginationRequest, options?: UseQueryOptions<INFTsResponse>) {
  return useQuery<INFTsResponse>(
    ['/collection-admin/list-nft-by-collectionid', params],
    async () => {
      const { data } = await axiosInstance.get('/collection-admin/list-nft-by-collectionid', { params });
      return data;
    },
    options
  );
}

export function useNFTQuery(id: string, options?: UseQueryOptions<INFTResponseV2>) {
  return useQuery<INFTResponseV2>(
    ['/nft-admin/item', id],
    async () => {
      const { data } = await axiosInstance.get(`/nft-admin/item/${id}`);
      return data.length > 0 ? data[0] : data;
    },
    options
  );
}

export function useLogNftQuery(params: any, options?: UseQueryOptions<INFTResponse>) {
  return useQuery<INFTResponse>(
    ['/nft-admin/log-nft', params],
    async () => {
      const { data } = await axiosInstance.get(`/nft-admin/log-nft`, { params });
      return data.length > 0 ? data[0] : data;
    },
    { ...options, enabled: !!params.id }
  );
}

export function useGetTop10Nfts(params: any, options?: UseQueryOptions<ITop10sResponse>) {
  return useQuery<ITop10sResponse>(
    ['/nft-admin/top-nfts', params],
    async () => {
      const { data } = await axiosInstance.get(`/nft-admin/top-nfts`, { params });
      return {
        list: data,
        pagination: { totalItems: 0, itemCount: 0, itemsPerPage: 0, totalPages: 0, currentPage: 0 },
      };
    },
    options
  );
}

export function useGetSellerPurchase(
  request: { tokenId: string | number },
  options?: UseQueryOptions<ISellerResponse>
) {
  return useQuery<ISellerResponse>(
    ['/nft-admin/detai-purchase-rule-seller-nft', request],
    async () => {
      const { data } = await axiosInstance.post(`/nft-admin/detai-purchase-rule-seller-nft`, request);
      return data;
    },
    options
  );
}

export function useGetBuyerPurchase(request: { tokenId: string | number }, options?: UseQueryOptions<ISellerResponse>) {
  return useQuery<ISellerResponse>(
    ['/nft-admin/detai-purchase-rule-buyer-nft', request],
    async () => {
      const { data } = await axiosInstance.post(`/nft-admin/detai-purchase-rule-buyer-nft`, request);
      return data;
    },
    options
  );
}

export function useGetListBuyerPurchase(params: any, options?: UseQueryOptions<ITop10sResponse>) {
  return useQuery<ITop10sResponse>(
    ['/nft-admin/list-purchase-rule-buyer-nft', params],
    async () => {
      const { data } = await axiosInstance.get(`/nft-admin/list-purchase-rule-buyer-nft`, { params });
      return data;
    },
    {
      ...options,
      enabled: !!params.tokenId,
    }
  );
}

export function useGetListSellerPurchase(params: any, options?: UseQueryOptions<ITop10sResponse>) {
  return useQuery<ITop10sResponse>(
    ['/nft-admin/list-purchase-rule-seller-nft', params],
    async () => {
      const { data } = await axiosInstance.get(`/nft-admin/list-purchase-rule-seller-nft`, { params });
      return data;
    },
    {
      ...options,
      enabled: !!params.tokenId,
    }
  );
}

export function useListPurchaseRuleDefault(
  params: IAllPurchaseRuleDefaultRequest,
  options?: UseQueryOptions<ITop10sResponse>
) {
  return useQuery<ITop10sResponse>(
    ['/nft-admin/list-purchase-rule-default', params],
    async () => {
      const { data } = await axiosInstance.get(`/nft-admin/list-purchase-rule-default`, { params });
      return data;
    },
    options
  );
}

export function useGetHistoryPurchase(params: IHistoryPurchaseRuleRequest, options?: UseQueryOptions<any>) {
  return useQuery<any>(
    ['/nft-admin/list-history-purchase-rule-default', params],
    async () => {
      const { data } = await axiosInstance.get('/nft-admin/list-history-purchase-rule-default', { params });
      return data;
    },
    options
  );
}

export function useGetPurchaseRuleDetail(id: string, options?: UseQueryOptions<any>) {
  return useQuery<any>(
    ['/nft-admin/get-purchase-rule-default', id],
    async () => {
      const { data } = await axiosInstance.get(`/nft-admin/get-purchase-rule-default/${id}`);
      return data;
    },
    options
  );
}
