import { axiosInstance } from 'api/axios';
import { INFTBuyerRequest, INFTSellerRequest, IUpdatePurchaseRuleRequest } from './types';

export const updateNftSeller = async (request: INFTSellerRequest): Promise<any> => {
  const { data } = await axiosInstance.post(`/nft-admin/update-purchase-rule-seller`, request);
  return data;
};

export const updateNftBuyer = async (request: INFTBuyerRequest): Promise<any> => {
  const { data } = await axiosInstance.post(`/nft-admin/update-purchase-rule-buyer`, request);
  return data;
};

export const resetNftSeller = async (request: { tokenId: string | number }): Promise<any> => {
  const { data } = await axiosInstance.post(`/nft-admin/reset-purchase-rule-seller`, request);
  return data;
};

export const resetNftBuyer = async (request: { tokenId: string | number }): Promise<any> => {
  const { data } = await axiosInstance.post(`/nft-admin/reset-purchase-rule-buyer`, request);
  return data;
};

export const burnNft = async (request: { tokenId: number }) => {
  const { data } = await axiosInstance.post(`/nft-admin/burn-nft`, request);
  return data;
};

export const updatePurchaseRuleDefault = async (request: IUpdatePurchaseRuleRequest) => {
  const { data } = await axiosInstance.post(`/nft-admin/create-purchase-rule-default`, request);
  return data;
};
