import { IPaginationRequest } from './../types';
import { IPaginationResponse } from 'api/types';

export interface INFTResponse {
  collection_id: number;
  collection_name: string;
  contract_address: string;
  description: string;
  edition: number;
  human_owner: string;
  image_type: string;
  image_url: string;
  is_onsale: null;
  name: string;
  order_id: null;
  owner: string;
  owner_email: string;
  owner_first_name: string;
  owner_last_name: string;
  owner_name: string;
  price: null;
  status: string;
  token_id: string;
  token_uri: string;
  payment_token: string;
}

export interface INFTResponseV2 {
  imageUrl: string;
  owner: string;
  tokenId: string;
  description: string;
  imageType: string;
  nameNFT: string;
  collectionName: string;
  brandName: string;
  categoryName: string;
  paymentToken: string;
  status: any;
  sellerOffer: any;
  lastTradedPrice: any;
  totalTimesTraded: any;
  isNftBurn: string;
}

export interface INFTsResponse {
  list: INFTResponse[];
  pagination: IPaginationResponse;
}

export interface ITop10NftRequest {
  page?: number;
  limit?: number;
  from?: number;
  to?: number;
  address?: string;
  categoryCommissionId?: number;
  brandId?: number;
  collectionId?: number;
}

export interface ITop10Response {
  token_id: string;
  traded_number: string;
  traded_value: number;
  name: string;
  collection_name: string;
  collection_id: number;
  owner_name: string;
  vendor_name: string;
  brand_name: string;
  brand_id: number;
  category_name: string;
  category_id: number;
  logo_url_category: string;
}

export interface ITop10sResponse {
  list: ITop10Response[];
  pagination: IPaginationResponse;
}

export interface INFTSellerRequest {
  tokenId: number;
  sellerOffer: number;
  sellerOfferLower: number;
  sellerOfferUpper: number;
  adjustExpirationSeller: number;
}

export interface INFTBuyerRequest {
  nftId: number;
  buyerBid: number;
  adjustExpirationBuyer: number;
}

export interface ISellerResponse {
  isDefault: boolean;
  id: number;
  categoryCommissionId: number;
  brandId: number;
  collectionId: number;
  vendorId: any;
  logId: number;
  sellerOfferLower: number;
  sellerOfferUpper: number;
  buyerBidLower: number;
  buyerBidUpper: number;
  status: boolean;
  createdAt: string;
  updatedAt: string;
  createdBy: string;
  updatedBy: string;
  adjustExpirationSeller: string;
  defaultSellerOfferLower: number;
  defaultSellerOfferUpper: number;
  adjustExpirationBuyer: string;
}

export interface IAllPurchaseRuleDefaultRequest extends IPaginationRequest {
  category_name: string;
  collection_name: string;
  brand_name: string;
  vendor_name: string;
  seller_offer_lower: number;
  seller_offer_upper: number;
  buyer_bid_lower: number;
  buyer_bid_upper: number;
  sort_type: number;
}

export interface IHistoryPurchaseRuleRequest extends IAllPurchaseRuleDefaultRequest {
  id: number;
  status: number;
}

export interface IUpdatePurchaseRuleRequest {
  id?: number;
  categoryCommissionId?: number;
  brandId?: number;
  collectionId?: number;
  vendorId?: number;
  sellerOfferLower: number;
  sellerOfferUpper: number;
  buyerBidLower: number;
  buyerBidUpper: number;
  action: string;
}
