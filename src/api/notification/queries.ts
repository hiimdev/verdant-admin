import { axiosInstance } from 'api/axios';
import { IPaginationRequest } from 'api/types';
import { useQuery, UseQueryOptions } from 'react-query';
import { INotificationResponse, INotificationsResponse, INotificationsTypeResponse } from './types';

export function useNotificationsQuery(request: IPaginationRequest, options?: UseQueryOptions<INotificationsResponse>) {
  return useQuery<INotificationsResponse>(
    ['/notification/admin/paging-filter-manual', request],
    async () => {
      const { data } = await axiosInstance.post('/notification/admin/paging-filter-manual', request);
      return data;
    },
    options
  );
}

export function useNotificationQuery(id: string, options?: UseQueryOptions<INotificationResponse>) {
  return useQuery<INotificationResponse>(
    ['/notification/admin/item', id],
    async () => {
      const { data } = await axiosInstance.get(`/notification/admin/item/${id}`);
      return data;
    },
    options
  );
}

export function useGetNotifycationType(
  params: IPaginationRequest,
  options?: UseQueryOptions<INotificationsTypeResponse>
) {
  return useQuery<INotificationsTypeResponse>(
    ['/notification/admin/paging-filter-manual-type', params],
    async () => {
      const { data } = await axiosInstance.post(`/notification/admin/paging-filter-manual-type`, params);
      return data;
    },
    options
  );
}

export function useGetNotifycationTypeAuto(
  params: IPaginationRequest,
  options?: UseQueryOptions<INotificationsTypeResponse>
) {
  return useQuery<INotificationsTypeResponse>(
    ['/notification/admin/paging-filter-automatic-type', params],
    async () => {
      const { data } = await axiosInstance.post(`/notification/admin/paging-filter-automatic-type`, params);
      return data;
    },
    options
  );
}

export function useGetDetailNotiType(id: string, options?: UseQueryOptions<any>) {
  return useQuery<any>(
    ['/notification/admin/item-notification-type', id],
    async () => {
      const { data } = await axiosInstance.get(`/notification/admin/item-notification-type/${id}`);
      return data;
    },
    options
  );
}

export function useGetNotificationAuto(request: IPaginationRequest, options?: UseQueryOptions<INotificationsResponse>) {
  return useQuery<INotificationsResponse>(
    ['/notification/admin/paging-filter-automatic', request],
    async () => {
      const { data } = await axiosInstance.post('/notification/admin/paging-filter-automatic', request);
      return data;
    },
    options
  );
}
