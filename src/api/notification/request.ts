import { axiosInstance } from 'api/axios';
import { INotificationCreateRequest } from './types';

export const createNotification = async (params: INotificationCreateRequest): Promise<any> => {
  const { data } = await axiosInstance.post(`/notification/admin/create-notification`, params);
  return data;
};

export const updateNotification = async (params: INotificationCreateRequest): Promise<any> => {
  const { data } = await axiosInstance.post(`/notification/admin/update-notification`, params);
  return data;
};

export const createNotificationType = async (request: { name: string }): Promise<any> => {
  const { data } = await axiosInstance.post(`/notification/admin/create-notification-type`, request);
  return data;
};
export const updateNotificationType = async (request: { name: string }): Promise<any> => {
  const { data } = await axiosInstance.post(`/notification/admin/update-notification-type`, request);
  return data;
};
