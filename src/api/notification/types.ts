import { IPaginationResponse } from 'api/types';

export interface INotificationCreateRequest {
  subject: string;
  content: string;
  linkView: string;
  sendingTime: number;
  status: number;
  recipient: number;
  notificationTypeId: number;
}

export interface INotificationResponse {
  content: string;
  createdAt: string;
  createdBy: string;
  id: number;
  linkView: string;
  sendStatus: number;
  sendingTime: string;
  status: 0 | 1;
  subject: string;
  updatedAt: string;
  sendType: '' | '0' | '1' | '2';
  sendTo?: string;
  notificationTypeId: any;
}

export interface INotificationsResponse {
  list: INotificationResponse[];
  pagination: IPaginationResponse;
}

export interface INotificationsTypeResponse {
  list: any[];
  pagination: IPaginationResponse;
}
