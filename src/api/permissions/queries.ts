import { axiosInstance } from 'api/axios';
import { IPaginationRequest } from 'api/types';
import { useQuery, UseQueryOptions } from 'react-query';
import { IAdminGroupResponse, IListPermissionForAdminResponse, IPermissionItemRolePermission } from './types';

export function useGetListAllRole(
  params: IPaginationRequest & { status: 1 | 0 },
  options?: UseQueryOptions<IListPermissionForAdminResponse>
) {
  return useQuery<IListPermissionForAdminResponse>(
    ['permissions/paging-filter-role', params],
    async () => {
      const { data } = await axiosInstance.post('permissions/paging-filter-role', params);
      return data;
    },
    options
  );
}

export function usePermission(id: string, options?: UseQueryOptions<IAdminGroupResponse>) {
  return useQuery<IAdminGroupResponse>(
    ['/permissions/item-role/', id],
    async () => {
      const { data } = await axiosInstance.get(`/permissions/item-role/${id}`);
      return data[0];
    },
    options
  );
}

export function usePermissionItemRolePermission(id: string, options?: UseQueryOptions<IPermissionItemRolePermission>) {
  return useQuery<IPermissionItemRolePermission>(
    ['/permissions/item-role-permission/', id],
    async () => {
      const { data } = await axiosInstance.get(`/permissions/item-role-permission/${id}`);
      return {
        list: data,
        pagination: { totalItems: 0, itemCount: 0, itemsPerPage: 0, totalPages: 0, currentPage: 0 },
      };
    },
    options
  );
}

export function useGetPermissionForUser(
  params: IPaginationRequest,
  options?: UseQueryOptions<IListPermissionForAdminResponse>
) {
  return useQuery<IListPermissionForAdminResponse>(
    ['/permissions/list-permission', params],
    async () => {
      const { data } = await axiosInstance.get('/permissions/list-permission', { params });
      return {
        list: data,
        pagination: { totalItems: 0, itemCount: 0, itemsPerPage: 1000, totalPages: 0, currentPage: 0 },
      };
    },
    options
  );
}
