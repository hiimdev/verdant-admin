import { axiosInstance } from 'api/axios';

export const updateRole = async (params: any): Promise<any> => {
  const { data } = await axiosInstance.post(`permissions/update-role`, params);
  return data;
};

export const createRole = async (params: any): Promise<any> => {
  const { data } = await axiosInstance.post(`/permissions/create-role`, params);
  return data;
};

export const deleteRole = async (id: string): Promise<any> => {
  const { data } = await axiosInstance.delete(`/permissions/delete-role/${id}`);
  return data;
};
export const addPermission = async (params: any): Promise<any> => {
  const { data } = await axiosInstance.post(`/permissions/add-permission`, params);
  return data;
};
