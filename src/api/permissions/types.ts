import { IPaginationResponse } from 'api/types';

export interface IAdminResponse {
  avatar_url: string | null;
  email: string;
  fullName: string;
  group: number;
  id: number;
  isActive: number;
  status: string;
  updatedAt: string;
  username: string;
  roleId: number;
}
export interface IAdminGroupResponse {
  createdAt: String;
  createdBy: String;
  description: String;
  id: number;
  name: String;
  status: number;
  updatedAt: String;
  updatedBy: String;
}
export interface IListPermissionbyId {
  checked: String;
  createdAt: String | null;
  createdBy: String | null;
  permissionId: Number;
  permissionName: String;
  updatedAt: String | null;
  updatedBy: String | null;
}
export interface IListPermissionForAdminResponse {
  list: IAdminGroupResponse[];
  pagination: IPaginationResponse;
}

export interface IPermissionItemRolePermission {
  list: IAdminGroupResponse[];
  pagination: IPaginationResponse;
}

export interface IAdminListResponse {
  list: IAdminResponse[];
  pagination: IPaginationResponse;
}
