import { axiosInstance } from 'api/axios';
import { IPaginationRequest } from 'api/types';
import { useQuery, UseQueryOptions } from 'react-query';
import { IPromoResponseV2, IPromosResponse } from './types';

export function useGetPromoCode(request: IPaginationRequest, options?: UseQueryOptions<IPromosResponse>) {
  return useQuery<IPromosResponse>(
    ['/promo/paging_filter', request],
    async () => {
      const { data } = await axiosInstance.post(`/promo/paging_filter`, request);
      console.log(data);
      return data;
    },
    options
  );
}

export function useGetPromoListLog(request: IPaginationRequest, options?: UseQueryOptions<IPromosResponse>) {
  return useQuery<IPromosResponse>(
    ['/promo/paging_filter_detail', request],
    async () => {
      const { data } = await axiosInstance.post(`/promo/paging_filter_detail`, request);
      return data;
    },
    options
  );
}

export function useGetDetailPromo(id: string | number, options?: UseQueryOptions<IPromoResponseV2>) {
  return useQuery<IPromoResponseV2>(
    ['/promo/item', id],
    async () => {
      const { data } = await axiosInstance.get(`/promo/item/${id}`);
      return data;
    },
    options
  );
}
