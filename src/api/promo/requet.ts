import { axiosInstance } from 'api/axios';
import { IPromoDownloadRequest, IPromoRequest } from './types';

export const createPromo = async (request: IPromoRequest): Promise<any> => {
  const { data } = await axiosInstance.post(`/promo/create-promo`, request);
  return data;
};

export const updatePromo = async (request: IPromoRequest): Promise<any> => {
  const { data } = await axiosInstance.post(`/promo/update-promo`, request);
  return data;
};

export const downloadExcel = async (params: IPromoDownloadRequest): Promise<any> => {
  const data = await axiosInstance.get(`/promo/download`, { params, responseType: 'arraybuffer' });
  return data;
};

export const downLoadAllPromo = async (params: any): Promise<any> => {
  const data = await axiosInstance.get(`/promo/download-list`, { params, responseType: 'arraybuffer' });
  return data;
};
