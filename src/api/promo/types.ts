import { IPaginationResponse } from 'api/types';

export interface IUserBrandRequest {
  vendorName?: string;
  id: string;
  brandIds?: any[];
}

export interface ICountriesResponse {
  id: number;
  code: string;
  name: string;
}

export interface IPromoResponse {
  promo_limit_limit: number;
  id: number;
  name: string;
  code: string;
  category_commission_id: number;
  brand_id: number;
  collection_id: number;
  minters: string;
  price: number;
  percent: any;
  max_amount: any;
  promo_limit_id: number;
  start_time: any;
  end_time: any;
  is_use: number;
  category_name: string;
  collection_name: any;
  brand_name: string;
  vendor_name: any;
}

export interface IPromoResponseV2 {
  id: number;
  name: string;
  categoryCommissionId: any;
  brandId: any;
  collectionId: any;
  limit: any;
  minters: any;
  promoType: number;
  price: any;
  percent: number;
  maxAmount: any;
  minAmount: any;
  isActive: number;
  discountType: number;
  startTime: any;
  endTime: any;
  createdAt: string;
  updatedAt: string;
  code: string;
  type: any;
  idPromoDetail: number;
  codeNumber: string;
  codeLength: string;
  status: 0 | 1 | 2 | 3;
  vendor_project: string;
  vendorId: string;
  isVendor: boolean;
}

export interface IPromosResponse {
  list: IPromoResponse[];
  pagination: IPaginationResponse;
}

export interface IPromoRequest {
  codeNumber: number;
  codeLength: number;
  prefix: string;
  suffixes: string;
  promoLimit: number;
  name: string;
  categoryCommissionId: number;
  brandId: number;
  collectionId: number;
  minters: string;
  price: number;
  type: string;
  startTime: number;
  endTime: number;
  maxAmount: number;
  percent: any;
}

export interface IPromoDownloadRequest {
  promoId?: number;
  name?: string;
  code?: string;
  limitUser?: number;
  promoType?: number;
  discountType?: number;
  price?: number;
  percent?: number;
  categoryName?: string;
  brandName?: string;
  collectionName?: string;
  startTime?: number;
  endTime?: number;
  minAmount?: number;
  maxAmount?: number;
  vendor?: string;
}
