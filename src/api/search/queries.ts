import { axiosInstance } from 'api/axios';
import { IPaginationRequest } from 'api/types';
import { useQuery, UseQueryOptions } from 'react-query';

export function useGetBlackList(params: IPaginationRequest, options?: UseQueryOptions<any>) {
  return useQuery<any>(
    ['/user-admin/black-list', params],
    async () => {
      const { data } = await axiosInstance.get('/user-admin/black-list', { params });
      return data;
    },
    options
  );
}

export function useGetWhiteList(params: IPaginationRequest, options?: UseQueryOptions<any>) {
  return useQuery<any>(
    ['/whitelist/minter', params],
    async () => {
      const { data } = await axiosInstance.get(`/whitelist/minter`, { params });
      return data;
    },
    options
  );
}

export function useGetBlackListToAdd(params: IPaginationRequest, options?: UseQueryOptions<any>) {
  return useQuery<any>(
    ['/user-admin/list-user-add-blacklist', params],
    async () => {
      const { data } = await axiosInstance.get(`/user-admin/list-user-add-blacklist`, { params });
      return data;
    },
    options
  );
}
