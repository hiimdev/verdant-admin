import { axiosInstance } from 'api/axios';
import { IActionRequest } from './types';

export const addBlackList = async (params: IActionRequest): Promise<any> => {
  const { data } = await axiosInstance.post(`/user-admin/add-black-list`, params);
  return data;
};

export const addWhiteList = async (params: IActionRequest): Promise<any> => {
  const { data } = await axiosInstance.post(`/user-admin/add-white-list`, params);
  return data;
};

export const deleteWhiteBlackList = async (params: IActionRequest): Promise<any> => {
  const { data } = await axiosInstance.post(`/user-admin/remove-white-black-list`, params);
  return data;
};
