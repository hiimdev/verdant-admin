import { IPaginationResponse } from 'api/types';

export interface IBlackWhiteResponse {
  brandName: string;
  createdAt: string;
  description: string;
  descriptionType: string;
  descriptionUrl: string;
  logotype: string;
  logoUrl: string;
  status: string | null;
  updatedAt: string;
}

export interface IBlackWhitesResponse {
  list: IBlackWhiteResponse[];
  pagination: IPaginationResponse;
}

export interface IActionRequest {
  wallet?: string;
  email?: string;
}
