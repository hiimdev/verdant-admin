import { convertPriceEther } from './../../utils/common';
import { axiosInstance } from 'api/axios';
import { IPaginationRequest } from 'api/types';
import { useQuery, UseQueryOptions } from 'react-query';
import {
  IBusinessTransactionRequest,
  IBusinessTransactionResponse,
  IListTradedTransactionResponse,
  IStatiicalTransactionResponse,
  ITransactionsResponse,
} from './types';
import moment from 'moment';

export function useGetTransaction(request: IPaginationRequest, options?: UseQueryOptions<ITransactionsResponse>) {
  return useQuery<ITransactionsResponse>(
    ['/transactions/paging_filter', request],
    async () => {
      const { data } = await axiosInstance.post('/transactions/paging_filter', request);
      return data;
    },
    options
  );
}

export function useGetBusinessTransaction(
  params: IBusinessTransactionRequest,
  options?: UseQueryOptions<IBusinessTransactionResponse[]>
) {
  return useQuery<IBusinessTransactionResponse[]>(
    ['/transactions/transactions-business-admin', params],
    async () => {
      const { data } = await axiosInstance.get('/transactions/transactions-business-admin', { params });
      const temp = data.map((item: IBusinessTransactionResponse) => {
        return {
          ...item,
          traded_value: convertPriceEther(item?.traded_value || 0),
          traded_number: Number(item?.traded_number),
          day: moment(new Date(item.day)).format('DD-MMM-YYYY'),
        };
      });
      return temp;
    },
    options
  );
}

export function useGetStaticalTransaction(
  params: IBusinessTransactionRequest,
  options?: UseQueryOptions<IStatiicalTransactionResponse>
) {
  return useQuery<IStatiicalTransactionResponse>(
    ['/transactions/transactions-statistical-admin', params],
    async () => {
      const { data } = await axiosInstance.get('/transactions/transactions-statistical-admin', { params });
      return data;
    },
    options
  );
}

export function useGetVendorStatical(params: any, options?: UseQueryOptions<any>) {
  return useQuery<any>(
    ['/transactions/vendor-management-list', params],
    async () => {
      const { data } = await axiosInstance.get('/transactions/vendor-management-list', { params });
      return data;
    },
    options
  );
}

export function useGetNFTMitedVendor(
  params: { adddress: string; from?: string | number; to?: string | number },
  options?: UseQueryOptions<any>
) {
  return useQuery<any>(
    ['/transactions/vendor-nfts-management-list', params],
    async () => {
      const { data } = await axiosInstance.get('/transactions/vendor-nfts-management-list', { params });
      return data;
    },
    options
  );
}

export function useGetTransactionTransfer(
  params: IPaginationRequest,
  options?: UseQueryOptions<ITransactionsResponse>
) {
  return useQuery<ITransactionsResponse>(
    ['/transactions/list-nfts-transfer', params],
    async () => {
      const { data } = await axiosInstance.get('/transactions/list-nfts-transfer', { params });
      return data;
    },
    options
  );
}

export function useGetMasterWallet(options?: UseQueryOptions<any>) {
  return useQuery<any>(
    ['/user-admin/master-wallet'],
    async () => {
      const { data } = await axiosInstance.get('/user-admin/master-wallet');
      return data;
    },
    options
  );
}

export function useGetVerifierWallet(options?: UseQueryOptions<any>) {
  return useQuery<any>(
    ['/user-admin/verifier-wallet'],
    async () => {
      const { data } = await axiosInstance.get('/user-admin/verifier-wallet');
      return data;
    },
    options
  );
}

export function useGetGasTankWallet(options?: UseQueryOptions<any>) {
  return useQuery<string>(
    ['/user-admin/gas-tank-wallet'],
    async () => {
      const { data } = await axiosInstance.get('/user-admin/gas-tank-wallet');
      return data;
    },
    options
  );
}

export function useGetOperatorWallet(options?: UseQueryOptions<any>) {
  return useQuery<any>(
    ['/user-admin/operator-wallet'],
    async () => {
      const { data } = await axiosInstance.get('/user-admin/operator-wallet');
      return data;
    },
    options
  );
}

export function useGetTradedTransaction(
  params: IPaginationRequest,
  options?: UseQueryOptions<IListTradedTransactionResponse>
) {
  return useQuery<IListTradedTransactionResponse>(
    ['/transactions/transaction-trade', params],
    async () => {
      const { data } = await axiosInstance.get('/transactions/transaction-trade', { params });
      return data;
    },
    options
  );
}
