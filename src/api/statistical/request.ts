import { axiosInstance } from 'api/axios';
import { ITransferTransactionRequest } from './types';

export const addAdminTransfer = async (params: ITransferTransactionRequest): Promise<any> => {
  const { data } = await axiosInstance.post(`/transactions/create`, params);
  return data;
};
// export function useGetSignatureExpTime(options?: UseQueryOptions<any>) {
//   return useQuery<any>(
//     ['/admin/signature'],
//     async () => {
//       const { data } = await axiosInstance.post('/admin/signature');
//       return data;
//     },
//     options
//   );
// }
export const useGetSignatureExpTime = async (params: any): Promise<any> => {
  const { data } = await axiosInstance.post(`/admin/signature`, params);
  return data;
};

export const downloadTransaction = async (params: any): Promise<any> => {
  const data = await axiosInstance.get(`/transactions/download`, { params, responseType: 'arraybuffer' });
  return data;
};

export const downloadTransactionTraded = async (params: any): Promise<any> => {
  const data = await axiosInstance.get(`/transactions/download-transction-trade`, {
    params,
    responseType: 'arraybuffer',
  });
  return data;
};
