import { IPaginationResponse } from 'api/types';

export interface IBusinessTransactionRequest {
  from: number | string;
  to: number | string;
}

export interface ITransactionResponse {
  token_id: string;
  contract_address: string;
  from: string;
  to: string;
  nft_action: string;
  txid: string;
  date: string;
  log_action: string | null;
  price: string | null;
  payment_token: string | null;
  owner_from_name: string | null;
  owner_from_email: string | null;
  owner_to_name: string;
  owner_to_email: string;
}

export interface ITransactionsResponse {
  list: ITransactionResponse[];
  pagination: IPaginationResponse;
}

export interface IStatiicalTransactionResponse {
  totalTrading: string | number;
  totalNFTsMinted: string | number;
  totalNFTsSold: string | number;
  maxPrice: string | number;
}

export interface IBusinessTransactionResponse {
  day: number;
  traded_value: number;
  traded_number: string | number;
}

export interface ITransferTransactionRequest {
  txid: string;
}

export interface ITradedTransactionResponse {
  txid: string;
  name: string;
  collectionName: string;
  brandName: string;
  categoryName: string;
  tokenId: string;
  vendorName: string;
  createdAt: string;
  price: string;
  buyerPain: string;
  buyerEmail: string;
  sellerReceive: string;
  goodsTaxRate: string;
  goodsTaxAddress: string;
  serviceFeeRate: string;
  serviceFeeTaxRate: string;
  serviceFeeVerdant: string;
  serviceFeeTaxVerdant: string;
  serviceFeeAddress: string;
  serviceFeeTaxAddress: string;
  serviceFeeTaxAddressAmount: string;
}
export interface IListTradedTransactionResponse {
  list: ITradedTransactionResponse[];
  pagination: IPaginationResponse;
}
