import { axiosInstance } from 'api/axios';
import { IPaginationRequest } from 'api/types';
import { useQuery, UseQueryOptions } from 'react-query';
import { ITagResponse, ITagsResponse } from './types';

export function useGetListTags(request: IPaginationRequest, options?: UseQueryOptions<ITagsResponse>) {
  return useQuery<ITagsResponse>(
    ['/nft-admin/tag/paging_filter', request],
    async () => {
      const { data } = await axiosInstance.post(`/nft-admin/tag/paging_filter`, request);
      return data;
    },
    options
  );
}

export function useGetTagDetails(id: string, options?: UseQueryOptions<ITagResponse>) {
  return useQuery<ITagResponse>(
    ['/nft-admin/detail-tag', id],
    async () => {
      const { data } = await axiosInstance.get(`/nft-admin/detail-tag/${id}`);
      return data;
    },
    options
  );
}
