import { axiosInstance } from 'api/axios';
import { ICreateTagRequest } from './types';

export const createTag = async (params: ICreateTagRequest): Promise<any> => {
  const { data } = await axiosInstance.post(`/nft-admin/create-tag`, params);
  return data;
};

export const updateTag = async (params: ICreateTagRequest): Promise<any> => {
  const { data } = await axiosInstance.post(`/nft-admin/update-tag`, params);
  return data;
};

export const deleteTag = async (id: string): Promise<any> => {
  const { data } = await axiosInstance.delete(`/nft-admin/delete-tag/${id}`);
  return data;
};
