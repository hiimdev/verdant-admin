import { IPaginationResponse } from 'api/types';

export interface IUserResponse {
  avatar_url: string;
  email: string;
  first_name: string;
  id: number;
  last_name: string;
  phone: string;
  username: string;
  wallet: string;
  avatarUrl: string;
  brandId: string;
  brand_name: string;
  brand_id: string;
  countNFT: string;
  status: string;
  brandName: string;
  listBrand: IBrandInfo[];
  nftBrands?: string;
  vendor_name?: string;
  vendorName?: string;
  referralName?: string;
  referral_name?: string;
  vendorId?: string | number;
}

export interface IBrandInfo {
  id: number;
  brandName: string;
  totalNft: string;
}

export interface IUsersResponse {
  list: IUserResponse[];
  pagination: IPaginationResponse;
}

export interface IUserBrandRequest {
  vendorId?: number | string;
  id: string;
  brandIds?: any[];
  referralName?: string;
}

export interface ICountryResponse {
  id: number;
  code: string;
  name: string;
}

export interface ICountriesResponse {
  list: ICountryResponse[];
  pagination: IPaginationResponse;
}

export interface ITagResponse {
  id: number;
  tagName: string;
  categoryCommissionId: number;
  description: string;
  createdAt: string;
  updatedAt: string;
  createdBy: string;
  updatedBy: string;
  // listCategory: { id: number; brandName: string }[];
  brandId: number;
  categoryId: number;
  collectionId: number;
}

export interface ITagsResponse {
  list: ITagResponse[];
  pagination: IPaginationResponse;
}

export interface ICreateTagRequest {
  tagName: string;
  categoryCommissionIds?: number[];
  description: string;
}
