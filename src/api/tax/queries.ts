import { axiosInstance } from 'api/axios';
import { IPaginationRequest } from 'api/types';
import { useQuery, UseQueryOptions } from 'react-query';
import { ITaxsResponse } from './types';

export function useGetListTax(params: IPaginationRequest, options?: UseQueryOptions<ITaxsResponse>) {
  return useQuery<ITaxsResponse>(
    ['/tax/list', params],
    async () => {
      const { data } = await axiosInstance.get('/tax/list', { params });
      return data;
    },
    options
  );
}

export function useGetTaxDetail(id: string, options?: UseQueryOptions<any>) {
  return useQuery<any>(
    ['/tax/item', id],
    async () => {
      const { data } = await axiosInstance.get(`/tax/item/${id}`);
      return data;
    },
    options
  );
}

export function useGetListTaxDetail(params: IPaginationRequest, options?: UseQueryOptions<any>) {
  return useQuery<any>(
    ['/tax/list-tax', params],
    async () => {
      const { data } = await axiosInstance.get('/tax/list-tax', { params });
      return data;
    },
    options
  );
}

export function useGetListTaxHistory(params: IPaginationRequest, options?: UseQueryOptions<any>) {
  return useQuery<any>(
    ['/tax/list-history', params],
    async () => {
      const { data } = await axiosInstance.get('/tax/list-history', { params });
      return data;
    },
    options
  );
}
