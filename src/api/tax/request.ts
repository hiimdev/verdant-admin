import { axiosInstance } from 'api/axios';

export const updateTax = async (params: any): Promise<any> => {
  const { data } = await axiosInstance.post(`/tax/update`, params);
  return data;
};

export const createTax = async (params: any): Promise<any> => {
  const { data } = await axiosInstance.post(`/tax/create`, params);
  return data;
};

export const deleteTax = async (id: string): Promise<any> => {
  const { data } = await axiosInstance.delete(`/tax/delete/${id}`);
  return data;
};

export const createTaxValue = async (params: any): Promise<any> => {
  const { data } = await axiosInstance.post(`/tax/create-tax`, params);
  return data;
};
