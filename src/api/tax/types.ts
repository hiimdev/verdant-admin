import { IPaginationResponse } from 'api/types';

export interface ITaxResponse {
  answer: string | null;
  categoryId: string | null;
  createdAt: string;
  id: number;
  question: string | null;
  updated_at: string;
  isGts: null | number;
}

export interface ITaxsResponse {
  list: ITaxResponse[];
  pagination: IPaginationResponse;
}

export interface ICreateTaxRequest {
  action: 'edit' | 'add';
  taxId: number;
  taxFee: number;
  taxFeeBuyer: number;
  categoryCommissionId: string | number;
  collectionId: string | number;
  brandId: string | number;
  vendorId: string;
  countryId: string | number;
}
