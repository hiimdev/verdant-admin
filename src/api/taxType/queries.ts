import { axiosInstance } from 'api/axios';
import { IPaginationRequest } from 'api/types';
import { useQuery, UseQueryOptions } from 'react-query';
import { ITaxsTypeResponse } from './types';

export function useGetListTaxType(params: IPaginationRequest, options?: UseQueryOptions<ITaxsTypeResponse>) {
  return useQuery<ITaxsTypeResponse>(
    ['/tax/list-tax-type', params],
    async () => {
      const { data } = await axiosInstance.get('/tax/list-tax-type', { params });
      return data;
    },
    options
  );
}

export function useGetTaxTypeDetail(id: string, options?: UseQueryOptions<any>) {
  return useQuery<any>(
    ['/tax/item-tax-type', id],
    async () => {
      const { data } = await axiosInstance.get(`/tax/item-tax-type/${id}`);
      return data;
    },
    options
  );
}
