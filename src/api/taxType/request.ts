import { axiosInstance } from 'api/axios';

export const updateTaxType = async (params: any): Promise<any> => {
  const { data } = await axiosInstance.put(`/tax/update-tax-type`, params);
  return data;
};

export const createTaxType = async (params: any): Promise<any> => {
  const { data } = await axiosInstance.post(`/tax/create-tax-type`, params);
  return data;
};

export const deleteTaxType = async (id: string): Promise<any> => {
  const { data } = await axiosInstance.delete(`/tax/delete-tax-type/${id}`);
  return data;
};
