import { IPaginationResponse } from 'api/types';

export interface ITaxTypeResponse {
  answer: string | null;
  categoryId: string | null;
  createdAt: string;
  id: number;
  question: string | null;
  updated_at: string;
  taxTypeName?: string;
}

export interface ITaxsTypeResponse {
  list: ITaxTypeResponse[];
  pagination: IPaginationResponse;
}

export interface ICreateTaxTypeRequest {
  taxTypeName: string;
  description: string;
  isGts: boolean;
}
