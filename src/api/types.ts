export interface ISuccessResponse {
  meta: {
    code: number;
    message: string;
    pagination: IPaginationResponse;
  };
  data: any;
  summary?: {
    total_number_dmt: number;
    total_number_minted: number;
    total_number_sold: number;
    total_number_price: number;
  };
}

export interface IError {
  meta: {
    code: number;
    message: string[] | string;
  };
}
export interface ICountResponse {
  totalNfts: number;
}

export interface IMeta {
  code: number;
  message: string[] | string;
}

export interface IPaginationRequest {
  page: number;
  limit: number;
  total?: number;
  name?: string;
  category_id?: string;
  brand_id?: string;
  user?: any;
  vendorId?: number | string;
  taxId?: number | string | '';
  countryId?: number | string | '';
  categoryId?: string;
  brandId?: string;
  userId?: string | '';
}

export interface IInfinitePaginationRequest {
  limit: number;
  total?: number;
  name?: string;
  category_id?: string;
  brand_id?: string;
  user?: any;
  vendorId?: number | string;
  taxId?: number | string | '';
  countryId?: number | string | '';
  categoryId?: string;
  brandId?: string;
  userId?: string | '';
}

export interface IPaginationResponse {
  totalItems: number;
  itemCount: number;
  itemsPerPage: number;
  totalPages: number;
  currentPage: number;
}
export interface IAboutCategory {
  page: number;
  limit: number;
  section: string;
  createdBy: string;
  updatedBy: string;
}
