import { axiosInstance } from 'api/axios';
import { IInfinitePaginationRequest, IPaginationRequest } from 'api/types';
import { useInfiniteQuery, UseInfiniteQueryOptions, useQuery, UseQueryOptions } from 'react-query';
import {
  ICountriesResponse,
  IExternalWalletResponse,
  IUserResponse,
  IUsersResponse,
  IVendorResponse,
  IVendorsResponse,
  IVendorReceivedResponse,
} from './types';

export function useUsersQuery(request: IPaginationRequest, options?: UseQueryOptions<IUsersResponse>) {
  return useQuery<IUsersResponse>(
    ['/user-admin/paging_filter', request],
    async () => {
      const { data } = await axiosInstance.post('/user-admin/paging_filter', request);
      return data;
    },
    options
  );
}

export function useUserQuery(id: string, options?: UseQueryOptions<IUserResponse>) {
  return useQuery<IUserResponse>(
    ['/user-admin/detail-user', id],
    async () => {
      const { data } = await axiosInstance.get(`/user-admin/detail-user/${id}`);
      data.nftBrands = data.listBrand.map((item: any) => `${item.brandName}(${item.totalNFT}), `);
      const temp = Array.from(new Set(data.listBrand.map((item: any) => JSON.stringify(item))).values()).map(
        (item: any) => JSON.parse(item)
      );
      data.listBrand = temp;
      return data;
    },
    options
  );
}

export function useGetVendorCommission(params: IPaginationRequest, options?: UseQueryOptions<IUsersResponse>) {
  return useQuery<IUsersResponse>(
    ['/user-admin/address-book', params],
    async () => {
      const { data } = await axiosInstance.get(`/user-admin/address-book`, { params });
      return data;
    },
    options
  );
}

export function useGetListVendor(params: IPaginationRequest, options?: UseQueryOptions<IUsersResponse>) {
  return useQuery<IUsersResponse>(
    ['/user-admin/list-vender', params],
    async () => {
      const { data } = await axiosInstance.get(`/user-admin/list-vender`, { params });
      return data;
    },
    options
  );
}

export function useGetAllCountries(params?: IPaginationRequest, options?: UseQueryOptions<ICountriesResponse>) {
  return useQuery<ICountriesResponse>(
    ['/user/countries', params],
    async () => {
      const { data } = await axiosInstance.get(`/user/countries`, { params });
      return data;
    },
    options
  );
}

export function useGetAllCountriesInfinteQueries(
  params?: IInfinitePaginationRequest,
  options?: UseInfiniteQueryOptions<ICountriesResponse>
) {
  return useInfiniteQuery<ICountriesResponse>(
    ['/user/countries', params],
    async ({ pageParam = 1, queryKey }) => {
      const { data } = await axiosInstance.get(`/user/countries`, {
        params: {
          ...(queryKey[1] as IInfinitePaginationRequest),
          page: pageParam,
        },
      });
      return data;
    },
    {
      getNextPageParam: (data) => {
        return data?.pagination.currentPage === data?.pagination?.totalPages
          ? undefined
          : data?.pagination.currentPage + 1;
      },
      ...options,
    }
  );
}

export function useGetListReferral(params: IPaginationRequest, options?: UseQueryOptions<IUsersResponse>) {
  return useQuery<IUsersResponse>(
    ['/user-admin/list-referral', params],
    async () => {
      const { data } = await axiosInstance.get(`/user-admin/list-referral`, { params });
      return data;
    },
    options
  );
}

export function useGetVendorFilter(request: IPaginationRequest, options?: UseQueryOptions<IVendorsResponse>) {
  return useQuery<IVendorsResponse>(
    ['/vendor/paging_filter', request],
    async () => {
      const { data } = await axiosInstance.post(`/vendor/paging_filter`, request);
      return data;
    },
    options
  );
}

export function useGetDetailVendor(id: string, options?: UseQueryOptions<IVendorResponse>) {
  return useQuery<IVendorResponse>(
    ['/vendor/item', id],
    async () => {
      const { data } = await axiosInstance.get(`/vendor/item/${id}`);
      return data;
    },
    options
  );
}

export function useGetDetailVendorReceived(id: string, options?: UseQueryOptions<IVendorReceivedResponse>) {
  return useQuery<IVendorReceivedResponse>(
    ['/vendor/vendor-received', id],
    async () => {
      const { data } = await axiosInstance.get(`/vendor/vendor-received/${id}`);
      return data;
    },
    options
  );
}

export function useGetListVendorReceived(params: IPaginationRequest, options?: UseQueryOptions<IVendorsResponse>) {
  return useQuery<IVendorsResponse>(
    ['/vendor/list-vendor-received', params],
    async () => {
      const { data } = await axiosInstance.get(`/vendor/list-vendor-received`, { params });
      return data;
    },
    { ...options, enabled: !!params.vendorId }
  );
}

export function useGetListVendorFilter(params: IPaginationRequest, options?: UseQueryOptions<IVendorsResponse>) {
  return useQuery<IVendorsResponse>(
    ['/vendor/list-vendor-admin', params],
    async () => {
      const { data } = await axiosInstance.get(`/vendor/list-vendor-admin`, { params });
      return data;
    },
    options
  );
}

export function useGetListVendorInfinteQueries(
  params?: IInfinitePaginationRequest,
  options?: UseInfiniteQueryOptions<IVendorsResponse>
) {
  return useInfiniteQuery<IVendorsResponse>(
    ['/vendor/list-vendor-admin', params],
    async ({ pageParam = 1, queryKey }) => {
      const { data } = await axiosInstance.get(`/vendor/list-vendor-admin`, {
        params: {
          ...(queryKey[1] as IInfinitePaginationRequest),
          page: pageParam,
        },
      });
      return data;
    },
    {
      getNextPageParam: (data) => {
        return data?.pagination.currentPage === data?.pagination?.totalPages
          ? undefined
          : data?.pagination.currentPage + 1;
      },
      ...options,
    }
  );
}

export function useGetListUserFromVendor(params: IPaginationRequest, options?: UseQueryOptions<IVendorsResponse>) {
  return useQuery<IVendorsResponse>(
    ['/vendor/list-user-admin', params],
    async () => {
      const { data } = await axiosInstance.get(`/vendor/list-user-admin`, { params });
      return data;
    },
    options
  );
}

export function useGetCountryByTax(params?: IPaginationRequest, options?: UseQueryOptions<ICountriesResponse>) {
  return useQuery<ICountriesResponse>(
    ['/tax/list-countries', params],
    async () => {
      const { data } = await axiosInstance.get(`/tax/list-countries`, { params });
      return data;
    },
    options
  );
}

export function useGetExternalWallet(params: IPaginationRequest, options?: UseQueryOptions<IExternalWalletResponse>) {
  return useQuery<IExternalWalletResponse>(
    ['/user-admin/get-external-wallet', params],
    async () => {
      const id = params.userId;
      const { data } = await axiosInstance.get(`/user-admin/get-external-wallet/${id}`, { params });
      return {
        list: data,
        pagination: { totalItems: 0, itemCount: 0, itemsPerPage: 0, totalPages: 0, currentPage: 0 },
      };
    },
    { ...options, enabled: !!params.userId }
  );
}
