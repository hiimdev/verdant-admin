import { axiosInstance } from 'api/axios';
import {
  IAddUserVendor,
  ICreateUserVendor,
  IUpdateCountryRequest,
  IUpdateUserResponse,
  IUserBrandRequest,
  IUserKYCRequest,
  IVendorRequest,
  IWalletSystemRequest,
  IVendorReceivedRequest,
} from './types';

export const updateBrandUser = async (params: IUserBrandRequest): Promise<any> => {
  const { data } = await axiosInstance.post(`/user-admin/update-user`, params);
  return data;
};

export const createVendor = async (params: IVendorRequest): Promise<any> => {
  const { data } = await axiosInstance.post(`/vendor/create-vendor`, params);
  return data;
};

export const updateVendor = async (params: IVendorRequest): Promise<any> => {
  const { data } = await axiosInstance.post(`/vendor/update-vendor`, params);
  return data;
};

export const createVendorReceived = async (params: IVendorReceivedRequest): Promise<any> => {
  const { data } = await axiosInstance.post(`/vendor/vendor-received`, params);
  return data;
};

export const updateVendorReceived = async (params: IVendorReceivedRequest & { id: number }): Promise<any> => {
  const { data } = await axiosInstance.put(`/vendor/vendor-received/${params.id}`, params);
  return data;
};

export const deleteVendorReceived = async (id: string): Promise<any> => {
  const { data } = await axiosInstance.delete(`/vendor/vendor-received/${id}`);
  return data;
};

export const updateKYC = async (params: IUserKYCRequest): Promise<IUpdateUserResponse> => {
  const { data } = await axiosInstance.post(`/user-admin/update-kyc`, params);
  return data;
};

export const createUserVendor = async (params: ICreateUserVendor): Promise<any> => {
  const { data } = await axiosInstance.post(`/user-admin/create-user-vendor`, params);
  return data;
};
export const useAddUserVendor = async (params: IAddUserVendor): Promise<any> => {
  const data = await axiosInstance.post(`/user-admin/add-user-vendor`, params);
  return data;
};
export const useGetDetailUserVendor = async (email: string): Promise<any> => {
  const data = await axiosInstance.get(`/user-admin/check-exist-user/${email}`);
  return data;
};
export const postMethod = async (request: IWalletSystemRequest): Promise<any> => {
  const { data } = await axiosInstance.post(`/wallet/tx/send`, request);
  return data;
};

export const updateUserCountry = async (request: IUpdateCountryRequest): Promise<any> => {
  const { data } = await axiosInstance.post(`/user-admin/update-user-countries`, request);
  return data;
};
