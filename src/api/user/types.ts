import { IPaginationResponse } from 'api/types';

export interface IUserResponse {
  avatar_url: string;
  email: string;
  first_name: string;
  id: number;
  last_name: string;
  phone: string;
  username: string;
  wallet: string;
  avatarUrl: string;
  brandId: string;
  brand_name: string;
  brand_id: string;
  countNFT: string;
  status: string;
  brandName: string;
  listBrand: IBrandInfo[];
  nftBrands?: string;
  vendor_name?: string;
  vendorName?: string;
  referralName?: string;
  referral_name?: string;
  vendorId?: string;
  isVendor: number;
  totalTokens: string | number;
  isActiveKyc: string | number;
}

export interface IBrandInfo {
  id: number;
  brandName: string;
  totalNft: string;
}

export interface IUsersResponse {
  list: IUserResponse[];
  pagination: IPaginationResponse;
}

export interface IUserBrandRequest {
  vendorId?: number | string;
  id: string;
  brandIds?: any[];
  referralName?: string;
}

export interface ICountryResponse {
  id: number;
  code: string;
  name: string;
}

export interface ICountriesResponse {
  list: ICountryResponse[];
  pagination: IPaginationResponse;
}

export interface IVendorResponse {
  id: number;
  vendorName: string;
  description: string;
  createdAt: string;
  updatedAt: string;
  createdBy: string;
  updatedBy: string;
  totalUser: string;
  listBrand: { id: number; brandName: string }[];
  listCategory: { id: number; brandName: string; categoryCommissionId: number }[];
}

export interface IVendorReceivedResponse {
  id?: number;
  vendorId?: number;
  addressReceived: string;
  percentageReceived: number;
  isGts: number;
  status?: number;
  createdAt?: string;
  updatedAt?: string;
  createdBy?: string;
  updatedBy?: string;
}

export interface IVendorsResponse {
  list: IVendorResponse[];
  pagination: IPaginationResponse;
}

export interface IVendorRequest {
  vendorName: string;
  description: string;
  brandIds?: any[];
}

export interface IVendorReceivedRequest {
  vendorId: number | null;
  addressReceived: string;
  percentageReceived: number;
  isGts: boolean;
}

export interface IUserKYCRequest {
  id: string;
}
export interface ICreateUserVendor {
  email: string;
}
export interface IAddUserVendor {
  email: string;
  vendorId: Number;
}
export interface IWalletResponse {
  createdAt: string;
  id: number;
  status: string;
  updatedAt: string;
  wallet: string;
}
export interface IExternalWalletResponse {
  list: IWalletResponse[];
  pagination: IPaginationResponse;
}
export interface IWalletSystemRequest {
  currency?: string;
  method?: string;
  data?: any;
  emailCode?: string;
  twofa?: string;
  price?: any;
}

export interface IUpdateCountryRequest {
  userId: number;
  countryId: number;
}

export interface IUpdateUserResponse {
  id: number;
  username: string;
  email: string;
  password: string;
  avatarUrl: any;
  firstName: any;
  lastName: any;
  dateOfBirth: any;
  phone: any;
  createdAt: string;
  updatedAt: number;
  isActive2fa: number;
  isActiveEmailCode: number;
  twoFactorAuthenticationSecret: any;
  emailCode: any;
  isActiveKyc: boolean;
  wallet: string;
  status: string;
  type: string;
  token: string;
  emailCodeActive: string;
  group: string;
  data: any;
  countryId: number;
  isVendor: any;
  vendorName: any;
  referralName: any;
  vendorId: any;
  timeActive: any;
  addressLocation: any;
  nickname: string;
  createdBy: any;
  updatedBy: string;
  isDefaultCountry: boolean;
}
