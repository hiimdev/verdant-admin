import { FC, useEffect, useRef } from 'react';
import '@google/model-viewer';

export const Model3D: FC<{ src: string; className?: string }> = ({ src }) => {
  const modelViewRef = useRef<any>(null);
  useEffect(() => {
    if (modelViewRef && modelViewRef.current) {
      modelViewRef.current.addEventListener('progress', () => {});
      return () => {
        modelViewRef?.current?.removeEventListener('progress', () => {});
      };
    }
  }, [modelViewRef]);
  return (
    <model-viewer
      src={src || ''}
      ref={modelViewRef}
      autoplay
      auto-rotate
      camera-controls
      ar
      ar-modes="scene-viewer quick-look"
      interaction-prompt="none"
      data-js-focus-visible
      loading="eager"
      ar-status="not-presenting"
      // exposure="0.008"
    ></model-viewer>
  );
};
