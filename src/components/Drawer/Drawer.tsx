import { Button, Drawer as DrawerAntd, FormInstance, Spin } from 'antd';
import { FC, useEffect, useMemo } from 'react';
type IDrawerProps = {
  editingId?: string | null;
  handleClose: () => void;
  name: string | any;
  form?: FormInstance<any>;
  getLoading?: boolean;
  submitLoading?: boolean;
  viewOnly?: boolean;
  disabledSubmit?: boolean;
};

export const Drawer: FC<IDrawerProps> = ({
  editingId,
  handleClose,
  name,
  form,
  getLoading = false,
  submitLoading,
  viewOnly = false,
  disabledSubmit,
  children,
}) => {
  const action = editingId ? 'edit' : 'add';
  const visible = editingId !== null;

  useEffect(() => {
    form?.resetFields();
  }, [visible]);

  const title = useMemo(() => {
    return `${!viewOnly ? (action === 'edit' ? 'Update' : 'Add') : ''} ${name}`;
  }, [action, name, viewOnly]);

  return (
    <DrawerAntd
      title={title}
      visible={visible}
      height="100%"
      placement="bottom"
      onClose={handleClose}
      // drawerStyle={{ overflow: 'auto hidden' }}
      footer={
        <div
          style={{
            textAlign: 'right',
          }}
        >
          <Button disabled={submitLoading} onClick={handleClose} style={{ marginRight: 8 }}>
            {viewOnly ? 'Close' : 'Cancel'}
          </Button>
          {!viewOnly && (
            <Button
              disabled={submitLoading || getLoading || disabledSubmit}
              loading={submitLoading}
              onClick={form?.submit}
              type="primary"
            >
              Save
            </Button>
          )}
        </div>
      }
    >
      <Spin tip="Loading..." spinning={getLoading}>
        {!getLoading && children}
      </Spin>
    </DrawerAntd>
  );
};
