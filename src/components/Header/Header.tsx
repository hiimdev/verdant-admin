import { Button, Space } from 'antd';
import { FC } from 'react';
import styles from './styles.module.less';
import { AiOutlinePlus } from 'react-icons/ai';
import { ActionCreatorWithPayload } from '@reduxjs/toolkit';
import { useAppDispatch } from 'hooks/reduxHook';
import clsx from 'clsx';
import { DownloadOutlined } from '@ant-design/icons';
type IHeaderProps = {
  title: string;
  setEditingId?: ActionCreatorWithPayload<string | null, string>;
  viewOnly?: boolean;
  setParams?: any;
  titleButton?: string;
  exportParams?: string;
  handleExport?: any;
};

export const Header: FC<IHeaderProps> = ({
  title,
  titleButton,
  setEditingId,
  viewOnly = false,
  exportParams = false,
  handleExport,
  children,
}) => {
  const dispatch = useAppDispatch();
  const handleCreate = () => {
    if (setEditingId) {
      dispatch(setEditingId(''));
    }
  };

  return (
    <>
      <div className={styles.header}>
        <h2>{title}</h2>

        <div className={clsx(styles.addSearchContent, children ? styles.justifyBetween : '')}>
          {!viewOnly && (
            <Space style={{ display: 'flex', justifyContent: 'flex-end', marginBottom: 8 }}>
              <Button
                className={styles.createButton}
                size="large"
                type="primary"
                icon={<AiOutlinePlus />}
                onClick={handleCreate}
              >{`Add ${titleButton || title}`}</Button>
              {!!exportParams && (
                <a
                  href={`https://api-market.varvn.tech/promo/download-list?${exportParams}`}
                  target="_blank"
                  rel="noopener noreferrer"
                  download
                >
                  <Button
                    className={styles.exportButton}
                    size="large"
                    icon={<DownloadOutlined />}
                    onClick={handleExport}
                  >
                    Export
                  </Button>
                </a>
              )}
            </Space>
          )}
          <Space style={{ display: 'flex', justifyContent: 'flex-end' }}>{children}</Space>
        </div>
      </div>
    </>
  );
};
