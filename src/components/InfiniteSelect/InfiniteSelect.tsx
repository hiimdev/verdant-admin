import { Select as AntdSelect, SelectProps as AntdSelectProps, Spin } from 'antd';
import clsx from 'clsx';
import React, { FC } from 'react';
import styles from './styles.module.css';

type SelectProps = Pick<
  AntdSelectProps<any>,
  | 'className'
  | 'defaultValue'
  | 'loading'
  | 'onChange'
  | 'value'
  | 'showSearch'
  | 'placeholder'
  | 'filterOption'
  | 'disabled'
  | 'mode'
  | 'style'
  | 'onSelect'
  | 'onClick'
  | 'notFoundContent'
  | 'options'
> & {
  loadMore: () => void | any;
  hasMore: boolean;
  setLoading?: React.Dispatch<React.SetStateAction<boolean>>;
};

export const InfiniteSelect: FC<SelectProps> & { Option: typeof AntdSelect.Option } = ({
  children,
  className,
  loading,
  hasMore,
  loadMore,
  ...props
}) => {
  const onScroll = (event: React.UIEvent<HTMLDivElement, UIEvent>) => {
    var target = event.target as HTMLDivElement;
    if (!loading && hasMore && target.scrollTop + target.offsetHeight === target.scrollHeight) {
      loadMore();
      target.scrollTo(0, target.scrollHeight);
    }
  };

  return (
    <AntdSelect
      onPopupScroll={onScroll}
      size="large"
      dropdownClassName={styles.dropdown}
      className={clsx(styles.root, className)}
      {...props}
    >
      {children}
      {hasMore && (
        <AntdSelect.Option key="loading">
          {' '}
          <div className={styles['loader-loading-more']}>
            <Spin />
          </div>
        </AntdSelect.Option>
      )}
    </AntdSelect>
  );
};

InfiniteSelect.Option = AntdSelect.Option;
