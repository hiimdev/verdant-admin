import { forwardRef } from 'react';
import { Input as AntdInput, InputProps } from 'antd';
import styles from './Input.module.less';
import clsx from 'clsx';
import { TextAreaProps } from './types';

export type ModifiedInputProps = Partial<InputProps> & {
  id?: string;
  searchForm?: boolean;
  className?: string;
  surfaceClassName?: string;
  autoSize?: any;
};

const { Password: AntPassword, TextArea: AntdTextArea, Search: AntdInputSearch } = AntdInput;

export const Input = forwardRef<AntdInput, ModifiedInputProps>(function Input(
  { className, id, size = 'middle', ...props },
  ref
) {
  const inputWithSize = styles[size];

  return <AntdInput ref={ref} id={id} className={clsx(styles.input, inputWithSize, className)} {...props} />;
});

export const InputPassword = forwardRef<AntdInput, ModifiedInputProps>(function Input(
  { className, id, size = 'middle', ...props },
  ref
) {
  const inputWithSize = styles[size];
  return <AntPassword ref={ref} id={id} className={clsx(styles.input, inputWithSize, className)} {...props} />;
});

export const InputSearch = forwardRef<AntdInput, ModifiedInputProps>(function Input(
  { className, id, size = 'middle', ...props },
  ref
) {
  const inputWithSize = styles[size];
  return (
    <AntdInputSearch
      ref={ref}
      id={id}
      className={clsx(styles.input, styles.inputSearch, inputWithSize, className)}
      {...props}
    />
  );
});

export const InputTextArea = forwardRef<AntdInput, TextAreaProps>(function Input(
  { className, id, size = 'middle', ...props },
  ref
) {
  const inputWithSize = styles[size];
  return (
    <AntdTextArea
      ref={ref}
      id={id}
      className={clsx(styles.input, styles.textArea, inputWithSize, className)}
      {...props}
    />
  );
});
