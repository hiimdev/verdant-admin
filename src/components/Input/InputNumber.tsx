import { InputNumber as AntdInputNumber } from 'antd';
import clsx from 'clsx';
import { forwardRef } from 'react';
import styles from './Input.module.less';
import { InputNumberProps } from './types';

export const InputNumber = forwardRef<HTMLInputElement, InputNumberProps>(function InputNumber(
  { className, ...props },
  ref
) {
  return (
    <AntdInputNumber
      ref={ref}
      type="number"
      className={clsx(styles.input, styles.inputNumber, props.size && styles[props.size], className)}
      {...props}
    />
  );
});
