import { InputNumberProps as AntdInputNumberProps, InputProps as AntdInputProps } from 'antd';

export type InputProps = Pick<
  AntdInputProps,
  | 'placeholder'
  | 'id'
  | 'disabled'
  | 'defaultValue'
  | 'addonAfter'
  | 'addonBefore'
  | 'prefix'
  | 'size'
  | 'type'
  | 'value'
  | 'onChange'
  | 'onPressEnter'
  | 'className'
  | 'readOnly'
>;

export type InputNumberProps = Pick<
  AntdInputNumberProps,
  | 'defaultValue'
  | 'decimalSeparator'
  | 'disabled'
  | 'formatter'
  | 'max'
  | 'min'
  | 'parser'
  | 'precision'
  | 'size'
  | 'step'
  | 'value'
  | 'onChange'
  | 'onPressEnter'
  | 'className'
  | 'placeholder'
  | 'addonAfter'
  | 'addonBefore'
  | 'onKeyDown'
  | 'type'
>;

export type TextAreaProps = {
  allowClear?: boolean;
  autoSize?: any;
  bordered?: boolean;
  onPressEnter?(): void;
  value?: string;
  showCount?: boolean;
  maxLength?: number;
  className?: string;
  id?: any;
  size?: any;
  placeholder?: string;
  label?: string;
  disabled?: boolean;
};
