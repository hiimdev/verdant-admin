import { Button } from 'components/Button';
import { useWallet } from 'hooks/useWallet';
import { CURRENCY } from 'lib/config';
import { requestSwitchNetwork } from 'lib/requestSwitchNetwork';
import { FC } from 'react';
import { useInactiveListener } from 'hooks/useEagerConnect';
import { Space, Typography } from 'antd';
import { formatEther } from 'ethers/lib/utils';
import clsx from 'clsx';
import { shortenAddress, useEtherBalance } from '@usedapp/core';
import styles from './styles.module.less';
import Identicon from './Identicon';
export const HeaderContent: FC = () => {
  const { chainId, account, error, connect } = useWallet();
  const etherBalance = useEtherBalance(account);

  useInactiveListener();

  const handleSwitchNetwork = async () => {
    await requestSwitchNetwork(Number(Object.keys(CURRENCY)[0]));
  };

  const handleConnectWallet = async () => {
    await connect('injected');
  };

  if (
    (account && chainId !== 80001) ||
    String(error?.name) === 'UnsupportedChainIdError' ||
    String(error?.name) === 't'
  )
    return <Button onClick={handleSwitchNetwork}>Change to Polygon</Button>;
  if (!account || chainId !== 80001) return <Button onClick={handleConnectWallet}>Connect wallet</Button>;
  return (
    <Button
      className={clsx(styles.button, 'f-between')}
      // onClick={openAccountModal}
    >
      <Space align="center">
        <Typography style={{ paddingTop: 2 }}>
          {`${Number(formatEther(etherBalance || 0)).toFixed(2)} ${chainId ? CURRENCY[chainId] : ''}`}
        </Typography>
        <p>{account ? shortenAddress(account) : ''}</p>
        <Identicon />
      </Space>
    </Button>
  );
};
