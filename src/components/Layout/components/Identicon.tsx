// Identicon.tsx
import { useEffect, useRef } from 'react';
import { useWallet } from 'hooks/useWallet';
//@ts-ignore
import Jazzicon from '@metamask/jazzicon';
export default function Identicon() {
  const ref = useRef<HTMLDivElement>(null);
  const { account } = useWallet();

  useEffect(() => {
    if (account && ref.current) {
      ref.current.innerHTML = '';
      ref.current.appendChild(Jazzicon(20, parseInt(account.slice(2, 10), 16)));
    }
  }, [account]);

  return <div style={{ width: 20, height: 20 }} ref={ref} />;
}
