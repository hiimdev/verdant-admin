import clsx from 'clsx';
import { FC } from 'react';
import styles from './styles.module.less';

export const Video: FC<{ src: string; className?: string }> = ({ src, className }) => {
  return (
    <div className={clsx(className)}>
      <video className={clsx(styles.root)} autoPlay muted loop>
        <source src={src} type="video/mp4" />
      </video>
    </div>
  );
};
