import { ActionCreatorWithPayload } from '@reduxjs/toolkit';
import { Table } from 'components/Table';
import { FC } from 'react';

type IPaginatedTableAndSearchProps = {
  columns: any[];
  useQuery: any;
  requestParams?: any;
  scroll?: any;
  rowSelection?: any;
  summary?: any;
  title?: string;
  extra?: any;
  othersExtra?: any;
  setEditingId?: ActionCreatorWithPayload<string | null, string>;
  pagination?: boolean;
  permission?: string;
  handleReset?: any;
  mutateDownload?: any;
};

export const PaginatedTableAndSearch: FC<IPaginatedTableAndSearchProps> = (props) => {
  return <Table {...props} />;
};
