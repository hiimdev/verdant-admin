import clsx from 'clsx';
import { FC } from 'react';
import styles from './styles.module.less';
import { PaginationProps as AntdPaginationProps, Pagination as AntdPagination } from 'antd';

type PaginationProps = AntdPaginationProps & {
  className?: string;
};

export const Pagination: FC<PaginationProps> = ({
  className,
  current = 1,
  pageSize = 10,
  total,
  defaultCurrent,
  onChange,
  simple,
}) => {
  return (
    <AntdPagination
      className={clsx(styles.root, className)}
      current={current}
      pageSize={pageSize}
      defaultCurrent={defaultCurrent}
      total={total}
      showSizeChanger={false}
      onChange={onChange}
      simple={simple}
      hideOnSinglePage
    />
  );
};
