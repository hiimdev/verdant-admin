import { FilterFilled } from '@ant-design/icons';
import { Popover as AntdPopover } from 'antd';
import { RenderFunction } from 'antd/lib/_util/getRenderPropValue';
import React, { FC } from 'react';

export const Base: FC<{ content: React.ReactNode | RenderFunction }> = ({ content }) => {
  return (
    <AntdPopover content={content} title={null} trigger="click" placement="bottom">
      <FilterFilled style={{ color: '#666' }} />
    </AntdPopover>
  );
};
