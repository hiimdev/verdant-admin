import { FilterFilled } from '@ant-design/icons';
import { Col, Form, Popover as AntdPopover, Radio, Row } from 'antd';
import { Button } from 'components/Button';
import { FC, useState } from 'react';

type IProps = {
  name?: string;
  label: string;
  onSubmit?: (values?: any) => void;
  obj?: any;
  sort?: number[];
};

const Content: FC<{ label: string; onFinish: (value: any) => void; name?: string; obj?: any; sort?: number[] }> = ({
  onFinish,
  name,
  obj,
  sort,
}) => {
  const [form] = Form.useForm();

  const handleReset = () => {
    form.resetFields();
  };

  return (
    <Form onFinish={onFinish} form={form}>
      {name && (
        <Form.Item name={name}>
          <Radio.Group>
            {Object.keys(obj).map((key: any) => (
              <Radio key={key} value={[0, 1].includes(key) ? Number(key) : key}>
                {obj[key as 0 | 1 | 'request' | 'active']}
              </Radio>
            ))}
          </Radio.Group>
        </Form.Item>
      )}
      {sort && (
        <Form.Item name="sort">
          <Radio.Group>
            {sort?.map((key: any) => (
              <Radio key={key} value={key}>
                {key % 2 === 0 ? 'Desc' : 'Asc'}
              </Radio>
            ))}
          </Radio.Group>
        </Form.Item>
      )}
      <Row gutter={12}>
        <Col span={12}>
          <Form.Item style={{ marginBottom: 0 }}>
            <Button type="primary" htmlType="submit">
              Search
            </Button>
          </Form.Item>
        </Col>
        <Col span={12}>
          <Button onClick={handleReset}>Reset</Button>
        </Col>
      </Row>
    </Form>
  );
};

export const SelectFilter: FC<IProps> = ({ label, name, onSubmit, obj, sort }) => {
  const [visible, setVisible] = useState(false);

  const onFinish = (value: any) => {
    if (onSubmit) {
      onSubmit(value);
    }
    setVisible(false);
  };

  return (
    <AntdPopover
      onVisibleChange={() => setVisible(!visible)}
      visible={visible}
      content={<Content label={label} onFinish={onFinish} name={name} obj={obj} sort={sort} />}
      title={null}
      trigger="click"
      placement="bottomRight"
    >
      <FilterFilled style={{ color: '#a69b9a' }} />
    </AntdPopover>
  );
};
