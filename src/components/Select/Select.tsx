import { Select as AntdSelect, SelectProps as AntdSelectProps } from 'antd';
import clsx from 'clsx';
import { FC } from 'react';
import styles from './styles.module.css';

type SelectProps = Pick<
  AntdSelectProps<any>,
  | 'className'
  | 'defaultValue'
  | 'loading'
  | 'onChange'
  | 'value'
  | 'showSearch'
  | 'placeholder'
  | 'filterOption'
  | 'disabled'
  | 'mode'
  | 'style'
  | 'onSelect'
  | 'onClick'
  | 'notFoundContent'
  | 'options'
>;

export const Select: FC<SelectProps> & { Option: typeof AntdSelect.Option } = ({ children, className, ...props }) => {
  return (
    <AntdSelect size="large" dropdownClassName={styles.dropdown} className={clsx(styles.root, className)} {...props}>
      {children}
    </AntdSelect>
  );
};

Select.Option = AntdSelect.Option;
