/* eslint-disable @typescript-eslint/no-unused-vars */
import { ActionCreatorWithPayload } from '@reduxjs/toolkit';
import { Button, Card, Space, Table as TableAntd } from 'antd';
import { downloadTransaction } from 'api/statistical';
import { IError, IPaginationRequest } from 'api/types';
import { useAppDispatch, useAppSelector } from 'hooks/reduxHook';
import { FC, useEffect, useMemo, useState } from 'react';
import { useMutation } from 'react-query';
import { getPermission } from 'store/ducks/user/slice';
import { onDownload } from 'utils/js';
import { message } from 'utils/message';
import styles from './styles.module.less';
type ITableProps = {
  title?: string;
  columns: any[];
  useQuery: any;
  requestParams?: any;
  scroll?: any;
  rowSelection?: any;
  summary?: any;
  extra?: any;
  othersExtra?: any;
  setEditingId?: ActionCreatorWithPayload<string | null, string>;
  pagination?: boolean;
  permission?: string;
  handleReset?: any;
  mutateDownload?: any;
};

const NOT_SHOW = ['/'];

export const Table: FC<ITableProps> = ({
  scroll,
  columns,
  useQuery,
  requestParams,
  summary,
  title,
  extra,
  setEditingId,
  othersExtra,
  permission,
  pagination = true,
  rowSelection,
  handleReset,
  mutateDownload,
}) => {
  const [initialValue, setInitialValue] = useState(requestParams);
  const [page, setPage] = useState<IPaginationRequest>({ page: 1, limit: 10 });
  const dispatch = useAppDispatch();
  const permissions = useAppSelector(getPermission);
  const isAdd = useMemo(() => {
    return permissions.includes(permission || '');
  }, [permission, permissions]);

  useEffect(() => {
    setPage({ ...page, ...requestParams });
  }, [JSON.stringify(requestParams)]);

  const { data, status } = useQuery(page);

  const dataSource = useMemo(
    () =>
      data?.list?.map((item: any, index: number) => ({
        ...item,
        index: (page.page - 1) * page.limit + index + 1,
        total: Number(data?.pagination?.totalItems || 0),
      })) || [],
    [data]
  );

  const handleExtra = () => {
    if (setEditingId) {
      dispatch(setEditingId(''));
    }
  };
  const onReset = () => {
    handleReset();
    setPage({ page: 1, limit: 10, ...initialValue });
  };

  return (
    <Card
      title={title}
      extra={
        <Space size={10}>
          {!NOT_SHOW.includes(location.pathname) && (
            <Button onClick={onReset} size="large">
              Reset Filter
            </Button>
          )}
          {mutateDownload && (
            <Button size="large" onClick={() => mutateDownload(page)}>
              Export (excel/csv)
            </Button>
          )}
          {othersExtra}
          {setEditingId && isAdd ? (
            <div onClick={handleExtra}>
              {extra || (
                <Button onClick={handleExtra} size="large" type="primary">
                  Add New
                </Button>
              )}
            </div>
          ) : undefined}
        </Space>
      }
    >
      <TableAntd
        bordered
        className={styles.table}
        columns={columns}
        dataSource={dataSource}
        loading={status === 'loading'}
        rowKey="id"
        pagination={
          pagination
            ? {
                current: page.page,
                pageSize: page.limit,
                total: Number(data?.pagination?.totalItems || 0),
                showSizeChanger: true,
                showQuickJumper: true,
              }
            : false
        }
        summary={
          summary
            ? () => {
                return summary;
              }
            : () => {
                return <></>;
              }
        }
        onChange={(pagination) => {
          setPage({
            ...requestParams,
            page: Number(pagination.current),
            limit: Number(pagination.pageSize || 0),
          });
        }}
        scroll={scroll}
        rowSelection={rowSelection}
      />
    </Card>
  );
};
