import { FilterFilled } from '@ant-design/icons';
import { Button, DatePicker, Form, Popover as AntdPopover, Radio, Row } from 'antd';
import { Input } from 'components/Input';
import { Select } from 'components/Select';
import { FC, useState } from 'react';

type IProps = {
  name: string;
  label: string;
  onSubmit?: (values?: any) => void;
  sort?: number[];
  type?: 'select' | 'input' | 'none' | 'date';
  obj?: any;
};
const { RangePicker } = DatePicker;

const Content: FC<IProps> = ({ label, onSubmit, name, sort, obj, type }) => {
  const [form] = Form.useForm();

  const handleReset = () => {
    form.resetFields();
  };
  return (
    <Form
      onFinish={(values: any) => {
        const request = { ...values };
        if (values?.date) {
          request.startTimestamp = new Date(values.date[0] as any).getTime();
          request.endTimestamp = new Date(values.date[1] as any).getTime();
        }
        delete request?.date;
        onSubmit?.(request);
        handleReset();
      }}
      form={form}
    >
      {type === 'input' && (
        <Form.Item style={{ marginBottom: 10 }} name={name}>
          <Input placeholder={`Search ${label}`} />
        </Form.Item>
      )}
      {type === 'select' && (
        <Form.Item style={{ marginBottom: 10 }} name={name}>
          <Select placeholder="Select values">
            {Object.keys(obj).map((key: string) => (
              <Select.Option key={key} value={key}>
                {obj[Number(key || 0) as number]}
              </Select.Option>
            ))}
          </Select>
        </Form.Item>
      )}
      {sort && sort?.length > 0 && (
        <Form.Item style={{ marginBottom: 10 }} name="sort">
          <Radio.Group style={{ width: '100%' }}>
            <Row justify="space-between">
              {sort?.map((key: any) => (
                <Radio key={key} value={key}>
                  {key % 2 === 0 ? 'Asc' : 'Desc'}
                </Radio>
              ))}
            </Row>
          </Radio.Group>
        </Form.Item>
      )}
      {type === 'date' && (
        <Form.Item style={{ marginBottom: 10 }} name="date">
          <RangePicker size="large" />
        </Form.Item>
      )}
      <Row align="middle" justify="space-between">
        <Form.Item style={{ marginBottom: 0 }}>
          <Button size="middle" type="primary" htmlType="submit">
            Search
          </Button>
        </Form.Item>
        <Button onClick={handleReset}>Reset</Button>
      </Row>
    </Form>
  );
};

export const TableTitle: FC<IProps> = ({ label, onSubmit, name, sort, obj = {}, type = 'input' }) => {
  const [visible, setVisible] = useState(false);

  const onFinish = (value: any) => {
    if (onSubmit) {
      onSubmit(value);
    }
    setVisible(false);
  };

  return (
    <Row align="middle" justify="center" gutter={16} wrap={false}>
      {label}
      <AntdPopover
        onVisibleChange={() => setVisible(!visible)}
        visible={visible}
        content={<Content label={label} onSubmit={onFinish} name={name} sort={sort || []} type={type} obj={obj} />}
        title={null}
        trigger="click"
        placement="bottomRight"
      >
        <FilterFilled style={{ color: '#a69b9a' }} />
      </AntdPopover>
    </Row>
  );
};
