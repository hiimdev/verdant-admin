import { UploadProps as AntdUploadProps, Upload as UploadAntd } from 'antd';
import { UploadChangeParam } from 'antd/lib/upload';
import { UploadFile } from 'antd/lib/upload/interface';
import clsx from 'clsx';
import { FC, useEffect, useState } from 'react';
import { configForm } from 'utils/form';
import { message } from 'utils/message';
type UploadProps = AntdUploadProps & {
  className?: string;
  onChangeFile?: Function;
  imageUrl?: string;
};

export const Upload: FC<UploadProps> = ({ children, className, onChangeFile, imageUrl, ...props }) => {
  const [file, setFile] = useState<UploadFile<any>[]>();

  useEffect(() => {
    if (imageUrl) {
      setFile([
        {
          uid: '-1',
          status: 'done',
          url: imageUrl, // is imageUrl
          // name: value.substring(value.lastIndexOf('/') + 1),
          name: 'abc',
        },
      ]);
    }
  }, [imageUrl]);

  const onChange = (info: UploadChangeParam) => {
    const file = info?.file;

    if (file?.status === 'removed') return;

    if (file) {
      const isJpgOrPng = configForm.allowedImageContentType.includes(file.type as string);
      if (!isJpgOrPng) {
        message.error('You can only upload JPG/PNG/GIF file!');
        return;
      }
      const fileLarger = (file.size as number) / 1024 / 1024 < configForm.maxImageFileSize;
      if (!fileLarger) {
        message.error(`Image must smaller than ${configForm.maxImageFileSize}MB!`);
        return;
      }
    }

    setFile(info.fileList);
    if (onChangeFile) {
      onChangeFile(file);
    }
    return false;
  };

  const onRemove = () => {
    setFile(undefined);
    if (onChangeFile) {
      onChangeFile(undefined);
    }
  };

  const onPreview = async (file: any) => {
    let src = file.url;
    if (!src) {
      src = await new Promise((resolve) => {
        const reader = new FileReader();
        reader.readAsDataURL(file.originFileObj);
        reader.onload = () => resolve(reader.result);
      });
    }
    const image = new Image();
    image.src = src;
    const imgWindow = window.open(src);
    imgWindow?.document.write(image.outerHTML);
  };

  return (
    <UploadAntd
      {...props}
      className={clsx(className)}
      beforeUpload={() => false}
      maxCount={1}
      onChange={onChange}
      listType="picture-card"
      fileList={file}
      onRemove={onRemove}
      onPreview={onPreview}
      //   accept="image/png, image/jpeg, image/gif"
    >
      {children}
    </UploadAntd>
  );
};
