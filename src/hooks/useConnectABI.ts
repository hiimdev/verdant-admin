import { utils } from 'ethers';
import { Contract } from '@ethersproject/contracts';

export const useConnectABI = (token: any, address: string) => {
  const wethInterface = new utils.Interface(token);
  return new Contract(address, wethInterface);
};
