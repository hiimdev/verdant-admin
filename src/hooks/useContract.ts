import { useMemo } from 'react';
import { Contract } from '@ethersproject/contracts';
import { useEthers } from '@usedapp/core';
import { JsonRpcProvider } from '@ethersproject/providers';
import { POLYGON_RPC } from 'utils/constant';

export function useContract<T extends Contract = Contract>(address: string, ABI: any): T | null {
  const { library, account, chainId } = useEthers();

  return useMemo(() => {
    if (!address || !ABI || !library || !chainId || !account) {
      return null;
    }

    try {
      return new Contract(address, ABI, library.getSigner(account));
    } catch (error) {
      console.error('Failed To Get Contract', error);

      return null;
    }
  }, [address, ABI, library, account]) as T;
}

export function useContractNoSignner<T extends Contract = Contract>(address: string, ABI: any): T {
  return useMemo(() => {
    const httpProvider = new JsonRpcProvider(POLYGON_RPC);
    return new Contract(address, ABI, httpProvider);
  }, [address, ABI]) as T;
}
