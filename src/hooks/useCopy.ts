import { useState } from 'react';
import { message } from 'utils/message';

type CopyFn = (text: string) => void; // Return success

export function useCopy(): [boolean, CopyFn] {
  const [copied, setCopied] = useState(false);

  const copy = async (text: string) => {
    // if (!navigator?.clipboard) {
    //   return message.error('Clipboard not supported');
    // }

    // Try to save to clipboard then save it in the state if worked
    try {
      if (navigator?.clipboard) {
        await navigator.clipboard.writeText(text);
      } else {
        var textArea = document.createElement('textarea');
        textArea.value = text;
        textArea.style.position = 'fixed';
        textArea.style.left = '-999999px';
        textArea.style.top = '-999999px';
        document.body.appendChild(textArea);
        textArea.focus();
        textArea.select();
        document.execCommand('copy');
        textArea.remove();
      }
      message.success(`Copied: ${text}`);

      setCopied(true);
      setTimeout(() => {
        setCopied(false);
      }, 2000);
    } catch (error) {
      console.warn('Copy failed', error);
      setCopied(false);
    }
  };

  return [copied, copy];
}
