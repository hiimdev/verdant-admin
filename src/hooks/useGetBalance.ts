import { Contract } from '@ethersproject/contracts';
import { useCallback, useEffect, useMemo, useState } from 'react';
import { POLYGON_RPC } from 'utils/constant';
import { useContractNoSignner } from './useContract';
import Web3 from 'web3';

export function useGetTokenBalanceToken(address: string, token: string, ABI: any, updateStatus?: boolean) {
  const [value, setValue] = useState(undefined);
  const contract = useContractNoSignner<Contract>(token, ABI);
  useEffect(() => {
    (async () => {
      if (address) {
        const balance = await contract.balanceOf(address);
        setValue(balance);
      }
    })();
  }, [token, address, updateStatus]);
  return value;
}

const httpProvider = new Web3.providers.HttpProvider(POLYGON_RPC);
const lib = new Web3(httpProvider);

export function useGetTokenBalance_(address: string) {
  const [value, setValue] = useState<any>(undefined);
  useEffect(() => {
    (async () => {
      if (address) {
        const balance = await lib.eth.getBalance(address);
        setValue(balance);
      }
    })();
  }, [address]);
  const refetch = useCallback(() => {
    console.log('getting new balance');

    const getBalance = async () => {
      if (address) {
        const balance = await lib.eth.getBalance(address);
        setValue(balance);
      }
    };

    getBalance();
  }, [address]);

  const returnValue = useMemo(
    () => ({
      value: value || '0',
      refetch,
    }),
    [refetch, value]
  );

  return returnValue;
}

export function useGetTokenBalanceToken_(address: string, token: string, ABI: any) {
  const [value, setValue] = useState<string | undefined>(undefined);
  const contract = useContractNoSignner<Contract>(token, ABI);
  useEffect(() => {
    (async () => {
      if (address) {
        const balance = await contract.balanceOf(address);
        setValue(balance);
      }
    })();
  }, [token, address]);

  const refetch = useCallback(() => {
    console.log('getting new balance');
    const getBalance = async () => {
      const balance = await contract.balanceOf(address);
      setValue(balance);
    };

    getBalance();
  }, [address]);

  const returnValue = useMemo(
    () => ({
      value: value || '0',
      refetch,
    }),
    [refetch, value]
  );

  return returnValue;
}
