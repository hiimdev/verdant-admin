import { CONTRACT_ADDRESS } from '../utils/constant';
import { Contract } from '@ethersproject/contracts';
import { useEffect, useState } from 'react';
import { useContractNoSignner } from './useContract';
import NFT from 'contracts/NFTABI.json';

export function useGetMinter(address: string | null | undefined, update?: boolean) {
  const [value, setValue] = useState<any>(undefined);
  const contract = useContractNoSignner<Contract>(CONTRACT_ADDRESS.NFT, NFT);
  useEffect(() => {
    (async () => {
      if (address) {
        const isMinter = await contract._minters(address);
        setValue(isMinter);
      } else if (address === null) {
        setValue(false);
      }
    })();
  }, [address, update]);
  return value;
}
