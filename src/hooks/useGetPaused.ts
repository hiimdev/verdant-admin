import { Contract } from '@ethersproject/contracts';
import MarketABI from 'contracts/MarketABI.json';
import { useEffect, useState } from 'react';
import { CONTRACT_ADDRESS } from 'utils/constant';
import { useContractNoSignner } from './useContract';

export function useGetPaused(trigger: boolean) {
  const [value, setValue] = useState<boolean | undefined>(undefined);
  const [loading, setLoading] = useState<boolean>(false);
  const contract = useContractNoSignner<Contract>(CONTRACT_ADDRESS.MARKET, MarketABI);
  useEffect(() => {
    (async () => {
      setLoading(true);
      const isPaused = await contract.paused();
      setValue(isPaused);
      setLoading(false);
    })();
  }, [trigger]);
  return { value, loading };
}
