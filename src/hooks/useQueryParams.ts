export default function useQueryParams() {
  return new URLSearchParams(window.location.search);
}
