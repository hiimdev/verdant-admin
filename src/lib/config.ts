import { Config, Mumbai } from '@usedapp/core';
export const config: Config = {
  networks: [Mumbai],
  autoConnect: true,
};

export const CURRENCY = {
  [Mumbai.chainId]: 'MATIC',
};
