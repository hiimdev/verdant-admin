import { AnyAction, Dispatch } from '@reduxjs/toolkit';
import { Button as AntdButton, Space } from 'antd';
import { TableTitle } from 'components/Table/component/TableTitle';
import { AiFillEdit, AiFillEye } from 'react-icons/ai';
import { setEditingCategoryId } from 'store/ducks/about/slice';
import { CATEGORY_FAQ } from 'utils/constant';
import { timeFormatter } from 'utils/date';
import styles from './styles.module.less';

export const getColumns = (dispatch: Dispatch<AnyAction>, submitParams?: any, isUpdate?: boolean) => {
  return [
    {
      title: 'No',
      dataIndex: 'index',
      key: 'index',
      align: 'center',
      width: '80px',
    },
    {
      title: (
        <TableTitle
          name="section"
          label="Category"
          onSubmit={submitParams}
          sort={[CATEGORY_FAQ.SECTION_DESC, CATEGORY_FAQ.SECTION_ASC]}
        />
      ),
      dataIndex: 'section',
      key: 'section',
      render(data: string) {
        return <div className={styles.category} dangerouslySetInnerHTML={{ __html: data }}></div>;
      },
    },
    {
      title: (
        <TableTitle
          type="none"
          name="createdAt"
          label="Create At"
          onSubmit={submitParams}
          sort={[CATEGORY_FAQ.CREATEAT_DESC, CATEGORY_FAQ.CREATEAT_ASC]}
        />
      ),
      dataIndex: 'createdAt',
      key: 'createdAt',
      render(createdAt: string) {
        return createdAt ? timeFormatter(Number(createdAt)) : '-';
      },
      align: 'center',
    },
    {
      title: (
        <TableTitle
          name="createdBy"
          label="Created By"
          onSubmit={submitParams}
          sort={[CATEGORY_FAQ.CREATED_BY_DESC, CATEGORY_FAQ.CREATED_BY_ASC]}
        />
      ),
      dataIndex: 'createdBy',
      key: 'createdBy',
      align: 'center',
      render(value: any) {
        return <>{value ? value : '-'}</>;
      },
    },
    {
      title: (
        <TableTitle
          type="none"
          name="updatedAt"
          label="Update at"
          onSubmit={submitParams}
          sort={[CATEGORY_FAQ.UPDATEAT_DESC, CATEGORY_FAQ.UPDATEAT_ASC]}
        />
      ),
      dataIndex: 'updatedAt',
      key: 'updatedAt',
      render(value: string) {
        return value ? timeFormatter(Number(value)) : '-';
      },
    },
    {
      title: (
        <TableTitle
          name="updatedBy"
          label="Update By"
          onSubmit={submitParams}
          sort={[CATEGORY_FAQ.UPDATED_BY_DESC, CATEGORY_FAQ.UPDATED_BY_ASC]}
        />
      ),
      dataIndex: 'updatedBy',
      key: 'updatedBy',
      align: 'center',
      render(value: any) {
        return <>{value ? value : '-'}</>;
      },
    },
    {
      title: 'Action',
      align: 'center',
      dataIndex: 'categoryAboutId',
      fixed: 'right',
      render(id: string) {
        return (
          <Space>
            <AntdButton
              className={styles.btnAction}
              icon={isUpdate ? <AiFillEdit /> : <AiFillEye />}
              type="primary"
              onClick={() => dispatch(setEditingCategoryId(id))}
              title={isUpdate ? 'Edit' : 'View'}
            />
          </Space>
        );
      },
    },
  ];
};
