import { Form } from 'antd';
import { createAboutCategory, updateAboutCategory, useGetDetailCategoryAbout } from 'api/about';
import { IError } from 'api/types';
import { Drawer } from 'components/Drawer';
import { useAppDispatch, useAppSelector } from 'hooks/reduxHook';
import { FC, useEffect, useMemo, useState } from 'react';
import { useMutation, useQueryClient } from 'react-query';
import ReactQuill, { Quill } from 'react-quill';
import quillEmoji from 'react-quill-emoji';
import { getEditingCategoryId, setEditingCategoryId } from 'store/ducks/about/slice';
import { FONT_SIZE_QUILL } from 'utils/constant';
import { rules } from 'utils/form';
import { message } from 'utils/message';
import { IListType } from './type';

// var Size = Quill.import('attributors/style/size');
const Size = Quill.import('formats/size');

Size.whitelist = FONT_SIZE_QUILL;
var quill: any;
Quill.register(
  {
    'formats/emoji': quillEmoji.EmojiBlot,
    'modules/emoji-toolbar': quillEmoji.ToolbarEmoji,
    'modules/emoji-textarea': quillEmoji.TextAreaEmoji,
    'modules/emoji-shortname': quillEmoji.ShortNameEmoji,
    Size: true,
  },
  true
);
const Editor: FC<IListType> = ({ isUpdate: canUpdate }) => {
  const dispatch = useAppDispatch();
  const editingId = useAppSelector(getEditingCategoryId);
  const client = useQueryClient();
  const [value, setValue] = useState('');

  const [form] = Form.useForm();

  const action = editingId ? 'edit' : 'add';

  const isUpdate = useMemo(() => {
    return (canUpdate && editingId !== '') || editingId === '';
  }, []);

  const { data, status: getLoading } = useGetDetailCategoryAbout(editingId as string, { enabled: !!editingId });

  const { mutate: update, isLoading: loadingUpdate } = useMutation(updateAboutCategory, {
    onSuccess: () => {
      client.invalidateQueries('/articles/select-category-about');
      message.success('Update successfully!');
      handleClose();
    },
    onError: (error: IError) => {
      message.error(error?.meta.message[0]);
    },
  });

  const { mutate: create, isLoading: loadingCreate } = useMutation(createAboutCategory, {
    onSuccess: () => {
      client.invalidateQueries('/articles/select-category-about');
      message.success('Create successfully!');
      handleClose();
    },
    onError: (error: IError) => {
      message.error(error?.meta.message[0]);
    },
  });

  useEffect(() => {
    form.resetFields();
    if (editingId !== null) {
      form.setFieldsValue(data);
    }
  }, [data]);

  const handleClose = () => {
    dispatch(setEditingCategoryId(null));
  };

  const modules = useMemo(() => {
    return {
      toolbar: {
        container: [
          // [{ header: [1, 2, 3, 4, 5, 6, false] }],
          ['bold', 'italic', 'underline', 'strike', 'blockquote'],
          [{ list: 'ordered' }, { list: 'bullet' }],
          [{ color: [] }, { background: [] }],
          // ['link', 'image', 'video'],
          // ['emoji'],
          // ['code-block'],
          ['clean'],
          // [{ align: '' }, { align: 'center' }, { align: 'right' }, { align: 'justify' }],
          [{ size: FONT_SIZE_QUILL }],
        ],
      },

      clipboard: {
        // toggle to add extra line breaks when pasting HTML:
        matchVisual: false,
      },
      'emoji-toolbar': true,
      'emoji-textarea': true,
      'emoji-shortname': true,
    };
  }, []);

  const onFinish = (value: any) => {
    const mutate = action === 'edit' ? update : create;
    const params = action === 'edit' ? { id: Number(editingId), ...value } : value;
    mutate(params);
  };

  return (
    <Drawer
      name="About Category"
      editingId={editingId}
      handleClose={handleClose}
      form={form}
      getLoading={getLoading === 'loading'}
      submitLoading={loadingCreate || loadingUpdate}
      viewOnly={!isUpdate}
    >
      <Form form={form} onFinish={onFinish}>
        <Form.Item label="Section" name="section" labelCol={{ span: 3 }} wrapperCol={{ span: 18 }} rules={[rules]}>
          <ReactQuill
            theme="snow"
            value={value}
            onChange={setValue}
            modules={modules}
            ref={(el) => {
              quill = el;
            }}
            placeholder={'Enter your section'}
          />
        </Form.Item>
      </Form>
    </Drawer>
  );
};

export default Editor;
