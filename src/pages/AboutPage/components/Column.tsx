import { AnyAction, Dispatch } from '@reduxjs/toolkit';
import { Button as AntdButton, Image, Space } from 'antd';
import { TableTitle } from 'components/Table/component/TableTitle';
import { AiFillEdit, AiFillEye, AiOutlineDelete } from 'react-icons/ai';
import { setDeletingId, setEditingId } from 'store/ducks/about/slice';
import { ABOUT_STATUS, SORT_ABOUT } from 'utils/constant';
import { timeFormatter } from 'utils/date';
import styles from './styles.module.less';

export const getColumns = (dispatch: Dispatch<AnyAction>, submitParams?: any, isUpdate?: boolean) => {
  return [
    {
      title: 'No',
      key: 'index',
      dataIndex: 'index',
      align: 'center',
      width: '80px',
    },
    {
      title: (
        <TableTitle
          name="section"
          label="Category"
          onSubmit={submitParams}
          sort={[SORT_ABOUT.SECTION_DESC, SORT_ABOUT.SECTION_ASC]}
        />
      ),
      dataIndex: 'section',
      key: 'section',
      align: 'center',
      width: '150px',
      render(data: any) {
        return <div className={styles.categoryName} dangerouslySetInnerHTML={{ __html: data }}></div>;
      },
    },
    {
      title: (
        <TableTitle
          name="title"
          label="Title"
          onSubmit={submitParams}
          sort={[SORT_ABOUT.TITLE_DESC, SORT_ABOUT.TITLE_ASC]}
        />
      ),
      dataIndex: 'title',
      key: 'title',
      align: 'center',
      width: '150px',
      render(data: any) {
        return <>{data || '-'}</>;
      },
    },
    {
      title: (
        <TableTitle
          name="description"
          label="Description"
          onSubmit={submitParams}
          sort={[SORT_ABOUT.DESCRIPTION_DESC, SORT_ABOUT.DESCRIPTION_ASC]}
        />
      ),
      dataIndex: 'description',
      key: 'description',
      render(description: string) {
        return <div className={styles.description} dangerouslySetInnerHTML={{ __html: description }} />;
      },
      width: '200px',
    },
    {
      title: 'Image',
      align: 'center',
      render({ logoUrl, title, section }: any) {
        return (
          <>
            {section === 'Other' || section === 'About Verdant' ? (
              '-'
            ) : (
              <Image className={styles.image} src={logoUrl} alt={title} />
            )}
          </>
        );
      },
    },
    {
      title: (
        <TableTitle
          type="select"
          name="status"
          label="Status"
          onSubmit={(values: any) =>
            submitParams({ ...values, status: values.status ? Number(values.status) : undefined })
          }
          sort={[]}
          obj={ABOUT_STATUS}
        />
      ),
      dataIndex: 'status',
      key: 'status',
      align: 'center',
      width: '150px',
      render(data: any) {
        return <>{data ? 'Publish' : 'Unpublish'}</>;
      },
    },
    {
      title: (
        <TableTitle
          type="none"
          name="createdAt"
          label="Create At"
          onSubmit={submitParams}
          sort={[SORT_ABOUT.CREATEAT_DESC, SORT_ABOUT.CREATEAT_ASC]}
        />
      ),
      dataIndex: 'createdAt',
      key: 'createdAt',
      render(createdAt: string) {
        return timeFormatter(Number(createdAt));
      },
      align: 'center',
    },
    {
      title: (
        <TableTitle
          name="createdBy"
          label="Created By"
          onSubmit={submitParams}
          sort={[SORT_ABOUT.CREATED_BY_DESC, SORT_ABOUT.CREATED_BY_ASC]}
        />
      ),
      dataIndex: 'createdBy',
      key: 'createdBy',
      align: 'center',
    },
    {
      title: (
        <TableTitle
          type="none"
          name="updatedAt"
          label="Update at"
          onSubmit={submitParams}
          sort={[SORT_ABOUT.UPDATEAT_DESC, SORT_ABOUT.UPDATEAT_ASC]}
        />
      ),
      dataIndex: 'updatedAt',
      key: 'updatedAt',
      render(value: string) {
        return timeFormatter(Number(value));
      },
    },
    {
      title: (
        <TableTitle
          name="updatedBy"
          label="Update By"
          onSubmit={submitParams}
          sort={[SORT_ABOUT.UPDATED_BY_DESC, SORT_ABOUT.UPDATED_BY_ASC]}
        />
      ),
      dataIndex: 'updatedBy',
      key: 'updatedBy',
      align: 'center',
    },
    {
      title: 'Action',
      align: 'center',
      dataIndex: 'id',
      fixed: 'right',
      render(id: string) {
        return (
          <Space>
            <AntdButton
              className={styles.btnAction}
              icon={isUpdate ? <AiFillEdit /> : <AiFillEye />}
              type="primary"
              onClick={() => dispatch(setEditingId(id))}
              title={isUpdate ? 'Edit' : 'View'}
            />
            {isUpdate && (
              <AntdButton
                className={styles.btnAction}
                icon={<AiOutlineDelete />}
                danger
                onClick={() => dispatch(setDeletingId(id))}
                title="Delete"
              />
            )}
          </Space>
        );
      },
    },
  ];
};
