import { Modal } from 'antd';
import { useGetDetailAbout } from 'api/about';
import { deleteAbout } from 'api/about/request';
import { IError } from 'api/types';
import { useAppDispatch, useAppSelector } from 'hooks/reduxHook';
import { FC } from 'react';
import { useMutation, useQueryClient } from 'react-query';
import { getDeletingId, setDeletingId } from 'store/ducks/about/slice';
import { message } from 'utils/message';
const Deletor: FC = () => {
  const dispatch = useAppDispatch();
  const deletingId = useAppSelector(getDeletingId);
  const client = useQueryClient();

  const { data, isLoading } = useGetDetailAbout(deletingId as string, { enabled: !!deletingId });

  const handleClose = () => {
    dispatch(setDeletingId(null));
  };

  const { mutate: mutateDelete, isLoading: deleteLoading } = useMutation(deleteAbout, {
    onSuccess: () => {
      client.invalidateQueries('/articles/paging_filter_about');
      message.success('Delete successfully!');
      handleClose();
    },
    onError: (error: IError) => {
      message.error(error?.meta.message[0]);
    },
  });

  const onDelete = () => {
    mutateDelete(deletingId as string);
  };

  if (isLoading) {
    return null;
  }

  return (
    <>
      <Modal
        visible={!!deletingId}
        title={`Delete about: ${data?.title}`}
        okButtonProps={{
          loading: deleteLoading,
        }}
        cancelButtonProps={{
          disabled: deleteLoading,
        }}
        onCancel={handleClose}
        onOk={onDelete}
        okType="danger"
        okText="Confirm"
      >
        Are you sure you want to delete this Brand?
      </Modal>
    </>
  );
};

export default Deletor;
