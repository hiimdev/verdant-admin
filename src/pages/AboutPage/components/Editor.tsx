import { Form } from 'antd';
import { createAbout, updateAbout, useGetDetailAbout } from 'api/about';
import { uploadImage } from 'api/article/request';
import { IError } from 'api/types';
import { Drawer } from 'components/Drawer';
import { Input } from 'components/Input';
import { Select } from 'components/Select';
import { Upload } from 'components/Upload';
import { useAppDispatch, useAppSelector } from 'hooks/reduxHook';
import ImageResize from 'quill-image-resize-module-react';
import { FC, useEffect, useMemo, useState } from 'react';
import { AiOutlinePlus } from 'react-icons/ai';
import { useMutation, useQueryClient } from 'react-query';
import ReactQuill, { Quill } from 'react-quill';
import quillEmoji from 'react-quill-emoji';
import { getEditingId, setEditingId } from 'store/ducks/about/slice';
import { ABOUT_STATUS, FONT_SIZE_QUILL } from 'utils/constant';
import { convertFormValue, getUploadValueProps, inputProps, rules } from 'utils/form';
import { message } from 'utils/message';
import { IListType } from './type';

var quill: any;
var quill2: any;
Quill.register('modules/imageResize', ImageResize);
Quill.register(
  {
    'formats/emoji': quillEmoji.EmojiBlot,
    'modules/emoji-toolbar': quillEmoji.ToolbarEmoji,
    'modules/emoji-textarea': quillEmoji.TextAreaEmoji,
    'modules/emoji-shortname': quillEmoji.ShortNameEmoji,
    Size: true,
  },
  true
);
const Size = Quill.import('formats/size');
Size.whitelist = FONT_SIZE_QUILL;

const Editor: FC<IListType> = ({ isUpdate: canUpdate }) => {
  const dispatch = useAppDispatch();
  const editingId = useAppSelector(getEditingId);
  const client = useQueryClient();
  const [value, setValue] = useState('');

  const [form] = Form.useForm();

  const isUpdate = useMemo(() => {
    return (canUpdate && editingId !== '') || editingId === '';
  }, []);

  const action = editingId ? 'edit' : 'add';
  const { data, status: getLoading } = useGetDetailAbout(editingId as string, { enabled: !!editingId });

  const { mutate: update, isLoading: loadingUpdate } = useMutation(updateAbout, {
    onSuccess: () => {
      client.invalidateQueries('/articles/paging_filter_about');
      message.success('Update successfully!');
      handleClose();
    },
    onError: (error: IError) => {
      message.error(error?.meta.message[0]);
    },
  });

  const { mutate: upload } = useMutation(uploadImage, {
    onSuccess: (data) => {
      const range = quill.getEditorSelection();
      if (range) quill.getEditor().insertEmbed(range.index, 'image', data);
      const range2 = quill2.getEditorSelection();
      if (range2) quill2.getEditor().insertEmbed(range2.index, 'image', data);
    },
    onError: (error: IError) => {
      message.error(error?.meta.message[0]);
    },
  });

  const getImageHandle = () => {
    const input = document.createElement('input');

    input.setAttribute('type', 'file');
    input.setAttribute('accept', 'image/*');
    input.click();

    input.onchange = () => {
      const formData = new FormData();
      var file: any = input.files ? input.files[0] : null;
      formData.append('image', file);
      upload(formData);
    };
  };

  const modules = useMemo(() => {
    return {
      toolbar: {
        container: [
          [{ header: [1, 2, 3, 4, 5, false] }],
          ['bold', 'italic', 'underline', 'strike', 'blockquote'],
          [{ list: 'ordered' }, { list: 'bullet' }],
          [{ color: [] }, { background: [] }],
          ['link', 'image', 'video'],
          ['emoji'],
          ['code-block'],
          ['clean'],
          [{ align: '' }, { align: 'center' }, { align: 'right' }, { align: 'justify' }],
          [{ size: FONT_SIZE_QUILL }],
        ],
        handlers: {
          image: getImageHandle,
        },
      },

      clipboard: {
        // toggle to add extra line breaks when pasting HTML:
        matchVisual: false,
      },
      imageResize: {
        parchment: Quill.import('parchment'),
        modules: ['Resize', 'DisplaySize'],
      },
      'emoji-toolbar': true,
      'emoji-textarea': true,
      'emoji-shortname': true,
    };
  }, []);

  const { mutate: create, isLoading: loadingCreate } = useMutation(createAbout, {
    onSuccess: () => {
      client.invalidateQueries('/articles/paging_filter_about');
      message.success('Create successfully!');
      handleClose();
    },
    onError: (error: IError) => {
      message.error(error?.meta.message[0]);
    },
  });

  useEffect(() => {
    form.resetFields();
    if (editingId !== null) {
      form.setFieldsValue({
        ...data,
        description_photo: data?.description,
        image: data?.logoUrl,
        status: data?.status.toString(),
        section: data?.section.replace(/(<([^>]+)>)/gi, ''),
      });
    }
  }, [data]);

  const handleClose = () => {
    dispatch(setEditingId(null));
  };

  const onFinish = (value: any) => {
    const mutate = action === 'edit' ? update : create;
    const params = action === 'edit' ? { id: editingId, ...value, categoryAboutId: data?.categoryAboutId } : value;
    mutate(convertFormValue(params));
  };

  const handleChange = (file: File, field: string) => {
    form.setFieldsValue({ [field]: file });
  };

  return (
    <Drawer
      name={`Content ${data?.section ? `: ${data?.section.replace(/(<([^>]+)>)/gi, '')}` : ''}`}
      editingId={editingId}
      handleClose={handleClose}
      form={form}
      getLoading={getLoading === 'loading'}
      submitLoading={loadingCreate || loadingUpdate}
      viewOnly={!isUpdate}
    >
      <Form form={form} onFinish={onFinish}>
        {!editingId && (
          <Form.Item
            label="Category"
            name="categoryAboutId"
            labelCol={{ span: 3 }}
            wrapperCol={{ span: 18 }}
            rules={[{ required: true }]}
          >
            <Select disabled={!isUpdate} placeholder="Select category">
              {[
                {
                  section: 'How Vedant works',
                  categoryAboutId: 3,
                },
                {
                  section: 'How it works',
                  categoryAboutId: 2,
                },
              ].map((item: any) => (
                <Select.Option key={item.categoryAboutId} value={item.categoryAboutId}>
                  {item.section}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
        )}
        {editingId && (
          <Form.Item
            label="Category"
            name="section"
            labelCol={{ span: 3 }}
            wrapperCol={{ span: 18 }}
            rules={[rules, inputProps.brand.name]}
          >
            <Input disabled />
          </Form.Item>
        )}
        <Form.Item
          label="Title"
          name="title"
          labelCol={{ span: 3 }}
          wrapperCol={{ span: 18 }}
          rules={[
            rules,
            { type: 'string', max: data?.categoryAboutId === 2 ? 50 : data?.categoryAboutId === 3 ? 100 : 50 },
          ]}
        >
          <Input disabled={!isUpdate} placeholder={'Enter your title'} />
        </Form.Item>
        <Form.Item
          label="Description"
          name="description"
          labelCol={{ span: 3 }}
          wrapperCol={{ span: 18 }}
          rules={[rules]}
        >
          <ReactQuill
            theme="snow"
            value={value}
            onChange={setValue}
            modules={modules}
            ref={(el) => {
              quill = el;
            }}
          />
        </Form.Item>
        <Form.Item
          label="Status"
          name="status"
          labelCol={{ span: 3 }}
          wrapperCol={{ span: 18 }}
          rules={[{ required: true }]}
        >
          <Select disabled={!isUpdate} placeholder="Select status">
            {Object.keys(ABOUT_STATUS).map((key: string) => (
              <Select.Option key={key} value={key}>
                {ABOUT_STATUS[Number(key || 0) as 0 | 1]}
              </Select.Option>
            ))}
          </Select>
        </Form.Item>
        {data?.categoryAboutId !== 1 && data?.categoryAboutId !== 4 && (
          <Form.Item
            name="image"
            label="Image"
            labelCol={{ span: 3 }}
            wrapperCol={{ span: 18 }}
            getValueProps={getUploadValueProps}
          >
            <Upload
              disabled={!isUpdate}
              listType="picture-card"
              onChangeFile={(file: File) => handleChange(file, 'image')}
              imageUrl={data?.logoUrl}
            >
              <AiOutlinePlus />
            </Upload>
          </Form.Item>
        )}
      </Form>
    </Drawer>
  );
};

export default Editor;
