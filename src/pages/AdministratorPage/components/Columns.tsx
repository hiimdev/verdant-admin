import { AnyAction, Dispatch } from '@reduxjs/toolkit';
import { Button, Space } from 'antd';
import { TableTitle } from 'components/Table/component/TableTitle';
import { AiFillEdit, AiFillEye, AiOutlineDelete } from 'react-icons/ai';
import { setDeletingId, setEditingId } from 'store/ducks/administrator/slice';
import { ADMIN_SORT, ADMIN_STATUS_ENUMS } from 'utils/constant';
import { timeFormatter } from 'utils/date';

export const getColumns = (dispatch: Dispatch<AnyAction>, submitParams?: any, isUpdate?: boolean) => {
  return [
    {
      title: 'No',
      dataIndex: 'index',
      key: 'index',
      align: 'center',
      width: '80px',
    },
    {
      title: (
        <TableTitle
          name="fullName"
          label="Full name"
          onSubmit={submitParams}
          sort={[ADMIN_SORT.FULLNAME_DESC, ADMIN_SORT.FULLNAME_ASC]}
        />
      ),
      dataIndex: 'fullName',
      key: 'fullName',
    },
    {
      title: (
        <TableTitle
          label="Username"
          onSubmit={submitParams}
          name="username"
          sort={[ADMIN_SORT.USERNAME_DESC, ADMIN_SORT.USERNAME_ASC]}
        />
      ),
      dataIndex: 'username',
      key: 'username',
    },
    {
      title: (
        <TableTitle
          label="Email"
          onSubmit={submitParams}
          name="email"
          sort={[ADMIN_SORT.EMAIL_DESC, ADMIN_SORT.EMAIL_ASC]}
        />
      ),
      dataIndex: 'email',
      key: 'email',
    },
    {
      title: (
        <TableTitle
          label="Role"
          onSubmit={submitParams}
          name="roleName"
          sort={[ADMIN_SORT.GROUP_DESC, ADMIN_SORT.GROUP_ASC]}
        />
      ),
      dataIndex: 'roleName',
      key: 'roleName',
    },
    {
      title: (
        <TableTitle
          label="Status"
          type="select"
          onSubmit={(values: any) =>
            submitParams({ ...values, isActive: values.isActive ? Number(values.isActive) : undefined })
          }
          name="isActive"
          obj={ADMIN_STATUS_ENUMS}
        />
      ),
      dataIndex: 'isActive',
      key: 'status',
      render(isActive: 0 | 1) {
        return ADMIN_STATUS_ENUMS[isActive];
      },
    },
    {
      title: (
        <TableTitle
          type={'none'}
          label="Update Time"
          onSubmit={submitParams}
          name="updatedAt"
          sort={[ADMIN_SORT.UPDATEAT_DESC, ADMIN_SORT.UPDATEAT_ASC]}
        />
      ),
      dataIndex: 'updatedAt',
      key: 'updatedAt',
      render(value: string) {
        return timeFormatter(Number(value));
      },
    },
    {
      title: (
        <TableTitle
          label="Created By"
          onSubmit={submitParams}
          name="createdBy"
          sort={[ADMIN_SORT.CREATED_BY_DESC, ADMIN_SORT.CREATED_BY_ASC]}
        />
      ),
      dataIndex: 'createdBy',
      key: 'createdBy',
      align: 'center',
      render(data: any) {
        return <>{data || '-'}</>;
      },
    },
    {
      title: 'Action',
      align: 'center',
      dataIndex: 'id',
      fixed: 'right',
      render(id: any) {
        return (
          <Space>
            <Button
              icon={isUpdate ? <AiFillEdit /> : <AiFillEye />}
              type="primary"
              onClick={() => setEditingId && dispatch(setEditingId(String(id)))}
              title={isUpdate ? 'Edit' : 'View'}
            />
            {isUpdate && (
              <Button
                icon={<AiOutlineDelete />}
                danger
                onClick={() => setDeletingId && dispatch(setDeletingId(String(id)))}
                title="Delete"
              />
            )}
          </Space>
        );
      },
    },
  ];
};
