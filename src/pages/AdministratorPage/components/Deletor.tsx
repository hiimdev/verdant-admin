import { Modal } from 'antd';
import { useAppDispatch, useAppSelector } from 'hooks/reduxHook';
import { FC } from 'react';
import { getDeletingId, setDeletingId } from 'store/ducks/administrator/slice';
import { useAdminQuery } from 'api/admin';
import { useMutation, useQueryClient } from 'react-query';
import { deleteAdministrator } from 'api/admin/request';
import { message } from 'utils/message';
import { IError } from 'api/types';
const Deletor: FC = () => {
  const dispatch = useAppDispatch();
  const deletingId = useAppSelector(getDeletingId);
  const client = useQueryClient();

  const { data, isLoading } = useAdminQuery(deletingId as string, { enabled: !!deletingId });

  const handleClose = () => {
    dispatch(setDeletingId(null));
  };

  const { mutate: deleteAdmin, isLoading: deleteLoading } = useMutation(deleteAdministrator, {
    onSuccess: () => {
      client.invalidateQueries('/admin/paging_filter');
      message.success('Delete successfully!');
      handleClose();
    },
    onError: (error: IError) => {
      message.error(error?.meta.message[0]);
    },
  });

  const onDelete = () => {
    deleteAdmin(deletingId as string);
  };

  if (isLoading) {
    return null;
  }

  return (
    <Modal
      visible={!!deletingId}
      title={`Delete administrator: ${data?.username}`}
      okButtonProps={{
        loading: deleteLoading,
      }}
      cancelButtonProps={{
        disabled: deleteLoading,
      }}
      onCancel={handleClose}
      onOk={onDelete}
      okType="danger"
      okText="Confirm"
    >
      Are you sure you want to delete this Administrator?
    </Modal>
  );
};

export default Deletor;
