import { Form } from 'antd';
import { useAdminQuery } from 'api/admin';
import { createAdministrator, updateAdministrator } from 'api/admin/request';
import { useGetListAllRole } from 'api/permissions';
import { IError } from 'api/types';
import { Drawer } from 'components/Drawer';
import { Input, InputPassword } from 'components/Input';
import { Select } from 'components/Select';
import { useAppDispatch, useAppSelector } from 'hooks/reduxHook';
import { FC, useEffect, useMemo } from 'react';
import { useMutation, useQueryClient } from 'react-query';
import { getEditingId, setEditingId } from 'store/ducks/administrator/slice';
import { ADMIN_STATUS_ENUMS } from 'utils/constant';
import { convertFormValue, rules } from 'utils/form';
import { message } from 'utils/message';
import { REGEX } from 'utils/regex';
import { IListType } from './type';

const Editor: FC<IListType> = ({ isUpdate: canUpdate }) => {
  const dispatch = useAppDispatch();
  const editingId = useAppSelector(getEditingId);
  const client = useQueryClient();
  const { data: roles } = useGetListAllRole({ page: 1, limit: 50, status: 1 });

  const isUpdate = useMemo(() => {
    return (canUpdate && editingId !== '') || editingId === '';
  }, []);

  const [form] = Form.useForm();

  const action = editingId ? 'edit' : 'add';

  const { data, status: getLoading } = useAdminQuery(editingId as string, { enabled: !!editingId });

  const { mutate: update, isLoading: loadingUpdate } = useMutation(updateAdministrator, {
    onSuccess: () => {
      client.invalidateQueries('/admin/paging_filter');
      message.success('Update successfully!');
      handleClose();
    },
    onError: (error: IError) => {
      message.error(error?.meta.message[0]);
    },
  });
  const { mutate: create, isLoading: loadingCreate } = useMutation(createAdministrator, {
    onSuccess: () => {
      client.invalidateQueries('/admin/paging_filter');
      message.success('Create successfully!');
      handleClose();
    },
    onError: (error: IError) => {
      message.error(error?.meta.message[0]);
    },
  });

  useEffect(() => {
    form.resetFields();
    if (editingId !== null) {
      form.setFieldsValue(data);
    }
  }, [data]);

  const handleClose = () => {
    dispatch(setEditingId(null));
  };

  const onFinish = (value: any) => {
    const mutate = action === 'edit' ? update : create;
    const params =
      action === 'edit'
        ? {
            id: editingId,
            fullName: value.fullName,
            group: '0',
            isActive: String(value.isActive),
            roleId: value.roleId,
          }
        : { ...convertFormValue(value), password: value.password, group: '0' };
    mutate(params);
  };
  return (
    <Drawer
      name="Administrator"
      editingId={editingId}
      handleClose={handleClose}
      form={form}
      getLoading={getLoading === 'loading'}
      submitLoading={loadingCreate || loadingUpdate}
      viewOnly={!isUpdate}
    >
      <>
        <Form form={form} onFinish={onFinish}>
          <Form.Item
            label="Username"
            name="username"
            labelCol={{ span: 3 }}
            wrapperCol={{ span: 18 }}
            rules={[
              { ...rules, min: 6 },
              () => ({
                validator(_, value) {
                  if (REGEX.userName.test(value) || value.length < 6 || isUpdate) {
                    return Promise.resolve();
                  } else
                    return Promise.reject(
                      new Error(
                        'Username must not contain any Special Character(s). Only "." and  symbol is allowed in the Username field.'
                      )
                    );
                },
              }),
            ]}
          >
            <Input maxLength={30} disabled={action === 'edit'} placeholder={'Enter your user name'} />
          </Form.Item>

          {!editingId && (
            <Form.Item
              label="Password"
              name="password"
              labelCol={{ span: 3 }}
              wrapperCol={{ span: 18 }}
              rules={[
                { ...rules },
                () => ({
                  validator(_, value) {
                    if (REGEX.password.test(value)) {
                      return Promise.resolve();
                    }
                    return Promise.reject(
                      new Error('Password must be at least 8 characters and contain 1 special character or number')
                    );
                  },
                }),
              ]}
            >
              <InputPassword
                autoComplete="new-password"
                disabled={action === 'edit'}
                placeholder={'Enter your password'}
              />
            </Form.Item>
          )}
          <Form.Item
            label="Email"
            name="email"
            labelCol={{ span: 3 }}
            wrapperCol={{ span: 18 }}
            rules={[{ ...rules, type: 'email' }]}
          >
            <Input disabled={action === 'edit'} placeholder={'Enter your email'} />
          </Form.Item>
          <Form.Item
            label="Full name"
            name="fullName"
            labelCol={{ span: 3 }}
            wrapperCol={{ span: 18 }}
            rules={[
              { ...rules },
              () => ({
                validator(_, value) {
                  if (REGEX.onlyLettersAndSpaces.test(value)) {
                    return Promise.resolve();
                  }
                  return Promise.reject(new Error('Fullname can only contain letters'));
                },
              }),
            ]}
          >
            <Input disabled={!isUpdate} placeholder={'Enter your full name'} />
          </Form.Item>
          <Form.Item
            label="Role"
            name="roleId"
            labelCol={{ span: 3 }}
            wrapperCol={{ span: 18 }}
            rules={[{ required: true }]}
          >
            <Select placeholder="Select role" disabled={!isUpdate}>
              {roles?.list?.map((item) => (
                <Select.Option key={item.id} value={item.id}>
                  {item.name}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
          <Form.Item
            label="Status"
            name="isActive"
            labelCol={{ span: 3 }}
            wrapperCol={{ span: 18 }}
            rules={[{ required: true }]}
          >
            <Select placeholder="Select status" disabled={!isUpdate}>
              {Object.keys(ADMIN_STATUS_ENUMS).map((key: string) => (
                <Select.Option key={key} value={Number(key)}>
                  {ADMIN_STATUS_ENUMS[Number(key || 0) as 0 | 1]}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
        </Form>
      </>
    </Drawer>
  );
};

export default Editor;
