import { useAdminsQuery } from 'api/admin';
import { PaginatedTableAndSearch } from 'components/PaginatedTableAndSearch';
import { useAppDispatch } from 'hooks/reduxHook';
import { FC, useMemo, useState } from 'react';
import { setEditingId } from 'store/ducks/administrator/slice';
import { debounce } from 'utils/js';
import { Permission } from 'utils/permission';
import { getColumns } from './Columns';
import { IListType } from './type';

const List: FC<IListType> = ({ isUpdate }) => {
  const dispatch = useAppDispatch();
  const [params, setParams] = useState<any>({ sort: [] });
  const debouncedSetParams = useMemo(() => debounce(setParams, 500), []);
  const submitParams = (value: any) => {
    debouncedSetParams((prevParams: any) => ({
      ...prevParams,
      ...value,
      sort: value.sort ? [value.sort] : [],
    }));
  };
  const columns = useMemo(() => getColumns(dispatch, submitParams, isUpdate), [getColumns, isUpdate]);

  return (
    <PaginatedTableAndSearch
      permission={Permission.add_adminstrator}
      title={'Administrator'}
      columns={columns}
      useQuery={useAdminsQuery}
      requestParams={params}
      setEditingId={setEditingId}
      handleReset={() => setParams({ sort: [] })}
    />
  );
};

export default List;
