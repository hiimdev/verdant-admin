import { TableTitle } from 'components/Table/component/TableTitle';
import { timeFormatter } from 'utils/date';

export const getColumns = () => {
  return [
    {
      title: <TableTitle type="none" name="updated_at" label="Updated At" />,
      dataIndex: 'updated_at',
      key: 'updated_at',
      render(updated_at: string) {
        return timeFormatter(Number(updated_at));
      },
      align: 'center',
    },
    {
      title: <TableTitle name="updated_by" label="Updated By" />,
      dataIndex: 'updated_by',
      key: 'updated_by',
      align: 'center',
      render(value: any) {
        return <>{value ? value : '-'}</>;
      },
    },
    {
      title: <TableTitle name="seller_offer_lower" label="Lower Seller Offer" />,
      dataIndex: 'seller_offer_lower',
      key: 'seller_offer_lower',
      align: 'center',
      render(data: any) {
        return <>{data}%</>;
      },
    },
    {
      title: <TableTitle name="seller_offer_upper" label="Upper Seller Offer" />,
      dataIndex: 'seller_offer_upper',
      key: 'seller_offer_upper',
      align: 'center',
      render(data: any) {
        return <>{data}%</>;
      },
    },
    {
      title: <TableTitle name="buyer_bid_lower" label="Lower Buyer Bid" />,
      dataIndex: 'buyer_bid_lower',
      key: 'buyer_bid_lower',
      align: 'center',
      render(data: any) {
        return <>{data}%</>;
      },
    },
    {
      title: <TableTitle name="buyer_bid_upper" label="Upper Buyer Bid" />,
      dataIndex: 'buyer_bid_upper',
      key: 'buyer_bid_upper',
      align: 'center',
      render(data: any) {
        return <>{data}%</>;
      },
    },
    {
      title: <TableTitle name="status" label="Status" />,
      dataIndex: 'status',
      key: 'status',
      render(status: any) {
        return <>{status ? 'Active' : 'Deactive'}</>;
      },
      align: 'center',
    },
  ];
};
