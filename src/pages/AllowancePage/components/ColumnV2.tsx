import { AnyAction, Dispatch } from '@reduxjs/toolkit';
import { Button as AntdButton, Space } from 'antd';
import { TableTitle } from 'components/Table/component/TableTitle';
import { Dispatch as ReactDispatch, SetStateAction } from 'react';
import { AiFillEdit, AiFillEye, AiOutlineHistory } from 'react-icons/ai';
import { setAllowance } from 'store/ducks/system/slice';
import styles from './styles.module.less';

export const getColumns = (
  dispatch: Dispatch<AnyAction>,
  submitParams?: any,
  isUpdate?: boolean,
  setVisible?: ReactDispatch<SetStateAction<any>>
) => {
  return [
    {
      title: <TableTitle name="category_name" label="Category" onSubmit={submitParams} sort={[1, 2]} />,
      dataIndex: 'category_name',
      key: 'category_name',
      align: 'center',
      width: '150px',
    },
    {
      title: <TableTitle name="brand_name" label="Brand" onSubmit={submitParams} sort={[5, 6]} />,
      dataIndex: 'brand_name',
      key: 'brand_name',
      align: 'center',
      width: '150px',
    },
    {
      title: <TableTitle name="collection_name" label="Collection" onSubmit={submitParams} sort={[3, 4]} />,
      dataIndex: 'collection_name',
      key: 'collection_name',
    },
    {
      title: <TableTitle name="vendor_name" label="Vendor" onSubmit={submitParams} sort={[7, 8]} />,
      dataIndex: 'vendor_name',
      key: 'vendor_name',
      render(data: string) {
        return <>{data ?? '-'}</>;
      },
    },
    {
      title: <TableTitle name="seller_offer_lower" label="Lower Seller" onSubmit={submitParams} sort={[9, 10]} />,
      dataIndex: 'seller_offer_lower',
      key: 'seller_offer_lower',
      render(data: string) {
        return <>{`${Number(data)} %`}</>;
      },
      align: 'center',
    },
    {
      title: <TableTitle name="seller_offer_upper" label="Upper Seller" onSubmit={submitParams} sort={[11, 12]} />,
      dataIndex: 'seller_offer_upper',
      key: 'seller_offer_upper',
      render(data: string) {
        return <>{`${Number(data)} %`}</>;
      },
      align: 'center',
    },
    {
      title: <TableTitle name="buyer_bid_lower" label="Lower Buyer" onSubmit={submitParams} sort={[13, 14]} />,
      dataIndex: 'buyer_bid_lower',
      key: 'buyer_bid_lower',
      render(data: string) {
        return <>{`${Number(data)} %`}</>;
      },
      align: 'center',
    },
    {
      title: <TableTitle name="buyer_bid_upper" label="Upper Buyer" onSubmit={submitParams} sort={[15, 16]} />,
      dataIndex: 'buyer_bid_upper',
      key: 'buyer_bid_upper',
      render(data: string) {
        return <>{`${Number(data)} %`}</>;
      },
      align: 'center',
    },
    {
      title: 'Action',
      align: 'center',
      dataIndex: 'id',
      fixed: 'right',
      render(id: string) {
        return (
          <Space>
            <AntdButton
              className={styles.btnAction}
              icon={isUpdate ? <AiFillEdit /> : <AiFillEye />}
              type="primary"
              onClick={() => dispatch(setAllowance(id))}
              title={isUpdate ? 'Edit' : 'View'}
            />
            <AntdButton
              className={styles.btnAction}
              icon={<AiOutlineHistory />}
              onClick={() => setVisible?.(Number(id))}
              title="History"
            />
          </Space>
        );
      },
    },
  ];
};
