import { Col, Form, Row } from 'antd';
import { useGetBrandCommission, useGetCategoryCommission, useGetCollectionCommission } from 'api/commission';
import { useGetPurchaseRuleDetail } from 'api/nft';
import { updatePurchaseRuleDefault } from 'api/nft/request';
import { IUpdatePurchaseRuleRequest } from 'api/nft/types';
import { IError } from 'api/types';
import { useGetListVendorInfinteQueries } from 'api/user';
import { Drawer } from 'components/Drawer';
import { InfiniteSelect } from 'components/InfiniteSelect';
import { InputNumber } from 'components/Input';
import { Select } from 'components/Select';
import { useAppDispatch, useAppSelector } from 'hooks/reduxHook';
import { FC, useEffect, useMemo } from 'react';
import { useMutation, useQueryClient } from 'react-query';
import { getAllowance, setAllowance } from 'store/ducks/system/slice';
import { typeNumber } from 'utils/form';
import { message } from 'utils/message';
import { IListType } from './type';

const Editor: FC<IListType> = ({ isUpdate: canUpdate }) => {
  const dispatch = useAppDispatch();
  const editingId = useAppSelector(getAllowance);
  const client = useQueryClient();

  const isUpdate = useMemo(() => {
    return (canUpdate && editingId !== '') || editingId === '';
  }, []);

  const [form] = Form.useForm();

  const action = editingId ? 'edit' : 'add';

  const { data, status: getLoading } = useGetPurchaseRuleDetail(editingId as string, { enabled: !!editingId });

  const { mutate: update, isLoading: loadingUpdate } = useMutation(updatePurchaseRuleDefault, {
    onSuccess: () => {
      client.invalidateQueries('/nft-admin/list-purchase-rule-default');
      message.success('Successfully!');
      handleClose();
    },
    onError: (error: IError) => {
      message.error(error?.meta.message[0]);
    },
  });

  useEffect(() => {
    form.resetFields();
    if (editingId !== null) {
      form.setFieldsValue({
        categoryCommissionId: Number(data?.category_commission_id) || null,
        collectionId: data?.collection_id
          ? `${data?.collection_id}-${data?.brand_id}-${data?.category_commission_id}`
          : null,
        brandId: data?.brand_id ? `${data?.brand_id}-${data?.category_commission_id}` : null,
        vendorId: Number(data?.vendor_id) || null,
        sellerOfferLower: Number(data?.seller_offer_lower),
        sellerOfferUpper: Number(data?.seller_offer_upper),
        buyerBidLower: Number(data?.buyer_bid_lower),
        buyerBidUpper: Number(data?.buyer_bid_upper),
      });
    }
  }, [data]);

  const handleClose = () => {
    dispatch(setAllowance(null));
  };

  const onFinish = (value: any) => {
    const request: IUpdatePurchaseRuleRequest = {
      ...value,
      action,
      categoryCommissionId: Number(value?.categoryCommissionId),
      collectionId: Number(value?.collectionId?.split('-')[0]),
      brandId: Number(value?.brandId?.split('-')[0]),
      sellerOfferLower: Number(value?.sellerOfferLower.toFixed(3)),
      sellerOfferUpper: Number(value?.sellerOfferUpper.toFixed(3)),
      buyerBidLower: Number(value?.buyerBidLower.toFixed(3)),
      buyerBidUpper: Number(value?.buyerBidUpper.toFixed(3)),
    };
    if (action === 'edit') {
      request.id = Number(editingId);
    }
    if (!request.brandId) delete request.brandId;
    if (!request.categoryCommissionId) delete request.categoryCommissionId;
    if (!request.collectionId) delete request.collectionId;
    if (!request.vendorId) delete request.vendorId;

    update(request);
  };
  const { data: catagory, refetch: refetchCatagory } = useGetCategoryCommission({ page: 1, limit: 50 });
  const { data: listBrand, refetch: refetchBrand } = useGetBrandCommission(
    form.getFieldValue('categoryCommissionId')
      ? {
          page: 1,
          limit: 50,
          categoryId: form.getFieldValue('categoryCommissionId'),
        }
      : { page: 1, limit: 50 }
  );
  const { data: listCollection, refetch: refetchCollection } = useGetCollectionCommission(
    form.getFieldValue('categoryCommissionId') || form.getFieldValue('brandId')
      ? {
          page: 1,
          limit: 50,
          categoryId: form.getFieldValue('categoryCommissionId'),
          brandId: form.getFieldValue('brandId') ? form.getFieldValue('brandId').split('-')[0] : undefined,
        }
      : { page: 1, limit: 50 }
  );

  const {
    data: vendorResponse,
    fetchNextPage: fetchMoreVendor,
    hasNextPage: hasNextVendor,
    isFetchingNextPage: isFetchingMoreVendor,
    isLoading: isVendorLoading,
  } = useGetListVendorInfinteQueries({ limit: 20 });

  const vendors = vendorResponse?.pages
    ?.map((d) => d.list)
    .reduce((current, arr) => {
      return current.concat(arr);
    }, []);

  return (
    <Drawer
      name="Allowance"
      editingId={editingId}
      handleClose={handleClose}
      form={form}
      getLoading={getLoading === 'loading'}
      submitLoading={loadingUpdate}
      viewOnly={false}
    >
      <Form form={form} onFinish={onFinish}>
        <Form.Item label="Category" name="categoryCommissionId" labelCol={{ span: 3 }} wrapperCol={{ span: 18 }}>
          <Select
            placeholder="Select category"
            onChange={() => {
              form.setFieldsValue({ brandId: null, collectionId: null });
            }}
            onClick={() => {
              refetchCatagory();
            }}
          >
            {catagory?.list?.map((item) => (
              <Select.Option key={item.categoryId} value={item.categoryId}>
                {item.categoryName}
              </Select.Option>
            ))}
          </Select>
        </Form.Item>

        <Form.Item label="Brand" name="brandId" labelCol={{ span: 3 }} wrapperCol={{ span: 18 }}>
          <Select
            placeholder="Select brand"
            onChange={(val: string) => {
              form.setFieldsValue({ categoryCommissionId: Number(val.split('-')[1]), collectionId: null });
            }}
            onClick={() => {
              refetchBrand();
            }}
          >
            {listBrand?.list?.map((item) => (
              <Select.Option key={item.brandId} value={`${item.brandId}-${item.categoryId}`}>
                {item.brandName}
              </Select.Option>
            ))}
          </Select>
        </Form.Item>

        <Form.Item label="Collection" name="collectionId" labelCol={{ span: 3 }} wrapperCol={{ span: 18 }}>
          <Select
            placeholder="Select collection"
            onChange={(val: string) => {
              const split_arr = val.split('-');
              form.setFieldsValue({
                brandId: Number(split_arr[1]) ? `${split_arr[1]}-${split_arr[2]}` : null,
                categoryCommissionId: Number(split_arr[2]) ? Number(split_arr[2]) : null,
              });
            }}
            onClick={() => {
              refetchCollection();
            }}
          >
            {listCollection?.list?.map((item) => (
              <Select.Option key={item.collectionId} value={`${item.collectionId}-${item.brandId}-${item.categoryId}`}>
                {item.collectionName}
              </Select.Option>
            ))}
          </Select>
        </Form.Item>
        <Form.Item label="Vendor" name="vendorId" labelCol={{ span: 3 }} wrapperCol={{ span: 18 }}>
          <InfiniteSelect
            hasMore={hasNextVendor as boolean}
            loading={isFetchingMoreVendor || isVendorLoading}
            loadMore={fetchMoreVendor}
            placeholder="Select vendor"
          >
            {vendors?.map((item) => (
              <InfiniteSelect.Option key={item.id} value={item.id}>
                {item.vendorName}
              </InfiniteSelect.Option>
            ))}
          </InfiniteSelect>
        </Form.Item>
        <Row gutter={[8, 8]}>
          <Col span={3} style={{ textAlign: 'right' }}></Col>
          <Col span={18}>
            <Row gutter={[24, 24]} align="middle">
              <Col span={12}>Lower</Col>
              <Col span={12}>Upper</Col>
            </Row>
          </Col>
        </Row>
        <Row gutter={[8, 8]}>
          <Col span={3} style={{ textAlign: 'right' }}>
            Seller:
          </Col>
          <Col span={18}>
            <Row gutter={[24, 24]} align="middle">
              <Col span={12}>
                <Form.Item name="sellerOfferLower" style={{ marginBottom: 0 }} rules={[{ required: true }]}>
                  <InputNumber
                    placeholder={'Enter lower value'}
                    addonAfter="%"
                    min={0}
                    onKeyDown={(evt) => {
                      typeNumber(evt);
                    }}
                    type="number"
                  />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item name="sellerOfferUpper" style={{ marginBottom: 0 }} rules={[{ required: true }]}>
                  <InputNumber
                    placeholder={'Enter upper value'}
                    addonAfter="%"
                    min={0}
                    onKeyDown={(evt) => {
                      typeNumber(evt);
                    }}
                    type="number"
                  />
                </Form.Item>
              </Col>
            </Row>
          </Col>
        </Row>
        <Row gutter={[8, 8]} style={{ marginTop: '24px' }}>
          <Col span={3} style={{ textAlign: 'right' }}>
            Buyer:
          </Col>
          <Col span={18}>
            <Row gutter={[24, 24]} align="middle">
              <Col span={12}>
                <Form.Item name="buyerBidLower" style={{ marginBottom: 0 }} rules={[{ required: true }]}>
                  <InputNumber
                    placeholder={'Enter lower value'}
                    addonAfter="%"
                    min={0}
                    onKeyDown={(evt) => {
                      typeNumber(evt);
                    }}
                    type="number"
                  />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item name="buyerBidUpper" style={{ marginBottom: 0 }} rules={[{ required: true }]}>
                  <InputNumber
                    placeholder={'Enter upper value'}
                    addonAfter="%"
                    min={0}
                    onKeyDown={(evt) => {
                      typeNumber(evt);
                    }}
                    type="number"
                  />
                </Form.Item>
              </Col>
            </Row>
          </Col>
        </Row>
      </Form>
    </Drawer>
  );
};

export default Editor;
