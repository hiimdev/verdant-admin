import { useListPurchaseRuleDefault } from 'api/nft';
import { PaginatedTableAndSearch } from 'components/PaginatedTableAndSearch';
import { useAppDispatch } from 'hooks/reduxHook';
import { Dispatch, FC, SetStateAction, useMemo, useState } from 'react';
import { setAllowance } from 'store/ducks/system/slice';
import { debounce } from 'utils/js';
import { Permission } from 'utils/permission';
import { getColumns } from './ColumnV2';
import { IListType } from './type';

const List: FC<IListType & { setVisible: Dispatch<SetStateAction<any>> }> = ({ isUpdate, setVisible }) => {
  const dispatch = useAppDispatch();
  const [params, setParams] = useState({ sort: [] });
  const debouncedSetParams = useMemo(() => debounce(setParams, 500), []);
  console.log(params);

  const submitParams = (value: any) => {
    debouncedSetParams((prevParams: any) => ({
      ...prevParams,
      ...value,
      sort: value.sort ? `[${value.sort}]` : [],
    }));
  };
  const columns = useMemo(() => getColumns(dispatch, submitParams, isUpdate, setVisible), [getColumns, isUpdate]);

  return (
    <>
      <PaginatedTableAndSearch
        permission={Permission.scmt_setting_verify}
        title="Setting Allowance"
        setEditingId={setAllowance}
        columns={columns}
        useQuery={useListPurchaseRuleDefault}
        requestParams={params}
        handleReset={() => setParams({ sort: [] })}
      />
    </>
  );
};

export default List;
