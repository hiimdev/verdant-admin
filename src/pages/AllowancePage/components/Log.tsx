import { Modal } from 'antd';
import { useGetHistoryPurchase } from 'api/nft';
import { PaginatedTableAndSearch } from 'components/PaginatedTableAndSearch';
import { FC, useMemo } from 'react';
import { getColumns } from './Column';

const Log: FC<{ visible: number; handleClose: () => void }> = ({ visible = false, handleClose }) => {
  const columns = useMemo(() => getColumns(), [getColumns]);

  return (
    <Modal
      width={1150}
      visible={visible > 0}
      title={``}
      onOk={handleClose}
      onCancel={handleClose}
      cancelButtonProps={{ hidden: true }}
    >
      <PaginatedTableAndSearch
        title="History"
        columns={columns}
        useQuery={useGetHistoryPurchase}
        requestParams={{ id: visible }}
      />
    </Modal>
  );
};

export default Log;
