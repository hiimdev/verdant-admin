import { useAppSelector } from 'hooks/reduxHook';
import { useState } from 'react';
import { getAllowance } from 'store/ducks/system/slice';
import Editor from './components/Editor';
import List from './components/List';
import Log from './components/Log';

function Allowance() {
  const [visible, setVisible] = useState(0);
  const allowance = useAppSelector(getAllowance);

  return (
    <>
      <List isUpdate={true} setVisible={setVisible} />
      {allowance !== null && <Editor />}
      {visible > 0 && <Log visible={visible} handleClose={() => setVisible(0)} />}
    </>
  );
}

export default Allowance;
