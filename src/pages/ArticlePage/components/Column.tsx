import { AnyAction, Dispatch } from '@reduxjs/toolkit';
import { Button, Image, Space, Tooltip } from 'antd';
import { TableTitle } from 'components/Table/component/TableTitle';
import { AiFillEdit, AiFillEye, AiOutlineDelete } from 'react-icons/ai';
import { setDeletingId, setEditingId } from 'store/ducks/article/slice';
import { ARTICLE_SORT, ARTICLE_STATUS_ENUMS } from 'utils/constant';
import { timeFormatter } from 'utils/date';
import styles from './styles.module.less';

export const getColumns = (dispatch: Dispatch<AnyAction>, submitParams?: any, isUpdate?: boolean) => {
  return [
    {
      title: 'No',
      dataIndex: 'index',
      key: 'index',
      width: '80px',
      align: 'center',
    },
    {
      title: (
        <TableTitle
          name="title"
          label="Title"
          onSubmit={submitParams}
          sort={[ARTICLE_SORT.TITLE_DESC, ARTICLE_SORT.TITLE_ASC]}
        />
      ),
      dataIndex: 'title',
      key: 'title',
      width: '150px',
      render(data: string) {
        return (
          <Tooltip placement="top" title={data}>
            {data.slice(0, 15)}...
          </Tooltip>
        );
      },
    },
    {
      title: (
        <TableTitle
          type="select"
          name="status"
          label="Status"
          onSubmit={(values: any) =>
            submitParams({ ...values, status: values.status ? Number(values.status) : undefined })
          }
          sort={[]}
          obj={{ 1: 'Public', 2: 'Inpublic' }}
        />
      ),
      dataIndex: 'status',
      key: 'status',
      render(status: 0 | 1) {
        return ARTICLE_STATUS_ENUMS[status];
      },
    },
    {
      title: (
        <TableTitle
          type="none"
          name="publishDate"
          label="Publish Date"
          onSubmit={submitParams}
          sort={[ARTICLE_SORT.PUBLISH_DATE_DESC, ARTICLE_SORT.PUBLISH_DATE_ASC]}
        />
      ),
      dataIndex: 'publishDate',
      key: 'publishDate',
      render(value: string) {
        return <>{value ? timeFormatter(Number(value)) : '-'}</>;
      },
    },
    {
      title: 'Image',
      align: 'center',
      render({ image, title }: any) {
        return <Image className={styles.image} src={image} alt={title} />;
      },
    },
    {
      title: (
        <TableTitle
          name="categoryName"
          label="Catagory"
          onSubmit={submitParams}
          sort={[ARTICLE_SORT.CATEGORYNAME_DESC, ARTICLE_SORT.CATEGORYNAME_ASC]}
        />
      ),
      dataIndex: 'categoryName',
      key: 'categoryName',
    },
    {
      title: (
        <TableTitle
          name="createdBy"
          label="Created By"
          onSubmit={submitParams}
          sort={[ARTICLE_SORT.CREATED_BY_DESC, ARTICLE_SORT.CREATED_BY_ASC]}
        />
      ),
      dataIndex: 'createdBy',
      key: 'createdBy',
      align: 'center',
    },
    {
      title: (
        <TableTitle
          type="none"
          name="createdAt"
          label="Create At"
          onSubmit={submitParams}
          sort={[ARTICLE_SORT.CREATEAT_DESC, ARTICLE_SORT.CREATEAT_ASC]}
        />
      ),
      dataIndex: 'createdAt',
      key: 'createdAt',
      render(value: string) {
        return timeFormatter(Number(value));
      },
    },
    {
      title: (
        <TableTitle
          name="updatedBy"
          label="Update By"
          onSubmit={submitParams}
          sort={[ARTICLE_SORT.UPDATED_BY_DESC, ARTICLE_SORT.UPDATED_BY_ASC]}
        />
      ),
      dataIndex: 'updatedBy',
      key: 'updatedBy',
      align: 'center',
    },
    {
      title: (
        <TableTitle
          type="none"
          name="updatedAt"
          label="Update at"
          onSubmit={submitParams}
          sort={[ARTICLE_SORT.UPDATEAT_DESC, ARTICLE_SORT.UPDATEAT_ASC]}
        />
      ),
      dataIndex: 'updatedAt',
      key: 'updatedAt',
      render(value: string) {
        return timeFormatter(Number(value));
      },
    },
    {
      title: (
        <TableTitle
          name="author"
          label="Author"
          onSubmit={submitParams}
          sort={[ARTICLE_SORT.AUTHOR_DESC, ARTICLE_SORT.AUTHOR_ASC]}
        />
      ),
      dataIndex: 'author',
      key: 'author',
      align: 'center',
      render(author: string) {
        return <>{author ? author : '-'}</>;
      },
    },
    {
      title: 'Action',
      align: 'right',
      fixed: 'right',
      dataIndex: 'id',
      render(id: string) {
        return (
          <Space>
            <Button
              className={styles.btnAction}
              icon={isUpdate ? <AiFillEdit /> : <AiFillEye />}
              type="primary"
              onClick={() => dispatch(setEditingId(id))}
              title={isUpdate ? 'Edit' : 'View'}
            />
            {isUpdate && (
              <Button
                className={styles.btnAction}
                icon={<AiOutlineDelete />}
                danger
                onClick={() => dispatch(setDeletingId(id))}
                title="Delete"
              />
            )}
          </Space>
        );
      },
    },
  ];
};
