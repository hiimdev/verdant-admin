import { Modal } from 'antd';
import { useArticleQuery } from 'api/article';
import { deleteArticle } from 'api/article/request';
import { IError } from 'api/types';
import { useAppDispatch, useAppSelector } from 'hooks/reduxHook';
import { FC } from 'react';
import { useMutation, useQueryClient } from 'react-query';
import { getDeletingId, setDeletingId } from 'store/ducks/article/slice';
import { message } from 'utils/message';
const Deletor: FC = () => {
  const dispatch = useAppDispatch();
  const deletingId = useAppSelector(getDeletingId);
  const client = useQueryClient();

  const { data, status: getLoading } = useArticleQuery(deletingId as string, { enabled: !!deletingId });

  const handleClose = () => {
    dispatch(setDeletingId(null));
  };

  const { mutate: mutateDelete, isLoading: deleteLoading } = useMutation(deleteArticle, {
    onSuccess: () => {
      client.invalidateQueries('/articles/paging_filter');
      message.success('Delete successfully!');
      handleClose();
    },
    onError: (error: IError) => {
      message.error(error?.meta.message[0]);
    },
  });

  const onDelete = () => {
    mutateDelete(deletingId as string);
  };

  return (
    <>
      <Modal
        visible={!!deletingId}
        title={`Delete article: ${data?.title}`}
        okButtonProps={{
          loading: getLoading === 'loading' || deleteLoading,
        }}
        cancelButtonProps={{
          disabled: deleteLoading,
        }}
        onCancel={handleClose}
        onOk={onDelete}
        okType="danger"
        okText="Confirm"
      >
        Are you sure you want to delete this article?
      </Modal>
    </>
  );
};

export default Deletor;
