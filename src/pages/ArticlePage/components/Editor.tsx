import { DatePicker, Form } from 'antd';
import { useArticleQuery } from 'api/article';
import { createArticle, updateArticles, uploadImage } from 'api/article/request';
import { useGetArticlesListCategoryArticles } from 'api/articlesCategory';
import { ICategoryArticles } from 'api/articlesCategory/types';
import { IError } from 'api/types';
// import { message } from 'utils/message';
import { Drawer } from 'components/Drawer';
import { Input } from 'components/Input';
import { Select } from 'components/Select';
import { Upload } from 'components/Upload';
import { useAppDispatch, useAppSelector } from 'hooks/reduxHook';
import moment from 'moment';
import ImageResize from 'quill-image-resize-module-react';
import { FC, useEffect, useMemo, useState } from 'react';
import { AiOutlinePlus } from 'react-icons/ai';
import { useMutation, useQueryClient } from 'react-query';
import ReactQuill, { Quill } from 'react-quill';
import quillEmoji from 'react-quill-emoji';
import { getEditingId, setEditingId } from 'store/ducks/article/slice';
import { ARTICLE_STATUS_ENUMS } from 'utils/constant';
import { convertFormValue, getUploadValueProps, rules } from 'utils/form';
import { message } from 'utils/message';
import { REGEX } from 'utils/regex';
import { IListType } from './type';

var quill: any;
var quill2: any;
Quill.register('modules/imageResize', ImageResize);
Quill.register(
  {
    'formats/emoji': quillEmoji.EmojiBlot,
    'modules/emoji-toolbar': quillEmoji.ToolbarEmoji,
    'modules/emoji-textarea': quillEmoji.TextAreaEmoji,
    'modules/emoji-shortname': quillEmoji.ShortNameEmoji,
  },
  true
);

const Editor: FC<IListType> = ({ isUpdate: canUpdate }) => {
  const dispatch = useAppDispatch();
  const editingId = useAppSelector(getEditingId);
  const client = useQueryClient();
  const [value, setValue] = useState('');
  const [valueSummary, setValueSummary] = useState('');

  const { mutate: upload } = useMutation(uploadImage, {
    onSuccess: (data) => {
      const range = quill.getEditorSelection();
      if (range) quill.getEditor().insertEmbed(range.index, 'image', data);
      const range2 = quill2.getEditorSelection();
      if (range2) quill2.getEditor().insertEmbed(range2.index, 'image', data);
    },
    onError: (error: IError) => {
      message.error(error?.meta.message[0]);
    },
  });

  const getImageHandle = () => {
    const input = document.createElement('input');

    input.setAttribute('type', 'file');
    input.setAttribute('accept', 'image/*');
    input.click();

    input.onchange = () => {
      const formData = new FormData();
      var file: any = input.files ? input.files[0] : null;
      formData.append('image', file);
      upload(formData);
    };
  };

  const [form] = Form.useForm();

  const action = editingId ? 'edit' : 'add';

  const isUpdate = useMemo(() => {
    return (canUpdate && editingId !== '') || editingId === '';
  }, []);

  const { data, status: getLoading } = useArticleQuery(editingId as string, { enabled: !!editingId });

  const { data: categories } = useGetArticlesListCategoryArticles({ page: 1, limit: 50 });

  const { mutate: update, isLoading: loadingUpdate } = useMutation(updateArticles, {
    onSuccess: () => {
      client.invalidateQueries('/articles/paging_filter');
      message.success('Update successfully!');
      handleClose();
    },
    onError: (error: IError) => {
      message.error(error?.meta.message[0]);
    },
  });

  const modules = useMemo(() => {
    return {
      toolbar: {
        container: [
          [{ header: [1, 2, 3, 4, 5, false] }],
          ['bold', 'italic', 'underline', 'strike', 'blockquote'],
          [{ list: 'ordered' }, { list: 'bullet' }],
          [{ color: [] }, { background: [] }],
          ['link', 'image', 'video'],
          ['emoji'],
          ['code-block'],
          ['clean'],
          [{ align: '' }, { align: 'center' }, { align: 'right' }, { align: 'justify' }],
        ],
        handlers: {
          image: getImageHandle,
        },
      },

      clipboard: {
        // toggle to add extra line breaks when pasting HTML:
        matchVisual: false,
      },
      imageResize: {
        parchment: Quill.import('parchment'),
        modules: ['Resize', 'DisplaySize'],
      },
      'emoji-toolbar': true,
      'emoji-textarea': true,
      'emoji-shortname': true,
    };
  }, []);

  const { mutate: create, isLoading: loadingCreate } = useMutation(createArticle, {
    onSuccess: () => {
      client.invalidateQueries('/articles/paging_filter');
      message.success('Create successfully!');
      handleClose();
    },
    onError: (error: IError) => {
      message.error(error?.meta.message[0]);
    },
  });

  useEffect(() => {
    form.resetFields();
    if (editingId !== null) {
      form.setFieldsValue({
        ...data,
        publishDate: data?.publishDate ? moment(new Date(Number(data?.publishDate) || '')) : undefined,
        image: data?.image,
      });
    }
  }, [data]);

  const handleClose = () => {
    dispatch(setEditingId(null));
  };

  const onFinish = (value: any) => {
    const mutate = action === 'edit' ? update : create;
    let params = action === 'edit' ? { id: editingId, ...value } : value;
    if (value.publishDate) {
      params.publishDate = new Date(value.publishDate).getTime();
    } else {
      delete params.publishDate;
    }
    if (typeof value.image === 'string') {
      delete params.image;
    }
    mutate(convertFormValue(params));
  };

  const handleChange = (file: File, field: string) => {
    form.setFieldsValue({ [field]: file });
  };
  function isQuillEmpty(value: string) {
    if (value.replace(REGEX.space, '').trim().length === 0 && !value.includes('<img') && !value.includes('<iframe')) {
      return true;
    }
    return false;
  }

  return (
    <Drawer
      name="Article"
      editingId={editingId}
      handleClose={handleClose}
      form={form}
      getLoading={getLoading === 'loading'}
      submitLoading={loadingCreate || loadingUpdate}
      viewOnly={!isUpdate}
    >
      <Form form={form} onFinish={onFinish}>
        <Form.Item label="Title" name="title" labelCol={{ span: 3 }} wrapperCol={{ span: 18 }} rules={[rules]}>
          <Input disabled={!isUpdate} placeholder={'Enter your title'} />
        </Form.Item>
        <Form.Item
          label="Path Segment"
          name="pathSegment"
          labelCol={{ span: 3 }}
          wrapperCol={{ span: 18 }}
          rules={[rules]}
        >
          <Input disabled={!isUpdate} placeholder={'Enter your path segment'} />
        </Form.Item>

        <Form.Item labelCol={{ span: 3 }} wrapperCol={{ span: 18 }} name="author" label="Author">
          <Input maxLength={200} placeholder={'Enter author'} />
        </Form.Item>

        <Form.Item label="Summary" name="summary" labelCol={{ span: 3 }} wrapperCol={{ span: 18 }}>
          <ReactQuill
            ref={(el) => {
              quill2 = el;
            }}
            theme="snow"
            value={valueSummary}
            onChange={setValueSummary}
            modules={modules}
          />
        </Form.Item>
        <Form.Item
          label="Content"
          name="content"
          labelCol={{ span: 3 }}
          wrapperCol={{ span: 18 }}
          rules={[
            () => ({
              required: true,
              validator(_, value = '') {
                if (!isQuillEmpty(value)) {
                  return Promise.resolve();
                }
                return Promise.reject(new Error('Content is required!'));
              },
            }),
          ]}
        >
          <ReactQuill
            ref={(el) => {
              quill = el;
            }}
            theme="snow"
            value={value}
            onChange={setValue}
            modules={modules}
          />
        </Form.Item>

        <Form.Item
          name="image"
          label="Image Cover"
          labelCol={{ span: 3 }}
          wrapperCol={{ span: 18 }}
          getValueProps={getUploadValueProps}
          rules={[{ required: true }]}
        >
          <Upload
            disabled={!isUpdate}
            listType="picture-card"
            onChangeFile={(file: File) => handleChange(file, 'image')}
            imageUrl={data?.image}
            accept=".jpg, .jpeg, .png"
          >
            <AiOutlinePlus />
          </Upload>
        </Form.Item>
        <Form.Item
          label="Status"
          name="status"
          labelCol={{ span: 3 }}
          wrapperCol={{ span: 18 }}
          rules={[{ required: true }]}
        >
          <Select disabled={!isUpdate} placeholder="Select status">
            {Object.keys(ARTICLE_STATUS_ENUMS).map((key: string) => (
              <Select.Option key={key} value={Number(key)}>
                {ARTICLE_STATUS_ENUMS[Number(key || 0) as 0 | 1]}
              </Select.Option>
            ))}
          </Select>
        </Form.Item>
        <Form.Item label="Publish date" name="publishDate" labelCol={{ span: 3 }} wrapperCol={{ span: 18 }}>
          <DatePicker disabled={!isUpdate} format={(value) => value?.format('YYYY/MM/DD')} />
        </Form.Item>
        <Form.Item labelCol={{ span: 3 }} wrapperCol={{ span: 18 }} name="categoryArticleId" label="Category">
          <Select disabled={!isUpdate} placeholder="Select category" style={{ width: '100%' }}>
            {categories &&
              categories?.list?.map((item: ICategoryArticles, index) => (
                <Select.Option key={index} value={item.id}>
                  {item.categoryName}
                </Select.Option>
              ))}
          </Select>
        </Form.Item>
      </Form>
    </Drawer>
  );
};

export default Editor;
