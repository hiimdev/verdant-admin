import { useAppSelector } from 'hooks/reduxHook';
import { FC, useMemo } from 'react';
import { getPermission } from 'store/ducks/user/slice';
import Deletor from './components/Deletor';
import Editor from './components/Editor';
import List from './components/List';
import { getEditingId, getDeletingId } from 'store/ducks/article/slice';
import { Permission } from 'utils/permission';

const Article: FC = () => {
  const permissions = useAppSelector(getPermission);
  const editingId = useAppSelector(getEditingId);
  const deletingId = useAppSelector(getDeletingId);
  const isUpdate = useMemo(() => {
    return permissions.includes(Permission.edit_delete_article);
  }, [permissions]);
  console.log(deletingId, 'asaass', isUpdate);
  return (
    <>
      <List isUpdate={isUpdate} />
      {editingId !== null && <Editor isUpdate={isUpdate} />}
      {deletingId && isUpdate && <Deletor />}
    </>
  );
};

export default Article;
