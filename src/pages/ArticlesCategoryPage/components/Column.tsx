import { AnyAction } from '@reduxjs/toolkit';
import { Button as AntdButton, Space } from 'antd';
import { Dispatch } from 'react';
import { AiFillEdit, AiOutlineDelete } from 'react-icons/ai';
import { setDeletingId, setEditingId } from 'store/ducks/articles_category/slice';
import { timeFormatter } from 'utils/date';

export const getColumns = (dispatch: Dispatch<AnyAction>, isUpdate?: boolean, submitParams?: any) => {
  return [
    {
      title: 'No',
      key: 'index',
      dataIndex: 'index',
      align: 'center',
      width: '40px',
    },
    {
      title: 'Category Name',
      key: 'categoryName',
      dataIndex: 'categoryName',
      align: 'center',
      width: '80px',
    },
    {
      title: 'Category Description',
      key: 'categoryDescription',
      dataIndex: 'categoryDescription',
      align: 'center',
      width: '80px',
      render(data: any) {
        return <>{data ? data : '-'}</>;
      },
    },
    {
      title: 'Create At',
      key: 'createdAt',
      dataIndex: 'createdAt',
      align: 'center',
      width: '80px',
      render(createdAt: string) {
        return timeFormatter(Number(createdAt));
      },
    },
    {
      title: 'Create By',
      key: 'createdBy',
      dataIndex: 'createdBy',
      align: 'center',
      width: '80px',
      render(data: string) {
        return <>{data || '-'}</>;
      },
    },
    {
      title: 'Update At',
      key: 'updatedAt',
      dataIndex: 'updatedAt',
      align: 'center',
      width: '80px',
      render(updateAt: string) {
        return timeFormatter(Number(updateAt));
      },
    },
    {
      title: 'Update By',
      key: 'updatedBy',
      dataIndex: 'updatedBy',
      align: 'center',
      width: '80px',
      render(data: string) {
        return <>{data || '-'}</>;
      },
    },
    {
      title: 'Action',
      align: 'center',
      width: '80px',
      dataIndex: 'id',
      render(id: string) {
        return (
          <Space>
            {isUpdate && (
              <AntdButton
                icon={<AiFillEdit />}
                type="primary"
                onClick={() => dispatch(setEditingId(id))}
                title="Edit"
              />
            )}
            {isUpdate && (
              <AntdButton
                icon={<AiOutlineDelete />}
                danger
                onClick={() => dispatch(setDeletingId(id))}
                title="Delete"
              />
            )}
          </Space>
        );
      },
    },
  ];
};
