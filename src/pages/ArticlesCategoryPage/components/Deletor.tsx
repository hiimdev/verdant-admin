import { Modal } from 'antd';
import { useArticlesCategoryQuery } from 'api/articlesCategory';
import { deleteArticlesCategory } from 'api/articlesCategory/request';
import { IError } from 'api/types';
import { useAppDispatch, useAppSelector } from 'hooks/reduxHook';
import { FC } from 'react';
import { useMutation, useQueryClient } from 'react-query';
import { getDeletingId, setDeletingId } from 'store/ducks/articles_category/slice';
import { message } from 'utils/message';
const Deletor: FC = () => {
  const dispatch = useAppDispatch();
  const deletingId = useAppSelector(getDeletingId);
  const client = useQueryClient();

  const { data } = useArticlesCategoryQuery(deletingId as string, { enabled: !!deletingId });

  const handleClose = () => {
    dispatch(setDeletingId(null));
  };

  const { mutate: mutateDelete, isLoading: deleteLoading } = useMutation(deleteArticlesCategory, {
    onSuccess: () => {
      client.invalidateQueries('/articles/list-category-article');
      message.success('Delete successfully!');
      handleClose();
    },
    onError: (error: IError) => {
      message.error(error?.meta.message[0]);
    },
  });

  const onDelete = () => {
    mutateDelete(deletingId as string);
  };

  return (
    <>
      <Modal
        visible={!!deletingId}
        title={`Articles Category: ${data?.categoryName}`}
        okButtonProps={{
          loading: deleteLoading,
        }}
        cancelButtonProps={{
          disabled: deleteLoading,
        }}
        onCancel={handleClose}
        onOk={onDelete}
        okType="danger"
        okText="Confirm"
      >
        Are you sure you want to delete this Articles Category?
      </Modal>
    </>
  );
};

export default Deletor;
