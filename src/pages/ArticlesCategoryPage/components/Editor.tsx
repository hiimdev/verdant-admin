import { Form, Input } from 'antd';
import { FC, useEffect } from 'react';
import { Drawer } from 'components/Drawer';
import { useAppDispatch, useAppSelector } from 'hooks/reduxHook';
import { getEditingId, setEditingId } from 'store/ducks/articles_category/slice';
import { useMutation, useQueryClient } from 'react-query';
import { message } from 'utils/message';
import { IError } from 'api/types';
import { createArticlesCategory, updateArticlesCategory } from 'api/articlesCategory/request';
import { rules } from 'utils/form';
import { useArticlesCategoryQuery } from 'api/articlesCategory';

const Editor: FC = () => {
  const dispatch = useAppDispatch();
  const editingId = useAppSelector(getEditingId);
  const [form] = Form.useForm();
  const handleClose = () => {
    dispatch(setEditingId(null));
  };
  const action = editingId ? 'edit' : 'add';
  const client = useQueryClient();

  const { mutate: update, isLoading: loadingUpdate } = useMutation(updateArticlesCategory, {
    onSuccess: () => {
      client.invalidateQueries('/articles/list-category-article');
      message.success('Update successfully!');
      handleClose();
    },
    onError: (error: IError) => {
      message.error(error?.meta.message[0]);
    },
  });

  const { mutate: create, isLoading: loadingCreate } = useMutation(createArticlesCategory, {
    onSuccess: () => {
      client.invalidateQueries('/articles/list-category-article');
      message.success('Create successfully!');
      handleClose();
    },
    onError: (error: IError) => {
      message.error(error?.meta.message[0]);
    },
  });
  const onFinish = (value) => {
    const mutate = action === 'edit' ? update : create;
    const params = action === 'edit' ? { id: editingId, ...value } : value;
    mutate(params);
  };
  const { data } = useArticlesCategoryQuery(editingId as string, { enabled: !!editingId });
  useEffect(() => {
    form.resetFields();
    if (editingId !== null) {
      form.setFieldsValue(data);
    }
  }, [data]);
  return (
    <>
      <Drawer
        name="Articles Category"
        editingId={editingId}
        handleClose={handleClose}
        form={form}
        submitLoading={loadingCreate || loadingUpdate}
      >
        <Form form={form} onFinish={onFinish}>
          <Form.Item
            label="Category name"
            name="categoryName"
            labelCol={{ span: 3 }}
            wrapperCol={{ span: 18 }}
            rules={[rules]}
          >
            <Input type="string" placeholder={'Enter your category name'} />
          </Form.Item>
          <Form.Item
            label="Category Description"
            name="categoryDescription"
            labelCol={{ span: 3 }}
            wrapperCol={{ span: 18 }}
          >
            <Input type="string" placeholder={'Enter your category description'} />
          </Form.Item>
        </Form>
      </Drawer>
    </>
  );
};
export default Editor;
