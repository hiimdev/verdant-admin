import { useGetArticlesListCategoryArticles } from 'api/articlesCategory';
import { PaginatedTableAndSearch } from 'components/PaginatedTableAndSearch';
import { useAppDispatch } from 'hooks/reduxHook';
import { FC, useMemo } from 'react';
import { setEditingId } from 'store/ducks/articles_category/slice';
import { Permission } from 'utils/permission';
import { getColumns } from './Column';
import { IListType } from './type';

const List: FC<IListType> = ({ isUpdate }) => {
  const dispatch = useAppDispatch();
  const columns = useMemo(() => getColumns(dispatch, isUpdate), [getColumns, isUpdate]);
  return (
    <>
      <PaginatedTableAndSearch
        permission={Permission.add_category_articles}
        title="Articles Category"
        setEditingId={setEditingId}
        columns={columns}
        useQuery={useGetArticlesListCategoryArticles}
        // requestParams={params}
      />
    </>
  );
};
export default List;
