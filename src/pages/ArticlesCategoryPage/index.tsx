import { useAppSelector } from 'hooks/reduxHook';
import { FC, useMemo } from 'react';
import { getDeletingId, getEditingId, getLogId } from 'store/ducks/articles_category/slice';
import { getPermission } from 'store/ducks/user/slice';
import { Permission } from 'utils/permission';
import Deletor from './components/Deletor';
import Editor from './components/Editor';
import List from './components/List';

const ArticlesCategoryPage: FC = () => {
  const editingId = useAppSelector(getEditingId);
  const deletingId = useAppSelector(getDeletingId);
  const permissions = useAppSelector(getPermission);
  const isUpdate = useMemo(() => {
    return permissions.includes(Permission.edit_delete_category_articles);
  }, [permissions]);
  return (
    <>
      <List isUpdate={isUpdate} />
      {editingId !== null && <Editor></Editor>}
      {deletingId && <Deletor />}
    </>
  );
};

export default ArticlesCategoryPage;
