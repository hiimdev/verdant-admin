import { AnyAction, Dispatch } from '@reduxjs/toolkit';
import { Button as AntdButton, Image, Space, Tooltip } from 'antd';
import { TableTitle } from 'components/Table/component/TableTitle';
import { AiFillEdit, AiFillEye } from 'react-icons/ai';
import { setEditingId } from 'store/ducks/banner/slice';
import { BANNER_SORT } from 'utils/constant';
import { timeFormatter } from 'utils/date';
import styles from './styles.module.less';

export const getColumns = (dispatch: Dispatch<AnyAction>, submitParams?: any, isUpdate?: boolean) => {
  return [
    {
      title: 'ID',
      dataIndex: 'id',
      key: 'id',
      align: 'center',
      width: '80px',
    },
    {
      title: (
        <TableTitle
          name="title"
          label="Title"
          onSubmit={submitParams}
          sort={[BANNER_SORT.TITLE_DESC, BANNER_SORT.TITLE_ASC]}
        />
      ),
      dataIndex: 'title',
      key: 'title',
      render(data: string) {
        return (
          <>
            <Tooltip placement="top" title={{ data }}></Tooltip>
            {data.slice(0, 15)}...
          </>
        );
      },
    },
    {
      title: 'Image',
      align: 'center',
      render({ image, title }: any) {
        return <Image className={styles.image} src={image} alt={title} />;
      },
    },
    {
      title: (
        <TableTitle
          name="description"
          label="Description"
          onSubmit={submitParams}
          sort={[BANNER_SORT.DESCRIPTION_DESC, BANNER_SORT.DESCRIPTION_ASC]}
        />
      ),
      dataIndex: 'description',
      key: 'description',
      align: 'center',
      render(data: any) {
        return <div className={styles.description} dangerouslySetInnerHTML={{ __html: data }} />;
      },
      width: '200px',
    },
    {
      title: (
        <TableTitle
          name="textButton"
          label="Text button"
          onSubmit={submitParams}
          sort={[BANNER_SORT.TEXTBUTTON_DESC, BANNER_SORT.TEXTBUTTON_ASC]}
        />
      ),
      dataIndex: 'textButton',
      key: 'textButton',
      align: 'center',
      render(data: any) {
        return <div dangerouslySetInnerHTML={{ __html: data }} />;
      },
    },
    {
      title: (
        <TableTitle
          type={'none'}
          label="Create At"
          onSubmit={submitParams}
          name="createdAt"
          sort={[BANNER_SORT.CREATEAT_DESC, BANNER_SORT.CREATEAT_ASC]}
        />
      ),
      dataIndex: 'createdAt',
      key: 'createdAt',
      render(createdAt: string) {
        return timeFormatter(Number(createdAt));
      },
      align: 'center',
    },
    {
      title: (
        <TableTitle
          name="createdBy"
          label="Created By"
          onSubmit={submitParams}
          sort={[BANNER_SORT.CREATED_BY_DESC, BANNER_SORT.CREATED_BY_ASC]}
        />
      ),
      dataIndex: 'createdBy',
      key: 'createdBy',
      align: 'center',
    },
    {
      title: (
        <TableTitle
          type={'none'}
          label="Update At"
          onSubmit={submitParams}
          name="updatedAt"
          sort={[BANNER_SORT.UPDATEAT_DESC, BANNER_SORT.UPDATEAT_ASC]}
        />
      ),
      dataIndex: 'updatedAt',
      key: 'updatedAt',
      render(value: string) {
        return timeFormatter(Number(value));
      },
    },
    {
      title: (
        <TableTitle
          name="updatedBy"
          label="Update By"
          onSubmit={submitParams}
          sort={[BANNER_SORT.UPDATED_BY_DESC, BANNER_SORT.UPDATED_BY_ASC]}
        />
      ),
      dataIndex: 'updatedBy',
      key: 'updatedBy',
      align: 'center',
    },
    {
      title: 'Action',
      align: 'center',
      dataIndex: 'id',
      fixed: 'right',
      render(id: string) {
        return (
          <Space>
            <AntdButton
              className={styles.btnAction}
              icon={isUpdate ? <AiFillEdit /> : <AiFillEye />}
              type="primary"
              onClick={() => dispatch(setEditingId(id))}
              title={isUpdate ? 'Edit' : 'View'}
            />
            {/* {isUpdate && (
              <AntdButton
                className={styles.btnAction}
                icon={<AiOutlineDelete />}
                danger
                onClick={() => dispatch(setDeletingId(id))}
                title="Delete"
              />
            )} */}
          </Space>
        );
      },
    },
  ];
};
