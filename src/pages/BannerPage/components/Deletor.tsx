import { Modal } from 'antd';
import { useAppDispatch, useAppSelector } from 'hooks/reduxHook';
import { FC } from 'react';
import { useMutation, useQueryClient } from 'react-query';
import { message } from 'utils/message';
import { IError } from 'api/types';
import { getDeletingId, setDeletingId } from 'store/ducks/banner/slice';
import { useFaqQuery } from 'api/faq';
import { deleteBanner } from 'api/banner/request';
const Deletor: FC = () => {
  const dispatch = useAppDispatch();
  const deletingId = useAppSelector(getDeletingId);
  const client = useQueryClient();

  const { data, isLoading } = useFaqQuery(deletingId as string, { enabled: !!deletingId });

  const handleClose = () => {
    dispatch(setDeletingId(null));
  };

  const { mutate: mutateDelete, isLoading: deleteLoading } = useMutation(deleteBanner, {
    onSuccess: () => {
      client.invalidateQueries('/articles/list-banner');
      message.success('Delete successfully!');
      handleClose();
    },
    onError: (error: IError) => {
      message.error(error?.meta.message[0]);
    },
  });

  const onDelete = () => {
    mutateDelete(deletingId as string);
  };

  if (isLoading) {
    return null;
  }

  return (
    <>
      <Modal
        visible={!!deletingId}
        title={`Delete banner: ${data?.id}`}
        okButtonProps={{
          loading: deleteLoading,
        }}
        cancelButtonProps={{
          disabled: deleteLoading,
        }}
        onCancel={handleClose}
        onOk={onDelete}
        okType="danger"
        okText="Confirm"
      >
        Are you sure you want to delete this banner?
      </Modal>
    </>
  );
};

export default Deletor;
