import { Form } from 'antd';
import { useGetDetailBanner } from 'api/banner';
import { updateBanner } from 'api/banner/request';
import { IError } from 'api/types';
import { Drawer } from 'components/Drawer';
import { Input, InputTextArea } from 'components/Input';
import { Upload } from 'components/Upload';
import { useAppDispatch, useAppSelector } from 'hooks/reduxHook';
import { FC, useEffect, useMemo } from 'react';
import { AiOutlinePlus } from 'react-icons/ai';
import { useMutation, useQueryClient } from 'react-query';
import { getEditingId, setEditingId } from 'store/ducks/banner/slice';
import { convertFormValue, getUploadValueProps, rules } from 'utils/form';
import { message } from 'utils/message';
import { IListType } from './type';

const Editor: FC<IListType> = ({ isUpdate: canUpdate }) => {
  const dispatch = useAppDispatch();
  const editingId = useAppSelector(getEditingId);
  const client = useQueryClient();

  const [form] = Form.useForm();

  const isUpdate = useMemo(() => {
    return (canUpdate && editingId !== '') || editingId === '';
  }, []);

  const action = editingId ? 'edit' : 'add';

  const { data, status: getLoading } = useGetDetailBanner(editingId as string, { enabled: !!editingId });

  const { mutate, isLoading } = useMutation(updateBanner, {
    onSuccess: () => {
      client.invalidateQueries('/articles/list-banner');
      message.success('Update successfully!');
      handleClose();
    },
    onError: (error: IError) => {
      message.error(error?.meta.message[0]);
    },
  });

  useEffect(() => {
    form.resetFields();
    if (editingId !== null) {
      form.setFieldsValue(data);
    }
  }, [data]);

  const handleClose = () => {
    dispatch(setEditingId(null));
  };

  const onFinish = (value: any) => {
    const params = { id: editingId, ...value, text_button: value.textButton };
    mutate(convertFormValue(params));
  };

  const handleChange = (file: File, field: string) => {
    form.setFieldsValue({ [field]: file });
  };

  return (
    <Drawer
      name="Banner"
      editingId={editingId}
      handleClose={handleClose}
      form={form}
      getLoading={getLoading === 'loading' || status === 'loading'}
      submitLoading={isLoading}
      viewOnly={!isUpdate}
    >
      <Form form={form} onFinish={onFinish}>
        <Form.Item
          label="Title"
          name="title"
          labelCol={{ span: 3 }}
          wrapperCol={{ span: 18 }}
          rules={[rules, { type: 'string', max: 100 }]}
        >
          <Input disabled={!isUpdate} placeholder={'Enter your title'} />
        </Form.Item>
        <Form.Item
          label="Description"
          name="description"
          labelCol={{ span: 3 }}
          wrapperCol={{ span: 18 }}
          rules={[rules, { type: 'string', max: 200 }]}
        >
          <InputTextArea disabled={!isUpdate} placeholder={'Enter your description'} />
        </Form.Item>
        <Form.Item
          label="Link"
          name="link"
          labelCol={{ span: 3 }}
          wrapperCol={{ span: 18 }}
          rules={[rules, { type: 'string' }]}
        >
          <Input disabled={!isUpdate} placeholder={'Enter your link'} />
        </Form.Item>
        <Form.Item
          label="Text in Button"
          name="textButton"
          labelCol={{ span: 3 }}
          wrapperCol={{ span: 18 }}
          rules={[rules]}
        >
          <Input disabled={!isUpdate} placeholder={'Enter your link'} />
        </Form.Item>
        <Form.Item
          name="image"
          label="Logo"
          labelCol={{ span: 3 }}
          wrapperCol={{ span: 18 }}
          getValueProps={getUploadValueProps}
          rules={[{ required: true }]}
        >
          <Upload
            disabled={!isUpdate}
            listType="picture-card"
            onChangeFile={(file: File) => handleChange(file, 'image')}
            imageUrl={data?.image}
          >
            <AiOutlinePlus />
          </Upload>
        </Form.Item>
      </Form>
    </Drawer>
  );
};

export default Editor;
