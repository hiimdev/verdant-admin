import { AnyAction } from '@reduxjs/toolkit';
import { Button, Image, Popconfirm, Space } from 'antd';
import { useBrandCountNtfs } from 'api/brand';
import { TableTitle } from 'components/Table/component/TableTitle';
import { Dispatch, useState } from 'react';
import { AiFillEdit, AiFillEye, AiOutlineDelete } from 'react-icons/ai';
import { setDeletingId, setEditingId } from 'store/ducks/brand/slice';
import { ADMIN_STATUS_ENUMS, BRAND_SORT } from 'utils/constant';
import { timeFormatter } from 'utils/date';
import styles from './styles.module.less';

export const getColumns = (dispatch: Dispatch<AnyAction>, mutate: any, submitParams?: any, isUpdate?: boolean) => {
  return [
    {
      title: 'No',
      key: 'index',
      dataIndex: 'index',
      align: 'center',
      width: '80px',
    },
    {
      title: (
        <TableTitle
          name="brandName"
          label="Brand name"
          onSubmit={submitParams}
          sort={[BRAND_SORT.BRANDNAME_DESC, BRAND_SORT.BRANDNAME_ASC]}
        />
      ),
      dataIndex: 'brandName',
      key: 'brandName',
      width: '150px',
    },
    {
      title: (
        <TableTitle
          name="NFTsMinted"
          label="Total NFTs Minted"
          onSubmit={(values: any) =>
            submitParams({
              ...values,
              NFTsMinted: Number(values.NFTsMinted),
            })
          }
          sort={[BRAND_SORT.NFTSMINTED_DESC, BRAND_SORT.NFTSMINTED_ASC]}
        />
      ),
      dataIndex: 'NFTsMinted',
      key: 'NFTsMinted',
      align: 'center',
    },
    {
      title: (
        <TableTitle
          name="NFTsTraded"
          label="Total NFTs Traded"
          onSubmit={(values: any) =>
            submitParams({
              ...values,
              NFTsTraded: Number(values.NFTsTraded),
            })
          }
          sort={[BRAND_SORT.NFTSTRADED_DESC, BRAND_SORT.NFTSTRADED_ASC]}
        />
      ),
      dataIndex: 'NFTsTraded',
      key: 'NFTsTraded',
      align: 'center',
    },
    {
      title: (
        <TableTitle
          name="description"
          label="Description"
          onSubmit={submitParams}
          sort={[BRAND_SORT.DESCRIPTION_DESC, BRAND_SORT.DESCRIPTION_ASC]}
        />
      ),
      dataIndex: 'description',
      key: 'description',
      render(description: string) {
        return <div className={styles.description}>{description}</div>;
      },
      width: '300px',
    },
    {
      title: 'Logo',
      render({ logoBlackUrl, brandName }: any) {
        return <Image className={styles.image} src={logoBlackUrl} alt={brandName} />;
      },
      align: 'center',
    },
    {
      title: 'Description image',
      render({ descriptionUrl, brandName }: any) {
        return <Image className={styles.image} src={descriptionUrl} alt={brandName} />;
      },
      align: 'center',
    },
    {
      title: (
        <TableTitle
          name="category_category_name"
          label="Category"
          onSubmit={submitParams}
          sort={[BRAND_SORT.CATEGORYNAME_DESC, BRAND_SORT.CATEGORYNAME_ASC]}
        />
      ),
      dataIndex: 'category_category_name',
      key: 'category_category_name',
      render(description: string) {
        return <div className={styles.description}>{description}</div>;
      },
      align: 'center',
    },
    {
      title: (
        <TableTitle
          type="none"
          name="createdAt"
          label="Create At"
          onSubmit={submitParams}
          sort={[BRAND_SORT.CREATEDAT_DESC, BRAND_SORT.CREATEDAT_ASC]}
        />
      ),
      dataIndex: 'createdAt',
      key: 'createdAt',
      render(value: string) {
        return timeFormatter(Number(value));
      },
    },
    {
      title: (
        <TableTitle
          name="createdBy"
          label="Created By"
          onSubmit={submitParams}
          sort={[BRAND_SORT.CREATEDBY_DESC, BRAND_SORT.CREATEDBY_ASC]}
        />
      ),
      dataIndex: 'createdBy',
      key: 'createdBy',
      align: 'center',
      render(data: string) {
        return data || '-';
      },
    },
    {
      title: (
        <TableTitle
          type="none"
          name="updatedAt"
          label="Update at"
          onSubmit={submitParams}
          sort={[BRAND_SORT.UPDATEAT_DESC, BRAND_SORT.UPDATEAT_ASC]}
        />
      ),
      dataIndex: 'updatedAt',
      key: 'updatedAt',
      render(value: string) {
        return timeFormatter(Number(value));
      },
    },
    {
      title: (
        <TableTitle
          name="updatedBy"
          label="Update By"
          onSubmit={submitParams}
          sort={[BRAND_SORT.UPDATEDBY_DESC, BRAND_SORT.UPDATEDBY_ASC]}
        />
      ),
      dataIndex: 'updatedBy',
      key: 'updatedBy',
      align: 'center',
      render(data: string) {
        return data || '-';
      },
    },
    {
      title: (
        <TableTitle
          type="select"
          name="status"
          label="Status"
          onSubmit={(values: any) =>
            submitParams({ ...values, status: values.status ? Number(values.status) : undefined })
          }
          sort={[]}
          obj={ADMIN_STATUS_ENUMS}
        />
      ),
      dataIndex: 'status',
      key: 'status',
      render(isActive: 0 | 1) {
        return ADMIN_STATUS_ENUMS[isActive];
      },
      align: 'center',
    },
    {
      title: 'Action',
      align: 'center',
      dataIndex: 'id',
      fixed: 'right',
      render(id: string) {
        const { data, refetch, status } = useBrandCountNtfs(id, { enabled: false });
        const [visible, setVisible] = useState(false);

        const handleVisibleChange = (e: any) => {
          setVisible(e);
          if (e) {
            refetch();
          }
        };

        return (
          <Space>
            <Button
              className={styles.btnAction}
              icon={isUpdate ? <AiFillEdit /> : <AiFillEye />}
              type="primary"
              onClick={() => dispatch(setEditingId(id))}
              title={isUpdate ? 'Edit' : 'View'}
            />
            {isUpdate &&
              (Number(data?.totalNfts) > 0 ? (
                <Popconfirm
                  title={
                    status === 'success'
                      ? `In this brand, there are ${Number(
                          data?.totalNfts
                        )} NFTs that haven't been burned. They must be burned before this brand is removed!`
                      : 'Loading...'
                  }
                  okText="OK"
                  onConfirm={() => {
                    dispatch(setDeletingId(null));
                  }}
                  onVisibleChange={handleVisibleChange}
                  visible={visible}
                >
                  <Button className={styles.btnAction} icon={<AiOutlineDelete />} danger title="Delete" />
                </Popconfirm>
              ) : (
                <Popconfirm
                  title={status === 'success' ? 'Are you sure you want to delete this brand?' : 'Loading...'}
                  okText="Yes"
                  cancelText="No"
                  onConfirm={() => {
                    mutate(id);
                  }}
                  onVisibleChange={handleVisibleChange}
                  visible={visible}
                >
                  <Button className={styles.btnAction} icon={<AiOutlineDelete />} danger title="Delete" />
                </Popconfirm>
              ))}
          </Space>
        );
      },
    },
  ];
};
