import { Form } from 'antd';
import { useBrandCountNtfs, useBrandQuery } from 'api/brand';
import { createBrand, updateBrand } from 'api/brand/request';
import { useCategoriesQuery } from 'api/category';
import { ICategoryResponse } from 'api/category/types';
import { IError } from 'api/types';
import { Drawer } from 'components/Drawer';
import { Input, InputTextArea } from 'components/Input';
import { Select } from 'components/Select';
import { Upload } from 'components/Upload';
import { useAppDispatch, useAppSelector } from 'hooks/reduxHook';
import { FC, useEffect, useMemo } from 'react';
import { AiOutlinePlus } from 'react-icons/ai';
import { useMutation, useQueryClient } from 'react-query';
import { getEditingId, setEditingId } from 'store/ducks/brand/slice';
import { BRAND_STATUS_ENUMS } from 'utils/constant';
import { convertFormValue, getUploadValueProps, inputProps, rules } from 'utils/form';
import { message } from 'utils/message';
import { IListType } from './type';

const Editor: FC<IListType> = ({ isUpdate: canUpdate }) => {
  const dispatch = useAppDispatch();
  const editingId = useAppSelector(getEditingId);
  const client = useQueryClient();

  const [form] = Form.useForm();

  const isUpdate = useMemo(() => {
    return (canUpdate && editingId !== '') || editingId === '';
  }, []);

  const action = editingId ? 'edit' : 'add';

  const { data, status: getLoading } = useBrandQuery(editingId as string, { enabled: !!editingId });
  const { data: categories } = useCategoriesQuery({ page: 1, limit: 10 });

  const { mutate: update, isLoading: loadingUpdate } = useMutation(updateBrand, {
    onSuccess: () => {
      client.invalidateQueries('/brand/paging_filter');
      message.success('Update successfully!');
      handleClose();
    },
    onError: (error: IError) => {
      message.error(error?.meta.message[0]);
    },
  });

  const { mutate: create, isLoading: loadingCreate } = useMutation(createBrand, {
    onSuccess: () => {
      client.invalidateQueries('/brand/paging_filter');
      message.success('Create successfully!');
      handleClose();
    },
    onError: (error: IError) => {
      message.error(error?.meta.message[0]);
    },
  });

  useEffect(() => {
    form.resetFields();
    if (editingId !== null) {
      form.setFieldsValue({
        ...data,
        description_photo: data?.descriptionUrl,
        logo: data?.logoUrl,
        logoBlack: data?.logoBlackUrl,
        avatar: data?.avatar,
      });
    }
  }, [data]);

  const handleClose = () => {
    dispatch(setEditingId(null));
  };

  const onFinish = (value: any) => {
    const mutate = action === 'edit' ? update : create;
    const params = action === 'edit' ? { id: editingId, ...value } : value;
    mutate({ ...convertFormValue(params), status: value.status || 0 });
  };

  const handleChange = (file: File, field: string) => {
    form.setFieldsValue({ [field]: file });
  };
  const { data: brandCountNtfs } = useBrandCountNtfs(editingId as string, { enabled: !!editingId });

  return (
    <Drawer
      name="Brand"
      editingId={editingId}
      handleClose={handleClose}
      form={form}
      getLoading={getLoading === 'loading'}
      submitLoading={loadingCreate || loadingUpdate}
      viewOnly={!isUpdate}
    >
      <Form form={form} onFinish={onFinish}>
        <Form.Item
          label="Brand name"
          name="brandName"
          labelCol={{ span: 3 }}
          wrapperCol={{ span: 18 }}
          rules={[rules, inputProps.brand.name]}
        >
          <Input disabled={!isUpdate} placeholder={'Enter your brand name'} />
        </Form.Item>
        <Form.Item labelCol={{ span: 3 }} wrapperCol={{ span: 18 }} name="categoryCommissionId" label="Category">
          <Select
            disabled={!isUpdate || Number(brandCountNtfs?.totalNfts) > 0}
            placeholder="Select category"
            style={{ width: '100%' }}
          >
            {categories &&
              categories?.list?.map((item: ICategoryResponse, index) => (
                <Select.Option key={index} value={item.id}>
                  {item.categoryName}
                </Select.Option>
              ))}
          </Select>
        </Form.Item>

        <Form.Item
          label="Description"
          name="description"
          labelCol={{ span: 3 }}
          wrapperCol={{ span: 18 }}
          rules={[rules]}
        >
          <InputTextArea disabled={!isUpdate} placeholder={'Enter your description'} autoSize={{ minRows: 3 }} />
        </Form.Item>

        <Form.Item
          name="avatar"
          label="Avatar"
          labelCol={{ span: 3 }}
          wrapperCol={{ span: 18 }}
          getValueProps={getUploadValueProps}
          rules={[{ required: true }]}
        >
          <Upload
            disabled={!isUpdate}
            listType="picture-card"
            onChangeFile={(file: File) => handleChange(file, 'avatar')}
            imageUrl={data?.avatar}
            accept=".jpg, .jpeg, .png"
          >
            <AiOutlinePlus />
          </Upload>
        </Form.Item>

        <Form.Item
          name="logo"
          label="Logo Light"
          labelCol={{ span: 3 }}
          wrapperCol={{ span: 18 }}
          getValueProps={getUploadValueProps}
          rules={[{ required: true }]}
        >
          <Upload
            disabled={!isUpdate}
            listType="picture-card"
            onChangeFile={(file: File) => handleChange(file, 'logo')}
            imageUrl={data?.logoUrl}
            accept=".jpg, .jpeg, .png"
          >
            <AiOutlinePlus />
          </Upload>
        </Form.Item>
        <Form.Item
          name="logoBlack"
          label="Logo Dark"
          labelCol={{ span: 3 }}
          wrapperCol={{ span: 18 }}
          getValueProps={getUploadValueProps}
          rules={[{ required: true }]}
        >
          <Upload
            disabled={!isUpdate}
            listType="picture-card"
            onChangeFile={(file: File) => handleChange(file, 'logoBlack')}
            imageUrl={data?.logoBlackUrl}
            accept=".jpg, .jpeg, .png"
          >
            <AiOutlinePlus />
          </Upload>
        </Form.Item>
        <Form.Item
          name="description_photo"
          label="Description image"
          labelCol={{ span: 3 }}
          wrapperCol={{ span: 18 }}
          getValueProps={getUploadValueProps}
          rules={[{ required: true }]}
        >
          <Upload
            disabled={!isUpdate}
            listType="picture-card"
            onChangeFile={(file: File) => handleChange(file, 'description_photo')}
            imageUrl={data?.descriptionUrl}
            accept=".jpg, .jpeg, .png"
          >
            <AiOutlinePlus />
          </Upload>
        </Form.Item>
        <Form.Item label="Status" name="status" labelCol={{ span: 3 }} wrapperCol={{ span: 18 }}>
          <Select disabled={!isUpdate} placeholder="Select status">
            {Object.keys(BRAND_STATUS_ENUMS).map((key: string) => (
              <Select.Option key={key} value={Number(key)}>
                {BRAND_STATUS_ENUMS[Number(key || 0) as 0 | 1]}
              </Select.Option>
            ))}
          </Select>
        </Form.Item>
      </Form>
    </Drawer>
  );
};

export default Editor;
