import { message } from 'antd';
import useBreakpoint from 'antd/lib/grid/hooks/useBreakpoint';
import { useBrandsQuery } from 'api/brand';
import { deleteBrand } from 'api/brand/request';
import { IError } from 'api/types';
import { PaginatedTableAndSearch } from 'components/PaginatedTableAndSearch';
import { useAppDispatch } from 'hooks/reduxHook';
import { FC, useMemo, useState } from 'react';
import { useMutation, useQueryClient } from 'react-query';
import { setEditingId } from 'store/ducks/brand/slice';
import { debounce } from 'utils/js';
import { Permission } from 'utils/permission';
import { getColumns } from './Column';
import { IListType } from './type';

const List: FC<IListType> = ({ isUpdate }) => {
  const client = useQueryClient();
  const { mutate } = useMutation(deleteBrand, {
    onSuccess: () => {
      client.invalidateQueries('/brand/paging_filter');
      message.success('Delete successfully!');
    },
    onError: (error: IError) => {
      message.error(error?.meta.message[0]);
    },
  });
  const dispatch = useAppDispatch();
  const [params, setParams] = useState({ sort: [] });
  const debouncedSetParams = useMemo(() => debounce(setParams, 500), []);

  const submitParams = (value: any) => {
    debouncedSetParams((prevParams: any) => ({
      ...prevParams,
      ...value,
      sort: value.sort ? [value.sort] : [],
    }));
  };
  const columns = useMemo(() => getColumns(dispatch, mutate, submitParams, isUpdate), [getColumns, isUpdate]);

  const screen = useBreakpoint();

  return (
    <>
      <PaginatedTableAndSearch
        permission={Permission.add_brand}
        title="Brand"
        setEditingId={setEditingId}
        columns={columns}
        useQuery={useBrandsQuery}
        requestParams={params}
        scroll={screen.xxl ? undefined : { x: 2100 }}
        handleReset={() => setParams({ sort: [] })}
      />
    </>
  );
};

export default List;
