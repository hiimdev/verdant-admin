import { useAppSelector } from 'hooks/reduxHook';
import { FC, useMemo } from 'react';
import { getEditingId } from 'store/ducks/brand/slice';
import { getPermission } from 'store/ducks/user/slice';
import { Permission } from 'utils/permission';
import Editor from './components/Editor';
import List from './components/List';

const Brand: FC = () => {
  const permissions = useAppSelector(getPermission);
  const editingId = useAppSelector(getEditingId);

  const isUpdate = useMemo(() => {
    return permissions.includes(Permission.edit_delete_brand);
  }, [permissions]);

  return (
    <>
      <List isUpdate={isUpdate} />
      {editingId !== null && <Editor isUpdate={isUpdate} />}
    </>
  );
};

export default Brand;
