import { Card, Col, DatePicker, Empty, Row, Space, Spin, Statistic } from 'antd';
import { useGetBrandCommission, useGetCategoryCommission, useGetCollectionCommission } from 'api/commission';
import { useGetBusinessTransaction, useGetStaticalTransaction } from 'api/statistical';
import { useGetListVendorFilter } from 'api/user';
import { IVendorResponse } from 'api/user/types';
import { Chart } from 'chart.js';
import zoomPlugin from 'chartjs-plugin-zoom';
import { Select } from 'components/Select';
import { useAppSelector } from 'hooks/reduxHook';
import moment from 'moment';
import { FC, useEffect, useMemo, useState } from 'react';
import { getGroup } from 'store/ducks/user/slice';
import { convertPriceEther } from 'utils/common';
import { TITLE_BUSSINESS_STATICAL } from 'utils/constant';
import { debounce } from 'utils/js';
import LineChart from './LineChart';
import styles from './styles.module.less';
import Top10NFTs from './Top10NFT';

const { RangePicker } = DatePicker;

let now = new Date();

const Content: FC = () => {
  const startDate = useMemo(() => new Date(now.getFullYear(), now.getMonth(), 1), []);
  const endDate = useMemo(() => new Date(now.getFullYear(), now.getMonth() + 1, 0), []);
  const group = useAppSelector(getGroup);
  const [params, setParams] = useState<any>({ from: startDate.getTime().toString(), to: endDate.getTime().toString() });

  const { data: categories, refetch: refetchCategory } = useGetCategoryCommission(
    { page: 1, limit: 50 },
    { enabled: group === '0' }
  );
  const { data: brands, refetch: refetchBrand } = useGetBrandCommission(
    params?.categoryCommissionId
      ? {
          page: 1,
          limit: 50,
          categoryId: params.categoryCommissionId,
        }
      : { page: 1, limit: 50 },
    { enabled: group === '0' }
  );
  const { data: collections, refetch: refetchCollection } = useGetCollectionCommission(
    params?.categoryCommissionId || params?.brandId
      ? {
          page: 1,
          limit: 50,
          categoryId: params.categoryCommissionId,
          brandId: params.brandId,
        }
      : { page: 1, limit: 50 },
    { enabled: group === '0' }
  );

  const { data } = useGetStaticalTransaction(params);
  const { data: dataBusiness, isLoading } = useGetBusinessTransaction(params);
  const { data: listVendor } = useGetListVendorFilter({ page: 1, limit: 50 });

  const debouncedSetParams = useMemo(() => debounce(setParams, 500), []);

  useEffect(() => {
    Chart.register(zoomPlugin);
  }, []);

  const handleClickRefecthCollection = () => {
    refetchCollection();
  };

  const handleClickRefecthBrand = () => {
    refetchBrand();
  };

  const handleClickRefecthCategory = () => {
    refetchCategory();
  };
  return (
    <>
      <div style={{ marginBottom: '20px' }}>
        <Card title="Filter">
          <Space wrap size={20}>
            <RangePicker
              size="large"
              onChange={(ev: any) =>
                debouncedSetParams((prevParams: any) => ({
                  ...prevParams,
                  from: ev && ev[0] ? new Date(ev[0]).getTime() : '',
                  to: ev && ev[1] ? new Date(ev[1]).getTime() : '',
                }))
              }
              defaultValue={[moment(startDate, 'YYYY-MM-DD'), moment(endDate, 'YYYY-MM-DD')]}
            />
            {group !== '1' && (
              <>
                <Select
                  placeholder="Select vendor"
                  style={{ width: '180px' }}
                  onChange={(e) =>
                    debouncedSetParams((prevParams: any) => ({
                      ...prevParams,
                      vendorId: e,
                    }))
                  }
                >
                  <Select.Option value={undefined}>Select vendor</Select.Option>
                  {listVendor?.list?.map((item: IVendorResponse) => (
                    <Select.Option key={item.id} value={item.id}>
                      {item.vendorName}
                    </Select.Option>
                  ))}
                </Select>

                <Select
                  placeholder="Select category"
                  style={{ width: '180px' }}
                  value={params.categoryCommissionData}
                  onChange={(e) => {
                    debouncedSetParams((prevParams: any) => ({
                      ...prevParams,
                      categoryCommissionId: e,
                      brandData: null,
                      collectionData: null,
                    }));
                  }}
                  notFoundContent={!categories ? <Spin /> : <Empty />}
                  onClick={handleClickRefecthCategory}
                >
                  <Select.Option value={undefined}>Select category</Select.Option>
                  {categories?.list?.map((item: any) => (
                    <Select.Option key={item.categoryId} value={item.categoryId}>
                      {item.categoryName}
                    </Select.Option>
                  ))}
                </Select>

                <Select
                  placeholder="Select brand"
                  value={params.brandData}
                  style={{ width: '180px' }}
                  onClick={handleClickRefecthBrand}
                  notFoundContent={!brands ? <Spin /> : <Empty />}
                  onChange={(e) => {
                    debouncedSetParams((prevParams: any) => ({
                      ...prevParams,
                      brandId: JSON.parse(e)?.brandId,
                      categoryCommissionId: JSON.parse(e)?.categoryCommissionId,
                      brandData: JSON.parse(e)?.brandData,
                      categoryCommissionData: JSON.parse(e)?.categoryCommissionData,
                      collectionData: null,
                    }));
                  }}
                >
                  <Select.Option value={undefined}>Select brand</Select.Option>
                  {brands?.list?.map((item: any) => (
                    <Select.Option
                      key={item.brandId}
                      value={JSON.stringify({
                        brandId: item.brandId,
                        categoryCommissionId: item.categoryId,
                        brandData: item.brandName,
                        categoryCommissionData: item.categoryName,
                      })}
                    >
                      {item.brandName}
                    </Select.Option>
                  ))}
                </Select>

                <Select
                  placeholder="Select collection"
                  style={{ width: '180px' }}
                  value={params.collectionData}
                  onChange={(e) => {
                    debouncedSetParams((prevParams: any) => ({
                      ...prevParams,
                      collectionData: JSON.parse(e)?.collectionData,
                      collectionId: JSON.parse(e)?.collectionId,
                      brandId: JSON.parse(e)?.brandId,
                      categoryCommissionId: JSON.parse(e)?.categoryCommissionId,
                      brandData: JSON.parse(e)?.brandData,
                      categoryCommissionData: JSON.parse(e)?.categoryCommissionData,
                    }));
                  }}
                  onClick={handleClickRefecthCollection}
                  notFoundContent={!collections ? <Spin /> : <Empty />}
                >
                  <Select.Option value={undefined}>Select collection</Select.Option>
                  {collections?.list?.map((item: any) => (
                    <Select.Option
                      key={item.collectionId}
                      value={JSON.stringify({
                        collectionId: item.collectionId,
                        brandId: item.brandId,
                        categoryCommissionId: item.categoryId,
                        collectionData: item.collectionName,
                        brandData: item.brandName,
                        categoryCommissionData: item.categoryName,
                      })}
                    >
                      {item.collectionName}
                    </Select.Option>
                  ))}
                </Select>
              </>
            )}
          </Space>
        </Card>
      </div>
      <Card title="Statistic" className={styles.card}>
        <Row justify="space-between" gutter={[36, 36]}>
          {data ? (
            Object.keys(data).map((key: string, index) => (
              <Col span={6} key={key}>
                <Card
                  style={{
                    background: index === 0 ? '#13c2c2' : index === 1 ? '#fa8c16' : index === 2 ? '#20c997' : '#f5222d',
                  }}
                >
                  <Statistic
                    style={{ textAlign: 'center' }}
                    title={
                      <strong style={{ color: '#fff', fontSize: '14px' }}>
                        {
                          TITLE_BUSSINESS_STATICAL[
                            key as 'totalTrading' | 'totalNFTsMinted' | 'totalNFTsSold' | 'maxPrice'
                          ]
                        }
                      </strong>
                    }
                    value={
                      key === 'totalTrading' || key === 'maxPrice'
                        ? `${convertPriceEther(
                            data[key as 'totalTrading' | 'totalNFTsMinted' | 'totalNFTsSold' | 'maxPrice'] || 0
                          )} USDC`
                        : data[key as 'totalTrading' | 'totalNFTsMinted' | 'totalNFTsSold' | 'maxPrice'] || 0
                    }
                  />
                </Card>
              </Col>
            ))
          ) : (
            <Col span={24}>
              <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', minHeight: '100px' }}>
                <Spin />
              </div>
            </Col>
          )}
          <Col span={24}>
            <Spin spinning={isLoading}>
              <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                <strong>Total Traded Value</strong>
                <strong>Total NFTs Traded</strong>
              </div>
              <LineChart dataBusiness={dataBusiness || []} />
            </Spin>
          </Col>
        </Row>
      </Card>
      <Card title="Top Nfts" style={{ marginTop: '20px' }}>
        <Top10NFTs params={params} />
      </Card>
    </>
  );
};

export default Content;
