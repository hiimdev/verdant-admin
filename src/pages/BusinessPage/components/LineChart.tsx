import zoomPlugin from 'chartjs-plugin-zoom';
import { Line } from 'react-chartjs-2';
import React, { FC, useEffect, useMemo } from 'react';
import { Chart } from 'chart.js';
import { IBusinessTransactionResponse } from 'api/statistical/types';

interface Props {
  dataBusiness: IBusinessTransactionResponse[];
}

const options = {
  plugins: {
    zoom: {
      zoom: {
        wheel: {
          enabled: true,
          modifierKey: 'ctrl',
        },
        mode: 'x',
        sensitivity: 0.5,
      },
    },
  },
  scales: {
    y: {
      type: 'linear' as const,
      display: true,
      position: 'left' as const,
    },
    y1: {
      type: 'linear' as const,
      display: true,
      position: 'right' as const,
      grid: {
        drawOnChartArea: false,
      },
    },
  },
};

const LineChart: FC<Props> = ({ dataBusiness }) => {
  useEffect(() => {
    Chart.register(zoomPlugin);
  }, []);

  const dataLine = useMemo(() => {
    return {
      labels: dataBusiness?.map((e) => e.day) || [],
      datasets: [
        {
          label: 'Total Traded Value',
          data: dataBusiness?.map((e) => e.traded_value),
          fill: true,
          backgroundColor: 'rgba(75,192,192,0.1)',
          borderColor: 'rgb(255, 99, 132)',
          yAxisID: 'y',
        },
        {
          label: 'Total NFTs Traded',
          data: dataBusiness?.map((e) => Number(e.traded_number)),
          fill: true,
          backgroundColor: 'rgba(75,192,192,0.1)',
          borderColor: 'rgba(75,192,192,0.8)',
          yAxisID: 'y1',
        },
      ],
    };
  }, [dataBusiness]);

  return (
    <>
      <Line type="" data={dataLine || []} options={options} />
    </>
  );
};

export default React.memo(LineChart);
