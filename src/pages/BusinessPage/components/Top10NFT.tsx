import { PaginatedTableAndSearch } from 'components/PaginatedTableAndSearch';
import { FC, useMemo } from 'react';
import { useGetTop10Nfts } from 'api/nft';
import { convertPriceEther, convertVendorName } from 'utils/common';

const getColumns = () => {
  return [
    {
      title: 'No',
      key: 'index',
      dataIndex: 'index',
      align: 'center' as const,
    },
    {
      title: 'NFT name',
      key: 'name',
      dataIndex: 'name',
      align: 'center' as const,
    },
    {
      title: 'Vendor',
      key: 'vendor_name',
      align: 'center' as const,
      width: '150px',
      render(data: any) {
        return <>{data?.vendor_name || convertVendorName(data)}</>;
      },
    },
    {
      title: 'Collection name',
      key: 'collection_name',
      dataIndex: 'collection_name',
      align: 'center' as const,
      width: '200px',
    },
    {
      title: 'Number of times Traded',
      dataIndex: 'traded_number',
      key: 'traded_number',
      align: 'center' as const,
      width: '180px',
      sorter: (a: any, b: any) => Number(a.traded_number) - Number(b.traded_number),
    },
    {
      title: 'Traded Value(USDC)',
      dataIndex: 'traded_value',
      key: 'traded_value',
      align: 'center' as const,
      width: '160px',
      sorter: (a: any, b: any) =>
        Number(convertPriceEther(a?.traded_value)) - Number(convertPriceEther(b?.traded_value)),
      render(data: any) {
        return <>{convertPriceEther(data || '0')}</>;
      },
    },
  ];
};

const Top10NFTs: FC<any> = (props) => {
  const columns = useMemo(() => getColumns(), [getColumns]);
  return (
    <div style={{ marginTop: '20px' }}>
      <PaginatedTableAndSearch columns={columns} useQuery={useGetTop10Nfts} requestParams={{ ...props.params }} />
    </div>
  );
};

export default Top10NFTs;
