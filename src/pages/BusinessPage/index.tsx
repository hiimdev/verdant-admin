import { FC } from 'react';
import Content from './components/Content';

const BusinessPage: FC = () => {
  return (
    <>
      <Content />
    </>
  );
};

export default BusinessPage;
