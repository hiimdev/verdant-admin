import { AnyAction, Dispatch } from '@reduxjs/toolkit';
import { Button, Image, Popconfirm, Space } from 'antd';
import { AiFillEdit, AiFillEye, AiOutlineDelete } from 'react-icons/ai';
import { CATEGORY_SORT } from 'utils/constant';
import { timeFormatter } from 'utils/date';
import styles from './styles.module.less';
import { setDeletingId, setEditingId } from 'store/ducks/category/slice';
import { TableTitle } from 'components/Table/component/TableTitle';
import { useCategoryCountNtfs } from 'api/category';
import { useState } from 'react';

export const getColumns = (dispatch: Dispatch<AnyAction>, mutate: any, submitParams?: any, isUpdate?: boolean) => {
  return [
    {
      title: 'No',
      dataIndex: 'index',
      key: 'index',
      align: 'center',
      width: '80px',
    },
    {
      title: (
        <TableTitle
          label="Catergory Name"
          onSubmit={submitParams}
          name="categoryName"
          sort={[CATEGORY_SORT.NAME_DESC, CATEGORY_SORT.NAME_ASC]}
        />
      ),
      dataIndex: 'categoryName',
      key: 'categoryName',
    },
    {
      title: (
        <TableTitle
          label="Total NFTs Minted"
          onSubmit={(values: any) => {
            submitParams({
              ...values,
              NFTsMinted: Number(values.NFTsMinted),
            });
          }}
          name="NFTsMinted"
          sort={[CATEGORY_SORT.NFTSMINTED_DESC, CATEGORY_SORT.NFTSMINTED_ASC]}
        />
      ),
      dataIndex: 'NFTsMinted',
      key: 'NFTsMinted',
      align: 'center',
    },
    {
      title: (
        <TableTitle
          label="Total NFTs Traded"
          onSubmit={(values: any) =>
            submitParams({
              ...values,
              NFTsTraded: Number(values.NFTsTraded),
            })
          }
          name="NFTsTraded"
          sort={[CATEGORY_SORT.NFTSTRADED_DESC, CATEGORY_SORT.NFTSTRADED_ASC]}
        />
      ),
      dataIndex: 'NFTsTraded',
      key: 'NFTsTraded',
      align: 'center',
    },
    {
      title: (
        <TableTitle
          label="Description"
          onSubmit={submitParams}
          name="categoryDescription"
          sort={[CATEGORY_SORT.DESCRIPTION_DESC, CATEGORY_SORT.DESCRIPTION_ASC]}
        />
      ),
      dataIndex: 'categoryDescription',
      key: 'categoryDescription',
      render(description: string) {
        return <div className={styles.description}>{description}</div>;
      },
      width: '200px',
    },
    {
      title: 'Logo',
      render({ logoUrl, categoryName }: any) {
        return <Image className={styles.image} src={logoUrl} alt={categoryName} />;
      },
      align: 'center',
    },
    {
      title: 'Description image',
      render({ descriptionUrl, categoryName }: any) {
        return <Image className={styles.image} src={descriptionUrl} alt={categoryName} />;
      },
      align: 'center',
    },
    {
      title: 'Restricted Category',
      dataIndex: 'restricted',
      render(value) {
        return <>{value ? 'Yes' : 'No'}</>;
      },
      align: 'center',
    },
    {
      title: 'Float Price',
      dataIndex: 'floatPrice',
      render(value) {
        return <>{value ? 'Yes' : 'No'}</>;
      },
      align: 'center',
    },
    {
      title: (
        <TableTitle
          type="none"
          label="Created at"
          onSubmit={submitParams}
          name="createdAt"
          sort={[CATEGORY_SORT.CREATEDAT_DESC, CATEGORY_SORT.CREATEDAT_ASC]}
        />
      ),
      dataIndex: 'createdAt',
      key: 'createdAt',
      render(createdAt: string) {
        return timeFormatter(Number(createdAt));
      },
      align: 'center',
    },
    {
      title: (
        <TableTitle
          label="Created By"
          onSubmit={submitParams}
          name="createdBy"
          sort={[CATEGORY_SORT.UPDATEDBY_DESC, CATEGORY_SORT.UPDATEDBY_ASC]}
        />
      ),
      dataIndex: 'createdBy',
      key: 'createdBy',
      align: 'center',
      render(data: string) {
        return data || '-';
      },
    },
    {
      title: (
        <TableTitle
          type="none"
          label="Update at"
          onSubmit={submitParams}
          name="updatedAt"
          sort={[CATEGORY_SORT.UPDATEAT_DESC, CATEGORY_SORT.UPDATEAT_ASC]}
        />
      ),
      dataIndex: 'updatedAt',
      key: 'updatedAt',
      render(updatedAt: string) {
        return timeFormatter(Number(updatedAt));
      },
      align: 'center',
    },
    {
      title: (
        <TableTitle
          label="Updated By"
          onSubmit={submitParams}
          name="updatedBy"
          sort={[CATEGORY_SORT.UPDATEDBY_DESC, CATEGORY_SORT.CREATEDBY_ASC]}
        />
      ),
      dataIndex: 'updatedBy',
      key: 'updatedBy',
      align: 'center',
      render(data: string) {
        return data || '-';
      },
    },
    {
      title: 'Action',
      align: 'center',
      dataIndex: 'id',
      fixed: 'right',
      render(id: string) {
        const { data, refetch, status } = useCategoryCountNtfs(id as string, { enabled: false });
        const [visible, setVisible] = useState(false);

        const handleVisibleChange = (e: any) => {
          setVisible(e);
          if (e) {
            refetch();
          }
        };

        return (
          <Space>
            <Button
              icon={isUpdate ? <AiFillEdit /> : <AiFillEye />}
              type="primary"
              onClick={() => dispatch(setEditingId(id))}
              title={isUpdate ? 'Edit' : 'View'}
            />
            {isUpdate &&
              (Number(data?.totalNfts) > 0 ? (
                <Popconfirm
                  title={
                    status === 'success'
                      ? `In this category, there are ${Number(
                          data?.totalNfts
                        )} NFTs that haven't been burned. They must be burned before this category is removed!`
                      : 'Loading...'
                  }
                  okText="OK"
                  onConfirm={() => {
                    dispatch(setDeletingId(null));
                  }}
                  onVisibleChange={handleVisibleChange}
                  visible={visible}
                >
                  <Button icon={<AiOutlineDelete />} danger title="Delete" />
                </Popconfirm>
              ) : (
                <Popconfirm
                  title={status === 'success' ? 'Are you sure you want to delete this category?' : 'Loading...'}
                  okText="Yes"
                  cancelText="No"
                  onConfirm={() => {
                    mutate(id);
                  }}
                  onVisibleChange={handleVisibleChange}
                  visible={visible}
                >
                  <Button icon={<AiOutlineDelete />} danger title="Delete" />
                </Popconfirm>
              ))}
          </Space>
        );
      },
    },
  ];
};
