import { Form } from 'antd';
import { useCategoryQuery } from 'api/category';
import { createCategory, updateCategory } from 'api/category/request';
import { IError } from 'api/types';
import { Drawer } from 'components/Drawer';
import { Input } from 'components/Input';
import { Select } from 'components/Select';
import { Upload } from 'components/Upload';
import { useAppDispatch, useAppSelector } from 'hooks/reduxHook';
import { FC, useEffect, useMemo } from 'react';
import { AiOutlinePlus } from 'react-icons/ai';
import { useMutation, useQueryClient } from 'react-query';
import { getEditingId, setEditingId } from 'store/ducks/category/slice';
import { convertFormValue, getUploadValueProps, rules } from 'utils/form';
import { message } from 'utils/message';
import { IListType } from './type';

const LIST = [
  { value: 0, label: 'No' },
  { value: 1, label: 'Yes' },
];

const Editor: FC<IListType> = ({ isUpdate: canUpdate }) => {
  const dispatch = useAppDispatch();
  const editingId = useAppSelector(getEditingId);
  const client = useQueryClient();

  const isUpdate = useMemo(() => {
    return (canUpdate && editingId !== '') || editingId === '';
  }, []);

  const [form] = Form.useForm();

  const action = editingId ? 'edit' : 'add';

  const { data, status: getLoading } = useCategoryQuery(editingId as string, { enabled: !!editingId });

  const { mutate: update, isLoading: loadingUpdate } = useMutation(updateCategory, {
    onSuccess: () => {
      client.invalidateQueries('/category/paging_filter');
      message.success('Update successfully!');
      handleClose();
    },
    onError: (error: IError) => {
      message.error(error?.meta.message[0]);
    },
  });

  const { mutate: create, isLoading: loadingCreate } = useMutation(createCategory, {
    onSuccess: () => {
      client.invalidateQueries('/category/paging_filter');
      message.success('Create successfully!');
      handleClose();
    },
    onError: (error: IError) => {
      message.error(error?.meta.message[0]);
    },
  });

  useEffect(() => {
    form.resetFields();
    if (editingId !== null) {
      form.setFieldsValue(data);
      form.setFieldsValue({ logo: data?.logoUrl });
      form.setFieldsValue({ description_photo: data?.descriptionUrl });
    }
  }, [data]);

  const handleClose = () => {
    dispatch(setEditingId(null));
  };

  const handleChange = (file: File, field: string) => {
    form.setFieldsValue({ [field]: file });
  };

  const onFinish = (value: any) => {
    const mutate = action === 'edit' ? update : create;
    const params = action === 'edit' ? { id: editingId, ...value } : value;
    mutate({ ...convertFormValue(params) });
  };

  return (
    <Drawer
      name="Category"
      editingId={editingId}
      handleClose={handleClose}
      form={form}
      getLoading={getLoading === 'loading'}
      submitLoading={loadingCreate || loadingUpdate}
      viewOnly={!isUpdate}
    >
      <Form form={form} onFinish={onFinish}>
        <Form.Item
          label="Category name"
          name="categoryName"
          labelCol={{ span: 3 }}
          wrapperCol={{ span: 18 }}
          rules={[rules]}
        >
          <Input disabled={!isUpdate} placeholder={'Enter your category name'} />
        </Form.Item>
        <Form.Item
          label="Category description"
          name="categoryDescription"
          labelCol={{ span: 3 }}
          wrapperCol={{ span: 18 }}
          rules={[rules]}
        >
          <Input disabled={!isUpdate} placeholder={'Enter your description'} />
        </Form.Item>
        <Form.Item
          name="logo"
          label="Logo"
          labelCol={{ span: 3 }}
          wrapperCol={{ span: 18 }}
          getValueProps={getUploadValueProps}
          rules={[{ required: true }]}
        >
          <Upload
            disabled={!isUpdate}
            listType="picture-card"
            onChangeFile={(file: File) => handleChange(file, 'logo')}
            imageUrl={data?.logoUrl}
            accept=".jpg, .jpeg, .png"
          >
            <AiOutlinePlus />
          </Upload>
        </Form.Item>
        <Form.Item
          name="description_photo"
          label="Description image"
          labelCol={{ span: 3 }}
          wrapperCol={{ span: 18 }}
          getValueProps={getUploadValueProps}
          rules={[{ required: true }]}
        >
          <Upload
            disabled={!isUpdate}
            listType="picture-card"
            onChangeFile={(file: File) => handleChange(file, 'description_photo')}
            imageUrl={data?.descriptionUrl}
            accept=".jpg, .jpeg, .png"
          >
            <AiOutlinePlus />
          </Upload>
        </Form.Item>
        <Form.Item
          label="Restricted"
          name="restricted"
          labelCol={{ span: 3 }}
          wrapperCol={{ span: 18 }}
          rules={[{ required: true }]}
        >
          <Select placeholder="Select value" onChange={() => {}}>
            {LIST.map((item: any) => (
              <Select.Option key={item?.value} value={item?.value}>
                {item?.label}
              </Select.Option>
            ))}
          </Select>
        </Form.Item>
        <Form.Item
          label="Float Price (Gold)"
          name="floatPrice"
          labelCol={{ span: 3 }}
          wrapperCol={{ span: 18 }}
          rules={[{ required: true }]}
        >
          <Select placeholder="Select value" onChange={() => {}}>
            {LIST.map((item: any) => (
              <Select.Option key={item?.value} value={item?.value}>
                {item?.label}
              </Select.Option>
            ))}
          </Select>
        </Form.Item>
      </Form>
    </Drawer>
  );
};

export default Editor;
