import { useAppSelector } from 'hooks/reduxHook';
import { FC, useMemo } from 'react';
import { getPermission } from 'store/ducks/user/slice';
import Editor from './components/Editor';
import List from './components/List';
import { getEditingId } from 'store/ducks/category/slice';
import { Permission } from 'utils/permission';

const Category: FC = () => {
  const permissions = useAppSelector(getPermission);
  const editingId = useAppSelector(getEditingId);

  const isUpdate = useMemo(() => {
    return permissions.includes(Permission.edit_delete_category);
  }, [permissions]);

  return (
    <>
      <List isUpdate={isUpdate} />
      {editingId !== null && <Editor isUpdate={isUpdate} />}
    </>
  );
};

export default Category;
