import { useContractFunction } from '@usedapp/core';
import { Form } from 'antd';
import { useGetChainLinkDetail } from 'api/admin';
import { setChainLink } from 'api/admin/request';
import { IError } from 'api/types';
import { Drawer } from 'components/Drawer';
import { Input } from 'components/Input';
import { useAppDispatch, useAppSelector } from 'hooks/reduxHook';
import { FC, useEffect } from 'react';
import { useMutation, useQueryClient } from 'react-query';
import { getWeightId, setWeightId } from 'store/ducks/system/slice';
import { CONTRACT_ADDRESS, MARKET_ADDRESS } from 'utils/constant';
import { message } from 'utils/message';
import { IListType } from './type';
import MarketABI from 'contracts/MarketABI.json';
import { useConnectABI } from 'hooks/useConnectABI';
import { getLoadingButtonCallSMCT } from 'utils/common';
import { useWallet } from 'hooks/useWallet';

const Editor: FC<IListType> = () => {
  const dispatch = useAppDispatch();
  const editingId = useAppSelector(getWeightId);
  const client = useQueryClient();
  const { account } = useWallet();
  // const isUpdate = useMemo(() => {
  //   return (canUpdate && editingId !== '') || editingId === '';
  // }, []);

  const [form] = Form.useForm();

  const { data, status: getLoading } = useGetChainLinkDetail(editingId as string, { enabled: !!editingId });

  const { state, send } = useContractFunction(
    useConnectABI(MarketABI, CONTRACT_ADDRESS.MARKET),
    'setFloatingPaymentMethod'
  );

  const { mutate: create, isLoading: loadingCreate } = useMutation(setChainLink, {
    onSuccess: (res, request) => {
      send(request.assetName, request.paymentToken, [[`${request.aggregatorAddress}`, false]]);

      // send(request.assetName, request.paymentToken, [
      //   [request.aggregatorAddress, false],
      //   [`0x69B6c8721EBE62B2dC2Ff272a1a47cC8a755cFe2`, true],
      // ]);
    },
    onError: (error: IError) => {
      message.error(error?.meta.message[0]);
    },
  });

  useEffect(() => {
    form.resetFields();
    if (data) {
      form.setFieldsValue({
        ...data,
      });
    }
  }, [data]);

  const handleClose = () => {
    dispatch(setWeightId(null));
  };

  const onFinish = (value: any) => {
    if (account === MARKET_ADDRESS) {
      create(value);
    } else {
      message.info('Please login correctly Digicap account');
    }
  };

  useEffect(() => {
    if (state.status === 'Success') {
      client.invalidateQueries('/config/list-chain-link');
      message.success('Successfully!');
      handleClose();
    } else if (['Fail', 'Exception'].includes(state.status)) {
      message.error(state.errorMessage as string);
    }
  }, [state]);

  return (
    <Drawer
      name="Chain Link"
      editingId={editingId}
      handleClose={handleClose}
      form={form}
      getLoading={getLoading === 'loading'}
      submitLoading={loadingCreate || getLoadingButtonCallSMCT(state)}
      viewOnly={!!editingId}
    >
      <Form form={form} onFinish={onFinish}>
        <Form.Item
          label="Asset Name"
          name="assetName"
          labelCol={{ span: 3 }}
          wrapperCol={{ span: 18 }}
          rules={[{ required: true }]}
        >
          <Input placeholder={'Enter Asset Name'} />
        </Form.Item>
        <Form.Item
          label="Aggregator Address"
          name="aggregatorAddress"
          labelCol={{ span: 3 }}
          wrapperCol={{ span: 18 }}
          rules={[{ required: true }]}
        >
          <Input placeholder={'Enter Aggregator Address'} />
        </Form.Item>
        <Form.Item
          label="Payment Token"
          name="paymentToken"
          labelCol={{ span: 3 }}
          wrapperCol={{ span: 18 }}
          rules={[{ required: true }]}
        >
          <Input placeholder={'Enter Payment Token'} />
        </Form.Item>
      </Form>
    </Drawer>
  );
};

export default Editor;
