import { useGetListChainLink } from 'api/admin';
import { PaginatedTableAndSearch } from 'components/PaginatedTableAndSearch';
import { useAppDispatch } from 'hooks/reduxHook';
import { FC, useMemo, useState } from 'react';
import { setWeightId } from 'store/ducks/system/slice';
import { debounce } from 'utils/js';
import { Permission } from 'utils/permission';
import { getColumns } from './Column';
import { IListType } from './type';

const List: FC<IListType> = ({ isUpdate }) => {
  const dispatch = useAppDispatch();
  const [params, setParams] = useState({ sort: [] });
  const debouncedSetParams = useMemo(() => debounce(setParams, 500), []);

  const submitParams = (value: any) => {
    debouncedSetParams((prevParams: any) => ({
      ...prevParams,
      ...value,
      sort: value.sort ? `[${value.sort}]` : [],
    }));
  };
  const columns = useMemo(() => getColumns(dispatch, submitParams, isUpdate), [getColumns, isUpdate]);

  return (
    <>
      <PaginatedTableAndSearch
        permission={Permission.scmt_setting_verify}
        title="List Chain Link"
        setEditingId={setWeightId}
        columns={columns}
        useQuery={useGetListChainLink}
        requestParams={params}
        handleReset={() => setParams({ sort: [] })}
      />
    </>
  );
};

export default List;
