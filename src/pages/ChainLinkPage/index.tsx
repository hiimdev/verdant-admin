import { useAppSelector } from 'hooks/reduxHook';
import { getWeightId } from 'store/ducks/system/slice';
import Editor from './components/Editor';
import List from './components/List';

function Allowance() {
  const weightId = useAppSelector(getWeightId);

  return (
    <>
      <List isUpdate={true} />
      {weightId !== null && <Editor />}
    </>
  );
}

export default Allowance;
