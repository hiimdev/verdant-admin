import { AnyAction, Dispatch } from '@reduxjs/toolkit';
import { Button, Image, Popconfirm, Space } from 'antd';
import { AiFillEdit, AiOutlineDelete, AiOutlineEye, AiOutlineInfoCircle } from 'react-icons/ai';
import { COLLECTION_SORT } from 'utils/constant';
import { timeFormatter } from 'utils/date';
import styles from './styles.module.less';
import { setCollectionId, setEditingId, setViewId } from 'store/ducks/collection/slice';
import { TableTitle } from 'components/Table/component/TableTitle';
import { useCollectionCountNtfs } from 'api/collection';
import { setDeletingId } from 'store/ducks/administrator/slice';
import { useState } from 'react';

export const getColumns = (dispatch: Dispatch<AnyAction>, mutate: any, submitParams?: any, isUpdate?: boolean) => {
  return [
    {
      title: 'No',
      dataIndex: 'index',
      key: 'index',
      align: 'center',
      width: '80px',
    },
    {
      title: (
        <TableTitle
          name="name"
          label="Collection name"
          onSubmit={submitParams}
          sort={[COLLECTION_SORT.NAME_DESC, COLLECTION_SORT.NAME_ASC]}
        />
      ),
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: (
        <TableTitle
          name="NFTsMinted"
          label="Total NFTs Minted"
          onSubmit={(values: any) =>
            submitParams({ ...values, NFTsMinted: values.NFTsMinted ? Number(values.NFTsMinted) : undefined })
          }
          sort={[COLLECTION_SORT.NFTSMINTED_DESC, COLLECTION_SORT.NFTSMINTED_ASC]}
        />
      ),
      dataIndex: 'NFTsMinted',
      key: 'NFTsMinted',
      render(value: any) {
        return <>{value ? value : '-'}</>;
      },
      align: 'center',
    },
    {
      title: (
        <TableTitle
          name="NFTsTraded"
          label="Total NFTs Traded"
          onSubmit={(values: any) =>
            submitParams({ ...values, NFTsTraded: values.NFTsTraded ? Number(values.NFTsTraded) : undefined })
          }
          sort={[COLLECTION_SORT.NFTSTRADED_DESC, COLLECTION_SORT.NFTSTRADED_ASC]}
        />
      ),
      dataIndex: 'NFTsTraded',
      key: 'NFTsTraded',
      render(value: any) {
        return <>{value ? value : '-'}</>;
      },
      align: 'center',
    },
    {
      title: (
        <TableTitle
          name="description"
          label="Description"
          onSubmit={submitParams}
          sort={[COLLECTION_SORT.DESCRIPTION_DESC, COLLECTION_SORT.DESCRIPTION_ASC]}
        />
      ),
      dataIndex: 'description',
      key: 'description',
      render(description: string) {
        return <div className={styles.description}>{description}</div>;
      },
      width: '200px',
    },
    {
      title: 'Logo',
      render({ imageUrl, name }: any) {
        return <Image className={styles.image} src={imageUrl} alt={name} />;
      },
      align: 'center',
    },
    {
      title: (
        <TableTitle
          name="minPrice"
          label="Lowest Seller Offer"
          onSubmit={(values: any) =>
            submitParams({ ...values, minPrice: values.minPrice ? Number(values.minPrice) : undefined })
          }
          sort={[COLLECTION_SORT.PRICEMIN_DESC, COLLECTION_SORT.PRICEMIN_ASC]}
        />
      ),
      dataIndex: 'minPrice',
      key: 'minPrice',
      render(value: any) {
        return <>{value || '-'}</>;
      },
      align: 'center',
    },
    {
      title: (
        <TableTitle
          name="maxPrice"
          label="Highest Seller Offer"
          onSubmit={(values: any) =>
            submitParams({ ...values, maxPrice: values.maxPrice ? Number(values.maxPrice) : undefined })
          }
          sort={[COLLECTION_SORT.PRICEMAX_DESC, COLLECTION_SORT.PRICEMAX_ASC]}
        />
      ),
      key: 'maxPrice',
      dataIndex: 'maxPrice',
      render(value: any) {
        return <>{value || '-'}</>;
      },
      align: 'center',
    },
    {
      title: (
        <TableTitle
          name="brandName"
          label="Brand"
          onSubmit={submitParams}
          sort={[COLLECTION_SORT.BRAND_DESC, COLLECTION_SORT.BRAND_ASC]}
        />
      ),
      key: 'brandName',
      dataIndex: 'brandName',
      render(value: any) {
        return <>{value ? value : '-'}</>;
      },
    },
    {
      title: (
        <TableTitle
          type={'none'}
          label="Created at"
          onSubmit={submitParams}
          name="createdAt"
          sort={[COLLECTION_SORT.CREATEDAT_DESC, COLLECTION_SORT.CREATEDAT_ASC]}
        />
      ),
      dataIndex: 'createdAt',
      key: 'createdAt',
      render(createdAt: string) {
        return timeFormatter(Number(createdAt));
      },
      align: 'center',
    },
    {
      title: (
        <TableTitle
          label="Created By"
          onSubmit={submitParams}
          name="createdBy"
          sort={[COLLECTION_SORT.CREATEDBY_DESC, COLLECTION_SORT.CREATEDBY_ASC]}
        />
      ),
      dataIndex: 'createdBy',
      key: 'createdBy',
      align: 'center',
      render(data: string) {
        return data || '-';
      },
    },
    {
      title: (
        <TableTitle
          type={'none'}
          label="Update at"
          onSubmit={submitParams}
          name="updatedAt"
          sort={[COLLECTION_SORT.UPDATEAT_DESC, COLLECTION_SORT.UPDATEAT_ASC]}
        />
      ),
      dataIndex: 'updatedAt',
      key: 'updatedAt',
      render(value: string) {
        return timeFormatter(Number(value));
      },
    },
    {
      title: (
        <TableTitle
          label="Updated By"
          onSubmit={submitParams}
          name="updatedBy"
          sort={[COLLECTION_SORT.UPDATEDBY_DESC, COLLECTION_SORT.UPDATEDBY_ASC]}
        />
      ),
      dataIndex: 'updatedBy',
      key: 'updatedBy',
      align: 'center',
      render(data: string) {
        return data || '-';
      },
    },
    {
      title: 'Action',
      align: 'center',
      dataIndex: 'id',
      fixed: 'right',
      width: '200px',
      render(id: any) {
        const { data, refetch, status } = useCollectionCountNtfs(id as string, { enabled: false });
        const [visible, setVisible] = useState(false);

        const handleVisibleChange = (e: any) => {
          setVisible(e);
          if (e) {
            refetch();
          }
        };
        return (
          <Space>
            <Button
              className={styles.btnAction}
              icon={<AiOutlineInfoCircle />}
              onClick={() => dispatch(setCollectionId(String(id)))}
              title="NFT List"
            />
            <Button
              className={styles.btnAction}
              icon={<AiOutlineEye />}
              type="primary"
              onClick={() => dispatch(setViewId(String(id)))}
              title="View"
            />
            {isUpdate && (
              <>
                <Button
                  className={styles.btnAction}
                  icon={<AiFillEdit />}
                  onClick={() => dispatch(setEditingId(String(id)))}
                  title="Edit"
                />
                {Number(data?.totalNfts) > 0 ? (
                  <Popconfirm
                    title={
                      status === 'success'
                        ? `In this collection, there are ${Number(
                            data?.totalNfts
                          )} NFTs that haven't been burned. They must be burned before this collection is removed!`
                        : 'Loading...'
                    }
                    okText="OK"
                    onConfirm={() => {
                      dispatch(setDeletingId(null));
                    }}
                    onVisibleChange={handleVisibleChange}
                    visible={visible}
                  >
                    <Button icon={<AiOutlineDelete />} danger title="Delete" />
                  </Popconfirm>
                ) : (
                  <Popconfirm
                    title={status === 'success' ? 'Are you sure you want to delete this collection?' : 'Loading...'}
                    okText="Yes"
                    cancelText="No"
                    onConfirm={() => {
                      mutate(id);
                    }}
                    onVisibleChange={handleVisibleChange}
                    visible={visible}
                  >
                    <Button icon={<AiOutlineDelete />} danger title="Delete" />
                  </Popconfirm>
                )}
              </>
            )}
          </Space>
        );
      },
    },
  ];
};
