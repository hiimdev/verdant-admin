import { Form } from 'antd';
import { useCollectionCountNtfs, useCollectionQuery } from 'api/collection';
import { addCollection, updateCollection } from 'api/collection/request';
import { useGetBrandCommission } from 'api/commission';
import { IError } from 'api/types';
import { Drawer } from 'components/Drawer';
import { Input, InputTextArea } from 'components/Input';
import { Select } from 'components/Select';
import { Upload } from 'components/Upload';
import { useAppDispatch, useAppSelector } from 'hooks/reduxHook';
import { FC, useEffect, useMemo } from 'react';
import { AiOutlinePlus } from 'react-icons/ai';
import { useMutation, useQueryClient } from 'react-query';
import { getEditingId, setEditingId } from 'store/ducks/collection/slice';
import { convertToFormData, getUploadValueProps } from 'utils/form';
import { message } from 'utils/message';
import { IListType } from './type';

const LIST_TOKEN = [{ value: 'USDC', title: 'USDC' }];
const { Option } = Select;

const Editor: FC<IListType> = ({ isUpdate: canUpdate }) => {
  const dispatch = useAppDispatch();
  const editingId = useAppSelector(getEditingId);
  const client = useQueryClient();
  const { data } = useCollectionQuery(editingId as string, { enabled: !!editingId });
  const action = editingId ? 'edit' : 'add';

  const [form] = Form.useForm();

  const { data: listBrand } = useGetBrandCommission({ page: 1, limit: 50 });

  const isUpdate = useMemo(() => {
    return (canUpdate && editingId !== '') || editingId === '';
  }, []);

  useEffect(() => {
    form.resetFields();
    if (editingId !== null) {
      form.setFieldsValue({ ...data, image: data?.imageUrl });
    }
  }, [data]);

  const { mutate: create, isLoading: loadingCreate } = useMutation(addCollection, {
    onSuccess: () => {
      client.invalidateQueries('/collection-admin/paging_filter');
      message.success('Create successfully!');
      handleClose();
    },
    onError: (error: IError) => {
      message.error(error?.meta.message[0]);
    },
  });

  const { mutate: update, isLoading: loadingUpdate } = useMutation(updateCollection, {
    onSuccess: () => {
      client.invalidateQueries('/collection-admin/paging_filter');
      message.success('Update successfully!');
      handleClose();
    },
    onError: (error: IError) => {
      message.error(error?.meta.message[0]);
    },
  });

  const handleClose = () => {
    dispatch(setEditingId(null));
  };

  const onFinish = (value: any) => {
    const mutate = action === 'edit' ? update : create;
    const params =
      action === 'edit'
        ? { id: editingId, ...value }
        : { ...value, name: value.name.trim(), description: value.description.trim() };
    mutate(convertToFormData(params));
  };

  const handleChange = (file: File, field: string) => {
    form.setFieldsValue({ [field]: file });
  };
  const { data: collectionCountNfts } = useCollectionCountNtfs(editingId as string);

  return (
    <Drawer
      name="Collection"
      editingId={editingId}
      handleClose={handleClose}
      form={form}
      getLoading={false}
      submitLoading={loadingCreate || loadingUpdate}
      viewOnly={!isUpdate}
    >
      <Form form={form} onFinish={onFinish}>
        <Form.Item
          label="Name Collection"
          name="name"
          rules={[{ required: true }, { min: 3, max: 100 }]}
          labelCol={{ span: 3 }}
          wrapperCol={{ span: 18 }}
        >
          <Input disabled={!isUpdate} placeholder="Collection name" maxLength={100} />
        </Form.Item>
        <Form.Item
          label="Description"
          name="description"
          labelCol={{ span: 3 }}
          wrapperCol={{ span: 18 }}
          rules={[{ required: true }]}
        >
          <InputTextArea
            disabled={!isUpdate}
            placeholder="Provide a detailed description of your collection. "
            maxLength={500}
          />
        </Form.Item>

        <Form.Item
          label="Payment tokens"
          name="paymentToken"
          labelCol={{ span: 3 }}
          wrapperCol={{ span: 18 }}
          rules={[{ required: true }]}
        >
          <Select disabled={!isUpdate} placeholder="Choose your token">
            {LIST_TOKEN.map((item, index) => (
              <Option key={index} value={item.value}>
                {item.title}
              </Option>
            ))}
          </Select>
        </Form.Item>
        <Form.Item
          name="image"
          label="Logo"
          labelCol={{ span: 3 }}
          wrapperCol={{ span: 18 }}
          getValueProps={getUploadValueProps}
          rules={[{ required: true }]}
        >
          <Upload
            disabled={!isUpdate}
            listType="picture-card"
            onChangeFile={(file: File) => handleChange(file, 'image')}
            imageUrl={data?.imageUrl}
          >
            <AiOutlinePlus />
          </Upload>
        </Form.Item>
        <Form.Item
          label="Brand"
          name="brandId"
          rules={[{ required: true }]}
          labelCol={{ span: 3 }}
          wrapperCol={{ span: 18 }}
        >
          <Select disabled={!isUpdate || Number(collectionCountNfts?.totalNfts) > 0} placeholder="Choose your brand">
            {listBrand?.list?.map((item: any, index: number) => (
              <Option key={index} value={item.brandId}>
                {item.brandName}
              </Option>
            ))}
          </Select>
        </Form.Item>
      </Form>
    </Drawer>
  );
};

export default Editor;
