import { Modal } from 'antd';
import { useNFTsQueryByCollection } from 'api/nft';
import { PaginatedTableAndSearch } from 'components/PaginatedTableAndSearch';
import { useAppDispatch, useAppSelector } from 'hooks/reduxHook';
import { FC, useMemo } from 'react';
import { convertPriceEther } from 'utils/common';
import { getCollectionId, setCollectionId } from 'store/ducks/collection/slice';

const getColumns = () => {
  return [
    {
      title: 'Token ID',
      dataIndex: 'token_id',
      key: 'tokenId',
    },
    {
      title: 'Name',
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: 'Edition',
      dataIndex: 'edition',
      key: 'edition',
    },
    {
      title: 'Status',
      dataIndex: 'is_onsale',
      key: 'is_onsale',
      render(is_onsale: any) {
        return <>{is_onsale ? 'On Sale' : 'Not On Sale'}</>;
      },
    },
    {
      title: 'Last Traded Price',
      dataIndex: 'price',
      key: 'price',
      render(price: any) {
        return <>{price ? convertPriceEther(price) : '-'}</>;
      },
    },
    {
      title: 'Collection',
      dataIndex: 'collection_name',
      key: 'collectionName',
    },
    {
      title: 'Owner',
      dataIndex: 'owner_name',
      key: 'ownerName',
    },
  ];
};

const Nfts: FC = () => {
  const dispatch = useAppDispatch();
  const collectionId = useAppSelector(getCollectionId);
  const columns = useMemo(() => getColumns(), [getColumns]);

  const visible = collectionId !== null;

  // useEffect(() => {
  //   dispatch(setRequestParams(params));
  // }, [params]);

  const handleClose = () => {
    dispatch(setCollectionId(null));
  };

  return (
    <Modal
      width={1100}
      visible={visible}
      title={`Collection ID: ${collectionId}`}
      onOk={handleClose}
      onCancel={handleClose}
      cancelButtonProps={{ hidden: true }}
    >
      <PaginatedTableAndSearch
        title="Nfts"
        columns={columns}
        useQuery={useNFTsQueryByCollection}
        requestParams={{ collectionId }}
      />
    </Modal>
  );
};

export default Nfts;
