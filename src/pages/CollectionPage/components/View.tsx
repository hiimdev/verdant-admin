import { Descriptions, Image, Modal } from 'antd';
import { useCollectionQuery } from 'api/collection';
import { useAppDispatch, useAppSelector } from 'hooks/reduxHook';
import { FC } from 'react';
import { getViewId, setViewId } from 'store/ducks/collection/slice';
import styles from './styles.module.less';

const View: FC = () => {
  const dispatch = useAppDispatch();
  const editingId = useAppSelector(getViewId);
  const { data, isLoading } = useCollectionQuery(editingId as string, { enabled: !!editingId });

  const visible = editingId !== null && editingId !== '';

  const handleClose = () => {
    dispatch(setViewId(null));
  };

  if (isLoading) {
    return null;
  }

  return (
    <Modal
      width={768}
      visible={visible}
      title={`Collection: ${data?.name || ''}`}
      onOk={handleClose}
      onCancel={handleClose}
      cancelButtonProps={{ hidden: true }}
    >
      {data && (
        <Descriptions labelStyle={{ width: '1px' }} column={1} bordered className={styles.previewDescriptions}>
          <Descriptions.Item label="Image">
            {data.imageUrl && <Image src={data.imageUrl} alt={'image'} className={styles.image} />}
          </Descriptions.Item>
          <Descriptions.Item label="Collection">{data.name || '-'}</Descriptions.Item>
          <Descriptions.Item label="Brand">{data.brandName || '-'}</Descriptions.Item>
          <Descriptions.Item label="Description">{data.description || '-'}</Descriptions.Item>
          {/* <Descriptions.Item label="Payment token">{data.paymentToken || '-'}</Descriptions.Item> */}
          <Descriptions.Item label="Number of Minted NFTs">{data.numberNft || 0}</Descriptions.Item>
          <Descriptions.Item label="Min listing Price">{data.minPrice || '-'}</Descriptions.Item>
          <Descriptions.Item label="Max listing Price">{data.maxPrice || '-'}</Descriptions.Item>
        </Descriptions>
      )}
    </Modal>
  );
};

export default View;
