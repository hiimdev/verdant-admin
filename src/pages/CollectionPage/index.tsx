import { useAppSelector } from 'hooks/reduxHook';
import { FC, useMemo } from 'react';
import { getEditingId, getViewId, getCollectionId } from 'store/ducks/collection/slice';
import { getPermission } from 'store/ducks/user/slice';
import { Permission } from 'utils/permission';
import Editor from './components/Editor';
import List from './components/List';
import Nfts from './components/Nfts';
import View from './components/View';

const Collection: FC = () => {
  const permissions = useAppSelector(getPermission);
  const editingId = useAppSelector(getEditingId);
  const collectionId = useAppSelector(getCollectionId);
  const viewId = useAppSelector(getViewId);

  const isUpdate = useMemo(() => {
    return permissions.includes(Permission.edit_collection);
  }, [permissions]);

  return (
    <>
      <List isUpdate={isUpdate} />
      {viewId && <View />}
      {collectionId && <Nfts />}
      {editingId !== null && <Editor isUpdate={isUpdate} />}
    </>
  );
};

export default Collection;
