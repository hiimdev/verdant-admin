import { CloseCircleOutlined, InfoCircleOutlined, SaveOutlined } from '@ant-design/icons';
import { AnyAction, Dispatch } from '@reduxjs/toolkit';
import { Button, Space } from 'antd';
import {
  IBrandsCommissionResponse,
  ICategoriesCommissionResponse,
  ICollectionsCommissionResponse,
} from 'api/commission';
import { IVendorsResponse } from 'api/user/types';
import { Input } from 'components/Input';
import { TableTitle } from 'components/Table/component/TableTitle';
import { AiFillEdit } from 'react-icons/ai';
import { setLogId } from 'store/ducks/commission/slice';
import { COMMISSION_SORT } from 'utils/constant';
import { typeNumber } from 'utils/form';
import styles from './styles.module.less';

export const getColumns = (
  dispatch: Dispatch<AnyAction>,
  idEdit: any,
  setIdEdit: any,
  setValueBuyer: any,
  setValueSeller: any,
  listCategoryCommission?: ICategoriesCommissionResponse,
  listBrand?: IBrandsCommissionResponse,
  listCollection?: ICollectionsCommissionResponse,
  listVendor?: IVendorsResponse,
  editingId?: any | null,
  submitParams?: any,
  onEdit?: any,
  isUpdate?: boolean,
  valueBuyer?: any,
  valueSeller?: any
) => {
  return [
    {
      title: (
        <TableTitle
          label="Category"
          onSubmit={submitParams}
          name="category_commission_id"
          sort={[COMMISSION_SORT.CATEGORY_DESC, COMMISSION_SORT.CATEGORY_ASC]}
        />
      ),
      dataIndex: 'category_commission_id',
      key: 'category_commission_id',
      align: 'center',
      width: '200px',
      render(data: any, record: any) {
        return <>{record.category_name}</>;
      },
    },
    {
      title: (
        <TableTitle
          label="Brand"
          onSubmit={submitParams}
          name="brand_id"
          sort={[COMMISSION_SORT.BRAND_DESC, COMMISSION_SORT.BRAND_ASC]}
        />
      ),
      dataIndex: 'brand_id',
      key: 'brand_id',
      align: 'center',
      width: '200px',
      render(data: any, record: any) {
        return <>{record?.brand_name || '-'}</>;
      },
    },
    {
      title: 'Collection',
      dataIndex: 'collection_id',
      key: 'collection_id',
      align: 'center',
      width: '200px',
      render(data: any, record: any) {
        return <>{record.collection_name || '-'}</>;
      },
    },
    {
      title: 'Vendor',
      dataIndex: 'vendor_id',
      key: 'vendor_id',
      align: 'center',
      render(data: any, record: any) {
        return <>{data ? record?.vendor_name : '-'}</>;
      },
    },
    {
      title: 'Buyer Service Fees',
      key: 'buyer_commission',
      align: 'center',
      render(data: any, record: any) {
        return idEdit === record.id ? (
          <div style={{ margin: '6px 0' }}>
            <Input
              step={0.1}
              type="number"
              suffix="%"
              min={0}
              onChange={(e) => {
                setValueBuyer(Number(e.target.value) * 100);
                if (!valueSeller) {
                  setValueSeller(record.seller_commission);
                }
              }}
              defaultValue={data.buyer_commission / 100}
              onKeyDown={(evt) => {
                typeNumber(evt);
              }}
            />
          </div>
        ) : (
          <>{data.buyer_commission / 100}%</>
        );
      },
    },
    {
      title: 'Seller Service Fees',
      key: 'seller_commission',
      align: 'center',
      render(data: any, record: any) {
        return idEdit === record.id ? (
          <div style={{ margin: '6px 0' }}>
            <Input
              step={0.1}
              type="number"
              suffix="%"
              min={0}
              onChange={(e) => {
                if (!valueBuyer) {
                  setValueBuyer(record.buyer_commission);
                }
                setValueSeller(Number(e.target.value) * 100);
              }}
              defaultValue={data.seller_commission / 100}
              onKeyDown={(evt) => {
                typeNumber(evt);
              }}
            />
          </div>
        ) : (
          <>{data.seller_commission / 100}%</>
        );
      },
    },
    {
      title: 'Log',
      align: 'center',
      width: '150px',
      fixed: 'right',
      render(data: any, record: any) {
        return idEdit === record.id ? (
          <Space>
            <Button
              icon={<SaveOutlined />}
              className={styles.btnAction}
              onClick={() => {
                onEdit(data);
              }}
              title="Save"
            />

            <Button
              icon={<CloseCircleOutlined />}
              className={styles.btnAction}
              onClick={() => {
                setIdEdit(null);
                setValueBuyer();
                setValueSeller();
              }}
              title="Cancel"
            />
          </Space>
        ) : (
          <Space>
            {isUpdate && (
              <Button
                icon={<AiFillEdit />}
                className={styles.btnAction}
                onClick={() => {
                  setIdEdit(record.id);
                }}
                type="primary"
                title="Edit"
              />
            )}
            <Button
              title="Log"
              icon={<InfoCircleOutlined />}
              onClick={() =>
                dispatch(
                  setLogId({
                    category_commission_id: record.category_commission_id,
                    collection_id: record.collection_id,
                    brand_id: record.brand_id,
                  })
                )
              }
              className={styles.btnAction}
            />
          </Space>
        );
      },
    },
  ];
};
