import { useContractFunction } from '@usedapp/core';
import { Form } from 'antd';
import {
  createCommission,
  IBrandCommissionResponse,
  ICategoryCommissionResponse,
  ICollectionCommissionResponse,
  ICommissionRequest,
  useGetBrandCommission,
  useGetCategoryCommission,
  useGetCollectionCommission,
} from 'api/commission';
import { IError } from 'api/types';
import { Drawer } from 'components/Drawer';
import { InputNumber, InputTextArea } from 'components/Input';
import { Select } from 'components/Select';
import MarketABI from 'contracts/MarketABI.json';
import { useAppDispatch, useAppSelector } from 'hooks/reduxHook';
import { useConnectABI } from 'hooks/useConnectABI';
import { useWallet } from 'hooks/useWallet';
import { FC, useEffect, useMemo } from 'react';
import { useMutation, useQueryClient } from 'react-query';
import { getEditingId, setEditingId } from 'store/ducks/commission/slice';
import { getLoadingButtonCallSMCT } from 'utils/common';
import { CONTRACT_ADDRESS, MARKET_ADDRESS } from 'utils/constant';
import { typeNumber } from 'utils/form';
import { message } from 'utils/message';
import { IListType } from './type';

const Editor: FC<IListType> = () => {
  const dispatch = useAppDispatch();
  const editingId = useAppSelector(getEditingId);

  const client = useQueryClient();
  const { account } = useWallet();

  const [form] = Form.useForm();

  const { data: catagory, refetch: refetchCatagory } = useGetCategoryCommission({ page: 1, limit: 50 });

  const { data: listBrand, refetch: refetchBrand } = useGetBrandCommission(
    form.getFieldValue('categoryCommissionId')
      ? {
          page: 1,
          limit: 50,
          categoryId: form.getFieldValue('categoryCommissionId'),
        }
      : { page: 1, limit: 50 }
  );

  const { data: listCollection, refetch: refetchCollection } = useGetCollectionCommission(
    form.getFieldValue('categoryCommissionId') || form.getFieldValue('brand_id')
      ? {
          page: 1,
          limit: 50,
          categoryId: form.getFieldValue('categoryCommissionId'),
          brandId: form.getFieldValue('brand_id') ? form.getFieldValue('brand_id').split('-')[0] : undefined,
        }
      : { page: 1, limit: 50 }
  );

  const { send: sendDefaultServiceFee, state: stateSetDefaultFee } = useContractFunction(
    useConnectABI(MarketABI, CONTRACT_ADDRESS.MARKET),
    'setSystemFee'
  );

  const { send, state: stateSetCommission } = useContractFunction(
    useConnectABI(MarketABI, CONTRACT_ADDRESS.MARKET),
    'setCommissions'
  );

  const { mutate: create, isLoading: loadingCreate } = useMutation(createCommission, {
    onSuccess: (data: any) => {
      if (data.categoryCommissionId) {
        send(
          [data.categoryCommissionId],
          [data.brandId || 0],
          [data.collectionId || 0],
          [data.vendorId ?? '0x0000000000000000000000000000000000000000'],
          [data.sellerCommission],
          [data.buyerCommission]
        );
      } else {
        sendDefaultServiceFee(data.sellerCommission, data.buyerCommission, 0, 0);
      }
    },
    onError: (error: IError) => {
      message.error(error?.meta.message[0]);
    },
  });
  const handleClose = () => {
    dispatch(setEditingId(null));
    form.resetFields();
  };

  const onFinish = (value) => {
    if (account !== MARKET_ADDRESS) {
      message.info('Please login metamask wallet account');
      return;
    }
    const request: ICommissionRequest = {
      ...value,
      brandId: value.brandData ? +value.brandData.split('-')[0] : null,
      collectionId: value.collectionData ? +value.collectionData.split('-')[0] : null,
      action: 'add',
      vendorId: editingId?.vendorId,
      sellerCommission: Number(value.sellerCommission) * 100 || 0,
      buyerCommission: Number(value.buyerCommission) * 100 || 0,
    };
    create(request);
  };

  useEffect(() => {
    if (stateSetCommission.status === 'Success' || stateSetDefaultFee.status === 'Success') {
      message.success('Add successfully!');
      client.invalidateQueries('/commission/list');
      handleClose();
    } else if (
      stateSetCommission.status === 'Fail' ||
      stateSetDefaultFee.status === 'Fail' ||
      stateSetCommission.status === 'Exception' ||
      stateSetDefaultFee.status === 'Exception'
    ) {
      message.error(stateSetCommission.errorMessage || 'Error');
    }
  }, [stateSetCommission, stateSetDefaultFee]);

  const loadingCommission = useMemo(() => {
    return (
      loadingCreate === true ||
      getLoadingButtonCallSMCT(stateSetCommission) ||
      getLoadingButtonCallSMCT(stateSetDefaultFee)
    );
  }, [stateSetCommission, stateSetDefaultFee, loadingCreate]);
  return (
    <Drawer
      viewOnly={false}
      name="Service Fees"
      editingId={editingId}
      handleClose={handleClose}
      form={form}
      getLoading={loadingCommission}
      submitLoading={loadingCreate || getLoadingButtonCallSMCT(stateSetCommission)}
    >
      <Form form={form} onFinish={onFinish}>
        <Form.Item
          rules={[{ required: true }]}
          label="Buyer Service Fees"
          labelCol={{ span: 3 }}
          name="buyerCommission"
        >
          <InputNumber
            addonAfter="%"
            onKeyDown={(evt) => {
              typeNumber(evt);
            }}
            type="number"
            placeholder="Buyer Service Fees"
          />
        </Form.Item>
        <Form.Item
          rules={[{ required: true }]}
          label="Seller Service Fees"
          labelCol={{ span: 3 }}
          name="sellerCommission"
        >
          <InputNumber
            addonAfter="%"
            onKeyDown={(evt) => {
              typeNumber(evt);
            }}
            type="number"
            placeholder="Seller Service Fees"
          />
        </Form.Item>

        <Form.Item label="Category" name="categoryCommissionId" labelCol={{ span: 3 }}>
          <Select
            placeholder="Select Category"
            style={{ width: '100%' }}
            onChange={() => {
              form.setFieldsValue({ brandData: null, collectionData: null });
            }}
            onClick={() => {
              refetchCatagory();
            }}
          >
            {catagory?.list?.map((item: ICategoryCommissionResponse) => (
              <Select.Option key={item.categoryId} value={item.categoryId}>
                {item.categoryName}
              </Select.Option>
            ))}
          </Select>
        </Form.Item>
        <Form.Item label="Brand" name="brandData" labelCol={{ span: 3 }}>
          <Select
            placeholder="Select Brand"
            style={{ width: '100%' }}
            onChange={(value) => {
              form.setFieldsValue({ categoryCommissionId: Number(value.split('-')[1]), collection_id: null });
            }}
            onClick={() => {
              refetchBrand();
            }}
          >
            {listBrand?.list?.map((item: IBrandCommissionResponse) => (
              <Select.Option key={item.brandId} value={`${item.brandId}-${item.categoryId}`}>
                {item.brandName}
              </Select.Option>
            ))}
          </Select>
        </Form.Item>
        <Form.Item label="Collection" name="collectionData" labelCol={{ span: 3 }}>
          <Select
            placeholder="Select collection"
            style={{ width: '100%' }}
            onChange={(val: string) => {
              const split_arr = val.split('-');
              form.setFieldsValue({
                brandData: `${split_arr[1]}-${split_arr[2]}`,
                categoryCommissionId: Number(split_arr[2]) ? Number(split_arr[2]) : null,
              });
            }}
            onClick={() => {
              refetchCollection();
            }}
          >
            {listCollection?.list?.map((item: ICollectionCommissionResponse) => (
              <Select.Option key={item.collectionId} value={`${item.collectionId}-${item.brandId}-${item.categoryId}`}>
                {item.collectionName}
              </Select.Option>
            ))}
          </Select>
        </Form.Item>
        {/* <Form.Item label="Vendor" name="vendorId" labelCol={{ span: 3 }}>
          <Select
            placeholder="Select Category"
            style={{ width: '100%' }}
            onChange={() => {
              form.setFieldsValue({ brandData: null, collectionData: null });
            }}
          >
            {listCategoryCommission?.list?.map((item: ICategoryCommissionResponse) => (
              <Select.Option key={item.categoryId} value={item.categoryId}>
                {item.categoryName}
              </Select.Option>
            ))}
          </Select>
        </Form.Item> */}
        <Form.Item name="description" labelCol={{ span: 3 }} label="Description">
          <InputTextArea placeholder={'Enter your description'} autoSize={{ minRows: 3 }} />
        </Form.Item>
      </Form>
    </Drawer>
  );
};

export default Editor;
