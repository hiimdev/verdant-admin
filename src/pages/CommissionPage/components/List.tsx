import { useContractFunction } from '@usedapp/core';
import { Form, Spin } from 'antd';
import {
  createCommission,
  ICommissionRequest,
  useGetBrandCommission,
  useGetCategoryCommission,
  useGetCollectionCommission,
  useGetCommission,
} from 'api/commission';
import { IError } from 'api/types';
import { useGetListVendorFilter } from 'api/user';
import { PaginatedTableAndSearch } from 'components/PaginatedTableAndSearch';
import MarketABI from 'contracts/MarketABI.json';
import { useAppDispatch, useAppSelector } from 'hooks/reduxHook';
import { useConnectABI } from 'hooks/useConnectABI';
import { useWallet } from 'hooks/useWallet';
import { FC, useEffect, useMemo, useState } from 'react';
import { useMutation, useQueryClient } from 'react-query';
import { getEditingId, setEditingId } from 'store/ducks/commission/slice';
import { getLoadingButtonCallSMCT } from 'utils/common';
import { CONTRACT_ADDRESS, MARKET_ADDRESS } from 'utils/constant';
import { message } from 'utils/message';
import { Permission } from 'utils/permission';
import { getColumns } from './Column';
import { IListType } from './type';

const List: FC<IListType> = ({ isUpdate }) => {
  const dispatch = useAppDispatch();
  const [form] = Form.useForm();
  const { data } = useGetCommission({ page: 1, limit: 10 });
  const [params, setParams] = useState({ name: '' });
  const editingId = useAppSelector(getEditingId);
  const { account } = useWallet();
  const [idEdit, setIdEdit] = useState(null);
  const [valueBuyer, setValueBuyer] = useState();
  const [valueSeller, setValueSeller] = useState();

  const client = useQueryClient();
  const { data: listVendor } = useGetListVendorFilter({ page: 1, limit: 50 });
  const { data: listCategoryCommission } = useGetCategoryCommission({ page: 1, limit: 50 });
  const { data: listCollection } = useGetCollectionCommission(
    editingId?.action === 'add' && editingId?.category_commission_id
      ? { page: 1, limit: 50, category_id: editingId?.category_commission_id, brand_id: editingId?.brand_id || null }
      : { page: 1, limit: 50 }
  );

  const { data: listBrand } = useGetBrandCommission(
    editingId?.action === 'add' && editingId?.category_commission_id
      ? { page: 1, limit: 50, category_id: editingId?.category_commission_id }
      : { page: 1, limit: 50 }
  );

  const { send, state: stateSetCommission } = useContractFunction(
    useConnectABI(MarketABI, CONTRACT_ADDRESS.MARKET),
    'setCommissions'
  );
  const { send: sendDefaultServiceFee, state: stateSetDefaultFee } = useContractFunction(
    useConnectABI(MarketABI, CONTRACT_ADDRESS.MARKET),
    'setSystemFee'
  );

  const { mutate: update, isLoading: isLoadingUpdateCommission } = useMutation(createCommission, {
    onSuccess: (data: any) => {
      if (data.categoryCommissionId) {
        send(
          [data.categoryCommissionId],
          [data.brandId || 0],
          [data.collectionId || 0],
          [data.vendorId || '0x0000000000000000000000000000000000000000'],
          [data.sellerCommission],
          [data.buyerCommission]
        );
      } else {
        sendDefaultServiceFee(Number(data.sellerCommission), Number(data.buyerCommission), 0, 0);
      }
    },
    onError: (error: IError, data) => {
      message.error(error?.meta.message[0]);
    },
  });

  const onEdit = (data) => {
    if (account !== MARKET_ADDRESS) {
      message.info('Please login metamask wallet account');
      return;
    }
    const request: ICommissionRequest = {
      buyerCommission: Number(valueBuyer),
      sellerCommission: Number(valueSeller),
      categoryCommissionId: data.category_commission_id,
      collectionId: data.collection_id,
      brandId: data.brand_id,
      action: 'edit',
      vendorId: data.vendor_id,
    };
    update(request);
  };
  const columns = useMemo(
    () =>
      getColumns(
        dispatch,
        idEdit,
        setIdEdit,
        setValueBuyer,
        setValueSeller,
        listCategoryCommission,
        listBrand,
        listCollection,
        listVendor,
        editingId,
        () => {},
        onEdit,
        isUpdate,
        valueBuyer,
        valueSeller
      ),
    [listCategoryCommission, listBrand, listCollection, valueBuyer, valueSeller, listVendor, idEdit, isUpdate]
  );

  useEffect(() => {
    if (data && data?.list.length > 0) {
      form.setFieldsValue({ commission: data?.list[0].commission });
    }
  }, [data]);

  const loadingCommission = useMemo(() => {
    return (
      isLoadingUpdateCommission === true ||
      getLoadingButtonCallSMCT(stateSetCommission) ||
      getLoadingButtonCallSMCT(stateSetDefaultFee)
    );
  }, [stateSetCommission, stateSetDefaultFee, isLoadingUpdateCommission]);

  useEffect(() => {
    if (stateSetCommission.status === 'Success' || stateSetDefaultFee.status === 'Success') {
      client.invalidateQueries('/commission/list');
      message.success('Update service fees success');
      setIdEdit(null);
    } else if (
      ['Exception', 'Fail'].includes(stateSetDefaultFee.status) ||
      ['Exception', 'Fail'].includes(stateSetCommission.status)
    ) {
      message.error(stateSetCommission.errorMessage || stateSetDefaultFee.errorMessage || 'Error');
    }
  }, [stateSetCommission, stateSetDefaultFee]);

  return (
    <Spin spinning={loadingCommission}>
      {/* <Header title="" setEditingId={setEditingId} viewOnly>
        <Space>
          <Input
            placeholder="Category Name"
            onChange={(ev: any) =>
              debouncedSetParams((prevParams: any) => ({
                ...prevParams,
                category_name: ev.target.value,
              }))
            }
          />
          <Input
            placeholder="Brand Name"
            onChange={(ev: any) =>
              debouncedSetParams((prevParams: any) => ({
                ...prevParams,
                brand_name: ev.target.value,
              }))
            }
          />
          <Input
            placeholder="Collection Name"
            onChange={(ev: any) =>
              debouncedSetParams((prevParams: any) => ({
                ...prevParams,
                collection_name: ev.target.value,
              }))
            }
          />
          <Button size="large" onClick={() => toggleModal(true)}>
            Withdraw
          </Button>
        </Space>
      </Header> */}
      <PaginatedTableAndSearch
        permission={Permission.add_service_fees}
        title="Service Fees"
        setEditingId={setEditingId}
        columns={columns}
        useQuery={useGetCommission}
        requestParams={params}
        handleReset={() => setParams({ name: '' })}
      />
    </Spin>
  );
};

export default List;
