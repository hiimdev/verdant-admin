import { Modal } from 'antd';
import { PaginatedTableAndSearch } from 'components/PaginatedTableAndSearch';
import { useAppDispatch, useAppSelector } from 'hooks/reduxHook';
import { FC, useMemo } from 'react';
import { getLogId, setLogId } from 'store/ducks/commission/slice';
import { timeFormatter } from 'utils/date';
import { useGetHistoryCommission } from 'api/commission';

const getColumns = () => {
  return [
    {
      title: 'Update date',
      dataIndex: 'updated_at',
      key: 'updated_at',
      render(value: string) {
        return timeFormatter(Number(value));
      },
      align: 'center' as const,
    },
    {
      title: 'Update by',
      dataIndex: 'updated_by',
      key: 'updated_by',
      align: 'center' as const,
      render(data: string) {
        return data ? data : '-';
      },
    },
    {
      title: 'Category',
      key: 'category_name',
      dataIndex: 'category_name',
      align: 'center' as const,
      render(data: string) {
        return data ? data : '-';
      },
    },
    {
      title: 'Brand',
      key: 'brand_name',
      dataIndex: 'brand_name',
      align: 'center' as const,
      render(data: string) {
        return data ? data : '-';
      },
    },
    {
      title: 'Collection',
      key: 'collection_name',
      dataIndex: 'collection_name',
      align: 'center' as const,
      render(data: string) {
        return data ? data : '-';
      },
    },
    {
      title: 'Vendor',
      key: 'vendor_name',
      dataIndex: 'vendor_name',
      align: 'center' as const,
      width: '150px',
      render(data: string) {
        return data ? data : '-';
      },
    },
    {
      title: 'Buyer Service Fees',
      dataIndex: 'buyer_commission',
      key: 'buyer_commission',
      align: 'center' as const,
      width: '160px',
      render(data: any) {
        return <>{parseFloat(data || 0) / 100}%</>;
      },
    },
    {
      title: 'Seller Service Fees',
      dataIndex: 'seller_commission',
      key: 'seller_commission',
      align: 'center' as const,
      width: '160px',
      render(data: any) {
        return <>{parseFloat(data || 0) / 100}%</>;
      },
    },
    {
      title: 'Status',
      dataIndex: 'status',
      key: 'status',
      align: 'center',
      render(value: string) {
        return <>{value ? 'Active' : 'Deactivate'}</>;
      },
    },
  ];
};

const Log: FC = () => {
  const dispatch = useAppDispatch();
  const logId = useAppSelector(getLogId);
  const columns = useMemo(() => getColumns(), [getColumns]);

  const visible = logId !== null;
  const params = useMemo(() => {
    let requestParams = { page: 1, limit: 10, ...logId };
    if (!logId.brand_id) {
      delete requestParams.brand_id;
    }
    if (!logId.collection_id) {
      delete requestParams.collection_id;
    }
    return requestParams;
  }, [logId]);

  const handleClose = () => {
    dispatch(setLogId(null));
  };
  return (
    <Modal
      width={1100}
      visible={visible}
      title={`History`}
      onOk={handleClose}
      onCancel={handleClose}
      cancelButtonProps={{ hidden: true }}
    >
      <PaginatedTableAndSearch columns={columns} useQuery={() => useGetHistoryCommission(params)} />
    </Modal>
  );
};

export default Log;
