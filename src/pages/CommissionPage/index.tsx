import { useAppSelector } from 'hooks/reduxHook';
import { FC, useMemo } from 'react';
import { getEditingId, getLogId } from 'store/ducks/commission/slice';
import { getPermission } from 'store/ducks/user/slice';
import { Permission } from 'utils/permission';
import Editor from './components/Editor';
// import Editor from './components/Editor';
import List from './components/List';
import Log from './components/Log';

const WhiteList: FC = () => {
  const permissions = useAppSelector(getPermission);
  const editingId = useAppSelector(getEditingId);
  const logId = useAppSelector(getLogId);
  const isUpdate = useMemo(() => {
    return permissions.includes(Permission.edit_service_fee);
  }, [permissions]);
  return (
    <>
      <List isUpdate={isUpdate} />
      {editingId !== null && <Editor />}
      {logId && <Log />}
    </>
  );
};

export default WhiteList;
