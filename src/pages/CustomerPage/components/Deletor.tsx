import { Modal } from 'antd';
import { useAppDispatch, useAppSelector } from 'hooks/reduxHook';
import { FC } from 'react';
import { useMutation, useQueryClient } from 'react-query';
import { message } from 'utils/message';
import { IError } from 'api/types';
import { getDeletingId, setDeletingId } from 'store/ducks/faqCategory/slice';
import { useFaqCategoryQuery } from 'api/faqCategory';
import { deleteFaqCategory } from 'api/faqCategory/request';
const Deletor: FC = () => {
  const dispatch = useAppDispatch();
  const deletingId = useAppSelector(getDeletingId);
  const client = useQueryClient();

  const { data, status: getLoading } = useFaqCategoryQuery(deletingId as string, { enabled: !!deletingId });

  const handleClose = () => {
    dispatch(setDeletingId(null));
  };

  const { mutate: mutateDelete, isLoading: deleteLoading } = useMutation(deleteFaqCategory, {
    onSuccess: () => {
      client.invalidateQueries('/articles/list-category');
      message.success('Delete successfully!');
      handleClose();
    },
    onError: (error: IError) => {
      message.error(error?.meta.message[0]);
    },
  });

  const onDelete = () => {
    mutateDelete(deletingId as string);
  };

  return (
    <>
      <Modal
        visible={!!deletingId}
        title={`Delete FAQ Category: ${data?.id}`}
        okButtonProps={{
          loading: getLoading === 'loading' || deleteLoading,
        }}
        cancelButtonProps={{
          disabled: deleteLoading,
        }}
        onCancel={handleClose}
        onOk={onDelete}
        okType="danger"
        okText="Confirm"
      >
        Are you sure you want to delete this FAQ Category?
      </Modal>
    </>
  );
};

export default Deletor;
