import { Form } from 'antd';
import { useFaqCategoryQuery } from 'api/faqCategory';
import { createFaqCategory, updateFaqCategory } from 'api/faqCategory/request';
import { IError } from 'api/types';
import { Drawer } from 'components/Drawer';
import { Input, InputTextArea } from 'components/Input';
import { useAppDispatch, useAppSelector } from 'hooks/reduxHook';
import { FC, useEffect } from 'react';
import { useMutation, useQueryClient } from 'react-query';
import { getEditingId, setEditingId } from 'store/ducks/faqCategory/slice';
import { rules } from 'utils/form';
import { message } from 'utils/message';

const Editor: FC = () => {
  const dispatch = useAppDispatch();
  const editingId = useAppSelector(getEditingId);
  const client = useQueryClient();

  const [form] = Form.useForm();

  const action = editingId ? 'edit' : 'add';

  const { data, status: getLoading } = useFaqCategoryQuery(editingId as string, { enabled: !!editingId });

  const { mutate: update, isLoading: loadingUpdate } = useMutation(updateFaqCategory, {
    onSuccess: () => {
      client.invalidateQueries('/articles/list-category');
      message.success('Update successfully!');
      handleClose();
    },
    onError: (error: IError) => {
      message.error(error?.meta.message[0]);
    },
  });

  const { mutate: create, isLoading: loadingCreate } = useMutation(createFaqCategory, {
    onSuccess: () => {
      client.invalidateQueries('/articles/list-category');
      message.success('Create successfully!');
      handleClose();
    },
    onError: (error: IError) => {
      message.error(error?.meta.message[0]);
    },
  });

  useEffect(() => {
    form.resetFields();
    if (editingId !== null) {
      form.setFieldsValue({ subject: 'subject', description: 'description' });
    }
  }, [data]);

  const handleClose = () => {
    dispatch(setEditingId(null));
  };

  const onFinish = (value: any) => {
    const mutate = action === 'edit' ? update : create;
    const params = action === 'edit' ? { id: editingId, ...value } : value;
    mutate(params);
  };

  return (
    <Drawer
      name="Customer Care"
      editingId={editingId}
      handleClose={handleClose}
      form={form}
      getLoading={getLoading === 'loading'}
      submitLoading={loadingCreate || loadingUpdate}
    >
      <Form form={form} onFinish={onFinish}>
        <Form.Item label="Subject" name="subject" labelCol={{ span: 3 }} rules={[rules]}>
          <Input placeholder={'Enter your category name'} />
        </Form.Item>
        <Form.Item label="Description" name="description" labelCol={{ span: 3 }} rules={[rules]}>
          <InputTextArea placeholder={'Enter your category name'} />
        </Form.Item>
      </Form>
    </Drawer>
  );
};

export default Editor;
