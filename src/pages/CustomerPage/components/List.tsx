import { AnyAction, Dispatch } from '@reduxjs/toolkit';
import { Space, Tooltip, Button as AntdButton } from 'antd';
import { useFaqCategoriesQuery } from 'api/faqCategory';
import { Button } from 'components/Button';
import { Header } from 'components/Header';
import { PaginatedTableAndSearch } from 'components/PaginatedTableAndSearch';
import { SearchFilter, SelectFilter } from 'components/Popover';
import { useAppDispatch } from 'hooks/reduxHook';
import { FC, useMemo, useState } from 'react';
import { AiFillEdit, AiOutlineDelete } from 'react-icons/ai';
import { setDeletingId, setEditingId } from 'store/ducks/faqCategory/slice';
import { CATEGORY_FAQ } from 'utils/constant';
import { timeFormatter } from 'utils/date';
import { debounce } from 'utils/js';
import styles from './styles.module.less';

const getColumns = (dispatch: Dispatch<AnyAction>, submitParams?: any) => {
  return [
    {
      title: 'No',
      dataIndex: 'index',
      key: 'index',
      render() {
        return 'Text';
      },
    },
    {
      title: (
        <div className="flex-center-between">
          First Name
          <SearchFilter label="name" onSubmit={submitParams} name="categoryName" />
        </div>
      ),
      dataIndex: 'categoryName',
      key: 'categoryName',
      render() {
        return 'Text';
      },
    },
    {
      title: (
        <div className="flex-center-between">
          Last Name
          <SearchFilter label="name" onSubmit={submitParams} name="categoryName" />
        </div>
      ),
      dataIndex: 'categoryName',
      key: 'categoryName',
      render() {
        return 'Text';
      },
    },
    {
      title: (
        <div className="flex-center-between">
          Email Name
          <SearchFilter label="name" onSubmit={submitParams} name="categoryName" />
        </div>
      ),
      dataIndex: 'categoryName',
      key: 'categoryName',
      render() {
        return 'Text';
      },
    },
    {
      title: (
        <div className="flex-center-between">
          Subject
          <SearchFilter label="name" onSubmit={submitParams} name="categoryName" />
        </div>
      ),
      dataIndex: 'categoryName',
      key: 'categoryName',
      render() {
        return 'Text';
      },
    },
    {
      title: (
        <div className="flex-center-between">
          Message
          <SearchFilter label="name" onSubmit={submitParams} name="categoryName" />
        </div>
      ),
      dataIndex: 'categoryName',
      key: 'categoryName',
      render() {
        return 'Text';
      },
    },
    {
      title: (
        <div className="flex-center-between">
          Received Time
          <SearchFilter label="name" onSubmit={submitParams} name="categoryName" />
        </div>
      ),
      dataIndex: 'categoryName',
      key: 'categoryName',
      render() {
        return 'Text';
      },
    },
    {
      title: (
        <div className="flex-center-between">
          Status
          <SearchFilter label="name" onSubmit={submitParams} name="categoryName" />
        </div>
      ),
      dataIndex: 'categoryName',
      key: 'categoryName',
      render() {
        return 'Text';
      },
    },
    {
      title: 'Action',
      align: 'center',
      dataIndex: 'id',
      render(id: string) {
        return (
          <Space>
            <Tooltip title="view" placement="top">
              <AntdButton
                className={styles.btnAction}
                icon={<AiFillEdit />}
                type="primary"
                onClick={() => dispatch(setEditingId(id))}
                title="view"
              />
            </Tooltip>
          </Space>
        );
      },
      fixed: 'right',
    },
  ];
};

const List: FC = () => {
  const dispatch = useAppDispatch();
  const [params, setParams] = useState({ sort: [] });

  const debouncedSetParams = useMemo(() => debounce(setParams, 500), []);

  const submitParams = (value: any) => {
    debouncedSetParams((prevParams: any) => ({
      ...prevParams,
      ...value,
      sort: value.sort ? [value.sort] : [],
    }));
  };
  const columns = useMemo(() => getColumns(dispatch, submitParams), [getColumns]);
  return (
    <>
      <Header title="Customer Care" setEditingId={setEditingId} viewOnly></Header>
      <PaginatedTableAndSearch
        columns={columns}
        useQuery={useFaqCategoriesQuery}
        requestParams={params}
        handleReset={() => setParams({ sort: [] })}
      />
    </>
  );
};

export default List;
