import { FC } from 'react';
import Editor from './components/Editor';
import List from './components/List';

const FaqCategory: FC = () => {
  return (
    <>
      <List />
      <Editor />
    </>
  );
};

export default FaqCategory;
