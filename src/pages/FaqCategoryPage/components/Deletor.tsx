import { Modal } from 'antd';
import { useFaqCategoryQuery } from 'api/faqCategory';
import { deleteFaqCategory } from 'api/faqCategory/request';
import { IError } from 'api/types';
import { useAppDispatch, useAppSelector } from 'hooks/reduxHook';
import { FC } from 'react';
import { useMutation, useQueryClient } from 'react-query';
import { getDeletingId, setDeletingId } from 'store/ducks/faqCategory/slice';
import { message } from 'utils/message';
const Deletor: FC = () => {
  const dispatch = useAppDispatch();
  const deletingId = useAppSelector(getDeletingId);
  const client = useQueryClient();

  const { data, isLoading } = useFaqCategoryQuery(deletingId as string, { enabled: !!deletingId });

  const handleClose = () => {
    dispatch(setDeletingId(null));
  };

  const { mutate: mutateDelete, isLoading: deleteLoading } = useMutation(deleteFaqCategory, {
    onSuccess: () => {
      client.invalidateQueries('/articles/paging_filter_category');
      message.success('Delete successfully!');
      handleClose();
    },
    onError: (error: IError) => {
      message.error(error?.meta.message[0]);
    },
  });

  const onDelete = () => {
    mutateDelete(deletingId as string);
  };
  if (isLoading) {
    return null;
  }
  return (
    <>
      <Modal
        visible={!!deletingId}
        title={`FAQ Category: ${data?.id}`}
        okButtonProps={{
          loading: deleteLoading,
        }}
        cancelButtonProps={{
          disabled: deleteLoading,
        }}
        onCancel={handleClose}
        onOk={onDelete}
        okType="danger"
        okText="Confirm"
      >
        Are you sure you want to delete this FAQ Category?
      </Modal>
    </>
  );
};

export default Deletor;
