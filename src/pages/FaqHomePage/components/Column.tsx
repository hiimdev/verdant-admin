import { AnyAction, Dispatch } from '@reduxjs/toolkit';
import { Button as AntdButton, Space } from 'antd';
import { TableTitle } from 'components/Table/component/TableTitle';
import { AiOutlineDelete, AiOutlineDoubleLeft, AiOutlineDown, AiOutlineUp } from 'react-icons/ai';
import { SORT_FAQ } from 'utils/constant';
import styles from './styles.module.less';

export const getColumns = (
  dispatch: Dispatch<AnyAction>,
  submitParams?: any,
  isUpdate?: boolean,
  mutateDelete?: any,
  sort?: any
) => {
  return [
    {
      title: 'No',
      dataIndex: 'index',
      key: 'index',
      align: 'center',
      width: '80px',
    },
    {
      title: (
        <TableTitle
          name="categoryName"
          label="Category name"
          onSubmit={submitParams}
          sort={[SORT_FAQ.CATEGORYNAME_DESC, SORT_FAQ.CATEGORYNAME_ASC]}
        />
      ),
      dataIndex: 'categoryName',
      key: 'categoryName',
      width: '200px',
    },
    // {
    //   title: 'Image',
    //   dataIndex: 'logoUrl',
    //   key: 'logoUrl',
    //   render(data: any) {
    //     return <Image src={data} alt="img" className={styles.image} />;
    //   },
    // },
    {
      title: (
        <TableTitle
          name="question"
          label="Question"
          onSubmit={submitParams}
          sort={[SORT_FAQ.QUESTION_DESC, SORT_FAQ.QUESTION_ASC]}
        />
      ),
      dataIndex: 'question',
      key: 'question',
      render(question: string) {
        return question ? question : '-';
      },
    },
    {
      title: (
        <TableTitle
          name="answer"
          label="Answer"
          onSubmit={submitParams}
          sort={[SORT_FAQ.ANSWER_DESC, SORT_FAQ.ANSWER_ASC]}
        />
      ),
      dataIndex: 'answer',
      key: 'answer',
      render(answer: string) {
        return <div className={styles.description}>{answer ? answer : '-'}</div>;
      },
    },
    {
      title: 'Sort',
      align: 'center',
      dataIndex: 'id',
      render(id: string) {
        return (
          <Space>
            <AntdButton
              className={styles.btnAction}
              icon={<AiOutlineUp />}
              onClick={() => sort({ id: id, sortType: 2 })}
              title="Bring to Front"
            />

            <AntdButton
              className={styles.btnAction}
              icon={<AiOutlineDoubleLeft />}
              style={{ transform: 'rotate(90deg)' }}
              type="primary"
              onClick={() => sort({ id: id, sortType: 1 })}
              title="Bring to Forward"
            />
            <AntdButton
              className={styles.btnAction}
              icon={<AiOutlineDown />}
              onClick={() => sort({ id: id, sortType: 4 })}
              title="Bring to Back"
            />
            <AntdButton
              className={styles.btnAction}
              icon={<AiOutlineDoubleLeft />}
              style={{ transform: 'rotate(-90deg)' }}
              type="primary"
              onClick={() => sort({ id: id, sortType: 3 })}
              title="Bring to Backward"
            />
          </Space>
        );
      },
    },

    {
      title: 'Action',
      align: 'center',
      dataIndex: 'id',
      fixed: 'right',
      render(id: string) {
        return (
          <Space>
            {isUpdate && (
              <AntdButton
                className={styles.btnAction}
                icon={<AiOutlineDelete />}
                danger
                onClick={() => mutateDelete(id)}
                title="Delete"
              />
            )}
          </Space>
        );
      },
    },
  ];
};
