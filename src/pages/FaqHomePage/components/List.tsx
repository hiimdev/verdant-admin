import { useFaqQueryHome } from 'api/faq';
import { deleteFaqHome, sortFAQHome } from 'api/faq/request';
import { IError } from 'api/types';
import { PaginatedTableAndSearch } from 'components/PaginatedTableAndSearch';
import { useAppDispatch } from 'hooks/reduxHook';
import { FC, useMemo, useState } from 'react';
import { useMutation, useQueryClient } from 'react-query';
import { setEditingId } from 'store/ducks/faq/slice';
import { debounce } from 'utils/js';
import { message } from 'utils/message';
import { getColumns } from './Column';
import { IListType } from './type';

const List: FC<IListType> = ({ isUpdate }) => {
  const dispatch = useAppDispatch();
  const [params, setParams] = useState({ sort: [] });
  const debouncedSetParams = useMemo(() => debounce(setParams, 500), []);
  const client = useQueryClient();

  const submitParams = (value: any) => {
    debouncedSetParams((prevParams: any) => ({
      ...prevParams,
      ...value,
      sort: value.sort ? [value.sort] : [],
    }));
  };

  const { mutate: deleteFAQ } = useMutation(deleteFaqHome, {
    onSuccess: () => {
      client.invalidateQueries('/articles/paging-filter-faq-priority');
      message.success('Delete successfully!');
    },
    onError: (error: IError) => {
      message.error(error?.meta.message[0]);
    },
  });

  const { mutate: sort } = useMutation(sortFAQHome, {
    onSuccess: () => {
      client.invalidateQueries('/articles/paging-filter-faq-priority');
      message.success('Update successfully!');
    },
    onError: (error: IError) => {
      message.error(error?.meta.message[0]);
    },
  });

  const columns = useMemo(() => getColumns(dispatch, submitParams, isUpdate, deleteFAQ, sort), [getColumns, isUpdate]);

  return (
    <>
      <PaginatedTableAndSearch
        title="FAQ Home Page"
        setEditingId={setEditingId}
        columns={columns}
        useQuery={useFaqQueryHome}
        requestParams={params}
        handleReset={() => setParams({ sort: [] })}
      />
    </>
  );
};

export default List;
