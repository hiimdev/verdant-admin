import { useAppSelector } from 'hooks/reduxHook';
import { FC, useMemo } from 'react';
import { getPermission } from 'store/ducks/user/slice';
import { Permission } from 'utils/permission';
import List from './components/List';

const FaqHomePage: FC = () => {
  const permissions = useAppSelector(getPermission);

  const isUpdate = useMemo(() => {
    return permissions.includes(Permission.edit_delete_faq);
  }, [permissions]);

  return (
    <>
      <List isUpdate={isUpdate} />
    </>
  );
};

export default FaqHomePage;
