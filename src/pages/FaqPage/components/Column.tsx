import { AnyAction, Dispatch } from '@reduxjs/toolkit';
import { Button as AntdButton, Space } from 'antd';
import { TableTitle } from 'components/Table/component/TableTitle';
import { AiFillEdit, AiFillEye, AiOutlineDelete, AiOutlinePlus } from 'react-icons/ai';
import { setDeletingId, setEditingId } from 'store/ducks/faq/slice';
import { SORT_FAQ } from 'utils/constant';
import { timeFormatter } from 'utils/date';
import styles from './styles.module.less';

export const getColumns = (dispatch: Dispatch<AnyAction>, submitParams?: any, isUpdate?: boolean, muateAdd?: any) => {
  return [
    {
      title: 'No',
      dataIndex: 'index',
      key: 'index',
      align: 'center',
      width: '80px',
    },
    {
      title: (
        <TableTitle
          name="categoryName"
          label="Category name"
          onSubmit={submitParams}
          sort={[SORT_FAQ.CATEGORYNAME_DESC, SORT_FAQ.CATEGORYNAME_ASC]}
        />
      ),
      dataIndex: 'categoryName',
      key: 'categoryName',
    },
    // {
    //   title: 'Image',
    //   dataIndex: 'logoUrl',
    //   key: 'logoUrl',
    //   render(data: any) {
    //     return <Image src={data} alt="img" className={styles.image} />;
    //   },
    // },
    {
      title: (
        <TableTitle
          name="question"
          label="Question"
          onSubmit={submitParams}
          sort={[SORT_FAQ.QUESTION_DESC, SORT_FAQ.QUESTION_ASC]}
        />
      ),
      dataIndex: 'question',
      key: 'question',
      render(question: string) {
        return question ? question : '-';
      },
      width: '200px',
    },
    {
      title: (
        <TableTitle
          name="answer"
          label="Answer"
          onSubmit={submitParams}
          sort={[SORT_FAQ.ANSWER_DESC, SORT_FAQ.ANSWER_ASC]}
        />
      ),
      dataIndex: 'answer',
      key: 'answer',
      render(answer: string) {
        return <div className={styles.description}>{answer ? answer : '-'}</div>;
      },
      width: '200px',
    },
    {
      title: (
        <TableTitle
          type="none"
          name="createdAt"
          label="Create At"
          onSubmit={submitParams}
          sort={[SORT_FAQ.CREATEAT_DESC, SORT_FAQ.CREATEAT_ASC]}
        />
      ),
      dataIndex: 'createdAt',
      key: 'createdAt',
      render(createdAt: string) {
        return timeFormatter(Number(createdAt));
      },
      align: 'center',
    },
    {
      title: (
        <TableTitle
          name="createdBy"
          label="Created By"
          onSubmit={submitParams}
          sort={[SORT_FAQ.CREATED_BY_DESC, SORT_FAQ.CREATED_BY_ASC]}
        />
      ),
      dataIndex: 'createdBy',
      key: 'createdBy',
      align: 'center',
    },
    {
      title: (
        <TableTitle
          type="none"
          name="updatedAt"
          label="Updated at"
          onSubmit={submitParams}
          sort={[SORT_FAQ.UPDATEAT_DESC, SORT_FAQ.UPDATEAT_ASC]}
        />
      ),
      dataIndex: 'updatedAt',
      key: 'updatedAt',
      render(value: string) {
        return timeFormatter(Number(value));
      },
    },
    {
      title: (
        <TableTitle
          name="updatedBy"
          label="Update By"
          onSubmit={submitParams}
          sort={[SORT_FAQ.UPDATED_BY_DESC, SORT_FAQ.UPDATED_BY_ASC]}
        />
      ),
      dataIndex: 'updatedBy',
      key: 'updatedBy',
      align: 'center',
    },
    {
      title: 'Action',
      align: 'center',
      dataIndex: 'id',
      fixed: 'right',
      render(id: string) {
        return (
          <Space>
            <AntdButton
              className={styles.btnAction}
              icon={isUpdate ? <AiFillEdit /> : <AiFillEye />}
              type="primary"
              onClick={() => dispatch(setEditingId(id))}
              title={isUpdate ? 'Edit' : 'View'}
            />
            {isUpdate && (
              <AntdButton
                className={styles.btnAction}
                icon={<AiOutlinePlus />}
                danger
                onClick={() => muateAdd(id)}
                title="Add"
              />
            )}
            {isUpdate && (
              <AntdButton
                className={styles.btnAction}
                icon={<AiOutlineDelete />}
                type="primary"
                onClick={() => dispatch(setDeletingId(id))}
                title="Delete"
              />
            )}
          </Space>
        );
      },
    },
  ];
};
