import { Form } from 'antd';
import { useFaqQuery } from 'api/faq';
import { createFaq, updateFaq } from 'api/faq/request';
import { useFaqCategoriesQuery } from 'api/faqCategory';
import { IError } from 'api/types';
import { Drawer } from 'components/Drawer';
import { Input, InputTextArea } from 'components/Input';
import { Select } from 'components/Select';
import { useAppDispatch, useAppSelector } from 'hooks/reduxHook';
import { FC, useEffect, useMemo } from 'react';
import { useMutation, useQueryClient } from 'react-query';
import { getEditingId, setEditingId } from 'store/ducks/faq/slice';
import { convertFormValue, rules } from 'utils/form';
import { message } from 'utils/message';
import { IListType } from './type';

const Editor: FC<IListType> = ({ isUpdate: canUpdate }) => {
  const dispatch = useAppDispatch();
  const editingId = useAppSelector(getEditingId);
  const client = useQueryClient();

  const [form] = Form.useForm();

  const action = editingId ? 'edit' : 'add';

  const isUpdate = useMemo(() => {
    return (canUpdate && editingId !== '') || editingId === '';
  }, []);

  const { data, status: getLoading } = useFaqQuery(editingId as string, { enabled: !!editingId });

  const { mutate: update, isLoading: loadingUpdate } = useMutation(updateFaq, {
    onSuccess: () => {
      client.invalidateQueries('/articles/list-faq');
      message.success('Update successfully!');
      handleClose();
    },
    onError: (error: IError) => {
      message.error(error?.meta.message[0]);
    },
  });

  const { mutate: create, isLoading: loadingCreate } = useMutation(createFaq, {
    onSuccess: () => {
      client.invalidateQueries('/articles/list-faq');
      message.success('Create successfully!');
      handleClose();
    },
    onError: (error: IError) => {
      message.error(error?.meta.message[0]);
    },
  });

  useEffect(() => {
    form.resetFields();
    if (editingId !== null) {
      form.setFieldsValue(data);
    }
  }, [data]);

  const handleClose = () => {
    dispatch(setEditingId(null));
  };

  const onFinish = (value: any) => {
    const mutate = action === 'edit' ? update : create;
    const params = action === 'edit' ? { id: editingId, ...value } : value;
    mutate(convertFormValue(params));
  };

  const { data: categories, status, refetch } = useFaqCategoriesQuery({ page: 1, limit: 50 });

  useEffect(() => {
    if (editingId !== null) {
      refetch();
    }
  }, [editingId]);

  const handleChange = (file: File, field: string) => {
    form.setFieldsValue({ [field]: file });
  };

  return (
    <Drawer
      name="FAQ"
      editingId={editingId}
      handleClose={handleClose}
      form={form}
      getLoading={getLoading === 'loading' || status === 'loading'}
      submitLoading={loadingCreate || loadingUpdate}
      viewOnly={!isUpdate}
    >
      <Form form={form} onFinish={onFinish}>
        <Form.Item
          label="Category name"
          name="categoryId"
          labelCol={{ span: 3 }}
          wrapperCol={{ span: 18 }}
          rules={[{ required: true }]}
        >
          <Select disabled={!isUpdate} placeholder={'Select category name'}>
            {categories?.list &&
              categories.list.map((category, index: number) => (
                <Select.Option key={index} value={category.id}>
                  {category.categoryName}
                </Select.Option>
              ))}
          </Select>
        </Form.Item>
        <Form.Item label="Question" name="question" labelCol={{ span: 3 }} wrapperCol={{ span: 18 }} rules={[rules]}>
          <Input disabled={!isUpdate} placeholder={'Enter your question'} />
        </Form.Item>
        <Form.Item label="Answer" name="answer" labelCol={{ span: 3 }} wrapperCol={{ span: 18 }} rules={[rules]}>
          <InputTextArea disabled={!isUpdate} placeholder={'Enter your answer'} />
        </Form.Item>
        {/* {editingId && (
          <Form.Item
            name="image"
            label="Logo"
            labelCol={{ span: 3 }}
            wrapperCol={{ span: 18 }}
            getValueProps={getUploadValueProps}
            rules={[{ required: true }]}
          >
            <Upload
              disabled={!isUpdate}
              listType="picture-card"
              onChangeFile={(file: File) => handleChange(file, 'image')}
              imageUrl={data?.logoUrl}
            >
              <AiOutlinePlus />
            </Upload>
          </Form.Item>
        )} */}
      </Form>
    </Drawer>
  );
};

export default Editor;
