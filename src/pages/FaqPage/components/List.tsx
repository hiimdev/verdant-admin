import useBreakpoint from 'antd/lib/grid/hooks/useBreakpoint';
import { useFaqsQuery } from 'api/faq';
import { addFAQHome } from 'api/faq/request';
import { IError } from 'api/types';
import { PaginatedTableAndSearch } from 'components/PaginatedTableAndSearch';
import { useAppDispatch } from 'hooks/reduxHook';
import { FC, useMemo, useState } from 'react';
import { useMutation, useQueryClient } from 'react-query';
import { setEditingId } from 'store/ducks/faq/slice';
import { debounce } from 'utils/js';
import { message } from 'utils/message';
import { Permission } from 'utils/permission';
import { getColumns } from './Column';
import { IListType } from './type';

const List: FC<IListType> = ({ isUpdate }) => {
  const dispatch = useAppDispatch();
  const [params, setParams] = useState({ sort: [] });
  const debouncedSetParams = useMemo(() => debounce(setParams, 500), []);
  const client = useQueryClient();

  const submitParams = (value: any) => {
    debouncedSetParams((prevParams: any) => ({
      ...prevParams,
      ...value,
      sort: value.sort ? [value.sort] : [],
    }));
  };
  const { mutate } = useMutation(addFAQHome, {
    onSuccess: () => {
      client.invalidateQueries('/paging-filter-priority-faq');
      message.success('Add successfully!');
    },
    onError: (error: IError) => {
      message.error(error?.meta.message[0]);
    },
  });
  const columns = useMemo(() => getColumns(dispatch, submitParams, isUpdate, mutate), [getColumns, isUpdate]);
  const screen = useBreakpoint();

  return (
    <>
      <PaginatedTableAndSearch
        permission={Permission.add_faq}
        title="FAQ"
        setEditingId={setEditingId}
        columns={columns}
        useQuery={useFaqsQuery}
        requestParams={params}
        scroll={screen.xxl ? undefined : { x: 1400 }}
        handleReset={() => setParams({ sort: [] })}
      />
    </>
  );
};

export default List;
