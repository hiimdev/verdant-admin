import { AnyAction, Dispatch } from '@reduxjs/toolkit';
import { Button as AntdButton, Space } from 'antd';
import { AiOutlineInfoCircle } from 'react-icons/ai';
import { setWeightId } from 'store/ducks/system/slice';
import { timeFormatter } from 'utils/date';
import styles from './styles.module.less';

export const getColumns = (dispatch: Dispatch<AnyAction>, submitParams?: any, isUpdate?: boolean) => {
  return [
    {
      title: 'No',
      dataIndex: 'index',
      key: 'index',
      align: 'center',
      width: '80px',
    },
    {
      title: 'Deviation percentage',
      dataIndex: 'deviation_percentage',
      key: 'deviation_percentage',
      align: 'center',
      render(data: string) {
        return <>{data}%</>;
      },
    },
    {
      title: 'Status',
      dataIndex: 'status',
      key: 'status',
      align: 'center',
      render(data: string) {
        return data ? 'Active' : 'Inactive';
      },
    },
    {
      title: 'Updated At',
      dataIndex: 'updated_at',
      key: 'updated_at',
      render(created_at: string) {
        return timeFormatter(Number(created_at));
      },
      align: 'center',
    },
    {
      title: 'Action',
      align: 'center',
      dataIndex: 'id',
      fixed: 'right',
      render(id: string) {
        return (
          <Space>
            <AntdButton
              className={styles.btnAction}
              icon={<AiOutlineInfoCircle />}
              type="primary"
              onClick={() => dispatch(setWeightId(id))}
              title={isUpdate ? 'Edit' : 'View'}
            />
          </Space>
        );
      },
    },
  ];
};
