import { Form } from 'antd';
import { useGetFloatPriceDetail } from 'api/admin';
import { setFloatPrice } from 'api/admin/request';
import { IError } from 'api/types';
import { Drawer } from 'components/Drawer';
import { Input } from 'components/Input';
import { useAppDispatch, useAppSelector } from 'hooks/reduxHook';
import { FC, useEffect } from 'react';
import { useMutation, useQueryClient } from 'react-query';
import { getWeightId, setWeightId } from 'store/ducks/system/slice';
import { message } from 'utils/message';
import { IListType } from './type';

const Editor: FC<IListType> = () => {
  const dispatch = useAppDispatch();
  const editingId = useAppSelector(getWeightId);
  const client = useQueryClient();

  // const isUpdate = useMemo(() => {
  //   return (canUpdate && editingId !== '') || editingId === '';
  // }, []);

  const [form] = Form.useForm();

  const { data, status: getLoading } = useGetFloatPriceDetail(editingId as string, { enabled: !!editingId });

  const { mutate: create, isLoading: loadingCreate } = useMutation(setFloatPrice, {
    onSuccess: () => {
      message.success('Successfully.');
      client.invalidateQueries('/chainlink/list-float-price');
      handleClose();
    },
    onError: (error: IError) => {
      message.error(error?.meta.message[0]);
    },
  });

  useEffect(() => {
    form.resetFields();
    if (data) {
      form.setFieldsValue({
        ...data,
      });
    }
  }, [data]);

  const handleClose = () => {
    dispatch(setWeightId(null));
  };

  const onFinish = (value: any) => {
    create(value);
  };

  return (
    <Drawer
      name="Deviation Percentage"
      editingId={editingId}
      handleClose={handleClose}
      form={form}
      getLoading={getLoading === 'loading'}
      submitLoading={loadingCreate}
      viewOnly={!!editingId}
    >
      <Form form={form} onFinish={onFinish}>
        <Form.Item
          label="Deviation Percentage"
          name="deviationPercentage"
          labelCol={{ span: 3 }}
          wrapperCol={{ span: 18 }}
          rules={[{ required: true }]}
        >
          <Input placeholder={'Enter value'} />
        </Form.Item>
      </Form>
    </Drawer>
  );
};

export default Editor;
