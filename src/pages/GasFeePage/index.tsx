import { Button, Form, Row, Typography } from 'antd';
import { useGetGas } from 'api/admin';
import { setGas } from 'api/admin/request';
import { IError } from 'api/types';
import clsx from 'clsx';
import { InputNumber } from 'components/Input';
import { useEffect } from 'react';
import { useMutation, useQueryClient } from 'react-query';
import { typeNumber } from 'utils/form';
import { message } from 'utils/message';
import styles from './styles.module.less';
function GasSetting() {
  const [form] = Form.useForm();
  const client = useQueryClient();
  const { data } = useGetGas();

  const { mutate: setGasMutate, isLoading: loadingGas } = useMutation(setGas, {
    onSuccess: () => {
      client.invalidateQueries('/admin/get-gas');
      message.success('Update gas successfully!');
    },
    onError: (error: IError) => {
      message.error(error?.meta.message[0]);
    },
  });

  const onFinish = (values) => {
    setGasMutate({
      gas: Number(values.gas),
    });
  };

  useEffect(() => {
    if (data?.configValue) {
      form.setFieldsValue({ gas: Number(data?.configValue) });
    }
  }, [data]);
  return (
    <div className={clsx(styles.root)}>
      <div className={clsx(styles.container)}>
        <Typography.Title className={clsx(styles.title)} level={3}>
          Gas Fee setting
        </Typography.Title>
        <Form form={form} onFinish={onFinish} layout="vertical">
          <Form.Item label="Percent increase from Standard gas fee:" name="gas" rules={[{ required: true }]}>
            <InputNumber
              onKeyDown={(evt) => {
                typeNumber(evt);
              }}
              type="number"
              addonAfter="%"
            />
          </Form.Item>
          <Row justify="center">
            <Button htmlType="submit" type="primary" loading={loadingGas}>
              Save
            </Button>
          </Row>
        </Form>
      </div>
    </div>
  );
}

export default GasSetting;
