import { Card, Col, Row } from 'antd';
import { useGetGasTankWallet } from 'api/statistical';
import clsx from 'clsx';
import { Input } from 'components/Input';
import { useGetTokenBalance_ } from 'hooks/useGetBalance';
import { convertPriceEther } from 'utils/common';
import styles from './styles.module.less';

function GasTankWallet() {
  const { data: master } = useGetGasTankWallet();
  const balanceMatic = useGetTokenBalance_(master || '');

  return (
    <div className={clsx(styles.root)}>
      <Card className={styles.content} title={`Balance of ${master}`}>
        <Row align="middle" gutter={[16, 16]}>
          <Col span={24} style={{ textAlign: 'right' }}>
            <Input style={{ textAlign: 'right' }} value={convertPriceEther(balanceMatic.value)} suffix="MATIC" />
          </Col>
        </Row>
      </Card>
    </div>
  );
}

export default GasTankWallet;
