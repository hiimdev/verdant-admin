import { Modal } from 'antd';
import { useAppDispatch, useAppSelector } from 'hooks/reduxHook';
import { FC } from 'react';
import { useMutation, useQueryClient } from 'react-query';
import { message } from 'utils/message';
import { IError } from 'api/types';
import { useCollectionQuery } from 'api/collection';
import { removeCollectionFromHotList } from 'api/collection/request';
import { getDeletingId, setDeletingId } from 'store/ducks/hotCollection/slice';
const Deletor: FC = () => {
  const dispatch = useAppDispatch();
  const deletingId = useAppSelector(getDeletingId);
  const client = useQueryClient();

  const { data, status: getLoading } = useCollectionQuery(deletingId as string, { enabled: !!deletingId });

  const handleClose = () => {
    dispatch(setDeletingId(null));
  };

  const { mutate: mutateDelete, isLoading: deleteLoading } = useMutation(removeCollectionFromHotList, {
    onSuccess: () => {
      client.invalidateQueries('/collection-admin/list-collection-hot-admin');
      message.success('Delete successfully!');
      handleClose();
    },
    onError: (error: IError) => {
      message.error(error?.meta.message[0]);
    },
  });

  const onDelete = () => {
    mutateDelete(deletingId as string);
  };

  return (
    <>
      <Modal
        visible={!!deletingId}
        title={`Delete: ${data?.name || ''}`}
        okButtonProps={{
          loading: getLoading === 'loading' || deleteLoading,
        }}
        cancelButtonProps={{
          disabled: deleteLoading,
        }}
        onCancel={handleClose}
        onOk={onDelete}
        okType="danger"
        okText="Confirm"
      >
        Are you sure you want to remove this Collection from list Hot Collection?
      </Modal>
    </>
  );
};

export default Deletor;
