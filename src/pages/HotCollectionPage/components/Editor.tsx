import { Form } from 'antd';
import { useListChooseCollection } from 'api/collection';
import { collectionAddListAdmin } from 'api/collection/request';
import { IError } from 'api/types';
import { Drawer } from 'components/Drawer';
import { Input } from 'components/Input';
import { Select } from 'components/Select';
import { useAppDispatch, useAppSelector } from 'hooks/reduxHook';
import { FC } from 'react';
import { useMutation, useQueryClient } from 'react-query';
import { getEditingId, setEditingId } from 'store/ducks/hotCollection/slice';
import { message } from 'utils/message';

const Editor: FC = () => {
  const dispatch = useAppDispatch();
  const editingId = useAppSelector(getEditingId);
  const client = useQueryClient();
  const { data: chooseCollections } = useListChooseCollection({ page: 1, limit: 100 });

  const [form] = Form.useForm();

  const { mutate, isLoading } = useMutation(collectionAddListAdmin, {
    onSuccess: () => {
      client.invalidateQueries('/collection-admin/list-collection-hot-admin');
      message.success('Update successfully!');
      handleClose();
    },
    onError: (error: IError) => {
      message.error(error?.meta.message[0]);
    },
  });

  const handleClose = () => {
    dispatch(setEditingId(null));
  };

  const onFinish = (value: any) => {
    mutate(value);
  };

  return (
    <Drawer
      name="Hot collection"
      editingId={editingId}
      handleClose={handleClose}
      form={form}
      getLoading={false}
      submitLoading={isLoading}
    >
      <Form form={form} onFinish={onFinish}>
        <Form.Item label="Collection" name="collectionId" labelCol={{ span: 3 }} rules={[{ required: true }]}>
          <Select placeholder="Select collection" style={{ width: '100%' }}>
            {chooseCollections?.list?.map((item) => (
              <Select.Option key={item.id} value={item.id}>
                {item.name}
              </Select.Option>
            ))}
          </Select>
        </Form.Item>
        <Form.Item label="Index" name="indexShow" labelCol={{ span: 3 }} rules={[{ required: true }]}>
          <Input placeholder={'Enter your index'} />
        </Form.Item>
      </Form>
    </Drawer>
  );
};

export default Editor;
