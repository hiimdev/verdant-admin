import { AnyAction, Dispatch } from '@reduxjs/toolkit';
import { Image, Space, Switch, Button as AntdButton, Tooltip } from 'antd';
import { useGetIsShow, useHotCollectionsQueryAdmin, useHotCollectionsQueryDB } from 'api/collection';
import { updateShowCollection } from 'api/collection/request';
import { IError } from 'api/types';
import clsx from 'clsx';
import { Header } from 'components/Header';
import { PaginatedTableAndSearch } from 'components/PaginatedTableAndSearch';
import { useAppDispatch } from 'hooks/reduxHook';
import { FC, useEffect, useMemo, useState } from 'react';
import { AiOutlineDelete, AiOutlineInfoCircle } from 'react-icons/ai';
import { FiExternalLink } from 'react-icons/fi';
import { useMutation } from 'react-query';
import { setCollectionId, setDeletingId, setEditingId } from 'store/ducks/hotCollection/slice';
import { message } from 'utils/message';
import styles from './styles.module.less';

const getColumns = (dispatch: Dispatch<AnyAction>, type: string) => {
  return [
    {
      title: 'Top',
      dataIndex: 'index',
      key: 'index',
    },
    {
      title: 'Index',
      dataIndex: 'index_show',
      key: 'index_show',
      render(value: any, record: any, index: any) {
        return <>{value || index}</>;
      },
    },
    {
      title: 'Name collection',
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: 'Brand Name',
      dataIndex: 'brand_name',
      key: 'brand_name',
      align: 'center',
      width: '140px',
      render(value: any) {
        return <>{value || '-'}</>;
      },
    },
    {
      title: 'Image',
      render({ image_url, name }: any) {
        return <Image className={styles.image} src={image_url} alt={name} />;
      },
      align: 'center',
    },
    {
      title: 'Action',
      align: 'center',
      width: '250px',
      render({ id }: any) {
        return (
          <Space>
            <a
              href={`https://digicap-var-nftmartketplace-frontend.vercel.app/collection/${id}`}
              target="_blank"
              rel="noopener noreferrer"
              className={clsx(styles.btnAction, styles.link)}
            >
              <FiExternalLink />
            </a>
            <Tooltip title="NFTs" placement="top">
              <AntdButton
                className={styles.btnAction}
                icon={<AiOutlineInfoCircle />}
                onClick={() => dispatch(setCollectionId(String(id)))}
                type="primary"
              />
            </Tooltip>
            {type === 'admin' && (
              <Tooltip title="Delete" placement="top">
                <AntdButton
                  className={styles.btnAction}
                  icon={<AiOutlineDelete />}
                  danger
                  onClick={() => dispatch(setDeletingId(id))}
                />
              </Tooltip>
            )}
          </Space>
        );
      },
      fixed: 'right',
    },
  ];
};

const List: FC = () => {
  const dispatch = useAppDispatch();
  const [type, setType] = useState<'db' | 'admin'>('db');
  const columns = useMemo(() => getColumns(dispatch, type), [getColumns, type]);
  const { data: dataShow } = useGetIsShow();
  const [check, setCheck] = useState<any>(0);

  useEffect(() => {
    setCheck(dataShow);
    setType(dataShow ? 'admin' : 'db');
  }, [dataShow]);

  const useHotCollection = useMemo(() => {
    return type === 'db' ? useHotCollectionsQueryDB : type === 'admin' && useHotCollectionsQueryAdmin;
  }, [type]);

  const onChange = (e: any) => {
    setType(e ? 'admin' : 'db');
    mutate({ isShow: e });
    setCheck(e);
  };

  const { mutate } = useMutation(updateShowCollection, {
    onSuccess: () => {},
    onError: (error: IError) => {
      message.error(error?.meta.message[0]);
    },
  });

  return (
    <>
      <Header title="Hot Collection" setEditingId={setEditingId} viewOnly={type === 'db'}>
        According to the chart <Switch onChange={onChange} checked={check} />
      </Header>
      <PaginatedTableAndSearch columns={columns} useQuery={useHotCollection} />
    </>
  );
};

export default List;
