import { Button, Space } from 'antd';
import { moveDownCollection, moveUpCollection } from 'api/collection/request';
import { IError } from 'api/types';
import { FC } from 'react';
import { AiFillCaretDown, AiFillCaretUp } from 'react-icons/ai';
import { useMutation, useQueryClient } from 'react-query';
import { message } from 'utils/message';
import styles from './styles.module.less';

type IMoveAction = {
  id: string;
  index: number;
  total: number;
};

export const MoveAction: FC<IMoveAction> = ({ id, index, total }) => {
  const client = useQueryClient();

  const { mutate: moveUp, isLoading: loadingMoveUp } = useMutation(moveUpCollection, {
    onSuccess: () => {
      client.invalidateQueries('/collection-admin/list-collection-hot-db');
      message.success('Move up successfully!');
    },
    onError: (error: IError) => {
      message.error(error?.meta.message[0]);
    },
  });

  const { mutate: moveDown, isLoading: loadingMoveDown } = useMutation(moveDownCollection, {
    onSuccess: () => {
      client.invalidateQueries('/collection-admin/list-collection-hot-db');
      message.success('Move down successfully!');
    },
    onError: (error: IError) => {
      message.error(error?.meta.message[0]);
    },
  });

  const move = (isUp: boolean) => {
    const mutation = isUp ? moveUp : moveDown;
    mutation(id);
  };

  return (
    <Space>
      <Button
        className={styles.btnAction}
        icon={<AiFillCaretUp />}
        type="primary"
        onClick={() => move(true)}
        title="Up"
        disabled={index === 1}
        loading={loadingMoveUp}
      />

      <Button
        className={styles.btnAction}
        icon={<AiFillCaretDown />}
        type="primary"
        onClick={() => move(false)}
        title="Down"
        disabled={index === total}
        loading={loadingMoveDown}
      />
    </Space>
  );
};
