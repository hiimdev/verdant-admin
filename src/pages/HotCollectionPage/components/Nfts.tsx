import { Modal } from 'antd';
import { useNFTsQueryByCollection } from 'api/nft';
import { PaginatedTableAndSearch } from 'components/PaginatedTableAndSearch';
import { useAppDispatch, useAppSelector } from 'hooks/reduxHook';
import { FC, useMemo } from 'react';
import { convertPriceEther } from 'utils/common';
import { getCollectionId, setCollectionId } from 'store/ducks/hotCollection/slice';

const getColumns = () => {
  return [
    {
      title: 'Token ID',
      dataIndex: 'token_id',
      key: 'tokenId',
    },
    {
      title: 'Name',
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: 'Edition',
      dataIndex: 'edition',
      key: 'edition',
    },
    {
      title: 'Status',
      dataIndex: 'is_onsale',
      key: 'is_onsale',
      render(is_onsale: any) {
        return <>{is_onsale ? 'On Sale' : 'Not On Sale'}</>;
      },
    },
    {
      title: 'Price',
      dataIndex: 'price',
      key: 'price',
      render(price: any) {
        return <>{price ? convertPriceEther(price) : '-'}</>;
      },
    },
    {
      title: 'Collection',
      dataIndex: 'collection_name',
      key: 'collectionName',
    },
    {
      title: 'Owner',
      dataIndex: 'owner_name',
      key: 'ownerName',
    },
  ];
};

const Nfts: FC = () => {
  const dispatch = useAppDispatch();
  const collecionId = useAppSelector(getCollectionId);
  const columns = useMemo(() => getColumns(), [getColumns]);

  const visible = collecionId !== null;

  const handleClose = () => {
    dispatch(setCollectionId(null));
  };

  return (
    <Modal
      width={1100}
      visible={visible}
      title={`NFT ID: ${collecionId}`}
      onOk={handleClose}
      onCancel={handleClose}
      cancelButtonProps={{ hidden: true }}
    >
      <PaginatedTableAndSearch columns={columns} useQuery={useNFTsQueryByCollection} requestParams={{ collecionId }} />
    </Modal>
  );
};

export default Nfts;
