import { FC } from 'react';
import Deletor from './components/Deletor';
import Editor from './components/Editor';
import List from './components/List';
import Nfts from './components/Nfts';

const HotCollection: FC = () => {
  return (
    <>
      <List />
      <Editor />
      <Deletor />
      <Nfts />
    </>
  );
};

export default HotCollection;
