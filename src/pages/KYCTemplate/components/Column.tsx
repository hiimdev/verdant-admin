import { CheckOutlined, PlusOutlined } from '@ant-design/icons';
import { AnyAction, Dispatch } from '@reduxjs/toolkit';
import { Button } from 'antd';
import { IKYCTemplate } from 'api/kycTemplate/types';
import { TableTitle } from 'components/Table/component/TableTitle';
import { PROMOCODE_TYPE_ENUM } from 'utils/constant';

export const getColumns = (
  dispatch: Dispatch<AnyAction>,
  submitParams?: any,
  isUpdate?: boolean,
  mutateSetTemplate?: any,
  templateLoading?: boolean
) => {
  return [
    {
      title: 'No',
      dataIndex: 'index',
      key: 'index',
      align: 'center',
    },
    {
      title: <TableTitle type={'input'} label="Description" onSubmit={submitParams} name="description" />,
      dataIndex: 'description',
      key: 'description',
      align: 'center',
      render(data: any) {
        return data;
      },
    },
    {
      title: <TableTitle type={'input'} label="Is Sign Up Template" onSubmit={submitParams} name="isSignUpTemplate" />,
      key: 'isSignUpTemplate',
      dataIndex: 'isSignUpTemplate',
      align: 'center',
      render(data: any) {
        return data ? 'Yes' : 'No';
      },
    },
    {
      title: <TableTitle label="Name" onSubmit={submitParams} name="name" />,
      dataIndex: 'name',
      key: 'name',
      align: 'center',
    },
    {
      title: (
        <TableTitle
          type="input"
          label="Sign Up Method Email"
          onSubmit={(values: any) =>
            submitParams({ ...values, discountType: values.discountType ? Number(values.discountType) : undefined })
          }
          name="signUpMethodEmail"
        />
      ),
      dataIndex: 'signUpMethodEmail',
      key: 'signUpMethodEmail',
      align: 'center',
      render(data: any) {
        return data ? 'Yes' : 'No';
      },
    },
    {
      title: (
        <TableTitle
          type="input"
          label="Sign Up Method Wallet"
          name="signUpMethodWallet"
          onSubmit={(values: any) =>
            submitParams({ ...values, promoType: values.promoType ? Number(values.promoType) : undefined })
          }
        />
      ),
      key: 'signUpMethodWallet',
      dataIndex: 'signUpMethodWallet',
      align: 'center',
      render(data: any) {
        return data ? 'Yes' : 'No';
      },
    },
    {
      title: (
        <TableTitle
          type="input"
          label="Template Id"
          name="templateId"
          obj={PROMOCODE_TYPE_ENUM}
          onSubmit={(values: any) =>
            submitParams({ ...values, promoType: values.promoType ? Number(values.promoType) : undefined })
          }
        />
      ),
      key: 'templateId',
      dataIndex: 'templateId',
      align: 'center',
      render(data: any) {
        return data;
      },
    },
    {
      title: <TableTitle type="input" label="Type" name="type" onSubmit={submitParams} />,
      dataIndex: 'type',
      key: 'type',
      align: 'center',
      render(data: any) {
        return data;
      },
    },

    {
      title: 'Action',
      align: 'center',
      render(data: IKYCTemplate & { index?: number; total?: number }) {
        return (
          <Button
            loading={templateLoading}
            type={data?.isUsing ? 'primary' : 'default'}
            onClick={() => {
              delete data.isUsing;
              delete data.index;
              delete data.total;
              mutateSetTemplate({
                ...data,
                isSignUpTemplate: data.isSignUpTemplate ? 1 : 0,
                signUpMethodEmail: data.signUpMethodEmail ? 1 : 0,
                signUpMethodWallet: data.signUpMethodWallet ? 1 : 0,
              });
            }}
            icon={data?.isUsing ? <CheckOutlined /> : <PlusOutlined />}
            title={data?.isUsing ? 'Current Active' : 'Set'}
          />
        );
      },
      fixed: 'right',
    },
  ];
};
