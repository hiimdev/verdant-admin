import { setTemplateKYC, useGetKYCTemplate } from 'api/kycTemplate';
import { IError } from 'api/types';
import { PaginatedTableAndSearch } from 'components/PaginatedTableAndSearch';
import { useAppDispatch } from 'hooks/reduxHook';
import { FC, useMemo, useState } from 'react';
import { useMutation, useQueryClient } from 'react-query';
import { debounce } from 'utils/js';
import { message } from 'utils/message';
import { Permission } from 'utils/permission';
import { getColumns } from './Column';
import { IListType } from './type';

const List: FC<IListType> = ({ isUpdate }) => {
  const dispatch = useAppDispatch();
  const [params, setParams] = useState<any>({ sort: [] });
  const debouncedSetParams = useMemo(() => debounce(setParams, 500), []);
  const client = useQueryClient();
  const submitParams = (value: any) => {
    debouncedSetParams((prevParams: any) => ({
      ...prevParams,
      ...value,
      status: value.status ? Number(value.status) : undefined,
      isVendor: value.isVendor ? Number(value.isVendor) : undefined,
      sort: value.sort ? [value.sort] : [],
    }));
  };

  const { mutate: mutateSetTemplate, isLoading: templateLoading } = useMutation(setTemplateKYC, {
    onSuccess: () => {
      client.invalidateQueries('/user-admin/list-templates-kyc');
      message.success('Update template kyc successfully!');
    },
    onError: (error: IError) => {
      message.error(error?.meta.message[0]);
    },
  });

  const columns = useMemo(
    () => getColumns(dispatch, submitParams, isUpdate, mutateSetTemplate, templateLoading),
    [getColumns, isUpdate]
  );

  return (
    <>
      <PaginatedTableAndSearch
        permission={Permission.add_promo_code}
        title="KYC Template Management"
        columns={columns}
        useQuery={useGetKYCTemplate}
        requestParams={params}
        handleReset={() => setParams({ sort: [] })}
      />
    </>
  );
};

export default List;
