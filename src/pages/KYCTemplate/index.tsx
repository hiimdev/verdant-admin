import { useAppSelector } from 'hooks/reduxHook';
import { FC, useMemo } from 'react';
import { getPermission } from 'store/ducks/user/slice';
import { Permission } from 'utils/permission';

import List from './components/List';

const KYCTemplate: FC = () => {
  const permissions = useAppSelector(getPermission);

  // --  Check permission in here
  const isUpdate = useMemo(() => {
    return permissions.includes(Permission.add_template);
  }, [permissions]);
  // ---------------- //
  // {{ FAKE VARIABLE }}

  return (
    <>
      <List isUpdate={isUpdate} />
    </>
  );
};

export default KYCTemplate;
