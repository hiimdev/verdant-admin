import { Button, Form, Row, Typography } from 'antd';
import { useGetLimitTransaction } from 'api/admin';
import { setLimitTransaction } from 'api/admin/request';
import { IError } from 'api/types';
import clsx from 'clsx';
import { InputNumber } from 'components/Input';
import { useEffect } from 'react';
import { useMutation, useQueryClient } from 'react-query';
import { typeNumber } from 'utils/form';
import { message } from 'utils/message';
import styles from './styles.module.less';

function LimitTransactionSetting() {
  const [form] = Form.useForm();
  const { data } = useGetLimitTransaction();
  const client = useQueryClient();

  const { mutate: setTransationLimitMutate, isLoading: loadingTransationLimit } = useMutation(setLimitTransaction, {
    onSuccess: () => {
      client.invalidateQueries('/admin/get-limit-transaction');
      message.success('Update transation limit successfully!');
    },
    onError: (error: IError) => {
      message.error(error?.meta.message[0]);
    },
  });

  const onFinish = (values) => {
    setTransationLimitMutate({
      limit: Number(values.limit),
    });
  };

  useEffect(() => {
    if (data?.configValue) {
      form.setFieldsValue({ limit: Number(data?.configValue) });
    }
  }, [data]);

  return (
    <div className={clsx(styles.root)}>
      <div className={clsx(styles.container)}>
        <Typography.Title className={clsx(styles.title)} level={3}>
          Limit transaction setting
        </Typography.Title>
        <Form form={form} onFinish={onFinish} layout="vertical">
          <Form.Item label="Set transaction limit" name="limit" rules={[{ required: true }]}>
            <InputNumber
              onKeyDown={(evt) => {
                typeNumber(evt);
              }}
              type="number"
            />
          </Form.Item>
          <Row justify="center">
            <Button htmlType="submit" type="primary" loading={loadingTransationLimit}>
              Save
            </Button>
          </Row>
        </Form>
      </div>
    </div>
  );
}

export default LimitTransactionSetting;
