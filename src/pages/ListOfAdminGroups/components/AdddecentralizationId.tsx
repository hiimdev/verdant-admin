import { Form, Table } from 'antd';
import { useGetPermissionForUser, usePermissionItemRolePermission } from 'api/permissions';
import { addPermission } from 'api/permissions/request';
import { IError } from 'api/types';
import clsx from 'clsx';
import { Drawer } from 'components/Drawer';
import { PaginatedTableAndSearch } from 'components/PaginatedTableAndSearch';
import { useAppDispatch, useAppSelector } from 'hooks/reduxHook';
import { IListType } from 'pages/AdministratorPage/components/type';
import { FC, useEffect, useMemo, useState } from 'react';
import { useMutation, useQueryClient } from 'react-query';
import { getAddDecentralizationId, setAddDecentralizationId } from 'store/ducks/administrator/slice';
import { message } from 'utils/message';
import styles from './styles.module.less';

const getColumns = () => {
  return [
    Table.SELECTION_COLUMN,
    {
      title: 'No',
      dataIndex: 'index',
      key: 'index',
      align: 'center',
    },
    {
      title: 'Permission Name',
      dataIndex: 'name',
      key: 'name',
      align: 'left',
    },
  ];
};

const AddDecentralizationId: FC<IListType> = ({ isUpdate }) => {
  const dispatch = useAppDispatch();
  const decentralizationId = useAppSelector(getAddDecentralizationId);
  const client = useQueryClient();
  const { data: listRole, status: getLoading } = usePermissionItemRolePermission(decentralizationId as string, {
    enabled: !!decentralizationId,
  });
  const [state, setState] = useState<any>({ selectedRowKeys: [] });
  useEffect(() => {
    const permissionId =
      listRole?.list.map((item: any) => (item.checked === '1' ? item.permissionId : undefined)) || [];
    setState({ selectedRowKeys: permissionId });
    form.setFieldsValue({ Permission: permissionId });
  }, [listRole]);
  const [form] = Form.useForm();

  const columns = useMemo(() => getColumns(), [getColumns]);
  const handleClose = () => {
    dispatch(setAddDecentralizationId(null));
  };
  const { mutate: update, isLoading: loadingUpdate } = useMutation(addPermission, {
    onSuccess: () => {
      client.invalidateQueries('/permissions/add-permission');
      message.success('Update successfully!');
      handleClose();
    },
    onError: (error: IError) => {
      message.error(error?.meta.message);
    },
  });

  const onFinish = (value: any) => {
    const params = {
      roleId: Number(decentralizationId),
      permissions: [...value.Permission],
    };
    update(params);
  };

  const rowSelection = {
    selectedRowKeys: state.selectedRowKeys,
    onChange: (selectedRowKeys: any) => {
      setState({ selectedRowKeys });
      form.setFieldsValue({ Permission: selectedRowKeys });
    },
    getCheckboxProps: (record: any) => ({
      value: record.id,
    }),
  };

  return (
    <Drawer
      name=""
      form={form}
      editingId={decentralizationId}
      handleClose={handleClose}
      getLoading={getLoading === 'loading' || status === 'loading'}
      submitLoading={loadingUpdate}
      viewOnly={!isUpdate}
    >
      <div className={clsx(styles.forminAdd)}>
        <Form onFinish={onFinish} form={form}>
          <Form.Item name="Permission">
            <PaginatedTableAndSearch
              columns={columns}
              title={'Permission'}
              useQuery={useGetPermissionForUser}
              requestParams={{ page: 1, limit: 100 }}
              extra={null}
              pagination={false}
              rowSelection={rowSelection}
            />
          </Form.Item>
        </Form>
      </div>
    </Drawer>
  );
};

export default AddDecentralizationId;
