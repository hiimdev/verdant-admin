import { AnyAction, Dispatch } from '@reduxjs/toolkit';
import { Space } from 'antd';
import { TableTitle } from 'components/Table/component/TableTitle';
import { AiFillEdit, AiFillEye, AiOutlineDelete, AiOutlineUserAdd } from 'react-icons/ai';
import { setAddDecentralizationId, setDeletingId, setEditingId } from 'store/ducks/administrator/slice';
import { timeFormatter } from 'utils/date';
import { Button as AntdButton } from 'antd';
import styles from './styles.module.less';
import { ADMIN_STATUS_ENUMS, ROLE_SORT } from 'utils/constant';

export const getColumns = (dispatch: Dispatch<AnyAction>, submitParams?: any, isUpdate?: boolean) => {
  return [
    {
      title: 'No',
      dataIndex: 'index',
      key: 'index',
      align: 'center',
    },
    {
      title: (
        <TableTitle
          label="Role name"
          onSubmit={submitParams}
          name="name"
          sort={[ROLE_SORT.NAME_DESC, ROLE_SORT.NAME_ASC]}
        />
      ),
      dataIndex: 'name',
      key: 'name',
      align: 'center',
      render(data: any) {
        return <>{data || '-'} </>;
      },
    },
    {
      title: (
        <TableTitle
          label="Description"
          onSubmit={submitParams}
          name="description"
          sort={[ROLE_SORT.DESCRIPTION_DESC, ROLE_SORT.DESCRIPTION_ASC]}
        />
      ),
      dataIndex: 'description',
      key: 'description',
      render(data: any) {
        return <>{data || '-'}</>;
      },
    },

    {
      title: (
        <TableTitle
          label="Created By"
          onSubmit={submitParams}
          name="createdBy"
          sort={[ROLE_SORT.CREATEDBY_DESC, ROLE_SORT.CREATEDBY_ASC]}
        />
      ),
      dataIndex: 'createdBy',
      key: 'createdBy',
      align: 'center',
      render(data: any) {
        return <>{data || '-'}</>;
      },
    },
    {
      title: (
        <TableTitle
          type={'none'}
          label="Create time"
          onSubmit={submitParams}
          name="createdAt"
          sort={[ROLE_SORT.CREATEDAT_DESC, ROLE_SORT.CREATEDAT_ASC]}
        />
      ),
      dataIndex: 'createdAt',
      key: 'createdAt',
      align: 'center',
      render(data: any) {
        return <>{timeFormatter(Number(data)) || '-'}</>;
      },
    },
    {
      title: (
        <TableTitle
          label="Status"
          type="select"
          onSubmit={(values: any) =>
            submitParams({ ...values, status: values.status ? Number(values.status) : undefined })
          }
          name="status"
          obj={ADMIN_STATUS_ENUMS}
        />
      ),
      dataIndex: 'status',
      key: 'status',
      align: 'center',
      render(data: any) {
        return <>{data === 0 ? 'Inactive' : 'Active'}</>;
      },
    },
    {
      title: <div style={{ textAlign: 'center' }}>Action</div>,
      dataIndex: 'id',
      width: '100px',
      render(id: any) {
        return (
          <>
            <Space>
              {isUpdate && (
                <AntdButton
                  className={styles.btnAction}
                  icon={<AiOutlineUserAdd />}
                  type="dashed"
                  onClick={() => {
                    dispatch(setAddDecentralizationId(String(id)));
                  }}
                  title="Permission"
                />
              )}

              <AntdButton
                className={styles.btnAction}
                icon={isUpdate && id > 2 ? <AiFillEdit /> : <AiFillEye />}
                type="primary"
                onClick={() => setEditingId && dispatch(setEditingId(String(id)))}
                title={isUpdate && id > 2 ? 'Edit' : 'View'}
              />

              {isUpdate && id > 2 && (
                <AntdButton
                  className={styles.btnAction}
                  icon={<AiOutlineDelete />}
                  danger
                  onClick={() => setDeletingId && dispatch(setDeletingId(String(id)))}
                  title="Delete"
                />
              )}
            </Space>
          </>
        );
      },
    },
  ];
};
