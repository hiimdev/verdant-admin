import { Modal } from 'antd';
import { useAppDispatch, useAppSelector } from 'hooks/reduxHook';
import { FC } from 'react';
import { getDeletingId, setDeletingId } from 'store/ducks/administrator/slice';
import { useMutation, useQueryClient } from 'react-query';
import { message } from 'utils/message';
import { IError } from 'api/types';
import { usePermission } from 'api/permissions';
import { deleteRole } from 'api/permissions/request';
const Deletor: FC = () => {
  const dispatch = useAppDispatch();
  const deletingId = useAppSelector(getDeletingId);
  const client = useQueryClient();
  const { data } = usePermission(deletingId as string, { enabled: !!deletingId });
  const handleClose = () => {
    dispatch(setDeletingId(null));
  };
  const { mutate: deleteAdmin, isLoading: deleteLoading } = useMutation(deleteRole, {
    onSuccess: () => {
      client.invalidateQueries('permissions/paging-filter-role');
      message.success('Delete successfully!');
      handleClose();
    },
    onError: (error: IError) => {
      message.error(error?.meta.message[0]);
    },
  });
  const onDelete = () => {
    deleteAdmin(deletingId as string);
  };

  return (
    <>
      <Modal
        visible={!!deletingId}
        title={`Delete administrator: ${data?.name}`}
        okButtonProps={{
          loading: deleteLoading,
        }}
        cancelButtonProps={{
          disabled: deleteLoading,
        }}
        onCancel={handleClose}
        onOk={onDelete}
        okType="danger"
        okText="Confirm"
      >
        Are you sure you want to delete this Group Admin?
      </Modal>
    </>
  );
};

export default Deletor;
