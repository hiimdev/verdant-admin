import { Form } from 'antd';
import { usePermission } from 'api/permissions';
import { createRole, updateRole } from 'api/permissions/request';
import { IError } from 'api/types';
import { Drawer } from 'components/Drawer';
import { Input } from 'components/Input';
import { Select } from 'components/Select';
import { useAppDispatch, useAppSelector } from 'hooks/reduxHook';
import { FC, useEffect, useMemo } from 'react';
import { useMutation, useQueryClient } from 'react-query';
import { setEditingId, getEditingId } from 'store/ducks/administrator/slice';
import { ADMIN_STATUS_PERMISSION } from 'utils/constant';
import { message } from 'utils/message';
import { IListType } from './type';

const Editor: FC<IListType> = ({ isUpdate: canUpdate }) => {
  const dispatch = useAppDispatch();
  const editingId = useAppSelector(getEditingId);
  const client = useQueryClient();
  const action = editingId ? 'edit' : 'add';

  const isUpdate = useMemo(() => {
    return (canUpdate && editingId !== '' && Number(editingId) > 2) || editingId === '';
  }, [editingId, canUpdate]);

  const { mutate: create, isLoading: isLoadingCreate } = useMutation(createRole, {
    onSuccess: () => {
      client.invalidateQueries('permissions/paging-filter-role');
      message.success('Success');
      dispatch(setEditingId(null));
    },
    onError: (error: IError) => {
      message.error(error?.meta.message[0]);
    },
  });
  const { mutate: update, isLoading: loadingUpdate } = useMutation(updateRole, {
    onSuccess: () => {
      client.invalidateQueries('permissions/paging-filter-role');
      message.success('Update successfully!');
      handleClose();
    },
    onError: (error: IError) => {
      message.error(error?.meta.message);
    },
  });

  const onFinish = (value: any) => {
    const mutate = action === 'edit' ? update : create;
    const params = { ...value, id: Number(editingId) };
    mutate(params);
  };

  const { data, status: getLoading } = usePermission(editingId as string, { enabled: !!editingId });
  const [form] = Form.useForm();

  useEffect(() => {
    form.resetFields();
    if (editingId) {
      form.setFieldsValue({ name: data?.name, description: data?.description, status: data?.status });
    }
  }, [data]);

  const handleClose = () => {
    dispatch(setEditingId(null));
  };

  return (
    <Drawer
      name="Role"
      form={form}
      editingId={editingId}
      handleClose={handleClose}
      getLoading={getLoading === 'loading' || status === 'loading'}
      submitLoading={isLoadingCreate || loadingUpdate}
      viewOnly={!isUpdate}
    >
      <Form form={form} onFinish={onFinish}>
        <Form.Item
          label="Name Role"
          labelCol={{ span: 3 }}
          wrapperCol={{ span: 18 }}
          name="name"
          rules={[{ required: true, min: 3, max: 30 }]}
        >
          <Input placeholder="Name role" />
        </Form.Item>
        <Form.Item
          label="Description"
          labelCol={{ span: 3 }}
          wrapperCol={{ span: 18 }}
          name="description"
          rules={[{ max: 100 }]}
        >
          <Input placeholder="Description" />
        </Form.Item>
        <Form.Item
          label="Status"
          name="status"
          labelCol={{ span: 3 }}
          wrapperCol={{ span: 18 }}
          rules={[{ required: true }]}
        >
          <Select placeholder="Select status">
            {Object.keys(ADMIN_STATUS_PERMISSION).map((key: string) => (
              <Select.Option key={key} value={Number(key)}>
                {ADMIN_STATUS_PERMISSION[Number(key || 0) as 0 | 1]}
              </Select.Option>
            ))}
          </Select>
        </Form.Item>
      </Form>
    </Drawer>
  );
};

export default Editor;
