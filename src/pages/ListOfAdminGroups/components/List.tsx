import { PaginatedTableAndSearch } from 'components/PaginatedTableAndSearch';
import { useAppDispatch } from 'hooks/reduxHook';
import { FC, useMemo, useState } from 'react';
import { setEditingId } from 'store/ducks/administrator/slice';
import { useGetListAllRole } from 'api/permissions';
import { debounce } from 'utils/js';
import { getColumns } from './Columns';
import { Permission } from 'utils/permission';
import { IListType } from './type';

const List: FC<IListType> = ({ isUpdate }) => {
  const dispatch = useAppDispatch();
  const [params, setParams] = useState<any>({ sort: [] });
  const debouncedSetParams = useMemo(() => debounce(setParams, 500), []);
  const submitParams = (value: any) => {
    debouncedSetParams((prevParams: any) => ({
      ...prevParams,
      ...value,
      sort: value.sort ? [value.sort] : [],
    }));
  };
  const columns = useMemo(() => getColumns(dispatch, submitParams, isUpdate), [getColumns, isUpdate]);
  return (
    <PaginatedTableAndSearch
      permission={Permission.add_list_of_admin_role}
      title="List of admin roles"
      setEditingId={setEditingId}
      columns={columns}
      useQuery={useGetListAllRole}
      requestParams={params}
      handleReset={() => setParams({ sort: [] })}
    />
  );
};

export default List;
