import { FC, useMemo } from 'react';
import List from './components/List';
import Editor from './components/Editor';
import Deletor from './components/Deletor';
import AddDecentralizationId from './components/AdddecentralizationId';
import { useAppSelector } from 'hooks/reduxHook';
import { getPermission } from 'store/ducks/user/slice';
import { getAddDecentralizationId, getEditingId, getDeletingId } from 'store/ducks/administrator/slice';
import { Permission } from 'utils/permission';

const ListAdminGroups: FC = () => {
  const permissions = useAppSelector(getPermission);
  const editingId = useAppSelector(getEditingId);
  const deletingId = useAppSelector(getDeletingId);
  const decentralizationId = useAppSelector(getAddDecentralizationId);

  const isUpdate = useMemo(() => {
    return permissions.includes(Permission.add_list_of_admin_role);
  }, [permissions]);

  return (
    <>
      <List isUpdate />
      {editingId !== null && <Editor isUpdate={isUpdate} />}
      {deletingId && isUpdate && <Deletor />}
      {decentralizationId && isUpdate && (
        <AddDecentralizationId isUpdate={isUpdate && Number(decentralizationId) > 2} />
      )}
    </>
  );
};
export default ListAdminGroups;
