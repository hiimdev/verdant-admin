import { Card, Form, Modal } from 'antd';
import { gen2FA, login } from 'api/login';
import { IError } from 'api/types';
import { Button } from 'components/Button';
import { Input, InputPassword } from 'components/Input';
import { LoadingFullpage } from 'components/Loading';
import { useCopy } from 'hooks/useCopy';
import { routes, routesEnum } from 'pages/Routes';
import { QRCodeSVG } from 'qrcode.react';
import { ChangeEvent, FC, useEffect, useState } from 'react';
import { AiOutlineCheck, AiOutlineCopy } from 'react-icons/ai';
import { useMutation } from 'react-query';
import { useHistory } from 'react-router-dom';
import { LOCAL_STORAGE } from 'utils/constant';
import cookie from 'utils/cookie';
import { message } from 'utils/message';
import { Permission } from 'utils/permission';
import styles from './styles.module.less';

interface permission {
  action: string;
  enum: string;
  id: number;
  name: string;
}
interface loginResponse {
  email: string;
  isActive2fa: number;
  permissions: permission[];
  token: string;
}

const Login: FC = () => {
  const history = useHistory();

  const [isModalVisible, setModalVisible] = useState<0 | 1 | 2>(0);
  const [code, setCode] = useState<string>('');
  const [secretKey, setSecretKey] = useState<{ qrCodeValue?: string; code?: string; loginParams: any }>({
    qrCodeValue: '',
    code: '',
    loginParams: {},
  });
  const [copied, copy] = useCopy();

  const { mutate: mutateLogin, status: statusLogin } = useMutation(login, {
    onError: (error: IError) => {
      message.error(error?.meta.message[0]);
    },
    onSuccess: (data: loginResponse) => {
      // localStorage.setItem(LOCAL_STORAGE.token, data.token);
      cookie.setCookie(cookie.COOKIES.token, data.token);
      const temp = data.permissions.map((item) => item.action);
      const temporary = routes.find((item) => data.permissions.find((_item) => item.permission === _item.action)) || {
        path: '',
      };
      history.push(temp.includes(Permission.view_dashboard) ? routesEnum.home : String(temporary?.path));
    },
  });
  const { mutateAsync: mutateAsyncGen2FA, isLoading: loadingGen2FA } = useMutation(gen2FA, {
    onError: (error: IError) => {
      message.error(error?.meta?.message || '');
    },
  });

  useEffect(() => {
    if (code.length >= 6) {
      mutateLogin({ ...secretKey.loginParams, twofa: code });
      handleClose();
    }
  }, [code]);

  const onFinish = async (values: any) => {
    const loginParams = {
      email: values.email,
      password: values.password,
    };
    const data = await mutateAsyncGen2FA(loginParams);

    if (data?.isActive2fa === 1) {
      setSecretKey({ loginParams: loginParams });
      setModalVisible(2);
    } else if (data?.isActive2fa === 0) {
      const qrCodeValue = `otpauth://totp/${values.email}?secret=${data.twoFactorAuthenticationSecret}&issuer=VerdantNFT`;
      setModalVisible(1);
      setSecretKey({
        qrCodeValue: qrCodeValue || '',
        code: data.twoFactorAuthenticationSecret || '',
        loginParams: loginParams,
      });
    }
  };

  const handleClose = () => {
    setModalVisible(0);
    setCode('');
  };
  const handleSecretKeyModal = () => {
    setModalVisible(2);
  };

  const handleLogin = () => {
    mutateLogin(secretKey.loginParams);
  };

  const onChange2FA = (e: ChangeEvent<HTMLInputElement>) => {
    setCode(e.target.value);
  };

  if (sessionStorage.getItem(LOCAL_STORAGE.token)) {
    return <LoadingFullpage />;
  }

  return (
    <>
      <Modal
        title="Google Authentication"
        visible={isModalVisible === 2}
        onOk={() => handleLogin()}
        onCancel={handleClose}
      >
        <Input value={code} onChange={(e) => onChange2FA(e)} />
      </Modal>
      <Modal
        title="Google Authentication"
        visible={isModalVisible === 1}
        onOk={() => handleSecretKeyModal()}
        onCancel={handleClose}
      >
        <div className={styles.gen2FAContent}>
          <div className={styles.gen2FANote}>
            Open an authentication app and scan the QR code or directly add the following key to the app.
          </div>
          <div className={styles.qrCode}>
            <QRCodeSVG value={secretKey?.qrCodeValue || ''} />
            <div className={styles.code}>
              {secretKey?.code}
              <Button
                onClick={() => copy(secretKey?.code || '')}
                icon={copied ? <AiOutlineCheck /> : <AiOutlineCopy />}
              ></Button>
            </div>
          </div>
        </div>
      </Modal>
      <div className={styles.loginContainer}>
        <div className={styles.loginContent}>
          <Card style={{ borderRadius: '10px' }} title="Login">
            <Form initialValues={{ remember: true }} onFinish={onFinish} autoComplete="off">
              <Form.Item
                label=""
                name="email"
                rules={[{ required: true, message: 'Please input your email!' }, { type: 'email' }]}
              >
                <Input placeholder="Email" />
              </Form.Item>

              <Form.Item label="" name="password" rules={[{ required: true, message: 'Please input your password!' }]}>
                <InputPassword placeholder="Password" />
              </Form.Item>

              <Form.Item style={{ margin: 'auto', textAlign: 'center' }}>
                <Button type="primary" htmlType="submit" loading={loadingGen2FA || statusLogin === 'loading'}>
                  Submit
                </Button>
              </Form.Item>
            </Form>
          </Card>
        </div>
      </div>
    </>
  );
};

export default Login;
