import { AnyAction, Dispatch } from '@reduxjs/toolkit';
import { Button as AntdButton, Space } from 'antd';
import { TableTitle } from 'components/Table/component/TableTitle';
import { AiFillEdit, AiFillEye } from 'react-icons/ai';
import { setEditingId } from 'store/ducks/muslim/slice';
import { MUSLIM_STATUS } from 'utils/constant';
import styles from './styles.module.less';

export const getColumns = (dispatch: Dispatch<AnyAction>, submitParams?: any, isUpdate?: boolean) => {
  return [
    {
      title: 'No',
      dataIndex: 'index',
      key: 'index',
      align: 'center',
      width: '80px',
    },
    {
      title: <TableTitle name="name" label="Name" onSubmit={submitParams} />,
      dataIndex: 'name',
      key: 'name',
      align: 'center',
    },
    {
      title: (
        <TableTitle
          name="is_muslim"
          label="Restricted Country"
          onSubmit={submitParams}
          type="select"
          obj={MUSLIM_STATUS}
        />
      ),
      dataIndex: 'is_muslim',
      key: 'is_muslim',
      render(is_muslim: number) {
        return <>{is_muslim ? 'Yes' : 'No'}</>;
      },
      align: 'center',
    },
    {
      title: <TableTitle name="legal_drinking_age" label="The Legal Drinking Age" onSubmit={submitParams} />,
      dataIndex: 'legal_drinking_age',
      key: 'legal_drinking_age',
      align: 'center',
    },
    {
      title: 'Action',
      align: 'center',
      dataIndex: 'country_id',
      fixed: 'right',
      render(id: string) {
        return (
          <Space>
            <AntdButton
              className={styles.btnAction}
              icon={isUpdate ? <AiFillEdit /> : <AiFillEye />}
              type="primary"
              onClick={() => dispatch(setEditingId(id))}
              title={isUpdate ? 'Edit' : 'View'}
            />
          </Space>
        );
      },
    },
  ];
};
