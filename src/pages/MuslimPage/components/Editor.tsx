import { Form } from 'antd';
import { useGetDetailMuslim } from 'api/muslim';
import { configMuslim } from 'api/muslim/request';
import { IError } from 'api/types';
import { Drawer } from 'components/Drawer';
import { Input } from 'components/Input';
import { Select } from 'components/Select';
import { useAppDispatch, useAppSelector } from 'hooks/reduxHook';
import { FC, useEffect, useMemo } from 'react';
import { useMutation, useQueryClient } from 'react-query';
import { getEditingId, setEditingId } from 'store/ducks/muslim/slice';
import { message } from 'utils/message';
import { IListType } from './type';

const Editor: FC<IListType> = ({ isUpdate: canUpdate }) => {
  const dispatch = useAppDispatch();
  const editingId = useAppSelector(getEditingId);
  const client = useQueryClient();

  const [form] = Form.useForm();

  const isUpdate = useMemo(() => {
    return (canUpdate && editingId !== '') || editingId === '';
  }, []);

  const { data, status: getLoading } = useGetDetailMuslim(editingId as string, { enabled: !!editingId });

  const { mutate, isLoading } = useMutation(configMuslim, {
    onSuccess: () => {
      client.invalidateQueries('/config/list');
      message.success('Successfully!');
      handleClose();
    },
    onError: (error: IError) => {
      message.error(error?.meta.message[0]);
    },
  });

  useEffect(() => {
    form.resetFields();
    if (editingId !== null) {
      form.setFieldsValue({
        ...data,
      });
    }
  }, [data]);

  const handleClose = () => {
    dispatch(setEditingId(null));
  };

  const onFinish = (value: any) => {
    mutate({ ...value, id: editingId, legalDrinkingAge: Number(value.legalDrinkingAge) });
  };

  return (
    <Drawer
      name="Alcohol Limitation"
      editingId={editingId}
      handleClose={handleClose}
      form={form}
      getLoading={getLoading === 'loading'}
      submitLoading={isLoading}
      viewOnly={!isUpdate}
    >
      <Form form={form} onFinish={onFinish}>
        <Form.Item
          label="Legal Drinking Age "
          name="legalDrinkingAge"
          labelCol={{ span: 3 }}
          wrapperCol={{ span: 18 }}
          rules={[{ required: true }]}
        >
          <Input type="number" disabled={!isUpdate} placeholder={'Enter your title'} />
        </Form.Item>
        <Form.Item
          label="Restricted Country"
          name="isMuslim"
          labelCol={{ span: 3 }}
          wrapperCol={{ span: 18 }}
          rules={[{ required: true }]}
        >
          <Select placeholder="Select value" onChange={() => {}}>
            {[
              { value: 1, label: 'Yes' },
              { value: 0, label: 'No' },
            ].map((item: any) => (
              <Select.Option key={item?.value} value={item?.value}>
                {item?.label}
              </Select.Option>
            ))}
          </Select>
        </Form.Item>
      </Form>
    </Drawer>
  );
};

export default Editor;
