import { useGetListMuslim } from 'api/muslim';
import { PaginatedTableAndSearch } from 'components/PaginatedTableAndSearch';
import { useAppDispatch } from 'hooks/reduxHook';
import { FC, useMemo, useState } from 'react';
import { debounce } from 'utils/js';
import { getColumns } from './Column';
import { IListType } from './type';

const List: FC<IListType> = ({ isUpdate }) => {
  const dispatch = useAppDispatch();
  const [params, setParams] = useState({ sort: [] });

  const debouncedSetParams = useMemo(() => debounce(setParams, 500), []);

  const submitParams = (value: any) => {
    debouncedSetParams((prevParams: any) => ({
      ...prevParams,
      ...value,
      sort: value.sort ? [value.sort] : [],
    }));
  };
  const columns = useMemo(() => getColumns(dispatch, submitParams, isUpdate), [getColumns, isUpdate]);
  return (
    <>
      <PaginatedTableAndSearch
        title="Alcohol Limitation"
        columns={columns}
        useQuery={useGetListMuslim}
        requestParams={params}
      />
    </>
  );
};

export default List;
