import { useAppSelector } from 'hooks/reduxHook';
import { FC, useMemo } from 'react';
import { getEditingId } from 'store/ducks/muslim/slice';
import { getPermission } from 'store/ducks/user/slice';
import { Permission } from 'utils/permission';
import Editor from './components/Editor';
import List from './components/List';

const MuslimPage: FC = () => {
  const permissions = useAppSelector(getPermission);
  const editingId = useAppSelector(getEditingId);

  const isUpdate = useMemo(() => {
    return permissions.includes(Permission.edit_muslim);
  }, [permissions]);

  return (
    <>
      <List isUpdate={isUpdate} />
      {editingId !== null && <Editor isUpdate={isUpdate} />}
    </>
  );
};

export default MuslimPage;
