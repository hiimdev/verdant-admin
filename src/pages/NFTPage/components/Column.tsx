import { AnyAction, Dispatch } from '@reduxjs/toolkit';
import { Button as AntdButton, Space } from 'antd';
import { TableTitle } from 'components/Table/component/TableTitle';
import { AiOutlineEye, AiOutlineHistory, AiOutlinePercentage } from 'react-icons/ai';
import { setEditingId, setLogId, setRateId } from 'store/ducks/nft/slice';
import { convertPriceEther } from 'utils/common';
import { NFT_ONSALE_ENUM, NFT_SORT } from 'utils/constant';
import { timeFormatter } from 'utils/date';
import styles from './styles.module.less';

export const getColumns = (
  dispatch: Dispatch<AnyAction>,
  submitParams?: any,
  isViewlog?: boolean,
  isUpdateAllowance?: boolean
) => {
  return [
    {
      title: 'No',
      dataIndex: 'index',
      key: 'index',
      align: 'center',
      width: '80px',
    },
    {
      title: (
        <TableTitle
          name="tokenId"
          label="Token id"
          onSubmit={(values: any) =>
            submitParams({ ...values, tokenId: values.tokenId ? Number(values.tokenId) : undefined })
          }
          sort={[NFT_SORT.TOKENID_DESC, NFT_SORT.TOKENID_ASC]}
        />
      ),
      dataIndex: 'tokenId',
      key: 'tokenId',
    },
    {
      title: (
        <TableTitle
          name="nameNFT"
          label="Name NFT"
          onSubmit={submitParams}
          sort={[NFT_SORT.NAME_DESC, NFT_SORT.NAME_ASC]}
        />
      ),
      dataIndex: 'nameNFT',
      key: 'nameNFT',
    },
    {
      title: (
        <TableTitle
          name="collectionName"
          label="Collection Name"
          onSubmit={submitParams}
          sort={[NFT_SORT.COLLECTION_DESC, NFT_SORT.COLLECTION_ASC]}
        />
      ),
      dataIndex: 'collectionName',
      key: 'collectionName',
    },
    {
      title: (
        <TableTitle
          type="select"
          name="status"
          label="Status"
          onSubmit={(values: any) =>
            submitParams({ ...values, status: values.status ? Number(values.status) : undefined })
          }
          sort={[]}
          obj={NFT_ONSALE_ENUM}
        />
      ),
      dataIndex: 'status',
      key: 'status',
      render(status: any, record: any) {
        return <>{record?.isNftBurn === '1' ? 'Burned' : status ? 'On Sale' : 'Not On Sale'}</>;
      },
    },
    {
      title: (
        <TableTitle
          name="sellerOffer"
          label="Seller Offer"
          onSubmit={submitParams}
          sort={[NFT_SORT.PRICE_DESC, NFT_SORT.PRICE_ASC]}
        />
      ),
      dataIndex: 'sellerOffer',
      key: 'sellerOffer',
      render(data: any) {
        return <>{data ? convertPriceEther(data) : '-'}</>;
      },
      align: 'center',
    },
    {
      title: (
        <TableTitle
          name="owner"
          label="Owner"
          onSubmit={submitParams}
          sort={[NFT_SORT.OWNER_DESC, NFT_SORT.OWNER_ASC]}
        />
      ),
      dataIndex: 'owner',
      key: 'owner',
      render(data: any) {
        return data || '-';
      },
    },
    {
      title: (
        <TableTitle
          name="totalTimesTraded"
          label="Total times traded"
          onSubmit={(values: any) =>
            submitParams({
              ...values,
              totalTimesTraded: values.totalTimesTraded ? Number(values.totalTimesTraded) : undefined,
            })
          }
          sort={[NFT_SORT.TOTAL_TIME_TRADE_DESC, NFT_SORT.TOTAL_TIME_TRADE_ASC]}
        />
      ),
      dataIndex: 'totalTimesTraded',
      key: 'totalTimesTraded',
      render(data: any) {
        return <>{data || '0'}</>;
      },
      align: 'center',
    },
    {
      title: (
        <TableTitle
          type="none"
          name="mintedAt"
          label="Minted at"
          onSubmit={submitParams}
          sort={[NFT_SORT.MINT_AT_DESC, NFT_SORT.MINT_AT_ASC]}
        />
      ),
      dataIndex: 'mintedAt',
      key: 'mintedAt',
      render(mintedAt: string) {
        return timeFormatter(Number(mintedAt));
      },
      align: 'center',
    },
    {
      title: (
        <TableTitle
          name="mintedBy"
          label="Minted By"
          onSubmit={submitParams}
          sort={[NFT_SORT.MINT_BY_DESC, NFT_SORT.MINT_BY_ASC]}
        />
      ),
      dataIndex: 'mintedBy',
      key: 'mintedBy',
      align: 'center',
      render(data: any) {
        return data || '-';
      },
    },
    {
      title: (
        <TableTitle
          type="none"
          name="lastTradedAt"
          label="Last traded at"
          onSubmit={submitParams}
          sort={[NFT_SORT.LAST_TRADE_AT_DESC, NFT_SORT.LAST_TRADE_AT_ASC]}
        />
      ),
      dataIndex: 'lastTradedAt',
      key: 'lastTradedAt',
      render(lastTradedAt: string) {
        return <>{lastTradedAt ? timeFormatter(Number(lastTradedAt)) : '-'}</>;
      },
      align: 'center',
      width: '140px',
    },
    {
      title: (
        <TableTitle
          name="lastTradedPrice"
          label="Last traded Price"
          onSubmit={submitParams}
          sort={[NFT_SORT.LAST_TRADE_PRICE_DESC, NFT_SORT.LAST_TRADE_PRICE_ASC]}
        />
      ),
      dataIndex: 'lastTradedPrice',
      key: 'lastTradedPrice',
      render(lastTradedPrice: any) {
        return <>{lastTradedPrice ? convertPriceEther(lastTradedPrice) : '-'}</>;
      },
      width: '150px',
      align: 'center',
    },

    {
      title: 'Action',
      align: 'center',
      dataIndex: 'tokenId',
      fixed: 'right',
      render(tokenId: any) {
        return (
          <Space>
            {isViewlog && (
              <AntdButton
                className={styles.btnAction}
                icon={<AiOutlineHistory />}
                type="default"
                onClick={() => dispatch(setLogId(String(tokenId)))}
                title="Log"
              />
            )}
            <AntdButton
              className={styles.btnAction}
              icon={<AiOutlineEye />}
              type="primary"
              onClick={() => dispatch(setEditingId(String(tokenId)))}
              title="View"
            />

            {isUpdateAllowance && (
              <AntdButton
                className={styles.btnAction}
                icon={<AiOutlinePercentage />}
                type="default"
                onClick={() => dispatch(setRateId(String(tokenId)))}
                title="Adjust rate"
              />
            )}
          </Space>
        );
      },
    },
  ];
};
