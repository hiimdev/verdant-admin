import { useNFTsQuery } from 'api/nft';
import { PaginatedTableAndSearch } from 'components/PaginatedTableAndSearch';
import { useAppDispatch } from 'hooks/reduxHook';
import { FC, useMemo, useState } from 'react';
import { debounce } from 'utils/js';
import { getColumns } from './Column';
import { IListType } from './type';

const NFT: FC<IListType> = ({ isViewlog, isUpdateAllowance }) => {
  const dispatch = useAppDispatch();
  const [params, setParams] = useState({ sort: [] });
  const debouncedSetParams = useMemo(() => debounce(setParams, 500), []);

  const submitParams = (value: any) => {
    debouncedSetParams((prevParams: any) => ({
      ...prevParams,
      ...value,
      sort: value.sort ? [value.sort] : [],
    }));
  };

  const columns = useMemo(
    () => getColumns(dispatch, submitParams, isViewlog, isUpdateAllowance),
    [getColumns, isViewlog, isUpdateAllowance]
  );

  return (
    <>
      <PaginatedTableAndSearch
        title="NFT"
        columns={columns}
        useQuery={useNFTsQuery}
        requestParams={params}
        handleReset={() => setParams({ sort: [] })}
      />
    </>
  );
};

export default NFT;
