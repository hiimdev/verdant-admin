import { Modal } from 'antd';
import { useLogNftQuery } from 'api/nft';
import { PaginatedTableAndSearch } from 'components/PaginatedTableAndSearch';
import { useAppDispatch, useAppSelector } from 'hooks/reduxHook';
import { FC, useMemo, useState } from 'react';
import { getLogId, setLogId } from 'store/ducks/nft/slice';
import { convertPriceEther, convertHistoryFromName, convertHistoryToName, convertLogAction } from 'utils/common';
import styles from './styles.module.less';
import { FiExternalLink } from 'react-icons/fi';
import { NFT_ACTION_ENUM, POLYGON_SCAN } from 'utils/constant';
import { relativeTime, timeFormatter } from 'utils/date';
import { debounce } from 'utils/js';
import { Select } from 'components/Select';
import clsx from 'clsx';

const getColumns = () => {
  return [
    {
      title: 'Date',
      key: 'updated_at',
      render: (data: any) => (
        <a href={`${POLYGON_SCAN}/tx/${data.txid}`} target="_blank" rel="noopener noreferrer" className={styles.date}>
          {relativeTime(data.updated_at)}
          <FiExternalLink />
        </a>
      ),
      align: 'center' as const,
    },
    {
      title: 'NFT Action',
      key: 'log_action',
      dataIndex: 'log_action',
      align: 'center' as const,
      width: '150px',
      render: (log_action: any, record: any) => <>{convertLogAction(log_action || record.nft_action)}</>,
    },
    {
      title: 'Price',
      dataIndex: 'price',
      key: 'price',
      align: 'center' as const,
      render: (price: any) => <>{convertPriceEther(price)} USDC</>,
      width: '150px',
    },
    {
      title: 'From',
      key: 'from',
      align: 'center' as const,
      render: (data: any) => <>{convertHistoryFromName(data)}</>,
    },
    {
      title: 'To',
      key: 'to',
      align: 'center' as const,
      render: (data: any) => <>{convertHistoryToName(data)}</>,
    },
    {
      title: 'Last action at',
      dataIndex: 'last_action_at',
      key: 'last_action_at',
      render(value: string) {
        return timeFormatter(Number(value));
      },
    },
    {
      title: 'Last action by',
      dataIndex: 'last_action_by',
      key: 'last_action_by',
      render(value: string) {
        return <>{value || '-'}</>;
      },
      align: 'center',
    },
  ];
};

const Log: FC = () => {
  const dispatch = useAppDispatch();
  const logId = useAppSelector(getLogId);
  const columns = useMemo(() => getColumns(), [getColumns]);
  const [params, setParams] = useState<any>({ nft_action: '' });

  const visible = logId !== null;

  const handleClose = () => {
    dispatch(setLogId(null));
    setParams({});
  };

  const debouncedSetParams = useMemo(() => debounce(setParams, 500), []);

  return (
    <Modal
      width={1150}
      visible={visible}
      title={`NFT ID: ${logId}`}
      onOk={handleClose}
      onCancel={handleClose}
      cancelButtonProps={{ hidden: true }}
    >
      <Select
        value={params.nft_action || null}
        className={clsx('auto-width', styles.select)}
        placeholder="NFT Action"
        onChange={(nft_action: any) =>
          debouncedSetParams((prevParams: any) => ({
            ...prevParams,
            nft_action: nft_action,
          }))
        }
      >
        {Object.keys(NFT_ACTION_ENUM).map((key: any) => (
          <Select.Option key={key} value={key}>
            {
              NFT_ACTION_ENUM[
                key as
                  | 'mint'
                  | 'burn'
                  | 'transfer'
                  | 'OrderCreated'
                  | 'Buy'
                  | 'OrderCancelled'
                  | 'OrderUpdated'
                  | 'BidCreated'
                  | 'AcceptBid'
                  | 'BidUpdated'
                  | 'BidCancelled'
              ]
            }
          </Select.Option>
        ))}
      </Select>
      <PaginatedTableAndSearch
        title="Logs"
        columns={columns}
        useQuery={useLogNftQuery}
        requestParams={{ ...params, id: logId }}
        handleReset={() => setParams({ sort: [] })}
      />
    </Modal>
  );
};

export default Log;
