import { Button, Col, DatePicker, Form, Modal, Row, Space, Tabs, Typography } from 'antd';
import { RangePickerProps } from 'antd/lib/date-picker';
import { useGetBuyerPurchase, useGetSellerPurchase } from 'api/nft';
import { resetNftBuyer, resetNftSeller, updateNftBuyer, updateNftSeller } from 'api/nft/request';
import { IError } from 'api/types';
import { Input } from 'components/Input';
import { useAppDispatch, useAppSelector } from 'hooks/reduxHook';
import moment from 'moment';
import { FC, useEffect, useMemo, useState } from 'react';
import { useMutation, useQueryClient } from 'react-query';
import { getLogRateId, getRateId, setLogRateId, setRateId } from 'store/ducks/nft/slice';
import { message } from 'utils/message';
import RateLog from './RateLog';

interface Props {
  isUpdate: boolean;
}

const Rate: FC<Props> = ({ isUpdate }) => {
  const dispatch = useAppDispatch();
  const rateId = useAppSelector(getRateId);
  const logRateId = useAppSelector(getLogRateId);
  const [tab, setTab] = useState('seller');
  const client = useQueryClient();

  const { data: seller } = useGetSellerPurchase({ tokenId: Number(rateId) }, { enabled: tab === 'seller' });
  const { data: buyer } = useGetBuyerPurchase({ tokenId: Number(rateId) }, { enabled: tab === 'bid' });

  const [form] = Form.useForm();

  useEffect(() => {
    if (tab === 'seller' && seller) {
      form.setFieldsValue({
        defaultLower: seller.isDefault ? seller.sellerOfferLower : seller.defaultSellerOfferLower,
        defaultUpper: seller.isDefault ? seller.sellerOfferUpper : seller.defaultSellerOfferUpper,
        sellerOfferLower: !seller.isDefault && seller?.sellerOfferLower,
        sellerOfferUpper: !seller.isDefault && seller?.sellerOfferUpper,
        exp: seller?.adjustExpirationSeller ? moment(new Date(Number(seller?.adjustExpirationSeller))) : undefined,
      });
    } else if (tab === 'bid' && buyer) {
      form.setFieldsValue({
        defaultLower: buyer.isDefault ? buyer.buyerBidLower : buyer.defaultSellerOfferLower,
        defaultUpper: buyer.isDefault ? buyer.buyerBidUpper : buyer.defaultSellerOfferUpper,
        sellerOfferLower: !buyer.isDefault && buyer?.buyerBidLower,
        sellerOfferUpper: !buyer.isDefault && buyer?.buyerBidUpper,
        exp: buyer?.adjustExpirationBuyer ? moment(new Date(Number(buyer?.adjustExpirationBuyer))) : undefined,
      });
    }
  }, [tab, seller, buyer]);

  const { mutate: updateSeller, isLoading: isLoadingSeller } = useMutation(updateNftSeller, {
    onSuccess: () => {
      message.success('Successfully!');
      client.invalidateQueries('/nft-admin/detai-purchase-rule-seller-nft');
      handleClose();
    },
    onError: (error: IError) => {
      message.error(error?.meta.message[0]);
    },
  });

  const { mutate: updateBuyer, isLoading: isLoadingBuyer } = useMutation(updateNftBuyer, {
    onSuccess: () => {
      message.success('Successfully!');
      client.invalidateQueries('/nft-admin/detai-purchase-rule-seller-nft');
      client.invalidateQueries('/nft-admin/detai-purchase-rule-buyer-nft');
      handleClose();
    },
    onError: (error: IError) => {
      message.error(error?.meta.message[0]);
    },
  });

  const showReset = useMemo(() => {
    return (tab === 'seller' && !seller?.isDefault) || (tab !== 'seller' && !buyer?.isDefault);
  }, [buyer, tab, seller]);

  const { mutate: resetSeller } = useMutation(resetNftSeller, {
    onSuccess: () => {
      message.success('Successfully!');
      handleClose();
    },
    onError: (error: IError) => {
      message.error(error?.meta.message[0]);
    },
  });

  const { mutate: resetBuyer } = useMutation(resetNftBuyer, {
    onSuccess: () => {
      client.invalidateQueries('/nft-admin/detai-purchase-rule-buyer-nft');
      message.success('Successfully!');
      handleClose();
    },
    onError: (error: IError) => {
      message.error(error?.meta.message[0]);
    },
  });

  const visible = rateId !== null;

  const handleClose = () => {
    dispatch(setRateId(null));
  };

  const onFinish = (values) => {
    const request: any = {
      tokenId: Number(rateId),
      sellerOfferLower: Number(values.sellerOfferLower),
      sellerOfferUpper: Number(values.sellerOfferUpper),
      buyerBidLower: Number(values.sellerOfferLower),
      buyerBidUpper: Number(values.sellerOfferUpper),
      sellerOffer: 0,
      buyerBid: Number(values.offer),
      adjustExpirationSeller: new Date(values.exp).getTime(),
      adjustExpirationBuyer: new Date(values.exp).getTime(),
    };
    if (tab === 'seller') {
      delete request.buyerBid;
      delete request.adjustExpirationBuyer;
      delete request.buyerBidLower;
      delete request.buyerBidUpper;
      updateSeller(request);
    } else if (tab === 'bid') {
      delete request.sellerOffer;
      delete request.adjustExpirationSeller;
      delete request.sellerOfferLower;
      delete request.sellerOfferUpper;
      updateBuyer(request);
    }
  };

  const disabledDate: RangePickerProps['disabledDate'] = (current) => {
    // Can not select days before today and today
    console.log(Date.now(), current && current.valueOf() <= Date.now(), 'lol');

    return current && current.valueOf() <= Date.now();
  };

  const handleReset = () => {
    const mutate = tab === 'seller' ? resetSeller : resetBuyer;
    mutate({ tokenId: Number(rateId) });
  };

  return (
    <Modal
      width={768}
      visible={visible}
      title={`Adjust Rate`}
      onOk={handleClose}
      onCancel={handleClose}
      cancelButtonProps={{ hidden: true }}
      footer={null}
    >
      <Tabs
        defaultActiveKey={tab}
        onChange={(e) => {
          setTab(e);
        }}
      >
        <Tabs.TabPane tab={<Typography.Title level={5}>Seller Offer</Typography.Title>} key="seller"></Tabs.TabPane>
        <Tabs.TabPane tab={<Typography.Title level={5}> Buyer Bid</Typography.Title>} key="bid"></Tabs.TabPane>
      </Tabs>
      <Form form={form} onFinish={onFinish} layout="horizontal">
        <Row gutter={[24, 12]}>
          <Col span={5}></Col>
          <Col span={10}>Lower</Col>
          <Col span={9}>Upper</Col>
          <Col span={5}>Default Percent</Col>
          <Col span={10}>
            <Form.Item name="defaultLower" labelAlign="left" labelCol={{ span: 5 }}>
              <Input type="number" suffix="%" placeholder="0%" disabled />
            </Form.Item>
          </Col>
          <Col span={9}>
            <Form.Item name="defaultUpper" labelAlign="left" labelCol={{ span: 5 }}>
              <Input type="number" suffix="%" placeholder="0%" disabled />
            </Form.Item>
          </Col>
          <Col span={5}>Adjust Percent</Col>
          <Col span={10}>
            <Form.Item name="sellerOfferLower" labelAlign="left" labelCol={{ span: 5 }} rules={[{ required: true }]}>
              <Input disabled={!isUpdate} type="number" suffix="%" placeholder="0%" />
            </Form.Item>
          </Col>
          <Col span={9}>
            <Form.Item name="sellerOfferUpper" labelAlign="left" labelCol={{ span: 5 }} rules={[{ required: true }]}>
              <Input disabled={!isUpdate} type="number" suffix="%" placeholder="0%" />
            </Form.Item>
          </Col>
        </Row>

        <Form.Item
          label="Adjust Expiration"
          name="exp"
          labelAlign="left"
          labelCol={{ span: 5 }}
          rules={[{ required: true }]}
        >
          <DatePicker
            disabledDate={disabledDate}
            disabled={!isUpdate}
            showTime
            format={(value) => value.format('DD/MM/YYYY HH:mm:ss')}
          />
        </Form.Item>
        <Row justify="end">
          <Space size={8}>
            <Button
              type="primary"
              onClick={() => {
                dispatch(setLogRateId({ id: rateId, type: 'haha' }));
              }}
            >
              History
            </Button>
            <Button onClick={handleClose}>Cancel</Button>
            {showReset && isUpdate && <Button onClick={handleReset}>Reset</Button>}
            {!showReset && isUpdate && (
              <Button
                style={{ width: '100px' }}
                htmlType="submit"
                type="primary"
                loading={isLoadingBuyer || isLoadingSeller}
              >
                Save
              </Button>
            )}
          </Space>
        </Row>
      </Form>
      {logRateId && <RateLog tab={tab} />}
    </Modal>
  );
};

export default Rate;
