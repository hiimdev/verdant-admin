import { Modal } from 'antd';
import { useGetListBuyerPurchase, useGetListSellerPurchase } from 'api/nft';
import { PaginatedTableAndSearch } from 'components/PaginatedTableAndSearch';
import { TableTitle } from 'components/Table/component/TableTitle';
import { useAppDispatch, useAppSelector } from 'hooks/reduxHook';
import { FC, useMemo } from 'react';
import { getLogRateId, setLogRateId } from 'store/ducks/nft/slice';
import { SORT_ABOUT } from 'utils/constant';
import { timeFormatter } from 'utils/date';

const getColumns = (tab: string) => {
  return [
    {
      title: 'Updated At',
      key: 'updatedAt',
      dataIndex: 'updatedAt',
      render: (data: any) => <>{timeFormatter(Number(data))}</>,
      align: 'center' as const,
    },
    {
      title: 'Updated By',
      key: 'updatedBy',
      dataIndex: 'updatedBy',
      align: 'center' as const,
      width: '150px',
    },
    {
      title: (
        <TableTitle
          name={`${tab === 'seller' ? 'sellerOfferLower' : 'buyerBidLower'}`}
          label={`${tab === 'seller' ? 'Lower Seller' : 'Lower Buyer'}`}
          sort={[SORT_ABOUT.DESCRIPTION_DESC, SORT_ABOUT.DESCRIPTION_ASC]}
        />
      ),
      dataIndex: `${tab === 'seller' ? 'sellerOfferLower' : 'buyerBidLower'}`,
      key: `${tab === 'seller' ? 'sellerOfferLower' : 'buyerBidLower'}`,
      align: 'center',
    },
    {
      title: (
        <TableTitle
          name={`${tab === 'seller' ? 'sellerOfferUpper' : 'buyerBidUpper'}`}
          label={`${tab === 'seller' ? 'Upper Seller' : 'Upper Buyer'}`}
          sort={[SORT_ABOUT.DESCRIPTION_DESC, SORT_ABOUT.DESCRIPTION_ASC]}
        />
      ),
      dataIndex: `${tab === 'seller' ? 'sellerOfferUpper' : 'buyerBidUpper'}`,
      key: `${tab === 'seller' ? 'sellerOfferUpper' : 'buyerBidUpper'}`,
      align: 'center',
    },
    {
      title: 'Start Date',
      dataIndex: 'createdAt',
      key: 'createdAt',
      render: (data: any) => <>{timeFormatter(data.createdAt)}</>,
      align: 'center' as const,
    },
    {
      title: 'Finish Date',
      key: 'adjustExpirationSeller',
      dataIndex: 'adjustExpirationSeller',
      render: (data: any) => <>{timeFormatter(Number(data))}</>,
      align: 'center' as const,
    },
  ];
};

const RateLog: FC<{ tab: string }> = ({ tab }) => {
  const dispatch = useAppDispatch();
  const id = useAppSelector(getLogRateId);
  const columns = useMemo(() => getColumns(tab), [getColumns, tab]);

  const visible = id !== null;

  const handleClose = () => {
    dispatch(setLogRateId(null));
  };

  return (
    <Modal
      width={1250}
      visible={visible}
      title={``}
      onOk={handleClose}
      onCancel={handleClose}
      cancelButtonProps={{ hidden: true }}
    >
      <PaginatedTableAndSearch
        title="History"
        columns={columns}
        useQuery={tab === 'bid' ? useGetListBuyerPurchase : useGetListSellerPurchase}
        requestParams={{ tokenId: id?.id }}
      />
    </Modal>
  );
};

export default RateLog;
