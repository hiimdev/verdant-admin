import { useContractFunction } from '@usedapp/core';
import { Button, Descriptions, Image, Modal } from 'antd';
import { useNFTQuery } from 'api/nft';
import { Model3D } from 'components/3DMode';
import { Video } from 'components/Media';
import { useAppDispatch, useAppSelector } from 'hooks/reduxHook';
import { useConnectABI } from 'hooks/useConnectABI';
import { FC, useEffect } from 'react';
import { getEditingId, setEditingId } from 'store/ducks/nft/slice';
import { convertPriceEther, getLoadingButtonCallSMCT } from 'utils/common';
import { CONTRACT_ADDRESS, MARKET_ADDRESS } from 'utils/constant';
import styles from './styles.module.less';
import NFTABI from 'contracts/NFTABI.json';
import { message } from 'utils/message';
import { useMutation, useQueryClient } from 'react-query';
import { useWallet } from 'hooks/useWallet';
import { burnNft } from 'api/nft/request';
import QRCode from 'qrcode.react';

const View: FC = () => {
  const dispatch = useAppDispatch();
  const editingId = useAppSelector(getEditingId);
  const { account } = useWallet();

  const { data } = useNFTQuery(editingId as string, { enabled: !!editingId });
  const client = useQueryClient();

  const visible = editingId !== null;

  const { state, send } = useContractFunction(useConnectABI(NFTABI, CONTRACT_ADDRESS.NFT), 'burnByAdmin');

  const onBurn = () => {
    if (account !== MARKET_ADDRESS) {
      message.info('Please login metamask wallet account');
      return;
    }
    send(editingId);
  };

  const { mutate: mutateBurn } = useMutation(burnNft, {
    onSuccess: () => {},
    onError: () => {},
  });

  useEffect(() => {
    if (state.status === 'Success') {
      mutateBurn({ tokenId: Number(editingId) });
      message.success('Burn Successfully');
      client.invalidateQueries('/nft-admin/paging_filter');
      client.invalidateQueries('/nft-admin/item');
    } else if (state.status === 'Exception' || state.status === 'Fail') {
      message.error(state?.errorMessage || 'Error');
    }
  }, [state]);

  const handleClose = () => {
    dispatch(setEditingId(null));
  };

  const handleScan = () => {
    window.location.href = `https://var-verdant.com/nft/${data?.tokenId}`;
  };

  return (
    <Modal
      width={768}
      visible={visible}
      title={`NFT: ${data?.tokenId || ''}`}
      onOk={handleClose}
      onCancel={handleClose}
      cancelButtonProps={{ hidden: true }}
    >
      {data && (
        <Descriptions labelStyle={{ width: '1px' }} column={1} bordered className={styles.previewDescriptions}>
          <Descriptions.Item label="Image">
            <div style={{ display: 'flex' }}>
              {data.imageUrl && data.imageType?.includes('image') && (
                <Image src={data.imageUrl} alt={'image'} className={styles.image} />
              )}
              {data.imageUrl && data.imageType?.includes('video') && (
                <Video src={data.imageUrl} className={styles.image} />
              )}

              {data.imageUrl && data.imageType?.includes('gltf-binary') && (
                <Model3D src={data.imageUrl} className={styles.image} />
              )}
              <QRCode
                className={styles.image}
                value={`https://var-verdant.com/nft/${data.tokenId}`}
                onClick={handleScan}
              />
            </div>
          </Descriptions.Item>
          <Descriptions.Item label="Name">{data.nameNFT}</Descriptions.Item>
          <Descriptions.Item label="Owner">{data.owner}</Descriptions.Item>
          <Descriptions.Item label="Collection">{data.collectionName || '-'}</Descriptions.Item>
          <Descriptions.Item label="Brand">{data.brandName || '-'}</Descriptions.Item>
          <Descriptions.Item label="Category">{data.categoryName || '-'}</Descriptions.Item>
          <Descriptions.Item label="Description">{data.description || '-'}</Descriptions.Item>
          <Descriptions.Item label="Payment token">{data.paymentToken || '-'}</Descriptions.Item>
          <Descriptions.Item label="Status">{data.status ? 'On Sale' : 'Not On Sale'}</Descriptions.Item>
          <Descriptions.Item label="Last Traded Price">
            {data.lastTradedPrice ? convertPriceEther(data.lastTradedPrice) : '-'}
          </Descriptions.Item>
          <Descriptions.Item label="Current Seller Offer">
            {data.sellerOffer ? convertPriceEther(data.sellerOffer) : '-'}
          </Descriptions.Item>
          {data.isNftBurn === '0' ? (
            <Descriptions.Item label="Burn NFT">
              <Button loading={getLoadingButtonCallSMCT(state)} onClick={onBurn}>
                Confirm
              </Button>
            </Descriptions.Item>
          ) : (
            <Descriptions.Item label="Burn NFT">NFT was burned</Descriptions.Item>
          )}
        </Descriptions>
      )}
    </Modal>
  );
};

export default View;
