export type IListType = {
  isUpdate?: boolean;
  isViewlog?: boolean;
  isUpdateAllowance?: boolean;
};
