import { FC, useMemo } from 'react';
import List from './components/List';
import Log from './components/Log';
import View from './components/View';
import { getEditingId, getLogId, getRateId } from 'store/ducks/nft/slice';
import { Permission } from 'utils/permission';
import { useAppSelector } from 'hooks/reduxHook';
import { getPermission } from 'store/ducks/user/slice';
import Rate from './components/Rate';

const NFT: FC = () => {
  const permissions = useAppSelector(getPermission);
  const editingId = useAppSelector(getEditingId);
  const logId = useAppSelector(getLogId);
  const rateId = useAppSelector(getRateId);

  const isViewlog = useMemo(() => {
    return permissions.includes(Permission.view_log_of_nfts);
  }, [permissions]);

  const isViewAllowance = useMemo(() => {
    return permissions.includes(Permission.view_allowance);
  }, [permissions]);

  const isEditAllowance = useMemo(() => {
    return permissions.includes(Permission.add_allowance);
  }, [permissions]);

  return (
    <>
      <List isViewlog={isViewlog} isUpdateAllowance={isViewAllowance} />
      {editingId && <View />}
      {logId && <Log />}
      {rateId && isViewAllowance && <Rate isUpdate={isEditAllowance} />}
    </>
  );
};

export default NFT;
