import { AnyAction, Dispatch } from '@reduxjs/toolkit';
import { Button as AntdButton, Image, Space } from 'antd';
import { Button } from 'components/Button';
import { TableTitle } from 'components/Table/component/TableTitle';
import { AiFillEdit, AiFillEye, AiOutlineDelete } from 'react-icons/ai';
import { setDeletingId, setEditingId } from 'store/ducks/news/slice';
import { NEWS_SORT, NEWS_STATUS } from 'utils/constant';
import { timeFormatter } from 'utils/date';
import styles from './styles.module.less';

export const getColumns = (dispatch: Dispatch<AnyAction>, submitParams?: any, isUpdate?: boolean) => {
  return [
    {
      title: 'No',
      dataIndex: 'index',
      key: 'index',
      align: 'center',
      width: '80px',
    },
    {
      title: (
        <TableTitle
          name="title"
          label="Title"
          onSubmit={submitParams}
          sort={[NEWS_SORT.TITLE_DESC, NEWS_SORT.TITLE_ASC]}
        />
      ),
      dataIndex: 'title',
      key: 'title',
    },
    {
      title: (
        <TableTitle
          label="Status"
          type="select"
          onSubmit={(values: any) =>
            submitParams({ ...values, Status: values.Status ? Number(values.Status) : undefined })
          }
          name="status"
          obj={NEWS_STATUS}
        />
      ),
      dataIndex: 'status',
      key: 'status',
      render(status: 0 | 1) {
        return NEWS_STATUS[status];
      },
    },
    {
      title: (
        <TableTitle
          type="none"
          name="publishDate"
          label="Publish Date"
          onSubmit={submitParams}
          sort={[NEWS_SORT.PUBLISH_DATE_DESC, NEWS_SORT.PUBLISH_DATE_ASC]}
        />
      ),
      dataIndex: 'publishDate',
      key: 'publishDate',
      render(publishDate: string) {
        return timeFormatter(Number(publishDate));
      },
      align: 'center',
    },

    {
      title: 'Image',
      render(image) {
        return <Image className={styles.image} src={image} alt="Image" />;
      },
      dataIndex: 'image',
      key: 'image',
      align: 'center',
    },
    {
      title: (
        <TableTitle
          name="categoryName"
          label="Category"
          onSubmit={submitParams}
          sort={[NEWS_SORT.CATEGORYNAME_DESC, NEWS_SORT.CATEGORYNAME_ASC]}
        />
      ),
      dataIndex: 'categoryName',
      key: 'categoryName',
    },
    {
      title: (
        <TableTitle
          type="none"
          name="createdAt"
          label="Created At"
          onSubmit={submitParams}
          sort={[NEWS_SORT.CREATEAT_DESC, NEWS_SORT.CREATEAT_ASC]}
        />
      ),
      dataIndex: 'createdAt',
      key: 'createdAt',
      render(createdAt: string) {
        return timeFormatter(Number(createdAt));
      },
      align: 'center',
    },
    {
      title: (
        <TableTitle
          name="createdBy"
          label="Created By"
          onSubmit={submitParams}
          sort={[NEWS_SORT.CREATED_BY_DESC, NEWS_SORT.CREATED_BY_ASC]}
        />
      ),
      dataIndex: 'createdBy',
      key: 'createdBy',
      align: 'center',
      render(value: any) {
        return <>{value ? value : '-'}</>;
      },
    },
    {
      title: (
        <TableTitle
          type="none"
          name="updatedAt"
          label="Update at"
          onSubmit={submitParams}
          sort={[NEWS_SORT.UPDATEAT_DESC, NEWS_SORT.UPDATEAT_ASC]}
        />
      ),
      dataIndex: 'updatedAt',
      key: 'updatedAt',
      render(value: string) {
        return timeFormatter(Number(value));
      },
    },
    {
      title: (
        <TableTitle
          name="updatedBy"
          label="Update By"
          onSubmit={submitParams}
          sort={[NEWS_SORT.UPDATED_BY_DESC, NEWS_SORT.UPDATED_BY_ASC]}
        />
      ),
      dataIndex: 'updatedBy',
      key: 'updatedBy',
      align: 'center',
      render(value: any) {
        return <>{value ? value : '-'}</>;
      },
    },
    {
      title: (
        <TableTitle
          name="author"
          label="Author"
          onSubmit={submitParams}
          sort={[NEWS_SORT.AUTHOR_DESC, NEWS_SORT.AUTHOR_ASC]}
        />
      ),
      dataIndex: 'author',
      key: 'author',
      align: 'center',
      render(author: string) {
        return <>{author ? author : '-'}</>;
      },
    },
    {
      title: 'Action',
      align: 'center',
      dataIndex: 'id',
      fixed: 'right',
      render(id: string) {
        return (
          <Space>
            <AntdButton
              className={styles.btnAction}
              icon={isUpdate ? <AiFillEdit /> : <AiFillEye />}
              type="primary"
              onClick={() => dispatch(setEditingId(id))}
              title={isUpdate ? 'Edit' : 'View'}
            />
            {isUpdate && (
              <Button
                className={styles.btnAction}
                icon={<AiOutlineDelete />}
                danger
                onClick={() => dispatch(setDeletingId(id))}
                title="Delete"
              />
            )}
          </Space>
        );
      },
    },
  ];
};
