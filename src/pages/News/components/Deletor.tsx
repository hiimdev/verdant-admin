import { Modal } from 'antd';
import { useGetNewsDetail } from 'api/news';
import { deleteNews } from 'api/news/request';
import { IError } from 'api/types';
import { useAppDispatch, useAppSelector } from 'hooks/reduxHook';
import { FC } from 'react';
import { useMutation, useQueryClient } from 'react-query';
import { getDeletingId, setDeletingId } from 'store/ducks/news/slice';
import { message } from 'utils/message';
const Deletor: FC = () => {
  const dispatch = useAppDispatch();
  const deletingId = useAppSelector(getDeletingId);
  const client = useQueryClient();

  const { data, isLoading } = useGetNewsDetail(deletingId as string, { enabled: !!deletingId });

  const handleClose = () => {
    dispatch(setDeletingId(null));
  };

  const { mutate: mutateDelete, isLoading: deleteLoading } = useMutation(deleteNews, {
    onSuccess: () => {
      client.invalidateQueries('/articles/paging-filter-news');
      message.success('Delete successfully!');
      handleClose();
    },
    onError: (error: IError) => {
      message.error(error?.meta.message[0]);
    },
  });

  const onDelete = () => {
    mutateDelete(deletingId as string);
  };
  if (isLoading) {
    return null;
  }
  return (
    <>
      <Modal
        visible={!!deletingId}
        title={`New: ${data?.title}`}
        okButtonProps={{
          loading: deleteLoading,
        }}
        cancelButtonProps={{
          disabled: deleteLoading,
        }}
        onCancel={handleClose}
        onOk={onDelete}
        okType="danger"
        okText="Confirm"
      >
        Are you sure you want to delete this new?
      </Modal>
    </>
  );
};

export default Deletor;
