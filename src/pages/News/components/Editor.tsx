import { DatePicker, Form } from 'antd';
import { useGetArticlesListCategoryArticles } from 'api/articlesCategory';
import { ICategoryArticles } from 'api/articlesCategory/types';
import { useGetNewsDetail } from 'api/news';
import { createNews, updateNews } from 'api/news/request';
import { IError } from 'api/types';
import { Drawer } from 'components/Drawer';
import { Input, InputTextArea } from 'components/Input';
import { Select } from 'components/Select';
import { Upload } from 'components/Upload';
import { useAppDispatch, useAppSelector } from 'hooks/reduxHook';
import moment from 'moment';
import { FC, useEffect, useMemo } from 'react';
import { AiOutlinePlus } from 'react-icons/ai';
import { useMutation, useQueryClient } from 'react-query';
import { getEditingId, setEditingId } from 'store/ducks/news/slice';
import { NEWS_STATUS } from 'utils/constant';
import { convertFormValue, getUploadValueProps, rules } from 'utils/form';
import { message } from 'utils/message';
import { IListType } from './type';

const Editor: FC<IListType> = ({ isUpdate: canUpdate }) => {
  const dispatch = useAppDispatch();
  const editingId = useAppSelector(getEditingId);
  const client = useQueryClient();

  const [form] = Form.useForm();
  const action = editingId ? 'edit' : 'add';

  const isUpdate = useMemo(() => {
    return (canUpdate && editingId !== '') || editingId === '';
  }, []);

  const { data: categories } = useGetArticlesListCategoryArticles({ page: 1, limit: 50 });
  const { data, status: getLoading } = useGetNewsDetail(editingId as string, { enabled: !!editingId });

  const { mutate: update, isLoading: loadingUpdate } = useMutation(updateNews, {
    onSuccess: () => {
      client.invalidateQueries('/articles/paging-filter-news');
      message.success('Update successfully!');
      handleClose();
    },
    onError: (error: IError) => {
      message.error(error?.meta.message[0]);
    },
  });

  const { mutate: create, isLoading: loadingCreate } = useMutation(createNews, {
    onSuccess: () => {
      client.invalidateQueries('/articles/paging-filter-news');
      message.success('Create successfully!');
      handleClose();
    },
    onError: (error: IError) => {
      message.error(error?.meta.message[0]);
    },
  });

  useEffect(() => {
    form.resetFields();
    if (editingId !== null) {
      form.setFieldsValue({
        ...data,
        publishDate: data?.publishDate ? moment(new Date(Number(data?.publishDate))) : undefined,
      });
    }
  }, [data]);

  const handleClose = () => {
    dispatch(setEditingId(null));
  };

  const onFinish = (value: any) => {
    const mutate = action === 'edit' ? update : create;
    const params = action === 'edit' ? { id: editingId, ...value } : value;
    if (value.publishDate) {
      params.publishDate = new Date(value.publishDate).getTime();
    } else {
      delete params.publishDate;
    }

    if (typeof value.image === 'string') {
      delete params.image;
    }
    mutate({
      ...convertFormValue(params),
      status: value.status || 0,
      publishDate: new Date(value.publishDate).getTime(),
    });
  };
  // update lỗi 500

  const handleChange = (file: File, field: string) => {
    form.setFieldsValue({ [field]: file });
  };

  return (
    <Drawer
      name="New"
      editingId={editingId}
      handleClose={handleClose}
      form={form}
      getLoading={getLoading === 'loading'}
      submitLoading={loadingCreate || loadingUpdate}
      viewOnly={!isUpdate}
    >
      <Form form={form} onFinish={onFinish}>
        <Form.Item label="Title" name="title" labelCol={{ span: 3 }} wrapperCol={{ span: 18 }} rules={[rules]}>
          <Input disabled={!isUpdate} placeholder={'Enter your title'} />
        </Form.Item>
        <Form.Item labelCol={{ span: 3 }} wrapperCol={{ span: 18 }} name="author" label="Author">
          <Input maxLength={200} placeholder={'Enter author'} />
        </Form.Item>
        <Form.Item label="Link" name="link" labelCol={{ span: 3 }} wrapperCol={{ span: 18 }} rules={[rules]}>
          <Input disabled={!isUpdate} placeholder={'Enter your link'} />
        </Form.Item>
        <Form.Item label="Summary" name="summary" labelCol={{ span: 3 }} wrapperCol={{ span: 18 }} rules={[rules]}>
          <InputTextArea disabled={!isUpdate} placeholder={'Enter your summary'} autoSize={{ minRows: 3 }} />
        </Form.Item>
        <Form.Item
          name="image"
          label="Image"
          labelCol={{ span: 3 }}
          wrapperCol={{ span: 18 }}
          getValueProps={getUploadValueProps}
          rules={[{ required: true }]}
        >
          <Upload
            disabled={!isUpdate}
            listType="picture-card"
            onChangeFile={(file: File) => handleChange(file, 'image')}
            imageUrl={data?.image}
          >
            <AiOutlinePlus />
          </Upload>
        </Form.Item>
        <Form.Item
          label="Status"
          name="status"
          labelCol={{ span: 3 }}
          wrapperCol={{ span: 18 }}
          rules={[{ required: true }]}
        >
          <Select disabled={!isUpdate} placeholder="Select status">
            {Object.keys(NEWS_STATUS).map((key: string) => (
              <Select.Option key={key} value={Number(key)}>
                {NEWS_STATUS[Number(key || 0) as 0 | 1]}
              </Select.Option>
            ))}
          </Select>
        </Form.Item>
        <Form.Item
          label="Publish date"
          name="publishDate"
          labelCol={{ span: 3 }}
          wrapperCol={{ span: 18 }}
          rules={[{ required: true }]}
        >
          <DatePicker disabled={!isUpdate} showTime format={(value) => value?.format('YYYY/MM/DD')} />
        </Form.Item>
        <Form.Item
          labelCol={{ span: 3 }}
          wrapperCol={{ span: 18 }}
          name="categoryArticleId"
          label="Category"
          rules={[{ required: true }]}
        >
          <Select disabled={!isUpdate} placeholder="Select category" style={{ width: '100%' }}>
            {categories &&
              categories?.list?.map((item: ICategoryArticles, index) => (
                <Select.Option key={index} value={item.id}>
                  {item.categoryName}
                </Select.Option>
              ))}
          </Select>
        </Form.Item>
      </Form>
    </Drawer>
  );
};

export default Editor;
