import useBreakpoint from 'antd/lib/grid/hooks/useBreakpoint';
import { useGetNews } from 'api/news';
import { PaginatedTableAndSearch } from 'components/PaginatedTableAndSearch';
import { useAppDispatch } from 'hooks/reduxHook';
import { FC, useMemo, useState } from 'react';
import { setEditingId } from 'store/ducks/news/slice';
import { debounce } from 'utils/js';
import { Permission } from 'utils/permission';
import { getColumns } from './Column';
import { IListType } from './type';

const List: FC<IListType> = ({ isUpdate }) => {
  const dispatch = useAppDispatch();
  const [params, setParams] = useState({ sort: [] });

  const debouncedSetParams = useMemo(() => debounce(setParams, 500), []);

  const submitParams = (value: any) => {
    debouncedSetParams((prevParams: any) => ({
      ...prevParams,
      ...value,
      sort: value.sort ? [value.sort] : [],
    }));
  };
  const screen = useBreakpoint();
  const columns = useMemo(() => getColumns(dispatch, submitParams, isUpdate), [getColumns, isUpdate]);
  return (
    <>
      <PaginatedTableAndSearch
        permission={Permission.add_news}
        title="News"
        setEditingId={setEditingId}
        columns={columns}
        useQuery={useGetNews}
        requestParams={params}
        scroll={screen.xxl ? undefined : { x: 1600 }}
        handleReset={() => setParams({ sort: [] })}
      />
    </>
  );
};

export default List;
