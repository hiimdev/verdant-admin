import { FC, useMemo } from 'react';
import Deletor from './components/Deletor';
import Editor from './components/Editor';
import List from './components/List';
import { getEditingId, getDeletingId } from 'store/ducks/news/slice';
import { getPermission } from 'store/ducks/user/slice';
import { useAppSelector } from 'hooks/reduxHook';
import { Permission } from 'utils/permission';

const News: FC = () => {
  const permissions = useAppSelector(getPermission);
  const editingId = useAppSelector(getEditingId);
  const deletingId = useAppSelector(getDeletingId);

  const isUpdate = useMemo(() => {
    return permissions.includes(Permission.edit_delete_news);
  }, [permissions]);

  return (
    <>
      <List isUpdate={isUpdate} />
      {editingId !== null && <Editor isUpdate={isUpdate} />}
      {deletingId && isUpdate && <Deletor />}
    </>
  );
};

export default News;
