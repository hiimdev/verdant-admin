import { EditOutlined } from '@ant-design/icons';
import { AnyAction, Dispatch } from '@reduxjs/toolkit';
import { Button, Space } from 'antd';
import { TableTitle } from 'components/Table/component/TableTitle';
import { AiOutlineEye } from 'react-icons/ai';
import { setEditingId, setShowingId } from 'store/ducks/notification/slice';
import {
  NOTIFICATION_LIST_SORT,
  NOTIFICATION_STATUS_ENUMS,
  NOTIFICATION_STATUS_SENDING_ENUMS,
  NOTIFICATION_TYPE_SEND_ENUMS,
} from 'utils/constant';
import { timeFormatter } from 'utils/date';
import styles from './styles.module.less';

export const getColumns = (dispatch: Dispatch<AnyAction>, submitParams?: any) => {
  return [
    {
      title: 'No',
      dataIndex: 'index',
      key: 'index',
      align: 'center',
      width: '80px',
    },
    {
      title: (
        <TableTitle
          name="recipient"
          label="Recipient"
          onSubmit={submitParams}
          sort={[NOTIFICATION_LIST_SORT.RECIPIENT_DESC, NOTIFICATION_LIST_SORT.RECIPIENT_ASC]}
        />
      ),
      dataIndex: 'recipient',
      key: 'recipient',
      render(sendStatus: 0 | 1 | 2) {
        return NOTIFICATION_TYPE_SEND_ENUMS[sendStatus];
      },
    },
    {
      title: (
        <TableTitle
          name="notificationTypeName"
          label="Type"
          onSubmit={submitParams}
          sort={[NOTIFICATION_LIST_SORT.NOTIFICATION_TYPE_DESC, NOTIFICATION_LIST_SORT.NOTIFICATION_TYPE_ASC]}
        />
      ),
      dataIndex: 'notificationTypeName',
      key: 'notificationTypeName',
      // render(recipientType: 0 | 1 | 2) {
      //   return recipientType ? NOTIFICATION_TYPE_ENUMS[recipientType] : 'Promotion/Marketing';
      // },
    },
    {
      title: (
        <TableTitle
          name="subject"
          label="Subject"
          onSubmit={submitParams}
          sort={[NOTIFICATION_LIST_SORT.SUBJECT_DESC, NOTIFICATION_LIST_SORT.SUBJECT_ASC]}
        />
      ),
      dataIndex: 'subject',
      key: 'subject',
    },
    {
      title: (
        <TableTitle
          label="Sending status"
          type="select"
          onSubmit={(values: any) =>
            submitParams({ ...values, status: values.status ? Number(values.status) : undefined })
          }
          name="isSending"
          obj={NOTIFICATION_STATUS_SENDING_ENUMS}
        />
      ),
      dataIndex: 'isSending',
      align: 'center',
      key: 'isSending',
      render(status: 0 | 1) {
        return NOTIFICATION_STATUS_SENDING_ENUMS[status];
      },
    },
    {
      title: (
        <TableTitle
          label="Status"
          type="select"
          onSubmit={(values: any) =>
            submitParams({ ...values, status: values.status ? Number(values.status) : undefined })
          }
          name="status"
          obj={NOTIFICATION_STATUS_ENUMS}
        />
      ),
      dataIndex: 'status',
      key: 'status',
      render(isActive: 0 | 1) {
        return NOTIFICATION_STATUS_ENUMS[isActive];
      },
      align: 'center',
    },
    {
      title: (
        <TableTitle
          type={'none'}
          label="Sending Time"
          onSubmit={submitParams}
          name="sendingTime"
          sort={[NOTIFICATION_LIST_SORT.SENDING_TIME_DESC, NOTIFICATION_LIST_SORT.SENDING_TIME_ASC]}
        />
      ),
      dataIndex: 'sendingTime',
      key: 'sendingTime',
      render(sendingTime: string) {
        return sendingTime ? timeFormatter(Number(sendingTime)) : '-';
      },
    },
    {
      title: (
        <TableTitle
          type={'none'}
          label="Created At"
          onSubmit={submitParams}
          name="createdAt"
          sort={[NOTIFICATION_LIST_SORT.CREATEDAT_DESC, NOTIFICATION_LIST_SORT.CREATEDAT_ASC]}
        />
      ),
      dataIndex: 'createdAt',
      key: 'createdAt',
      render(createdAt: string) {
        return timeFormatter(Number(createdAt));
      },
    },
    {
      title: (
        <TableTitle
          label="Created By"
          onSubmit={submitParams}
          name="createdBy"
          sort={[NOTIFICATION_LIST_SORT.CREATEDBY_DESC, NOTIFICATION_LIST_SORT.CREATEDBY_ASC]}
        />
      ),
      dataIndex: 'createdBy',
      key: 'createdBy',
    },

    {
      title: 'Action',
      align: 'right',
      dataIndex: 'id',
      render(id: any, record) {
        return (
          <Space>
            {!record.isSending && (
              <Button
                className={styles.btnAction}
                icon={<EditOutlined />}
                type="primary"
                onClick={() => dispatch(setEditingId(String(id)))}
                title="Edit"
              />
            )}
            <Button
              className={styles.btnAction}
              icon={<AiOutlineEye />}
              onClick={() => dispatch(setShowingId(String(id)))}
              title="View"
            />
          </Space>
        );
      },
    },
  ];
};
