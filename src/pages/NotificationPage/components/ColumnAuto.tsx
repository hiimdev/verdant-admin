import { AnyAction, Dispatch } from '@reduxjs/toolkit';
import { Button, Space } from 'antd';
import { TableTitle } from 'components/Table/component/TableTitle';
import { AiOutlineEye } from 'react-icons/ai';
import { setShowingId } from 'store/ducks/notification/slice';
import {
  NOTIFICATION_LIST_SORT,
  NOTIFICATION_STATUS_SENDING_ENUMS,
  NOTIFICATION_TYPE_SEND_ENUMS,
} from 'utils/constant';
import { timeFormatter } from 'utils/date';
import styles from './styles.module.less';

export const getColumns = (dispatch: Dispatch<AnyAction>, submitParams?: any) => {
  return [
    {
      title: 'No',
      dataIndex: 'index',
      key: 'index',
      align: 'cener',
      width: '80px',
    },
    {
      title: (
        <TableTitle
          name="recipient"
          label="Recipient"
          onSubmit={submitParams}
          sort={[NOTIFICATION_LIST_SORT.RECIPIENT_DESC, NOTIFICATION_LIST_SORT.RECIPIENT_ASC]}
        />
      ),
      dataIndex: 'recipient',
      key: 'recipient',
      render(sendStatus: 0 | 1 | 2) {
        return NOTIFICATION_TYPE_SEND_ENUMS[sendStatus];
      },
    },
    {
      title: 'Type',
      dataIndex: 'notificationTypeName',
      key: 'notificationTypeName',
      // render(recipientType: 0 | 1 | 2) {
      //   return recipientType ? NOTIFICATION_TYPE_ENUMS[recipientType] : 'Promotion/Marketing';
      // },
    },
    {
      title: 'Subject',
      dataIndex: 'subject',
      key: 'subject',
    },
    {
      title: 'Sending status',
      dataIndex: 'status',
      align: 'center',
      key: 'status',
      render(status: 0 | 1) {
        return NOTIFICATION_STATUS_SENDING_ENUMS[status];
      },
    },
    {
      title: 'Sending Time',
      dataIndex: 'sendingTime',
      key: 'sendingTime',
      render(sendingTime: string) {
        return sendingTime ? timeFormatter(Number(sendingTime)) : '-';
      },
    },
    {
      title: 'Action',
      align: 'right',
      dataIndex: 'id',
      render(id: any) {
        return (
          <Space>
            <Button
              className={styles.btnAction}
              icon={<AiOutlineEye />}
              onClick={() => dispatch(setShowingId(String(id)))}
              title="View"
            />
          </Space>
        );
      },
    },
  ];
};
