import { DatePicker, Form } from 'antd';
import { useGetNotifycationType, useNotificationQuery } from 'api/notification';
import { createNotification, updateNotification } from 'api/notification/request';
import { IError } from 'api/types';
import { useGetListVendor } from 'api/user';
import { Drawer } from 'components/Drawer';
import { Input, InputTextArea } from 'components/Input';
import { Select } from 'components/Select';
import { useAppDispatch, useAppSelector } from 'hooks/reduxHook';
import moment from 'moment';
import { FC, useEffect, useState } from 'react';
import { useMutation, useQueryClient } from 'react-query';
import { getEditingId, setEditingId } from 'store/ducks/notification/slice';
import { NOTIFICATION_STATUS_ENUMS, NOTIFICATION_TYPE_SEND_ENUMS } from 'utils/constant';
import { message } from 'utils/message';

const Editor: FC = () => {
  const dispatch = useAppDispatch();
  const editingId = useAppSelector(getEditingId);
  const client = useQueryClient();
  const [type, setType] = useState<'0' | '1' | '2' | ''>('');
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const [currentTypeId, setCurrentTypeId] = useState<any>(undefined);

  const [form] = Form.useForm();

  const action = editingId ? 'edit' : 'add';

  const { data, status: getLoading } = useNotificationQuery(editingId as string, { enabled: !!editingId });
  const { data: typeNoti } = useGetNotifycationType({ page: 1, limit: 50 });

  useEffect(() => {
    form.resetFields();
    if (editingId !== null) {
      form.setFieldsValue({
        ...data,
        sendingTime: data?.sendingTime ? moment(new Date(Number(data?.sendingTime) || '')) : undefined,
        status: data?.status.toString(),
        notificationTypeId: data?.notificationTypeId ? data?.notificationTypeId : undefined,
      });
      setType(data?.sendType ? data.sendType : '');
    }
  }, [data]);

  const { mutate: create, isLoading: loadingCreate } = useMutation(createNotification, {
    onSuccess: () => {
      client.invalidateQueries('/notification/admin/paging-filter-manual');
      message.success('Create successfully!');
      handleClose();
    },
    onError: (error: IError) => {
      message.error(error?.meta.message[0]);
    },
  });

  const { mutate: update, isLoading: loadingUpdate } = useMutation(updateNotification, {
    onSuccess: () => {
      client.invalidateQueries('/notification/admin/paging-filter-manual');
      message.success('Update successfully!');
      handleClose();
    },
    onError: (error: IError) => {
      message.error(error?.meta.message[0]);
    },
  });

  const handleClose = () => {
    dispatch(setEditingId(null));
  };

  const onFinish = (value: any) => {
    const mutate = action === 'edit' ? update : create;
    let params =
      action === 'edit'
        ? {
            ...value,
            sendType: Number(value.sendType),
            sendingTime: new Date(value.sendingTime).getTime(),
            recipientType: 2,
            recipient: Number(value.recipient),
            status: Number(value.status),
            id: editingId,
          }
        : {
            ...value,
            sendType: Number(value.sendType),
            sendingTime: new Date(value.sendingTime).getTime(),
            recipientType: 2,
            recipient: Number(value.recipient),
            status: Number(value.status),
            sendTo: '',
          };
    if (currentTypeId) {
      params.notificationTypeId = currentTypeId;
    }
    mutate(params);
  };
  const { data: listVendor } = useGetListVendor({ page: 1, limit: 50 });

  return (
    <Drawer
      name="Notification"
      editingId={editingId}
      handleClose={handleClose}
      form={form}
      submitLoading={loadingCreate || loadingUpdate}
      getLoading={getLoading === 'loading'}
    >
      <Form form={form} onFinish={onFinish}>
        <Form.Item
          label="Subject"
          name="subject"
          labelCol={{ span: 3 }}
          wrapperCol={{ span: 18 }}
          rules={[{ required: true }]}
        >
          <Input placeholder={'Enter your subject'} />
        </Form.Item>
        <Form.Item
          label="Content"
          name="content"
          labelCol={{ span: 3 }}
          wrapperCol={{ span: 18 }}
          rules={[{ required: true }]}
        >
          <InputTextArea placeholder={'Enter your content'} />
        </Form.Item>
        <Form.Item label="Link to view" name="linkView" labelCol={{ span: 3 }} wrapperCol={{ span: 18 }}>
          <Input placeholder={'Enter your link view'} />
        </Form.Item>
        <Form.Item
          label="Sending time"
          name="sendingTime"
          labelCol={{ span: 3 }}
          wrapperCol={{ span: 18 }}
          rules={[{ required: true }]}
        >
          <DatePicker showTime format={(value) => value.format('YYYY/MM/DD HH:mm:ss')} />
        </Form.Item>
        <Form.Item
          label="Status"
          name="status"
          labelCol={{ span: 3 }}
          wrapperCol={{ span: 18 }}
          rules={[{ required: true }]}
        >
          <Select placeholder="Select status">
            {Object.keys(NOTIFICATION_STATUS_ENUMS).map((key: string) => (
              <Select.Option key={key} value={key}>
                {NOTIFICATION_STATUS_ENUMS[Number(key || 0) as 0 | 1]}
              </Select.Option>
            ))}
          </Select>
        </Form.Item>
        <Form.Item
          label="Recipient"
          name="recipient"
          labelCol={{ span: 3 }}
          wrapperCol={{ span: 18 }}
          rules={[{ required: true }]}
        >
          <Select placeholder="Select type recipient" onChange={(e) => setType(e)}>
            {Object.keys(NOTIFICATION_TYPE_SEND_ENUMS).map((key: string) => (
              <Select.Option key={key} value={key}>
                {NOTIFICATION_TYPE_SEND_ENUMS[Number(key || 0) as 0 | 1 | 2]}
              </Select.Option>
            ))}
          </Select>
        </Form.Item>
        <Form.Item
          label="Type"
          name="notificationTypeId"
          labelCol={{ span: 3 }}
          wrapperCol={{ span: 18 }}
          rules={[{ required: true }]}
        >
          <Select placeholder="Select type" showSearch={true}>
            {typeNoti?.list.map((item: any) => (
              <Select.Option key={item.id} value={item.id}>
                {item.name}
              </Select.Option>
            ))}
          </Select>
        </Form.Item>
        {type === '1' && (
          <Form.Item label="Vendor" name="sendTo" labelCol={{ span: 3 }} wrapperCol={{ span: 18 }}>
            <Select placeholder="Select vendor">
              <Select.Option value={undefined}>Select vendor</Select.Option>
              {listVendor?.list?.map((item: any) => (
                <Select.Option key={item.wallet} value={item.wallet}>
                  {item.vendor_name}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
        )}
      </Form>
    </Drawer>
  );
};

export default Editor;
