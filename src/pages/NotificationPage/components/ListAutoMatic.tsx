import { useGetNotificationAuto } from 'api/notification';
import { PaginatedTableAndSearch } from 'components/PaginatedTableAndSearch';
import { useAppDispatch } from 'hooks/reduxHook';
import { FC, useMemo, useState } from 'react';
import { debounce } from 'utils/js';
import { getColumns } from './ColumnAuto';

const ListAutoMatic: FC = () => {
  const dispatch = useAppDispatch();
  const [params, setParams] = useState<any>({ sort: [] });
  const debouncedSetParams = useMemo(() => debounce(setParams, 500), []);
  const submitParams = (value: any) => {
    debouncedSetParams((prevParams: any) => ({
      ...prevParams,
      ...value,
      sort: value.sort ? [value.sort] : [],
    }));
  };
  const columns = useMemo(() => getColumns(dispatch, submitParams), [getColumns]);

  return (
    <>
      <PaginatedTableAndSearch
        title="Automatic Notification"
        columns={columns}
        useQuery={useGetNotificationAuto}
        requestParams={params}
        handleReset={() => setParams({ sort: [] })}
      />
    </>
  );
};

export default ListAutoMatic;
