import { Descriptions, Modal } from 'antd';
import { useNotificationQuery } from 'api/notification';
import { useAppDispatch, useAppSelector } from 'hooks/reduxHook';
import { FC } from 'react';
import styles from './styles.module.less';
import { timeFormatter } from 'utils/date';
import { NOTIFICATION_STATUS_ENUMS } from 'utils/constant';
import { getShowingId, setShowingId } from 'store/ducks/notification/slice';

const View: FC = () => {
  const dispatch = useAppDispatch();
  const editingId = useAppSelector(getShowingId);

  const { data } = useNotificationQuery(editingId as string, { enabled: !!editingId });

  const visible = editingId !== null;

  const handleClose = () => {
    dispatch(setShowingId(null));
  };

  return (
    <Modal
      width={768}
      visible={visible}
      title={`Notification: ${data?.subject || ''}`}
      onOk={handleClose}
      onCancel={handleClose}
      cancelButtonProps={{ hidden: true }}
    >
      {data && (
        <Descriptions labelStyle={{ width: '130px' }} column={1} bordered className={styles.previewDescriptions}>
          <Descriptions.Item label="Subject">{data.subject}</Descriptions.Item>
          <Descriptions.Item label="Content">{data.content}</Descriptions.Item>
          <Descriptions.Item label="Link view">{data.linkView}</Descriptions.Item>
          <Descriptions.Item label="Create time">{timeFormatter(Number(data.createdAt))}</Descriptions.Item>
          <Descriptions.Item label="Sending Time">{timeFormatter(Number(data.sendingTime))}</Descriptions.Item>
          <Descriptions.Item label="Status">{NOTIFICATION_STATUS_ENUMS[data.status]}</Descriptions.Item>
        </Descriptions>
      )}
    </Modal>
  );
};

export default View;
