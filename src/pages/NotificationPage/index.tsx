import { Tabs, Typography } from 'antd';
import { routesEnum } from 'pages/Routes';
import { FC } from 'react';
import { useHistory, useLocation } from 'react-router-dom';
import Editor from './components/Editor';
import List from './components/List';
import ListAutoMatic from './components/ListAutoMatic';
import View from './components/View';
import { getEditingId } from 'store/ducks/notification/slice';
import { useAppSelector } from 'hooks/reduxHook';

const Notification: FC = () => {
  const location = useLocation();
  const history = useHistory();
  const handleChange = (e: any) => {
    if (e === '1') {
      history.push({ pathname: routesEnum.notification });
    } else {
      history.push({ pathname: routesEnum.notification, search: '?tab=auto' });
    }
  };
  const editingId = useAppSelector(getEditingId);

  return (
    <>
      <Tabs style={{ minHeight: '90px' }} defaultActiveKey="1" onChange={handleChange}>
        <Tabs.TabPane tab={<Typography.Title level={2}> Manual Notification</Typography.Title>} key="1"></Tabs.TabPane>
        <Tabs.TabPane
          tab={<Typography.Title level={2}> Automatic Notification</Typography.Title>}
          key="2"
        ></Tabs.TabPane>
      </Tabs>
      {!location.search && <List />}
      {location.search && <ListAutoMatic />}
      <View />
      {editingId !== null && <Editor />}
    </>
  );
};

export default Notification;
