import { AnyAction, Dispatch } from '@reduxjs/toolkit';
import { TableTitle } from 'components/Table/component/TableTitle';
import { NOTIFICATION_TYPE_SORT } from 'utils/constant';
import { timeFormatter } from 'utils/date';

export const getColumns = (dispatch: Dispatch<AnyAction>, submitParams?: any) => {
  return [
    {
      title: 'No',
      dataIndex: 'index',
      key: 'index',
      align: 'center',
      width: '80px',
    },
    {
      title: (
        <TableTitle
          label="Type"
          onSubmit={submitParams}
          name="name"
          sort={[NOTIFICATION_TYPE_SORT.NAME_DESC, NOTIFICATION_TYPE_SORT.NAME_ASC]}
        />
      ),
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: (
        <TableTitle
          type={'none'}
          label="Created at"
          onSubmit={submitParams}
          name="createdAt"
          sort={[NOTIFICATION_TYPE_SORT.CREATEDAT_DESC, NOTIFICATION_TYPE_SORT.CREATEDAT_ASC]}
        />
      ),
      dataIndex: 'createdAt',
      key: 'createdAt',
      render(createdAt: string) {
        return timeFormatter(Number(createdAt));
      },
      align: 'center',
    },
    {
      title: (
        <TableTitle
          label="Created By"
          onSubmit={submitParams}
          name="createdBy"
          sort={[NOTIFICATION_TYPE_SORT.CREATEDBY_DESC, NOTIFICATION_TYPE_SORT.CREATEDBY_ASC]}
        />
      ),
      dataIndex: 'createdBy',
      key: 'createdBy',
      align: 'center',
      render(value: any) {
        return <>{value ? value : '-'}</>;
      },
    },
    {
      title: (
        <TableTitle
          type={'none'}
          label="Update at"
          onSubmit={submitParams}
          name="updatedAt"
          sort={[NOTIFICATION_TYPE_SORT.UPDATEAT_DESC, NOTIFICATION_TYPE_SORT.UPDATEAT_ASC]}
        />
      ),
      dataIndex: 'updatedAt',
      key: 'updatedAt',
      render(value: string) {
        return timeFormatter(Number(value));
      },
    },
    {
      title: (
        <TableTitle
          label="Update By"
          onSubmit={submitParams}
          name="updatedBy"
          sort={[NOTIFICATION_TYPE_SORT.UPDATEDBY_DESC, NOTIFICATION_TYPE_SORT.UPDATEDBY_ASC]}
        />
      ),
      dataIndex: 'updatedBy',
      key: 'updatedBy',
      align: 'center',
      render(value: any) {
        return <>{value ? value : '-'}</>;
      },
    },
  ];
};
