import { Form } from 'antd';
import { useGetDetailNotiType } from 'api/notification';
import { createNotificationType, updateNotificationType } from 'api/notification/request';
import { IError } from 'api/types';
import { Drawer } from 'components/Drawer';
import { Input } from 'components/Input';
import { useAppDispatch, useAppSelector } from 'hooks/reduxHook';
import { FC, useEffect } from 'react';
import { useMutation, useQueryClient } from 'react-query';
import { getEditingTypeId, setEditingTypeId } from 'store/ducks/notification/slice';
import { rules } from 'utils/form';
import { message } from 'utils/message';

const Editor: FC = () => {
  const dispatch = useAppDispatch();
  const editingId = useAppSelector(getEditingTypeId);
  const client = useQueryClient();

  const [form] = Form.useForm();
  const action = editingId ? 'edit' : 'add';

  const { data, status: getLoading } = useGetDetailNotiType(editingId as string, { enabled: !!editingId });

  const { mutate: update, isLoading: loadingUpdate } = useMutation(updateNotificationType, {
    onSuccess: () => {
      client.invalidateQueries('/notification/admin/paging-filter-manual-type');
      message.success('Update successfully!');
      handleClose();
    },
    onError: (error: IError) => {
      message.error(error?.meta.message[0]);
    },
  });

  const { mutate: create, isLoading: loadingCreate } = useMutation(createNotificationType, {
    onSuccess: () => {
      client.invalidateQueries('/notification/admin/paging-filter-manual-type');
      message.success('Create successfully!');
      handleClose();
    },
    onError: (error: IError) => {
      message.error(error?.meta.message[0]);
    },
  });

  useEffect(() => {
    form.resetFields();
    if (editingId !== null) {
      form.setFieldsValue(data);
    }
  }, [data]);

  const handleClose = () => {
    dispatch(setEditingTypeId(null));
  };

  const onFinish = (value: any) => {
    const mutate = action === 'edit' ? update : create;
    const params = action === 'edit' ? { id: editingId, ...value } : value;
    mutate(params);
  };

  return (
    <Drawer
      name="Notification Type"
      editingId={editingId}
      handleClose={handleClose}
      form={form}
      getLoading={getLoading === 'loading'}
      submitLoading={loadingCreate || loadingUpdate}
    >
      <Form form={form} onFinish={onFinish}>
        <Form.Item label="Type" name="name" labelCol={{ span: 3 }} wrapperCol={{ span: 18 }} rules={[rules]}>
          <Input placeholder={'Enter your type'} />
        </Form.Item>
      </Form>
    </Drawer>
  );
};

export default Editor;
