import { useGetNotifycationType } from 'api/notification';
import { PaginatedTableAndSearch } from 'components/PaginatedTableAndSearch';
import { useAppDispatch } from 'hooks/reduxHook';
import { FC, useMemo, useState } from 'react';
import { setEditingTypeId } from 'store/ducks/notification/slice';
import { debounce } from 'utils/js';
import { Permission } from 'utils/permission';
import { getColumns } from './ColumnManual';

const List: FC = () => {
  const dispatch = useAppDispatch();
  const [params, setParams] = useState({ sort: [] });

  const debouncedSetParams = useMemo(() => debounce(setParams, 500), []);

  const submitParams = (value: any) => {
    debouncedSetParams((prevParams: any) => ({
      ...prevParams,
      ...value,
      sort: value.sort ? [value.sort] : [],
    }));
  };
  const columns = useMemo(() => getColumns(dispatch, submitParams), [getColumns]);
  return (
    <>
      <PaginatedTableAndSearch
        permission={Permission.add_notification_type}
        title="Notification Type"
        setEditingId={setEditingTypeId}
        columns={columns}
        useQuery={useGetNotifycationType}
        requestParams={params}
        handleReset={() => setParams({ sort: [] })}
      />
    </>
  );
};

export default List;
