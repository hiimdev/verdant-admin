import { useGetNotifycationTypeAuto } from 'api/notification';
import { PaginatedTableAndSearch } from 'components/PaginatedTableAndSearch';
import { useAppDispatch } from 'hooks/reduxHook';
import { FC, useMemo, useState } from 'react';
import { debounce } from 'utils/js';
import { getColumns } from './ColumnAutomatic';

const ListAutoMatic: FC = () => {
  const dispatch = useAppDispatch();
  const [params, setParams] = useState({ sort: [] });

  const debouncedSetParams = useMemo(() => debounce(setParams, 500), []);

  const submitParams = (value: any) => {
    debouncedSetParams((prevParams: any) => ({
      ...prevParams,
      ...value,
      sort: value.sort ? [value.sort] : [],
    }));
  };
  const columns = useMemo(() => getColumns(dispatch, submitParams), [getColumns]);
  return (
    <>
      <PaginatedTableAndSearch title="Notification Type" columns={columns} useQuery={useGetNotifycationTypeAuto} />
    </>
  );
};

export default ListAutoMatic;
