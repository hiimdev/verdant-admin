import { Tabs, Typography } from 'antd';
import { routesEnum } from 'pages/Routes';
import { FC } from 'react';
import { useHistory, useLocation } from 'react-router-dom';
import Editor from './components/Editor';
import List from './components/List';
import ListAutoMatic from './components/ListAutoMatic';

const NotifyTypePage: FC = () => {
  const location = useLocation();
  const history = useHistory();
  const handleChange = (e: any) => {
    if (e === '1') {
      history.push({ pathname: routesEnum.notifyType });
    } else {
      history.push({ pathname: routesEnum.notifyType, search: '?tab=auto' });
    }
  };
  return (
    <>
      <Tabs style={{ minHeight: '90px' }} defaultActiveKey="1" onChange={handleChange}>
        <Tabs.TabPane
          tab={<Typography.Title level={2}> Manual Type Notification</Typography.Title>}
          key="1"
        ></Tabs.TabPane>
        <Tabs.TabPane
          tab={<Typography.Title level={2}> Automatic Type Notification</Typography.Title>}
          key="2"
        ></Tabs.TabPane>
      </Tabs>
      {!location.search && <List />}
      {location.search && <ListAutoMatic />}
      <Editor />
    </>
  );
};

export default NotifyTypePage;
