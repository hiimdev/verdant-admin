import { useContractFunction } from '@usedapp/core';
import { Button, Form, Row, Typography } from 'antd';
import { useGetOperatorWallet } from 'api/statistical';
import clsx from 'clsx';
import { Input } from 'components/Input';
import MarketABI from 'contracts/MarketABI.json';
import { useConnectABI } from 'hooks/useConnectABI';
import { useGetTokenBalance_ } from 'hooks/useGetBalance';
import { useEffect } from 'react';
import { getLoadingButtonCallSMCT } from 'utils/common';
import { CONTRACT_ADDRESS } from 'utils/constant';
import { message } from 'utils/message';
import styles from './styles.module.less';

function SmartContractSetting() {
  const [form] = Form.useForm();
  const { data } = useGetOperatorWallet();
  const balanceMatic = useGetTokenBalance_(data || '');

  const { state, send } = useContractFunction(useConnectABI(MarketABI, CONTRACT_ADDRESS.MARKET), 'whiteListOperator');
  const onFinish = (values) => {
    send(values.wallet, true);
  };

  useEffect(() => {
    if (state.status === 'Success') {
      message.success('Succcessfully');
    } else if (['Exception', 'Fail'].includes(state.status)) {
      message.error(state.errorMessage || 'Exception');
    }
  }, [state]);

  if (!data) {
    return null;
  }

  return (
    <div className={clsx(styles.root)}>
      <div className={clsx(styles.container)}>
        <Typography.Title className={clsx(styles.title)} level={3}>
          Operator wallet
        </Typography.Title>
        <Form
          form={form}
          onFinish={onFinish}
          layout="vertical"
          initialValues={{ wallet: data, balance: balanceMatic.value }}
        >
          <Form.Item label="Address wallet" name="wallet" rules={[{ required: true }]}>
            <Input />
          </Form.Item>

          <Form.Item label="Balance" name="balance">
            <Input type="number" suffix="MATIC" disabled />
          </Form.Item>

          <Row justify="center">
            <Button htmlType="submit" type="primary" loading={getLoadingButtonCallSMCT(state)}>
              Save
            </Button>
          </Row>
        </Form>
      </div>
    </div>
  );
}

export default SmartContractSetting;
