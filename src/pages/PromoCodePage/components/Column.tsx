import { AnyAction, Dispatch } from '@reduxjs/toolkit';
import { Button, Space } from 'antd';
import { TableTitle } from 'components/Table/component/TableTitle';
import { AiFillEye, AiOutlineDownload, AiOutlineEdit, AiOutlineEye } from 'react-icons/ai';
import { setEditingId, setLogId } from 'store/ducks/promoCode/slice';
import {
  DISCOUNT_TYPE_ENUM,
  PROMOCODE_TYPE_ENUM,
  PROMO_SORT_V1,
  PROMO_STATUS_ENUMS,
  REFERRAL_VENDOR,
} from 'utils/constant';
import { timeFormatter } from 'utils/date';
import styles from './styles.module.less';

export const getColumns = (dispatch: Dispatch<AnyAction>, submitParams?: any, onDownLoad?: any, isUpdate?: boolean) => {
  return [
    {
      title: 'No',
      dataIndex: 'index',
      key: 'index',
      align: 'center',
    },
    {
      title: (
        <TableTitle
          type={'select'}
          label="Created For"
          onSubmit={submitParams}
          name="is_vendor"
          obj={REFERRAL_VENDOR}
          sort={[PROMO_SORT_V1.VENDORPROJECT_DESC, PROMO_SORT_V1.VENDORPROJECT_ASC]}
        />
      ),
      dataIndex: 'is_vendor',
      key: 'is_vendor',
      align: 'center',
      render(data: string) {
        return <>{data ? 'Vendor' : 'Referral'}</>;
      },
    },
    {
      title: (
        <TableTitle
          type={'select'}
          label="Tagged To"
          onSubmit={submitParams}
          name="is_vendor"
          obj={REFERRAL_VENDOR}
          sort={[PROMO_SORT_V1.VENDORPROJECT_DESC, PROMO_SORT_V1.VENDORPROJECT_ASC]}
        />
      ),
      key: 'is_vendor',
      align: 'center',
      render(data: any) {
        return <>{data.vendor_project || data.referral_name || '-'}</>;
      },
    },
    {
      title: (
        <TableTitle
          label="Promo Code Name"
          onSubmit={submitParams}
          name="name"
          sort={[PROMO_SORT_V1.NAME_DESC, PROMO_SORT_V1.NAME_ASC]}
        />
      ),
      dataIndex: 'name',
      key: 'name',
      align: 'center',
    },
    {
      title: (
        <TableTitle
          type="select"
          label="Discount Type"
          obj={DISCOUNT_TYPE_ENUM}
          onSubmit={(values: any) =>
            submitParams({ ...values, discountType: values.discountType ? Number(values.discountType) : undefined })
          }
          name="discount_type"
        />
      ),
      dataIndex: 'discount_type',
      key: 'discount_type',
      align: 'center',
      render(data: string) {
        return <>{data ? 'Discount on NFT Price' : 'Discount on Service Fees'}</>;
      },
    },
    {
      title: (
        <TableTitle
          type="select"
          label="Discount By"
          name="discount_by"
          obj={PROMOCODE_TYPE_ENUM}
          onSubmit={(values: any) =>
            submitParams({ ...values, promoType: values.promoType ? Number(values.promoType) : undefined })
          }
        />
      ),
      key: 'discount_by',
      align: 'center',
      render(data: any) {
        return <>{data?.percent ? `${Number(data.percent)}%` : `${Number(data.min_amount) || '0'} USDC`}</>;
      },
    },
    {
      title: (
        <TableTitle
          type="select"
          label="Number of times used"
          name="discount_by"
          obj={PROMOCODE_TYPE_ENUM}
          onSubmit={(values: any) =>
            submitParams({ ...values, promoType: values.promoType ? Number(values.promoType) : undefined })
          }
        />
      ),
      key: 'discount_by',
      dataIndex: 'discount_by',
      align: 'center',
      render(data: string) {
        return data ? data : '-';
      },
    },
    {
      title: (
        <TableTitle
          type="none"
          label="Start Date"
          name="start_time"
          onSubmit={submitParams}
          sort={[PROMO_SORT_V1.STARTTIME_DESC, PROMO_SORT_V1.STARTTIME_ASC]}
        />
      ),
      dataIndex: 'start_time',
      key: 'start_time',
      align: 'center',
      render(data: string) {
        return <>{data ? timeFormatter(Number(data)) : '-'}</>;
      },
    },
    {
      title: (
        <TableTitle
          type="none"
          label="End Date"
          name="end_time"
          onSubmit={submitParams}
          sort={[PROMO_SORT_V1.ENDTIME_DESC, PROMO_SORT_V1.ENDTIME_ASC]}
        />
      ),
      dataIndex: 'end_time',
      key: 'end_time',
      align: 'center',
      render(data: string) {
        return <>{data ? timeFormatter(Number(data)) : '-'}</>;
      },
    },

    {
      title: <TableTitle type="select" label="Status" onSubmit={submitParams} name="status" obj={PROMO_STATUS_ENUMS} />,
      dataIndex: 'status',
      key: 'status',
      align: 'center',
      render(data: any) {
        return <>{PROMO_STATUS_ENUMS[data as 0 | 1 | 2 | 3]}</>;
      },
    },
    {
      title: 'Action',
      align: 'center',
      dataIndex: 'id',
      render(id: any) {
        return (
          <Space>
            <Button
              className={styles.btnAction}
              icon={<AiOutlineEye />}
              type="primary"
              onClick={() => dispatch(setLogId(String(id)))}
              title="Detail"
            />

            <Button
              className={styles.btnAction}
              icon={isUpdate ? <AiOutlineEdit /> : <AiFillEye />}
              onClick={() => dispatch(setEditingId(String(id)))}
              title={isUpdate ? 'Edit' : 'View'}
            />

            {isUpdate && (
              <Button
                onClick={() => onDownLoad(id)}
                title="Download"
                className={styles.btnAction}
                icon={<AiOutlineDownload />}
                type="primary"
              />
            )}
          </Space>
        );
      },
      fixed: 'right',
    },
  ];
};
