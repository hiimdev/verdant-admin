import { Card, Col, DatePicker, Empty, Form, Radio, Row, Spin, Tooltip, Typography } from 'antd';
import { RangePickerProps } from 'antd/lib/date-picker';
import {
  IBrandCommissionResponse,
  ICategoryCommissionResponse,
  ICollectionCommissionResponse,
  useGetBrandCommission,
  useGetCategoryCommission,
  useGetCollectionCommission,
} from 'api/commission';
import { useGetDetailPromo } from 'api/promo';
import { createPromo, updatePromo } from 'api/promo/requet';
import { IError } from 'api/types';
import { useGetListVendorFilter } from 'api/user';
import { IVendorResponse } from 'api/user/types';
import { Drawer } from 'components/Drawer';
import { Input, InputNumber } from 'components/Input';
import { Select } from 'components/Select';
import { useAppDispatch, useAppSelector } from 'hooks/reduxHook';
import moment from 'moment';
import { FC, useEffect, useMemo, useState } from 'react';
import { useMutation, useQueryClient } from 'react-query';
import { getEditingId, setEditingId } from 'store/ducks/promoCode/slice';
import { PROMO_STATUS_ENUMS } from 'utils/constant';
import { rules, typeNumber } from 'utils/form';
import { message } from 'utils/message';
import styles from './styles.module.less';
import { IListType } from './type';

const { Title } = Typography;

type Default = {
  typeCode: 'onecode' | 'morecode';
  byPrice: boolean;
  limitUser: boolean;
  limitDate: boolean;
  discountType: boolean;
  isVendor: boolean;
};

const Editor: FC<IListType> = ({ isUpdate: canUpdate }) => {
  const [defaultValue, setDefaultValue] = useState<Default>({
    typeCode: 'onecode',
    byPrice: true,
    limitUser: true,
    limitDate: true,
    discountType: false,
    isVendor: true,
  });

  const [form] = Form.useForm();
  const dispatch = useAppDispatch();
  const editingId = useAppSelector(getEditingId);
  const client = useQueryClient();
  const { data: listVendor } = useGetListVendorFilter({ page: 1, limit: 50 });
  const { data: promo, isLoading: isLoadingDetail } = useGetDetailPromo(editingId as string, { enabled: !!editingId });
  const action = editingId ? 'edit' : 'add';
  const isUpdate = useMemo(() => {
    return (canUpdate && editingId !== '') || editingId === '';
  }, []);

  useEffect(() => {
    form.resetFields();
    if (editingId !== null && promo) {
      form.setFieldsValue({
        ...promo,
        name: promo?.name,
        code: promo?.code,
        status: promo?.status || 0,
        minAmount: !promo?.promoType ? promo?.minAmount?.toString() : undefined,
        price: promo.price ? promo?.price?.toString() : undefined,
        maxAmount: promo?.maxAmount,
        promoLimit: !promo.limit ? undefined : promo.limit.toString(),
        startTime: promo?.startTime ? moment(new Date(Number(promo?.startTime) || '')) : undefined,
        endTime: promo?.endTime ? moment(new Date(Number(promo?.endTime) || '')) : undefined,
        collectionData: promo?.collectionId
          ? JSON.stringify({
              collectionId: promo.collectionId,
              brandId: promo.brandId,
              categoryCommissionId: promo.categoryCommissionId,
            })
          : undefined,
        brandData: promo?.brandId
          ? JSON.stringify({
              brandId: promo.brandId,
              categoryCommissionId: promo.categoryCommissionId,
            })
          : undefined,
        codeNumber: promo?.codeNumber ? promo?.codeNumber.toString() : '',
        codeLength: promo?.codeLength ? promo?.codeLength.toString() : '',
        percent: promo?.percent ? promo?.percent.toString() : '',
        vendorId: promo?.vendorId,
      });
      setDefaultValue({
        ...defaultValue,
        discountType: !!promo.discountType,
        byPrice: !promo.promoType,
        limitDate: promo?.startTime ? true : false,
        limitUser: !!promo?.limit,
        typeCode: promo?.type === 'onecode' ? 'onecode' : 'morecode',
        isVendor: !!promo.isVendor,
      });
    } else {
      form.setFieldsValue({ status: 0 });
    }
  }, [promo, editingId]);

  const {
    data: listCategoryCommission,
    status: statusCategory,
    refetch: refetchCategory,
  } = useGetCategoryCommission({ page: 1, limit: 50 }, { enabled: editingId ? true : false });

  const {
    data: listBrand,
    status: statusBrand,
    refetch: refetchBrand,
  } = useGetBrandCommission(
    form.getFieldValue('categoryCommissionId') || form.getFieldValue('collectionData')
      ? {
          page: 1,
          limit: 50,
          categoryId:
            form.getFieldValue('categoryCommissionId') ||
            JSON.parse(form.getFieldValue('collectionData'))?.categoryCommissionId,
        }
      : { page: 1, limit: 50 },
    { enabled: editingId ? true : false }
  );

  const {
    data: listCollection,
    status: statusCollection,
    refetch: refetchCollection,
  } = useGetCollectionCommission(
    form.getFieldValue('categoryCommissionId') || form.getFieldValue('brandData')
      ? {
          page: 1,
          limit: 50,
          categoryId:
            form.getFieldValue('categoryCommissionId') ||
            JSON.parse(form.getFieldValue('brandData'))?.categoryCommissionId,
          brandId: form.getFieldValue('brandData') ? JSON.parse(form.getFieldValue('brandData'))?.brandId : undefined,
        }
      : { page: 1, limit: 50 },
    { enabled: editingId ? true : false }
  );

  const handleClose = () => {
    dispatch(setEditingId(null));
  };

  const { mutate: create, isLoading: isLoadingCreate } = useMutation(createPromo, {
    onSuccess: () => {
      client.invalidateQueries('/promo/paging_filter');
      message.success('Create successfully!');
      handleClose();
    },
    onError: (error: IError) => {
      message.error(error?.meta.message[0]);
    },
  });

  const { mutate: update, isLoading: isLoadingUpdate } = useMutation(updatePromo, {
    onSuccess: () => {
      client.invalidateQueries('/promo/paging_filter');
      message.success('Update successfully!');
      handleClose();
    },
    onError: (error: IError) => {
      message.error(error?.meta.message[0]);
    },
  });

  const onFinish = (value: any) => {
    let request = {
      ...value,
      type: defaultValue.typeCode,
      codeLength: value?.codeLength ? Number(value?.codeLength) : undefined,
      codeNumber: value?.codeNumber ? Number(value?.codeNumber) : undefined,
      maxAmount: value?.maxAmount ? Number(value?.maxAmount) : undefined,
      minAmount: value?.minAmount ? Number(value?.minAmount) : undefined,
      promoLimit: value?.promoLimit ? Number(value?.promoLimit) : undefined,
      percent: value?.percent ? Number(value?.percent) : undefined,
      endTime: new Date(value.endTime).getTime(),
      startTime: new Date(value.startTime).getTime(),
      price: value?.price ? Number(value?.price) : 0,
      discountType: defaultValue.discountType ? 1 : 0,
      promoType: defaultValue?.byPrice ? 0 : 1,
      isVendor: defaultValue.isVendor,
      vendorId: !value.vendorId ? undefined : value.vendorId,
    };
    if (value.brandData) {
      request = { ...request, ...JSON.parse(value.brandData) };
    }
    if (value.collectionData) {
      request = { ...request, ...JSON.parse(value.collectionData) };
    }
    delete request.brandData;
    delete request.collectionData;
    const mutate = action === 'edit' ? update : create;
    const checkUpdateType =
      (!!request?.endTime && (promo?.endTime || '').toString() !== (request?.endTime || '').toString()) ||
      promo?.status !== request.status;
    const params =
      action === 'edit'
        ? { id: Number(editingId), ...request, updateType: checkUpdateType ? 'status' : 'none' }
        : request;
    mutate(params);
  };

  const handleClickRefecthCollection = () => {
    refetchCollection();
  };

  const handleClickRefecthBrand = () => {
    refetchBrand();
  };

  const handleClickRefecthCategory = () => {
    refetchCategory();
  };

  const handleValuesChange = async (e: any) => {
    if (e.categoryCommissionId) {
      form.resetFields(['brandData', 'collectionData']);
      return;
    }

    if (e.brandData) {
      form.resetFields(['categoryCommissionId', 'collectionData']);
      await refetchCategory();
      await refetchCollection();
      if (!form.getFieldValue('categoryCommissionId')) {
        form.setFieldsValue({ categoryCommissionId: JSON.parse(e.brandData)?.categoryCommissionId });
      }
      return;
    }

    if (e.collectionData) {
      form.resetFields(['brandData', 'categoryCommissionId']);
      await refetchCategory();
      await refetchBrand();
      const jsonParse = JSON.parse(e.collectionData);
      if (!form.getFieldValue('categoryCommissionId')) {
        form.setFieldsValue({
          categoryCommissionId: jsonParse?.categoryCommissionId,
        });
      }
      if (!form.getFieldValue('brandData')) {
        if (jsonParse.brandId !== null) {
          form.setFieldsValue({
            brandData: JSON.stringify({
              brandId: jsonParse.brandId,
              categoryCommissionId: jsonParse.categoryCommissionId,
            }),
          });
        }
      }
      return;
    }
  };

  const disabledDate: RangePickerProps['disabledDate'] = (current) => {
    // Can not select days before today and today
    return current && current <= moment().startOf('day');
  };

  return (
    <Drawer
      name="Promo Code"
      editingId={editingId}
      handleClose={handleClose}
      form={form}
      getLoading={
        (!!editingId &&
          (statusBrand === 'loading' || statusCategory === 'loading' || statusCollection === 'loading')) ||
        isLoadingDetail
      }
      submitLoading={isLoadingCreate || isLoadingUpdate}
      viewOnly={!isUpdate}
    >
      <Form
        form={form}
        onFinish={onFinish}
        layout="vertical"
        validateTrigger="onBlur"
        onValuesChange={handleValuesChange}
      >
        <Row gutter={[24, 24]}>
          <Col span={16}>
            <Card style={{ marginBottom: '20px' }}>
              <Title level={5} className={styles.title}>
                PROMO CODE NAME
              </Title>

              <Form.Item name="name" rules={[rules]}>
                <Input
                  maxLength={100}
                  disabled={(promo && promo?.status !== 0) || !isUpdate}
                  placeholder={'Enter promo code name'}
                />
              </Form.Item>
            </Card>
            <Card style={{ marginBottom: '20px' }}>
              <Title level={5} className={styles.title}>
                CODE TYPE
              </Title>
              <Radio.Group
                style={{ width: '100%' }}
                value={defaultValue.typeCode}
                onChange={(e) => {
                  setDefaultValue({ ...defaultValue, typeCode: e.target.value });
                  form.resetFields(['onecode', 'code', 'codeLength', 'prefix', 'suffixes', 'codeNumber']);
                  form.setFieldsValue({
                    promoLimit:
                      defaultValue.typeCode !== 'morecode' ? 1 : !promo?.limit ? undefined : promo?.limit.toString(),
                  });
                }}
              >
                <Radio value="onecode" style={{ marginBottom: '10px' }}>
                  <strong>One Code</strong>
                </Radio>
                <Form.Item
                  label="Code"
                  name="code"
                  rules={defaultValue.typeCode === 'morecode' ? [{}] : [rules]}
                  help="The user will enter this discount code when making a payment"
                >
                  <Input
                    placeholder={'Enter code'}
                    disabled={
                      (promo && promo && promo?.status !== 0) || defaultValue.typeCode === 'morecode' || !isUpdate
                    }
                  />
                </Form.Item>
                <Radio value="morecode" style={{ marginBottom: '10px' }}>
                  <strong>More Code</strong>
                </Radio>
                <Row gutter={[24, 24]}>
                  <Col span={12}>
                    <Form.Item
                      label="Number Code"
                      name="codeNumber"
                      rules={defaultValue?.typeCode === 'onecode' ? [{}] : [{ required: true }]}
                    >
                      <InputNumber
                        placeholder={'Enter number of code'}
                        min={0}
                        disabled={(promo && promo?.status !== 0) || defaultValue.typeCode === 'onecode' || !isUpdate}
                      />
                    </Form.Item>
                  </Col>
                  <Col span={12}>
                    <Form.Item
                      label="Code Length"
                      name="codeLength"
                      rules={defaultValue?.typeCode === 'onecode' ? [{}] : [{ required: true }]}
                    >
                      <InputNumber
                        placeholder={'Enter code length'}
                        min={0}
                        disabled={(promo && promo?.status !== 0) || defaultValue.typeCode === 'onecode' || !isUpdate}
                      />
                    </Form.Item>
                  </Col>
                  <Col span={12}>
                    <Form.Item label="Prefix" name="prefix">
                      <Input
                        placeholder={'Enter code prefix'}
                        disabled={(promo && promo?.status !== 0) || defaultValue.typeCode === 'onecode' || !isUpdate}
                      />
                    </Form.Item>
                  </Col>
                  <Col span={12}>
                    <Form.Item label="Suffix" name="suffixes">
                      <Input
                        placeholder={'Enter code suffix'}
                        disabled={(promo && promo?.status !== 0) || defaultValue.typeCode === 'onecode' || !isUpdate}
                      />
                    </Form.Item>
                  </Col>
                </Row>
              </Radio.Group>
            </Card>
            <Card style={{ marginBottom: '20px' }}>
              <Title level={5} className={styles.title}>
                CONFIGURATION
              </Title>

              <Radio.Group
                style={{ width: '100%' }}
                value={defaultValue.byPrice}
                onChange={(e) => {
                  setDefaultValue({ ...defaultValue, byPrice: e.target.value });
                  form.resetFields(['price', 'percent', 'maxAmount', 'minAmount']);
                }}
                disabled={!isUpdate}
              >
                <Radio value={true} style={{ marginBottom: '10px' }}>
                  <strong>Discount by money value</strong>
                </Radio>
                <Row gutter={[24, 24]}>
                  <Col span={12}>
                    <Form.Item label="Price" name="price" rules={defaultValue.byPrice ? [{ required: true }] : [{}]}>
                      <InputNumber
                        placeholder={'Enter price'}
                        addonAfter="USDC"
                        min={0}
                        disabled={(promo && promo?.status !== 0) || !defaultValue.byPrice || !isUpdate}
                        onKeyDown={(evt) => {
                          typeNumber(evt);
                        }}
                        type="number"
                      />
                    </Form.Item>
                  </Col>
                  <Col span={12}>
                    <Form.Item label="Minimun Spend" name="minAmount">
                      <InputNumber
                        placeholder={'Enter min amount'}
                        addonAfter="USDC"
                        min={0}
                        disabled={(promo && promo?.status !== 0) || !defaultValue.byPrice || !isUpdate}
                        onKeyDown={(evt) => {
                          typeNumber(evt);
                        }}
                        type="number"
                      />
                    </Form.Item>
                  </Col>
                </Row>
                <Radio value={false} style={{ marginBottom: '10px' }}>
                  <strong>Discount by pecent</strong>
                </Radio>
                <Row gutter={[24, 24]}>
                  <Col span={12}>
                    <Form.Item
                      label="Percent"
                      name="percent"
                      rules={defaultValue?.byPrice ? [{}] : [{ required: true }]}
                    >
                      <InputNumber
                        placeholder={'Enter percent'}
                        addonAfter="%"
                        min={0}
                        disabled={(promo && promo?.status !== 0) || defaultValue.byPrice || !isUpdate}
                        onKeyDown={(evt) => {
                          typeNumber(evt);
                        }}
                        type="number"
                      />
                    </Form.Item>
                  </Col>
                  <Col span={12}>
                    <Form.Item label="Maximum redution amount" name="maxAmount">
                      <InputNumber
                        placeholder={'Enter max amount'}
                        addonAfter="USDC"
                        min={0}
                        disabled={(promo && promo?.status !== 0) || defaultValue.byPrice || !isUpdate}
                        onKeyDown={(evt) => {
                          typeNumber(evt);
                        }}
                        type="number"
                      />
                    </Form.Item>
                  </Col>
                </Row>
              </Radio.Group>
              <Row gutter={[24, 24]}>
                <Col span={24}>
                  <strong>Apply for</strong>
                </Col>
                <Col span={12}>
                  <Form.Item shouldUpdate label="Category" name="categoryCommissionId">
                    <Select
                      placeholder="Select Category"
                      style={{ width: '100%' }}
                      onClick={handleClickRefecthCategory}
                      notFoundContent={!listCategoryCommission ? <Spin /> : <Empty />}
                      disabled={(promo && promo?.status !== 0) || !isUpdate}
                    >
                      {listCategoryCommission?.list?.map((item: ICategoryCommissionResponse) => (
                        <Select.Option key={item.categoryId} value={item.categoryId}>
                          {item.categoryName}
                        </Select.Option>
                      ))}
                    </Select>
                  </Form.Item>
                </Col>
                <Col span={12}>
                  <Form.Item shouldUpdate label="Brand" name="brandData">
                    <Select
                      placeholder="Select Brand"
                      style={{ width: '100%' }}
                      onClick={handleClickRefecthBrand}
                      notFoundContent={!listBrand ? <Spin /> : <Empty />}
                      disabled={(promo && promo?.status !== 0) || !isUpdate}
                    >
                      {listBrand?.list?.map((item: IBrandCommissionResponse) => (
                        <Select.Option
                          key={item.brandId}
                          value={JSON.stringify({
                            brandId: item.brandId,
                            categoryCommissionId: item.categoryId,
                          })}
                        >
                          {item.brandName}
                        </Select.Option>
                      ))}
                    </Select>
                  </Form.Item>
                </Col>
                <Col span={12}>
                  <Form.Item shouldUpdate label="Collection" name="collectionData">
                    <Select
                      disabled={(promo && promo?.status !== 0) || !isUpdate}
                      placeholder="Select Collection"
                      style={{ width: '100%' }}
                      onClick={handleClickRefecthCollection}
                      notFoundContent={!listCollection ? <Spin /> : <Empty />}
                    >
                      {listCollection?.list?.map((item: ICollectionCommissionResponse) => (
                        <Select.Option
                          key={item.collectionId}
                          value={JSON.stringify({
                            collectionId: item.collectionId,
                            brandId: item.brandId,
                            categoryCommissionId: item.categoryId,
                          })}
                        >
                          {item.collectionName}
                        </Select.Option>
                      ))}
                    </Select>
                  </Form.Item>
                </Col>
              </Row>
            </Card>
            <Card style={{ marginBottom: '20px' }}>
              <Title level={5} className={styles.title}>
                PROMO CODE CREATED FOR
              </Title>

              <Row gutter={[24, 24]}>
                <Radio.Group
                  disabled={!isUpdate}
                  style={{ width: '100%' }}
                  value={defaultValue.isVendor}
                  onChange={(e) => setDefaultValue({ ...defaultValue, isVendor: e.target.value })}
                >
                  <Radio value={true} style={{ marginBottom: '10px' }}>
                    <strong>Vendor</strong>
                  </Radio>
                  <Col span={12}>
                    <Form.Item name="vendorId">
                      <Select
                        placeholder="Select Vendor"
                        style={{ width: '100%' }}
                        disabled={(promo && promo?.status !== 0) || !defaultValue.isVendor || !isUpdate}
                      >
                        <Select.Option value={null}>Select Vendor</Select.Option>
                        {listVendor?.list?.map((item: IVendorResponse) => (
                          <Select.Option key={item?.id} value={item?.id}>
                            <Tooltip title={item?.vendorName}>{item?.vendorName}</Tooltip>
                          </Select.Option>
                        ))}
                      </Select>
                    </Form.Item>
                  </Col>
                </Radio.Group>
              </Row>
            </Card>
          </Col>
          <Col span={8}>
            <Card style={{ marginBottom: '20px' }}>
              <Title level={5} className={styles.title}>
                DISCOUNT TYPE
              </Title>

              <Radio.Group
                disabled={!isUpdate}
                style={{ width: '100%' }}
                value={defaultValue.discountType}
                onChange={(e) => setDefaultValue({ ...defaultValue, discountType: e.target.value })}
              >
                <Radio value={false} style={{ marginBottom: '10px' }}>
                  <strong>Discount on Service Fees</strong>
                </Radio>
                <Radio value={true} style={{ marginBottom: '10px' }}>
                  <strong>Discount on the Price of Nft</strong>
                </Radio>
              </Radio.Group>
            </Card>
            <Card style={{ marginBottom: '20px' }}>
              <Title level={5} className={styles.title}>
                NUMBER OF TIMES USE ON 01 CODE
              </Title>
              <Radio.Group
                disabled={!isUpdate}
                style={{ width: '100%' }}
                value={defaultValue.limitUser}
                onChange={(e) => {
                  setDefaultValue({ ...defaultValue, limitUser: e.target.value });
                  form.resetFields(['promoLimit']);
                }}
              >
                <Radio value={true} style={{ marginBottom: '10px' }}>
                  <strong>Limit number of times</strong>
                </Radio>
                <Form.Item name="promoLimit" rules={!defaultValue?.limitUser ? [{}] : [{ required: true }]}>
                  <InputNumber
                    placeholder={'Enter number of user'}
                    min={0}
                    disabled={
                      (promo && promo?.status !== 0) ||
                      !defaultValue.limitUser ||
                      !isUpdate ||
                      defaultValue.typeCode === 'morecode'
                    }
                    onKeyDown={(evt) => {
                      typeNumber(evt);
                    }}
                    type="number"
                  />
                </Form.Item>
                <Radio value={false} style={{ marginBottom: '10px' }} disabled={defaultValue.typeCode === 'morecode'}>
                  <strong>Unlimited</strong>
                </Radio>
              </Radio.Group>
            </Card>
            <Card style={{ marginBottom: '20px' }}>
              <Radio.Group
                disabled={!isUpdate}
                style={{ width: '100%' }}
                value={defaultValue.limitDate}
                onChange={(e) => {
                  setDefaultValue({ ...defaultValue, limitDate: e.target.value });
                  form.resetFields(['startTime', 'endTime']);
                  if (!defaultValue?.limitDate) {
                    form.setFieldsValue({
                      startTime: promo?.startTime ? moment(new Date(Number(promo?.startTime))) : moment(new Date()),
                    });
                  }
                }}
              >
                <Title level={5} className={styles.title}>
                  EXPIRY DATE
                </Title>
                <div></div>
                <Radio value={true} style={{ marginBottom: '10px' }}>
                  <strong>Limit</strong>
                </Radio>
                <Row gutter={[16, 16]}>
                  <Col span={12}>
                    <Form.Item
                      label="Start time"
                      name="startTime"
                      rules={!defaultValue?.limitDate ? [{}] : [{ required: true }]}
                    >
                      <DatePicker
                        disabledDate={disabledDate}
                        disabled={(promo && promo && promo?.status !== 0) || !defaultValue.limitDate || !isUpdate}
                        style={{ width: '100%' }}
                        // showTime
                        format={(value) => value?.format('YYYY/MM/DD')}
                      />
                    </Form.Item>
                  </Col>
                  <Col span={12}>
                    <Form.Item
                      label="End time"
                      name="endTime"
                      rules={!defaultValue?.limitDate ? [{}] : [{ required: true }]}
                    >
                      <DatePicker
                        disabledDate={disabledDate}
                        disabled={promo?.status === 3 || !defaultValue.limitDate || !isUpdate}
                        style={{ width: '100%' }}
                        // showTime
                        format={(value) => value?.format('YYYY/MM/DD')}
                      />
                    </Form.Item>
                  </Col>
                </Row>
                <Radio value={false} style={{ marginBottom: '10px' }}>
                  <strong>Never</strong>
                </Radio>
              </Radio.Group>
            </Card>
            <Card style={{ marginBottom: '20px' }}>
              <Title level={5} className={styles.title}>
                STATUS
              </Title>

              <Form.Item name="status" rules={[{ required: true }]}>
                <Select placeholder="Select status" disabled={promo?.status === 3 || !isUpdate}>
                  {Object.keys(
                    editingId
                      ? PROMO_STATUS_ENUMS
                      : {
                          0: 'New',
                        }
                  ).map((key: string) => (
                    <Select.Option
                      key={key}
                      value={Number(key)}
                      disabled={
                        (Number(key) === 0 && promo?.status === 1) ||
                        (Number(key) === 0 && promo?.status === 2) ||
                        (promo?.endTime < new Date() && Number(key) === 0 && promo?.status === 2) ||
                        (promo?.endTime < new Date() && Number(key) === 1 && promo?.status === 2)
                      }
                    >
                      {PROMO_STATUS_ENUMS[Number(key || 0) as 0 | 1]}
                    </Select.Option>
                  ))}
                </Select>
              </Form.Item>
            </Card>
          </Col>
        </Row>
      </Form>
    </Drawer>
  );
};

export default Editor;
