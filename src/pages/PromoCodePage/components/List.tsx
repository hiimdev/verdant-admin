import { Button } from 'antd';
import { useGetPromoCode } from 'api/promo';
import { downLoadAllPromo, downloadExcel } from 'api/promo/requet';
import { IError } from 'api/types';
import { PaginatedTableAndSearch } from 'components/PaginatedTableAndSearch';
import { useAppDispatch } from 'hooks/reduxHook';
import { FC, useMemo, useState } from 'react';
import { useMutation } from 'react-query';
import { setEditingId } from 'store/ducks/promoCode/slice';
import { debounce, onDownload } from 'utils/js';
import { message } from 'utils/message';
import { Permission } from 'utils/permission';
import { getColumns } from './Column';
import { IListType } from './type';

const List: FC<IListType> = ({ isUpdate }) => {
  const dispatch = useAppDispatch();
  const [params, setParams] = useState<any>({ sort: [] });
  const debouncedSetParams = useMemo(() => debounce(setParams, 500), []);

  const submitParams = (value: any) => {
    console.log(value, '1');
    debouncedSetParams((prevParams: any) => ({
      ...prevParams,
      ...value,
      status: value.status ? Number(value.status) : undefined,
      isVendor: value.isVendor ? Number(value.isVendor) : undefined,
      sort: value.sort ? [value.sort] : [],
    }));
  };

  const { mutate: downLoadDetail } = useMutation(downloadExcel, {
    onSuccess: (res) => {
      onDownload(res, 'Download');
    },
    onError: (error: IError) => {
      message.error(error?.meta.message[0]);
    },
  });
  const onDownLoad = (id) => {
    downLoadDetail({ promoId: id });
  };
  const columns = useMemo(() => getColumns(dispatch, submitParams, onDownLoad, isUpdate), [getColumns, isUpdate]);

  const { mutate: downLoadAll } = useMutation(downLoadAllPromo, {
    onSuccess: (res) => {
      onDownload(res, 'Download');
    },
    onError: (error: IError) => {
      message.error(error?.meta.message[0]);
    },
  });

  return (
    <>
      {/* <Header
        title=""
        titleButton="New Project"
        setEditingId={setEditingId}
        viewOnly
        exportParams={exportParams}
      ></Header> */}
      <PaginatedTableAndSearch
        permission={Permission.add_promo_code}
        title="Promo Code Management"
        setEditingId={setEditingId}
        columns={columns}
        useQuery={useGetPromoCode}
        requestParams={params}
        othersExtra={
          isUpdate ? (
            <Button size="large" onClick={() => downLoadAll(params)}>
              Export
            </Button>
          ) : null
        }
        handleReset={() => setParams({ sort: [] })}
      />
    </>
  );
};

export default List;
