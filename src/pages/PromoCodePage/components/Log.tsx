import { Modal } from 'antd';
import useBreakpoint from 'antd/lib/grid/hooks/useBreakpoint';
import { useGetPromoListLog } from 'api/promo';
import { downloadExcel } from 'api/promo/requet';
import { IError } from 'api/types';
import { Button } from 'components/Button';
import { PaginatedTableAndSearch } from 'components/PaginatedTableAndSearch';
import { SearchFilter } from 'components/Popover';
import { SelectFilterV2 } from 'components/Popover/SelectFilterV2';
import { useAppDispatch, useAppSelector } from 'hooks/reduxHook';
import { FC, useMemo, useState } from 'react';
import { useMutation } from 'react-query';

import { getLogId, setLogId } from 'store/ducks/promoCode/slice';
import { PROMOCODE_TYPE_ENUM, PROMO_SORT } from 'utils/constant';
import { timeFormatter } from 'utils/date';
import { debounce, onDownload } from 'utils/js';
import { message } from 'utils/message';

const getColumns = (submitParams?: any) => {
  return [
    {
      title: 'No',
      dataIndex: 'index',
      key: 'index',
      align: 'center',
    },
    {
      title: (
        <div className="flex-center-between">
          Name
          <SearchFilter
            label="name"
            onSubmit={submitParams}
            name="name"
            sort={[PROMO_SORT.NAME_DESC, PROMO_SORT.NAME_ASC]}
          />
        </div>
      ),
      dataIndex: 'name',
      key: 'name',
      align: 'center',
    },
    {
      title: (
        <div className="flex-center-between">
          Code
          <SearchFilter
            label="code"
            onSubmit={submitParams}
            name="code"
            sort={[PROMO_SORT.CODE_DESC, PROMO_SORT.CODE_ASC]}
          />
        </div>
      ),
      dataIndex: 'code',
      key: 'code',
      align: 'center',
    },
    {
      title: <div className="flex-center-between">Limit user</div>,
      dataIndex: 'promo_limit_limit',
      key: 'promo_limit_limit',
      align: 'center',
      render(data: string) {
        return <>{data || 'Unlimited'}</>;
      },
    },
    {
      title: (
        <div className="flex-center-between">
          Promote Type
          <SelectFilterV2
            label="promoType"
            onSubmit={submitParams}
            name="promoType"
            sort={[PROMO_SORT.PROMOTYPE_DESC, PROMO_SORT.PROMOTYPE_ASC]}
            obj={PROMOCODE_TYPE_ENUM}
          />
        </div>
      ),
      dataIndex: 'promo_type',
      key: 'promo_type',
      align: 'center',
      render(data: string) {
        return <>{data ? 'Percent' : 'Price'}</>;
      },
    },
    {
      title: 'Discount Type',
      dataIndex: 'discount_type',
      key: 'discount_type',
      align: 'center',
      render(data: string) {
        return <>{data ? 'Price' : 'Service Fees'}</>;
      },
    },
    {
      title: 'Status',
      dataIndex: 'is_use',
      key: 'is_use',
      align: 'center',
      render(data: string) {
        return <>{data ? 'Used' : 'Not Used'}</>;
      },
    },
    {
      title: (
        <div className="flex-center-between">
          Discount Price
          <SearchFilter
            label="price"
            onSubmit={submitParams}
            name="price"
            sort={[PROMO_SORT.PRICE_DESC, PROMO_SORT.PRICE_ASC]}
          />
        </div>
      ),
      dataIndex: 'price',
      key: 'price',
      align: 'center',
      render(data: string) {
        return <>{data ? `${Number(data)} USD` : '-'}</>;
      },
    },
    {
      title: (
        <div className="flex-center-between">
          Discount Percent
          <SearchFilter
            label="percent"
            onSubmit={submitParams}
            name="percent"
            sort={[PROMO_SORT.PERCENT_DESC, PROMO_SORT.PERCENT_ASC]}
          />
        </div>
      ),
      dataIndex: 'percent',
      key: 'percent',
      align: 'center',
      render(data: string) {
        return <>{data ? `${data}%` : '-'}</>;
      },
    },
    {
      title: 'Max Discount Price',
      dataIndex: 'max_amount',
      key: 'max_amount',
      align: 'center',
      render(data: string) {
        return <>{data ? `${Number(data)} USD` : '-'}</>;
      },
    },
    {
      title: (
        <div className="flex-center-between">
          Category
          <SearchFilter
            label="categoryName"
            onSubmit={submitParams}
            name="categoryName"
            sort={[PROMO_SORT.CATEGORYNAME_DESC, PROMO_SORT.CATEGORYNAME_ASC]}
          />
        </div>
      ),
      dataIndex: 'category_name',
      key: 'category_name',
      align: 'center',
    },
    {
      title: (
        <div className="flex-center-between">
          Brand
          <SearchFilter
            label="brandName"
            onSubmit={submitParams}
            name="brandName"
            sort={[PROMO_SORT.BRANDNAME_DESC, PROMO_SORT.BRANDNAME_ASC]}
          />
        </div>
      ),
      dataIndex: 'brand_name',
      key: 'brand_name',
      align: 'center',
      render(data: string) {
        return <>{data || '-'}</>;
      },
    },
    {
      title: (
        <div className="flex-center-between">
          Collection
          <SearchFilter
            label="collectionName"
            onSubmit={submitParams}
            name="collectionName"
            sort={[PROMO_SORT.COLLECTIONNAME_DESC, PROMO_SORT.COLLECTIONNAME_ASC]}
          />
        </div>
      ),
      dataIndex: 'collection_name',
      key: 'collection_name',
      align: 'center',
      render(data: string) {
        return <>{data || '-'}</>;
      },
    },
    {
      title: (
        <div className="flex-center-between">
          Start Date
          <SelectFilterV2
            label="startTime"
            onSubmit={submitParams}
            sort={[PROMO_SORT.STARTTIME_DESC, PROMO_SORT.STARTTIME_ASC]}
          />
        </div>
      ),
      dataIndex: 'start_time',
      key: 'start_time',
      align: 'center',
      render(data: string) {
        return <>{data ? timeFormatter(Number(data)) : '-'}</>;
      },
    },
    {
      title: (
        <div className="flex-center-between">
          End Date
          <SelectFilterV2
            label="endTime"
            onSubmit={submitParams}
            sort={[PROMO_SORT.ENDTIME_DESC, PROMO_SORT.ENDTIME_ASC]}
          />
        </div>
      ),
      dataIndex: 'end_time',
      key: 'end_time',
      align: 'center',
      render(data: string) {
        return <>{data ? timeFormatter(Number(data)) : '-'}</>;
      },
    },
  ];
};

const Log: FC = () => {
  const [params, setParams] = useState<any>({ sort: [] });
  const screens = useBreakpoint();
  const dispatch = useAppDispatch();
  const logId = useAppSelector(getLogId);
  const debouncedSetParams = useMemo(() => debounce(setParams, 500), []);

  const submitParams = (value: any) => {
    debouncedSetParams((prevParams: any) => ({
      ...prevParams,
      ...value,
      promoType: Number(value?.promoType) || undefined,
      price: Number(value?.price) || undefined,
      sort: value.sort ? [value.sort] : [],
    }));
  };

  const queryParams = useMemo(() => {
    const temp = params;
    for (const key in temp) {
      if (temp[key] === undefined) {
        delete temp[key];
      }
    }
    return temp;
  }, [params]);

  const visible = logId !== null;

  const handleClose = () => {
    dispatch(setLogId(null));
  };

  const columns = useMemo(() => getColumns(submitParams), [getColumns, logId, params]);

  const { mutate: downLoadDetail } = useMutation(downloadExcel, {
    onSuccess: (res) => {
      onDownload(res, 'Download');
    },
    onError: (error: IError) => {
      message.error(error?.meta.message[0]);
    },
  });
  const onDownLoad = () => {
    downLoadDetail({ promoId: logId, ...queryParams });
  };

  return (
    <Modal
      width={1150}
      visible={visible}
      onOk={handleClose}
      onCancel={handleClose}
      cancelButtonProps={{ hidden: true }}
    >
      <PaginatedTableAndSearch
        title="Promo Code - View Detail"
        columns={columns}
        useQuery={useGetPromoListLog}
        scroll={screens.xxl ? {} : { x: 1600 }}
        requestParams={{ ...params, promoId: Number(logId) }}
        othersExtra={<Button onClick={onDownLoad}>Export to excel</Button>}
        handleReset={() => setParams({ sort: [] })}
      />
    </Modal>
  );
};

export default Log;
