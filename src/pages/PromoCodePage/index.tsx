import { useAppSelector } from 'hooks/reduxHook';
import { FC, useMemo } from 'react';
import { getLogId } from 'store/ducks/promoCode/slice';
import { getPermission } from 'store/ducks/user/slice';
import { Permission } from 'utils/permission';
import Editor from './components/Editor';
import List from './components/List';
import Log from './components/Log';
import { getEditingId } from 'store/ducks/promoCode/slice';

const PromoCode: FC = () => {
  const logId = useAppSelector(getLogId);
  const permissions = useAppSelector(getPermission);
  const editingId = useAppSelector(getEditingId);

  const isUpdate = useMemo(() => {
    return permissions.includes(Permission.edit_download_detail_project);
  }, [permissions]);

  return (
    <>
      <List isUpdate={isUpdate} />
      {editingId !== null && <Editor isUpdate={isUpdate} />}
      {logId && <Log />}
    </>
  );
};

export default PromoCode;
