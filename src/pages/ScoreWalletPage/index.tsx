import { Button, Form, Row, Typography } from 'antd';
import { useGetScoreWallet } from 'api/admin';
import { setScoreWallet } from 'api/admin/request';
import { IError } from 'api/types';
import clsx from 'clsx';
import { InputNumber } from 'components/Input';
import { useEffect } from 'react';
import { useMutation, useQueryClient } from 'react-query';
import { message } from 'utils/message';
import styles from './styles.module.less';

function ScoreWalletSetting() {
  const [form] = Form.useForm();
  const client = useQueryClient();
  const { data } = useGetScoreWallet();

  const { mutate: setScoreWalletMutate, isLoading: loadingGas } = useMutation(setScoreWallet, {
    onSuccess: () => {
      client.invalidateQueries('/admin/get-gas');
      message.success('Update score wallet successfully!');
    },
    onError: (error: IError) => {
      message.error(error?.meta.message[0]);
    },
  });

  const onFinish = (values) => {
    setScoreWalletMutate({
      score: Number(values.score),
    });
  };

  useEffect(() => {
    if (data?.configValue) {
      form.setFieldsValue({ score: Number(data?.configValue) });
    }
  }, [data]);

  return (
    <div className={clsx(styles.root)}>
      <div className={clsx(styles.container)}>
        <Typography.Title className={clsx(styles.title)} level={3}>
          Score Wallet setting
        </Typography.Title>
        <Form form={form} onFinish={onFinish} layout="vertical">
          <Form.Item label="The maximum score for a wallet to pass KYT" name="score" rules={[{ required: true }]}>
            <InputNumber type="number" />
          </Form.Item>
          <Row justify="center">
            <Button htmlType="submit" type="primary" loading={loadingGas}>
              Save
            </Button>
          </Row>
        </Form>
      </div>
    </div>
  );
}

export default ScoreWalletSetting;
