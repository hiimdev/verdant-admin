import { Button, Form, Row, Typography } from 'antd';
import { useGetGasTank } from 'api/admin';
import { setGasTank } from 'api/admin/request';
import { IError } from 'api/types';
import clsx from 'clsx';
import { InputNumber } from 'components/Input';
import { useEffect } from 'react';
import { useMutation, useQueryClient } from 'react-query';
import { message } from 'utils/message';
import styles from './styles.module.less';
import { convertPriceEther } from 'utils/common';

function SetGasTankTransactionFee() {
  const [form] = Form.useForm();
  const client = useQueryClient();
  const { data } = useGetGasTank();

  const { mutate: setGasTankMutate, isLoading: loadingGas } = useMutation(setGasTank, {
    onSuccess: () => {
      client.invalidateQueries('/admin/get-seed-amount');
      message.success('Set gas tank transaction fee successfully!');
    },
    onError: (error: IError) => {
      message.error(error?.meta.message[0]);
    },
  });

  const onFinish = (values) => {
    setGasTankMutate({
      amount: Number(values.amount),
    });
  };

  useEffect(() => {
    if (data?.configValue) {
      form.setFieldsValue({ amount: convertPriceEther(data?.configValue) });
    }
  }, [data]);

  return (
    <div className={clsx(styles.root)}>
      <div className={clsx(styles.container)}>
        <Typography.Title className={clsx(styles.title)} level={3}>
          Gas Tank Transaction Fee
        </Typography.Title>
        <Form form={form} onFinish={onFinish} layout="vertical">
          <Form.Item label="Set Gas Tank Transaction Fee" name="amount" rules={[{ required: true }]}>
            <InputNumber type="number" addonAfter="MATIC" />
          </Form.Item>
          <Row justify="center">
            <Button htmlType="submit" type="primary" loading={loadingGas}>
              Save
            </Button>
          </Row>
        </Form>
      </div>
    </div>
  );
}

export default SetGasTankTransactionFee;
