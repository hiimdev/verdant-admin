import { Button, Form, Row, Typography } from 'antd';
import { useGetMailbox } from 'api/admin';
import { setMailboxContactUs } from 'api/admin/request';
import { IError } from 'api/types';
import clsx from 'clsx';
import { Input } from 'components/Input';
import { useEffect } from 'react';
import { useMutation, useQueryClient } from 'react-query';
import { message } from 'utils/message';
import styles from './styles.module.less';

function SetMailbox() {
  const [form] = Form.useForm();
  const client = useQueryClient();
  const { data } = useGetMailbox();

  const { mutate: setEmailbox, isLoading: loadingEmail } = useMutation(setMailboxContactUs, {
    onSuccess: () => {
      client.invalidateQueries('/admin/get-mailbox-contactus');
      message.success('Update mailbox successfully!');
    },
    onError: (error: IError) => {
      message.error(error?.meta.message[0]);
    },
  });

  const onFinish = (values) => {
    setEmailbox({
      email: values.email,
    });
  };

  useEffect(() => {
    if (data?.configValue) {
      form.setFieldsValue({ email: data?.configValue });
    }
  }, [data]);

  return (
    <div className={clsx(styles.root)}>
      <div className={clsx(styles.container)}>
        <Typography.Title className={clsx(styles.title)} level={3}>
          Mailbox Contact Us
        </Typography.Title>
        <Form form={form} onFinish={onFinish} layout="vertical">
          <Form.Item label="Set Mailbox Contact Us" name="email" rules={[{ required: true }]}>
            <Input type="string" />
          </Form.Item>
          <Row justify="center">
            <Button htmlType="submit" type="primary" loading={loadingEmail}>
              Save
            </Button>
          </Row>
        </Form>
      </div>
    </div>
  );
}

export default SetMailbox;
