import { CloseCircleOutlined, EditOutlined, InfoCircleOutlined, SaveOutlined } from '@ant-design/icons';
import { AnyAction, Dispatch } from '@reduxjs/toolkit';
import { Button, Space } from 'antd';
import { Input } from 'components/Input';
import { TableTitle } from 'components/Table/component/TableTitle';
import { setLogId } from 'store/ducks/commission/slice';
import { TAX_SORT } from 'utils/constant';
import { typeNumber } from 'utils/form';
import styles from './styles.module.less';

export const getColumns = (
  dispatch: Dispatch<AnyAction>,
  idEdit: any,
  setIdEdit: any,
  setValueInput: any,
  onEdit: (data: any) => void,
  submitParams?: any,
  isUpdate?: boolean,
  valueInput?: any
) => {
  return [
    {
      title: (
        <TableTitle
          label="Category"
          onSubmit={submitParams}
          name="category_commission_id"
          sort={[TAX_SORT.CATEGORY_DESC, TAX_SORT.CATEGORY_ASC]}
        />
      ),
      dataIndex: 'category_commission_id',
      key: 'category_commission_id',
      align: 'center',
      width: '200px',
      render(data: any, record: any) {
        return <>{record.category_name || '-'}</>;
      },
    },
    {
      title: (
        <TableTitle
          label="Brand"
          onSubmit={submitParams}
          name="brand_id"
          sort={[TAX_SORT.BRAND_DESC, TAX_SORT.BRAND_ASC]}
        />
      ),
      dataIndex: 'brand_id',
      key: 'brand_id',
      align: 'center',
      width: '200px',
      render(data: any, record: any) {
        return <>{record?.brand_name || '-'}</>;
      },
    },
    {
      title: (
        <TableTitle
          label="Collection"
          onSubmit={submitParams}
          name="collection_id"
          sort={[TAX_SORT.COLLECTION_DESC, TAX_SORT.COLLECTION_ASC]}
        />
      ),
      dataIndex: 'collection_id',
      key: 'collection_id',
      align: 'center',
      width: '200px',

      render(data: any, record: any) {
        return <>{record?.collection_name || '-'}</>;
      },
    },
    {
      title: (
        <TableTitle
          label="Vendor"
          onSubmit={submitParams}
          name="vendor_id"
          sort={[TAX_SORT.TAX_TYPE_DESC, TAX_SORT.VENDOR_ASC]}
        />
      ),
      dataIndex: 'vendor_id',
      key: 'vendor_id',
      align: 'center',
      width: '150px',
      render(data: any, record: any) {
        return <>{record?.vendor_name || '-'}</>;
      },
    },
    {
      title: (
        <TableTitle
          label="Country"
          onSubmit={submitParams}
          name="country_name"
          sort={[TAX_SORT.COUNTRY_DESC, TAX_SORT.COUNTRY_ASC]}
        />
      ),
      dataIndex: 'country_name',
      key: 'country_name',
      align: 'center',
      width: '180px',
      render(data: any) {
        return <>{data || '-'}</>;
      },
    },
    {
      title: (
        <TableTitle
          label="Tax Type"
          onSubmit={submitParams}
          name="tax_name"
          sort={[TAX_SORT.TAX_TYPE_DESC, TAX_SORT.TAX_TYPE_ASC]}
        />
      ),
      dataIndex: 'tax_name',
      key: 'tax_name',
      align: 'center',
      width: '140px',
      render(data: any) {
        return <>{data || '-'}</>;
      },
    },
    {
      title: (
        <TableTitle
          label="Seller Tax Value"
          onSubmit={submitParams}
          name="tax_fee"
          sort={[TAX_SORT.TAXFEE_DESC, TAX_SORT.TAXFEE_ASC]}
        />
      ),
      key: 'tax_fee',
      align: 'center',
      width: '200px',
      render(data: any, record: any) {
        return idEdit === record.id ? (
          <Input
            required
            type="number"
            suffix="%"
            min={0}
            onKeyDown={(evt: any) => {
              typeNumber(evt);
            }}
            value={valueInput.taxFee}
            onChange={(e) => {
              if (record.is_gts) {
                setValueInput((prev) => ({ ...prev, taxFee: e.target.value, taxFeeBuyer: e.target.value }));
              } else setValueInput((prev) => ({ ...prev, taxFee: e.target.value }));
            }}
            defaultValue={`${data?.tax_fee}` || 0}
          />
        ) : (
          <>{data.tax_fee}%</>
        );
      },
    },
    {
      title: (
        <TableTitle
          label="Buyer Tax Value"
          onSubmit={submitParams}
          name="tax_fee_buyer"
          sort={[TAX_SORT.TAXFEE_DESC, TAX_SORT.TAXFEE_ASC]}
        />
      ),
      key: 'tax_fee_buyer',
      align: 'center',
      width: '200px',
      render(data: any, record: any) {
        return idEdit === record.id ? (
          <Input
            required
            type="number"
            suffix="%"
            min={0}
            onKeyDown={(evt: any) => {
              typeNumber(evt);
            }}
            value={valueInput.taxFeeBuyer}
            onChange={(e) => {
              if (record.is_gts) {
                setValueInput((prev) => ({ ...prev, taxFee: e.target.value, taxFeeBuyer: e.target.value }));
              } else setValueInput((prev) => ({ ...prev, taxFeeBuyer: e.target.value }));
            }}
            defaultValue={`${data?.tax_fee_buyer}` || 0}
          />
        ) : (
          <>{data.tax_fee_buyer}%</>
        );
      },
    },
    {
      title: 'Log',
      align: 'center',
      width: '100px',
      fixed: 'right',
      render(data: any, record: any) {
        return idEdit === record.id ? (
          <Space>
            <Button
              icon={<SaveOutlined />}
              className={styles.btnAction}
              onClick={() => {
                onEdit(data);
              }}
              title="Save"
            />

            <Button
              icon={<CloseCircleOutlined />}
              className={styles.btnAction}
              onClick={() => {
                setIdEdit(null);
              }}
              title="Cancel"
            />
          </Space>
        ) : (
          <Space>
            {isUpdate && (
              <Button
                icon={<EditOutlined />}
                className={styles.btnAction}
                onClick={() => {
                  setIdEdit(record.id);
                  setValueInput({ taxFee: record.tax_fee, taxFeeBuyer: record.tax_fee_buyer });
                }}
                type="primary"
                title="Edit"
              />
            )}
            <Button
              icon={<InfoCircleOutlined />}
              title="Log"
              onClick={() =>
                dispatch(
                  setLogId({
                    category_commission_id: record.category_commission_id,
                    collection_id: record.collection_id,
                    brand_id: record.brand_id,
                    taxId: record.tax_id,
                  })
                )
              }
              className={styles.btnAction}
            />
          </Space>
        );
      },
    },
  ];
};
