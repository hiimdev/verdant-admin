import { Form } from 'antd';
import { useGetBrandCommission, useGetCategoryCommission, useGetCollectionCommission } from 'api/commission';
import { useGetListTax } from 'api/tax';
import { createTaxValue } from 'api/tax/request';
import { IError } from 'api/types';
import { useGetListVendorInfinteQueries } from 'api/user';
import { Drawer } from 'components/Drawer';
import { InfiniteSelect } from 'components/InfiniteSelect';
import { Input } from 'components/Input';
import { Select } from 'components/Select';
import { useAppDispatch, useAppSelector } from 'hooks/reduxHook';
import { FC, useState } from 'react';
import { useMutation, useQueryClient } from 'react-query';
import { getEditingId, setEditingId } from 'store/ducks/tax/slice';
import { typeNumber } from 'utils/form';
import { message } from 'utils/message';

const Editor: FC = () => {
  const dispatch = useAppDispatch();
  const editingId = useAppSelector(getEditingId);
  const client = useQueryClient();
  const [form] = Form.useForm();
  const [isGST, setIsGST] = useState(false);

  const { mutate: create, isLoading: isLoadingCreateCommission } = useMutation(createTaxValue, {
    onSuccess: () => {
      client.invalidateQueries('/tax/list-tax');
      message.success('Success');
      dispatch(setEditingId(null));
    },
    onError: (error: IError) => {
      message.error(error?.meta.message[0]);
    },
  });
  const handleClose = () => {
    dispatch(setEditingId(null));
  };

  const onFinish = (value: any) => {
    const mutate = create;
    const params = {
      action: 'add',
      categoryCommissionId: Number(value?.category_commission_id),
      collectionId: Number(value?.collection_id?.split('-')[0]),
      brandId: Number(value?.brand_id?.split('-')[0]),
      taxId: Number(value?.Taxtype_id),
      taxFee: Number(value?.taxFee),
      taxFeeBuyer: Number(value?.taxFeeBuyer),
      countryId: value?.countryId,
      vendorId: value?.vendorId,
    };

    mutate(params);
  };
  const { data: catagory, refetch: refetchCatagory } = useGetCategoryCommission({ page: 1, limit: 50 });
  const { data: listBrand, refetch: refetchBrand } = useGetBrandCommission(
    form.getFieldValue('category_commission_id')
      ? {
          page: 1,
          limit: 50,
          categoryId: form.getFieldValue('category_commission_id'),
        }
      : { page: 1, limit: 50 }
  );
  const { data: listCollection, refetch: refetchCollection } = useGetCollectionCommission(
    form.getFieldValue('category_commission_id') || form.getFieldValue('brand_id')
      ? {
          page: 1,
          limit: 50,
          categoryId: form.getFieldValue('category_commission_id'),
          brandId: form.getFieldValue('brand_id') ? form.getFieldValue('brand_id').split('-')[0] : undefined,
        }
      : { page: 1, limit: 50 }
  );
  const { data: listtax } = useGetListTax({ page: 1, limit: 50 });
  const {
    data: vendorResponse,
    fetchNextPage: fetchMoreVendor,
    hasNextPage: hasNextVendor,
    isFetchingNextPage: isFetchingMoreVendor,
    isLoading: isVendorLoading,
  } = useGetListVendorInfinteQueries({ limit: 20 });

  const vendors = vendorResponse?.pages
    ?.map((d) => d.list)
    .reduce((current, arr) => {
      return current.concat(arr);
    }, []);

  const onChangeTaxType = (e) => {
    setIsGST(!!listtax?.list.filter((item) => item.id === e)[0].isGts);
  };

  return (
    <Drawer
      name="Tax"
      editingId={editingId}
      handleClose={handleClose}
      form={form}
      submitLoading={isLoadingCreateCommission}
    >
      <Form form={form} onFinish={onFinish}>
        <Form.Item label="Category" name="category_commission_id" labelCol={{ span: 3 }} wrapperCol={{ span: 18 }}>
          <Select
            placeholder="Select category"
            onChange={() => {
              form.setFieldsValue({ brand_id: null, collection_id: null });
            }}
            onClick={() => {
              refetchCatagory();
            }}
          >
            {catagory?.list?.map((item) => (
              <Select.Option key={item.categoryId} value={item.categoryId}>
                {item.categoryName}
              </Select.Option>
            ))}
          </Select>
        </Form.Item>

        <Form.Item label="Brand" name="brand_id" labelCol={{ span: 3 }} wrapperCol={{ span: 18 }}>
          <Select
            placeholder="Select brand"
            onChange={(val: string) => {
              form.setFieldsValue({ category_commission_id: Number(val.split('-')[1]), collection_id: null });
            }}
            onClick={() => {
              refetchBrand();
            }}
          >
            {listBrand?.list?.map((item) => (
              <Select.Option key={item.brandId} value={`${item.brandId}-${item.categoryId}`}>
                {item.brandName}
              </Select.Option>
            ))}
          </Select>
        </Form.Item>

        <Form.Item label="Collection" name="collection_id" labelCol={{ span: 3 }} wrapperCol={{ span: 18 }}>
          <Select
            placeholder="Select collection"
            onChange={(val: string) => {
              const split_arr = val.split('-');
              form.setFieldsValue({
                brand_id: Number(split_arr[1]) ? `${split_arr[1]}-${split_arr[2]}` : null,
                category_commission_id: Number(split_arr[2]) ? Number(split_arr[2]) : null,
              });
            }}
            onClick={() => {
              refetchCollection();
            }}
          >
            {listCollection?.list?.map((item) => (
              <Select.Option key={item.collectionId} value={`${item.collectionId}-${item.brandId}-${item.categoryId}`}>
                {item.collectionName}
              </Select.Option>
            ))}
          </Select>
        </Form.Item>
        <Form.Item label="Vendor" name="vendorId" labelCol={{ span: 3 }} wrapperCol={{ span: 18 }}>
          <InfiniteSelect
            hasMore={hasNextVendor as boolean}
            loading={isFetchingMoreVendor || isVendorLoading}
            loadMore={fetchMoreVendor}
            placeholder="Select vendor"
          >
            {vendors?.map((item) => (
              <InfiniteSelect.Option key={item.id} value={item.id}>
                {item.vendorName}
              </InfiniteSelect.Option>
            ))}
          </InfiniteSelect>
        </Form.Item>
        <Form.Item
          label="Tax Type"
          name="Taxtype_id"
          labelCol={{ span: 3 }}
          wrapperCol={{ span: 18 }}
          rules={[{ required: true }]}
        >
          <Select placeholder="Select tax" onChange={onChangeTaxType}>
            {listtax?.list?.map((item: any) => (
              <Select.Option key={item?.id} value={item?.id}>
                {item?.tax_name}
              </Select.Option>
            ))}
          </Select>
        </Form.Item>
        <Form.Item
          label="Seller Tax Value"
          name="taxFee"
          labelCol={{ span: 3 }}
          wrapperCol={{ span: 18 }}
          rules={[{ required: true }]}
        >
          <Input
            type="number"
            suffix="%"
            defaultValue={0}
            min={0}
            onKeyDown={(evt: any) => {
              typeNumber(evt);
            }}
            onChange={(e) => {
              if (isGST) {
                form.setFieldsValue({
                  taxFeeBuyer: e.target.value,
                });
              }
            }}
          />
        </Form.Item>
        <Form.Item
          label="Buyer Tax Value"
          name="taxFeeBuyer"
          labelCol={{ span: 3 }}
          wrapperCol={{ span: 18 }}
          rules={[{ required: true }]}
        >
          <Input
            type="number"
            suffix="%"
            defaultValue={0}
            min={0}
            onChange={(e) => {
              if (isGST) {
                form.setFieldsValue({
                  taxFee: e.target.value,
                });
              }
            }}
            onKeyDown={(evt: any) => {
              typeNumber(evt);
            }}
          />
        </Form.Item>
      </Form>
    </Drawer>
  );
};

export default Editor;
