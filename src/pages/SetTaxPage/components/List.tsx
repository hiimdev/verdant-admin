import { Space, Spin } from 'antd';
import { useGetListTaxDetail } from 'api/tax';
import { createTaxValue } from 'api/tax/request';
import { ICreateTaxRequest } from 'api/tax/types';
import { IError } from 'api/types';
import { Header } from 'components/Header';
import { Input } from 'components/Input';
import { PaginatedTableAndSearch } from 'components/PaginatedTableAndSearch';
import { useAppDispatch } from 'hooks/reduxHook';
import { FC, useMemo, useState } from 'react';
import { useMutation, useQueryClient } from 'react-query';
import { setEditingId } from 'store/ducks/tax/slice';
import { debounce } from 'utils/js';
import { message } from 'utils/message';
import { Permission } from 'utils/permission';
import { getColumns } from './Column';
import { IListType } from './type';

const List: FC<IListType> = ({ isUpdate }) => {
  const dispatch = useAppDispatch();
  const [idEdit, setIdEdit] = useState(null);
  const client = useQueryClient();
  const [valueInput, setValueInput] = useState({ taxFee: 0, taxFeeBuyer: 0 });
  const [params, setParams] = useState({ name: '' });

  const { mutate: create, isLoading: isLoadingCreateCommission } = useMutation(createTaxValue, {
    onSuccess: () => {
      client.invalidateQueries('/tax/list-tax');
      message.success('Success');
      setIdEdit(null);
      setValueInput({ taxFee: 0, taxFeeBuyer: 0 });
    },
    onError: (error: IError) => {
      console.log(message.error(error?.meta.message));
      message.error(error?.meta.message[0]);
    },
  });

  const debouncedSetParams = useMemo(() => debounce(setParams, 500), []);

  const onEdit = (data) => {
    const request: ICreateTaxRequest = {
      categoryCommissionId: data.category_commission_id,
      collectionId: data.collection_id,
      brandId: data.brand_id,
      action: 'edit',
      vendorId: data.vendor_id,
      taxId: data.tax_id,
      taxFee: Number(valueInput.taxFee),
      taxFeeBuyer: Number(valueInput.taxFeeBuyer),
      countryId: data.country_id,
    };

    create(request);
  };
  const submitParams = (value: any) => {
    debouncedSetParams((prevParams: any) => ({
      ...prevParams,
      ...value,
      sort: value.sort ? [value.sort] : [],
    }));
  };
  const columns = useMemo(
    () => getColumns(dispatch, idEdit, setIdEdit, setValueInput, onEdit, submitParams, isUpdate, valueInput),
    [getColumns, idEdit, valueInput, isUpdate, valueInput]
  );

  return (
    <Spin spinning={isLoadingCreateCommission}>
      <Header title="" setEditingId={setEditingId} viewOnly>
        <Space>
          <Input
            placeholder="Category Name"
            onChange={(ev: any) =>
              debouncedSetParams((prevParams: any) => ({
                ...prevParams,
                category_name: ev.target.value,
              }))
            }
          />
          <Input
            placeholder="Brand Name"
            onChange={(ev: any) =>
              debouncedSetParams((prevParams: any) => ({
                ...prevParams,
                brand_name: ev.target.value,
              }))
            }
          />
          <Input
            placeholder="Collection Name"
            onChange={(ev: any) =>
              debouncedSetParams((prevParams: any) => ({
                ...prevParams,
                collection_name: ev.target.value,
              }))
            }
          />
        </Space>
      </Header>
      <PaginatedTableAndSearch
        permission={Permission.add_tax}
        title="Tax"
        setEditingId={setEditingId}
        scroll={{ x: 1500 }}
        columns={columns}
        useQuery={useGetListTaxDetail}
        requestParams={params}
        handleReset={() => setParams({ name: '' })}
      />
    </Spin>
  );
};

export default List;
