import { Modal } from 'antd';
import { PaginatedTableAndSearch } from 'components/PaginatedTableAndSearch';
import { useAppDispatch, useAppSelector } from 'hooks/reduxHook';
import { FC, useMemo } from 'react';
import { getLogId, setLogId } from 'store/ducks/commission/slice';
import { timeFormatter } from 'utils/date';
import { useGetListTaxHistory } from 'api/tax';

const getColumns = () => {
  return [
    {
      title: 'Update Date',
      dataIndex: 'updated_at',
      key: 'updated_at',
      render(value: string) {
        return timeFormatter(Number(value));
      },
      align: 'center' as const,
    },
    {
      title: 'Updated by (ID)',
      key: 'update_date_id',
      dataIndex: 'updated_by',
      align: 'center' as const,
      render(data: any) {
        return <>{data || '-'}</>;
      },
    },
    {
      title: 'Category',
      dataIndex: 'category_commission_id',
      key: 'category_commission_id',
      align: 'center' as const,
      render(data: any, record: any) {
        return <>{record.category_name || '-'}</>;
      },
    },
    {
      title: 'Brand',
      key: 'brand_name',
      dataIndex: 'brand_name',
      align: 'center' as const,
      render(data: any) {
        return <>{data || '-'}</>;
      },
    },
    {
      title: 'Collection',
      key: 'collection_name',
      dataIndex: 'collection_name',
      align: 'center' as const,
      render(data: any) {
        return <>{data || '-'}</>;
      },
    },
    {
      title: 'Vendor',
      key: 'vendor_name',
      dataIndex: 'vendor_name',
      align: 'center' as const,
      render(data: any) {
        return <>{data || '-'}</>;
      },
    },
    {
      title: 'Country',
      key: 'country_id',
      dataIndex: 'country_id',
      align: 'center' as const,
      render(data: any, record: any) {
        return <>{record.name || '-'}</>;
      },
    },
    {
      title: 'Tax type',
      dataIndex: 'tax_name',
      key: 'tax_type',
      align: 'center' as const,
      render(data: any) {
        return <>{data || '-'}</>;
      },
    },
    {
      title: 'Tax Value',
      dataIndex: 'tax_fee',
      key: 'tax_fee',
      align: 'center' as const,
      render(data: any) {
        return <>{data || '-'}</>;
      },
    },
    {
      title: 'Status',
      dataIndex: 'status',
      key: 'status',
      align: 'center',
      render(value: string) {
        return <>{value ? 'Active' : 'Deactivate'}</>;
      },
    },
  ];
};

const Log: FC = () => {
  const dispatch = useAppDispatch();
  const logId = useAppSelector(getLogId);
  const columns = useMemo(() => getColumns(), [getColumns]);

  const visible = logId !== null;
  const params = useMemo(() => {
    let requestParams = { page: 1, limit: 10, ...logId };
    if (!logId.brand_id) {
      delete requestParams.brand_id;
    }
    if (!logId.collection_id) {
      delete requestParams.collection_id;
    }
    if (!logId.taxId) {
      delete requestParams.taxId;
    }
    return requestParams;
  }, [logId]);

  const handleClose = () => {
    dispatch(setLogId(null));
  };
  return (
    <Modal
      width={1100}
      visible={visible}
      title={`History`}
      onOk={handleClose}
      onCancel={handleClose}
      cancelButtonProps={{ hidden: true }}
    >
      <PaginatedTableAndSearch columns={columns} useQuery={() => useGetListTaxHistory(params)} />
    </Modal>
  );
};

export default Log;
