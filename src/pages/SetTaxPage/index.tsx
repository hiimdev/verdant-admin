import { useAppSelector } from 'hooks/reduxHook';
import { FC, useMemo } from 'react';
import { getLogId } from 'store/ducks/commission/slice';
import { getEditingId } from 'store/ducks/tax/slice';
import { getPermission } from 'store/ducks/user/slice';
import { Permission } from 'utils/permission';
import Editor from './components/Editor';
import List from './components/List';
import Log from './components/Log';

const SetTax: FC = () => {
  const logId = useAppSelector(getLogId);
  const permissions = useAppSelector(getPermission);
  const editingId = useAppSelector(getEditingId);

  const isUpdate = useMemo(() => {
    return permissions.includes(Permission.edit_tax_detail);
  }, [permissions]);
  return (
    <>
      <List isUpdate={isUpdate} />
      {editingId !== null && <Editor />}
      {logId && <Log />}
    </>
  );
};

export default SetTax;
