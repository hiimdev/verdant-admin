import { Card, Col, Row, Typography } from 'antd';
import { useGetVMTStatistical } from 'api/admin';
import clsx from 'clsx';
import { Input } from 'components/Input';
import { convertPriceEther } from 'utils/common';
import styles from './styles.module.less';

const Statistical = () => {
  const { data } = useGetVMTStatistical();
  return (
    <div className={clsx(styles.root)}>
      <Card className={styles.content} title="Statistical VMT">
        <Row align="middle" gutter={[16, 16]}>
          <Col span={12}>
            <Typography.Text>The total number of VMTs have been created</Typography.Text>
          </Col>
          <Col span={12} style={{ textAlign: 'right' }}>
            <Input style={{ textAlign: 'right' }} value={convertPriceEther(data?.totalVMTsCreatedFormat) || 0} />
          </Col>
          <Col span={12}>
            <Typography.Text>The total number of VMTs has Vendor issued</Typography.Text>
          </Col>
          <Col span={12} style={{ textAlign: 'right' }}>
            <Input style={{ textAlign: 'right' }} value={convertPriceEther(data?.totalVMTsVendorIssuedFormat)} />
          </Col>

          <Col span={12}>
            <Typography.Text>The total number of VMTs remaining</Typography.Text>
          </Col>
          <Col span={12} style={{ textAlign: 'right' }}>
            <Input style={{ textAlign: 'right' }} value={convertPriceEther(data?.totalVMTsRemainingFormat)} />
          </Col>

          <Col span={12}>
            <Typography.Text>The total number of VMTs used to mint NFT</Typography.Text>
          </Col>
          <Col span={12} style={{ textAlign: 'right' }}>
            <Input style={{ textAlign: 'right' }} value={data?.totalVMTsVendorUserMint} />
          </Col>
        </Row>
      </Card>
    </div>
  );
};

export default Statistical;
