import { DeleteOutlined, EditOutlined } from '@ant-design/icons';
import { AnyAction, Dispatch } from '@reduxjs/toolkit';
import { Button, Space } from 'antd';
import { TableTitle } from 'components/Table/component/TableTitle';
import { AiFillEye } from 'react-icons/ai';
import { setDeletingId, setEditingId } from 'store/ducks/tag/slice';
import { TAG_SORT } from 'utils/constant';
import { timeFormatter } from 'utils/date';

export const getColumns = (dispatch: Dispatch<AnyAction>, submitParams?: any, isUpdate?: boolean) => {
  return [
    {
      title: 'No',
      dataIndex: 'index',
      key: 'index',
      align: 'center',
      width: '80px',
    },
    {
      title: (
        <TableTitle
          name="tagName"
          label="Tag name"
          onSubmit={submitParams}
          sort={[TAG_SORT.TAGNAME_DESC, TAG_SORT.TAGNAME_ASC]}
        />
      ),
      dataIndex: 'tagName',
      key: 'tagName',
      render(data: string) {
        return <>{data || '-'}</>;
      },
    },
    {
      title: (
        <TableTitle
          name="tagValue"
          label="Tag Value"
          onSubmit={submitParams}
          sort={[TAG_SORT.TAGVALUE_DESC, TAG_SORT.TAGVALUE_ASC]}
        />
      ),
      dataIndex: 'tagValue',
      key: 'tagValue',
      render(data: string) {
        return <>{data || '-'}</>;
      },
    },
    {
      title: (
        <TableTitle
          name="description"
          label="Description"
          onSubmit={submitParams}
          sort={[TAG_SORT.DESCRIPTION_DESC, TAG_SORT.DESCRIPTION_ASC]}
        />
      ),
      dataIndex: 'description',
      key: 'description',
      render(data: string) {
        return <>{data || '-'}</>;
      },
    },
    {
      title: (
        <TableTitle
          name="categoryName"
          label="Category"
          onSubmit={submitParams}
          sort={[TAG_SORT.CATEGORY_DESC, TAG_SORT.CATEGORY_ASC]}
        />
      ),
      dataIndex: 'categoryName',
      key: 'categoryName',
      render(data: string) {
        return <>{data || '-'}</>;
      },
    },
    {
      title: (
        <TableTitle
          name="brandName"
          label="Brand"
          onSubmit={submitParams}
          sort={[TAG_SORT.BRAND_DESC, TAG_SORT.BRAND_ASC]}
        />
      ),
      dataIndex: 'brandName',
      key: 'brandName',
      render(data: string) {
        return <>{data || '-'}</>;
      },
    },
    {
      title: (
        <TableTitle
          name="collectionName"
          label="Collection"
          onSubmit={submitParams}
          sort={[TAG_SORT.COLLECTION_DESC, TAG_SORT.COLLECTION_ASC]}
        />
      ),
      dataIndex: 'collectionName',
      key: 'collectionName',
      render(data: string) {
        return <>{data || '-'}</>;
      },
    },

    {
      title: (
        <TableTitle
          type="none"
          name="createdAt"
          label="Create At"
          onSubmit={submitParams}
          sort={[TAG_SORT.CREATED_AT_DESC, TAG_SORT.CREATED_AT_ASC]}
        />
      ),
      dataIndex: 'createdAt',
      key: 'createdAt',
      render(createdAt: string) {
        return timeFormatter(Number(createdAt));
      },
      align: 'center',
    },
    {
      title: (
        <TableTitle
          name="createdBy"
          label="Created By"
          onSubmit={submitParams}
          sort={[TAG_SORT.CREATED_BY_DESC, TAG_SORT.CREATED_BY_ASC]}
        />
      ),
      dataIndex: 'createdBy',
      key: 'createdBy',
      align: 'center',
    },
    {
      title: 'Action',
      align: 'center',
      dataIndex: 'id',
      fixed: 'right',
      render(id: any) {
        return (
          <Space>
            <Button
              icon={isUpdate ? <EditOutlined /> : <AiFillEye />}
              type="primary"
              onClick={() => dispatch(setEditingId(String(id)))}
              title={isUpdate ? 'Edit' : 'View'}
            />
            {isUpdate && (
              <Button
                icon={<DeleteOutlined />}
                type="default"
                onClick={() => dispatch(setDeletingId(String(id)))}
                title="Delete"
              />
            )}
          </Space>
        );
      },
    },
  ];
};
