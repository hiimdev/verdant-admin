import { Modal } from 'antd';
import { useAppDispatch, useAppSelector } from 'hooks/reduxHook';
import { FC } from 'react';
import { useMutation, useQueryClient } from 'react-query';
import { message } from 'utils/message';
import { IError } from 'api/types';
import { getDeletingId, setDeletingId } from 'store/ducks/tag/slice';
import { useGetTagDetails } from 'api/tag';
import { deleteTag } from 'api/tag/requet';

const Deletor: FC = () => {
  const dispatch = useAppDispatch();
  const deletingId = useAppSelector(getDeletingId);
  const client = useQueryClient();
  const { data, isLoading } = useGetTagDetails(deletingId as string, { enabled: !!deletingId });

  const handleClose = () => {
    dispatch(setDeletingId(null));
  };

  const { mutate, isLoading: deleteLoading } = useMutation(deleteTag, {
    onSuccess: () => {
      client.invalidateQueries('/nft-admin/tag/paging_filter');
      message.success('Delete successfully!');
      handleClose();
    },
    onError: (error: IError) => {
      message.error(error?.meta.message[0]);
    },
  });

  const onDelete = () => {
    if (deletingId) mutate(deletingId);
  };

  if (isLoading) {
    return null;
  }

  return (
    <>
      <Modal
        visible={!!deletingId}
        title={`Delete: ${data?.tagName}`}
        // icon={<AiFillExclamationCircle />}
        okButtonProps={{
          loading: deleteLoading,
        }}
        cancelButtonProps={{
          disabled: deleteLoading,
        }}
        onCancel={handleClose}
        onOk={onDelete}
        okType="danger"
        okText="Confirm"
      >
        Are you sure you want to remove this tag?
      </Modal>
    </>
  );
};

export default Deletor;
