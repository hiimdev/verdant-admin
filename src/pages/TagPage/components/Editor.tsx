import { Empty, Form, Spin } from 'antd';
import { useGetBrandCommission, useGetCategoryCommission, useGetCollectionCommission } from 'api/commission';
import { useGetTagDetails } from 'api/tag';
import { createTag, updateTag } from 'api/tag/requet';
import { IError } from 'api/types';
import { Drawer } from 'components/Drawer';
import { Input } from 'components/Input';
import { Select } from 'components/Select';
import { useAppDispatch, useAppSelector } from 'hooks/reduxHook';
import { FC, useEffect, useMemo } from 'react';
import { useMutation, useQueryClient } from 'react-query';
import { getEditingId, setEditingId } from 'store/ducks/tag/slice';
import { rules } from 'utils/form';
import { message } from 'utils/message';
import { IListType } from './type';

const Editor: FC<IListType> = ({ isUpdate: canUpdate }) => {
  const dispatch = useAppDispatch();
  const editingId = useAppSelector(getEditingId);
  const client = useQueryClient();

  const [form] = Form.useForm();

  const isUpdate = useMemo(() => {
    return (canUpdate && editingId !== '') || editingId === '';
  }, []);

  const action = editingId ? 'edit' : 'add';

  const { data, status: getLoading } = useGetTagDetails(editingId as string, { enabled: !!editingId });

  const { data: categories, refetch: refetchCategory } = useGetCategoryCommission({ page: 1, limit: 50 });
  const { data: brands, refetch: refetchBrand } = useGetBrandCommission(
    form.getFieldValue('categoryCommissionId') || form.getFieldValue('collectionData')
      ? {
          page: 1,
          limit: 50,
          categoryId:
            form.getFieldValue('categoryCommissionId') ||
            JSON.parse(form.getFieldValue('collectionData'))?.categoryCommissionId,
        }
      : { page: 1, limit: 50 }
  );

  const { data: collections, refetch: refetchCollection } = useGetCollectionCommission(
    form.getFieldValue('categoryCommissionId') || form.getFieldValue('brandData')
      ? {
          page: 1,
          limit: 50,
          categoryId:
            form.getFieldValue('categoryCommissionId') ||
            JSON.parse(form.getFieldValue('brandData'))?.categoryCommissionId,
          brandId: form.getFieldValue('brandData') ? JSON.parse(form.getFieldValue('brandData'))?.brandId : undefined,
        }
      : { page: 1, limit: 50 }
  );

  useEffect(() => {
    form.resetFields();
    if (editingId !== null) {
      form.setFieldsValue({
        ...data,
        collectionData: data?.collectionId
          ? JSON.stringify({
              collectionId: data.collectionId,
              brandId: data.brandId,
              categoryCommissionId: data.categoryCommissionId,
            })
          : undefined,
        brandData: data?.brandId
          ? JSON.stringify({
              brandId: data.brandId,
              categoryCommissionId: data.categoryCommissionId,
            })
          : undefined,
      });
    }
  }, [data]);

  const { mutate: create, isLoading: loadingCreate } = useMutation(createTag, {
    onSuccess: () => {
      client.invalidateQueries('/nft-admin/tag/paging_filter');
      message.success('Create successfully!');
      handleClose();
    },
    onError: (error: IError) => {
      message.error(error?.meta.message[0]);
    },
  });

  const { mutate: update, isLoading: loadingUpdate } = useMutation(updateTag, {
    onSuccess: () => {
      client.invalidateQueries('/nft-admin/tag/paging_filter');
      message.success('Update successfully!');
      handleClose();
    },
    onError: (error: IError) => {
      message.error(error?.meta.message[0]);
    },
  });

  const handleClose = () => {
    dispatch(setEditingId(null));
  };
  const onFinish = (value: any) => {
    const temp = Array.from(new Set(value.categoryCommissionIds));

    const mutate = action === 'edit' ? update : create;
    let request = {
      ...value,
    };
    if (value.brandData) {
      request = { ...request, ...JSON.parse(value.brandData) };
    }
    if (value.collectionData) {
      request = { ...request, ...JSON.parse(value.collectionData) };
    }
    delete request.brandData;
    delete request.collectionData;
    const params =
      action === 'add'
        ? { ...request, categoryCommissionIds: temp }
        : {
            ...request,
            id: Number(editingId),
            categoryCommissionIds: temp,
          };
    mutate(params);
  };

  const handleClickRefecthCollection = () => {
    refetchCollection();
  };

  const handleClickRefecthBrand = () => {
    refetchBrand();
  };

  const handleClickRefecthCategory = () => {
    refetchCategory();
  };

  const handleValuesChange = async (e: any) => {
    if (e.categoryCommissionId === '-1') {
      form.resetFields(['brandData', 'collectionData', 'categoryCommissionId']);
      return;
    } else if (e.brandData === '-1') {
      form.resetFields(['collectionData', 'brandData']);
      return;
    } else if (e.collectionData === '-1') {
      form.resetFields(['collectionData']);
      return;
    }

    if (e.brandData) {
      form.resetFields(['collectionData']);
      await refetchCategory();
      await refetchCollection();
      if (!form.getFieldValue('categoryCommissionId')) {
        form.setFieldsValue({ categoryCommissionId: JSON.parse(e.brandData)?.categoryCommissionId });
      }
      return;
    }

    if (e.collectionData) {
      await refetchCategory();
      await refetchBrand();
      const jsonParse = JSON.parse(e.collectionData);
      if (!form.getFieldValue('categoryCommissionId')) {
        form.setFieldsValue({
          categoryCommissionId: jsonParse?.categoryCommissionId,
        });
      }
      if (!form.getFieldValue('brandData')) {
        form.setFieldsValue({
          brandData: JSON.stringify({
            brandId: jsonParse.brandId,
            categoryCommissionId: jsonParse.categoryCommissionId,
          }),
        });
      }
      return;
    }
  };

  return (
    <Drawer
      name="Tag"
      editingId={editingId}
      handleClose={handleClose}
      form={form}
      submitLoading={loadingCreate || loadingUpdate}
      getLoading={getLoading === 'loading'}
      viewOnly={!isUpdate}
    >
      <Form form={form} onFinish={onFinish} onValuesChange={handleValuesChange}>
        <Form.Item label="Tag Name" name="tagName" labelCol={{ span: 3 }} wrapperCol={{ span: 18 }} rules={[rules]}>
          <Input disabled={!isUpdate} placeholder={'Enter your tag name'} />
        </Form.Item>
        <Form.Item
          label="Description"
          name="description"
          labelCol={{ span: 3 }}
          wrapperCol={{ span: 18 }}
          rules={[rules]}
        >
          <Input disabled={!isUpdate} placeholder={'Enter tag description'} />
        </Form.Item>
        <Form.Item label="Tag value" name="tagValue" labelCol={{ span: 3 }} wrapperCol={{ span: 18 }} rules={[rules]}>
          <Input disabled={!isUpdate} placeholder={'Enter tag tag value'} />
        </Form.Item>
        <Form.Item label="Category" name="categoryCommissionId" labelCol={{ span: 3 }} wrapperCol={{ span: 18 }}>
          <Select
            disabled={!isUpdate}
            placeholder="Select category"
            notFoundContent={!categories ? <Spin /> : <Empty />}
            onClick={handleClickRefecthCategory}
          >
            <Select.Option value="-1">Select category</Select.Option>
            {categories?.list.map((item) => (
              <Select.Option key={item.categoryId} value={item.categoryId}>
                {item.categoryName}
              </Select.Option>
            ))}
          </Select>
        </Form.Item>
        <Form.Item label="Brand" name="brandData" labelCol={{ span: 3 }} wrapperCol={{ span: 18 }}>
          <Select
            disabled={!isUpdate}
            placeholder="Select brand"
            onClick={handleClickRefecthBrand}
            notFoundContent={!brands ? <Spin /> : <Empty />}
          >
            <Select.Option value="-1">Select brand</Select.Option>
            {brands?.list.map((item) => (
              <Select.Option
                key={item.brandId}
                value={JSON.stringify({
                  brandId: item.brandId,
                  categoryCommissionId: item.categoryId,
                })}
              >
                {item.brandName}
              </Select.Option>
            ))}
          </Select>
        </Form.Item>
        <Form.Item label="Collection" name="collectionData" labelCol={{ span: 3 }} wrapperCol={{ span: 18 }}>
          <Select
            disabled={!isUpdate}
            placeholder="Select collection"
            onClick={handleClickRefecthCollection}
            notFoundContent={!collections ? <Spin /> : <Empty />}
          >
            <Select.Option value="-1">Select collection</Select.Option>
            {collections?.list.map((item) => (
              <Select.Option
                key={item.collectionId}
                value={JSON.stringify({
                  collectionId: item.collectionId,
                  brandId: item.brandId,
                  categoryCommissionId: item.categoryId,
                })}
              >
                {item.collectionName}
              </Select.Option>
            ))}
          </Select>
        </Form.Item>
      </Form>
    </Drawer>
  );
};

export default Editor;
