import { AnyAction, Dispatch } from '@reduxjs/toolkit';
import { Button as AntdButton, Space } from 'antd';
import { TableTitle } from 'components/Table/component/TableTitle';
import { AiFillEdit, AiFillEye, AiOutlineDelete } from 'react-icons/ai';
import { setDeletingId, setEditingId } from 'store/ducks/tax/slice';
import { TAX_LIST_SORT } from 'utils/constant';
import { timeFormatter } from 'utils/date';
import styles from './styles.module.less';

export const getColumns = (dispatch: Dispatch<AnyAction>, submitParams?: any, isUpdate?: boolean) => {
  return [
    {
      title: 'No',
      dataIndex: 'index',
      key: 'index',
      align: 'center',
      width: '80px',
    },
    {
      title: (
        <TableTitle
          name="name"
          label="Country"
          onSubmit={submitParams}
          sort={[TAX_LIST_SORT.COUNTRY_DESC, TAX_LIST_SORT.COUNTRY_ASC]}
        />
      ),
      dataIndex: 'name',
      key: 'name',
      render(data: any) {
        return <>{data || '-'}</>;
      },
    },
    {
      title: (
        <TableTitle
          label="Tax name"
          onSubmit={submitParams}
          name="tax_name"
          sort={[TAX_LIST_SORT.TAX_TYPE_DESC, TAX_LIST_SORT.TAX_TYPE_ASC]}
        />
      ),
      dataIndex: 'tax_name',
      key: 'tax_name',
    },
    {
      title: <TableTitle label="Tax type name" onSubmit={submitParams} name="taxTypeName" />,
      dataIndex: 'taxTypeName',
      key: 'taxTypeName',
      render(data: any) {
        return <>{data || '-'}</>;
      },
    },
    {
      title: <TableTitle label="Tax type description" onSubmit={submitParams} name="taxTypeDescription" />,
      dataIndex: 'taxTypeDescription',
      key: 'taxTypeDescription',
      render(data: any) {
        return <>{data || '-'}</>;
      },
    },
    {
      title: (
        <TableTitle
          label="Description"
          onSubmit={submitParams}
          name="description"
          sort={[TAX_LIST_SORT.DESCRIPTION_DESC, TAX_LIST_SORT.DESCRIPTION_ASC]}
        />
      ),
      dataIndex: 'description',
      key: 'description',
    },
    {
      title: (
        <TableTitle
          type={'none'}
          label="Updated at"
          onSubmit={submitParams}
          name="updated_at"
          sort={[TAX_LIST_SORT.UPDATEDAT_DESC, TAX_LIST_SORT.UPDATEDAT_ASC]}
        />
      ),
      dataIndex: 'updated_at',
      key: 'updated_at',
      render(data: string) {
        return timeFormatter(Number(data));
      },
    },
    {
      title: (
        <TableTitle
          label="Update By"
          onSubmit={submitParams}
          name="updated_by"
          sort={[TAX_LIST_SORT.UPDATEDBY_DESC, TAX_LIST_SORT.UPDATEDBY_ASC]}
        />
      ),
      dataIndex: 'updated_by',
      key: 'updated_by',
      render(data: any, record: any) {
        return <>{data || record?.created_by || '-'}</>;
      },
    },
    {
      title: 'Action',
      align: 'center',
      dataIndex: 'id',
      render(id: string) {
        return (
          <Space>
            <AntdButton
              className={styles.btnAction}
              icon={isUpdate ? <AiFillEdit /> : <AiFillEye />}
              type="primary"
              onClick={() => dispatch(setEditingId(id))}
              title={isUpdate ? 'Edit' : 'View'}
            />
            {isUpdate && (
              <AntdButton
                className={styles.btnAction}
                icon={<AiOutlineDelete />}
                danger
                onClick={() => dispatch(setDeletingId(id))}
                title="Delete"
              />
            )}
          </Space>
        );
      },
      fixed: 'right',
    },
  ];
};
