import { Modal } from 'antd';
import { useAppDispatch, useAppSelector } from 'hooks/reduxHook';
import { FC } from 'react';
import { useMutation, useQueryClient } from 'react-query';
import { message } from 'utils/message';
import { IError } from 'api/types';
import { getDeletingId, setDeletingId } from 'store/ducks/tax/slice';
import { useGetTaxDetail } from 'api/tax';
import { deleteTax } from 'api/tax/request';
const Deletor: FC = () => {
  const dispatch = useAppDispatch();
  const deletingId = useAppSelector(getDeletingId);
  const client = useQueryClient();

  const { data, isLoading } = useGetTaxDetail(deletingId as string, { enabled: !!deletingId });

  const handleClose = () => {
    dispatch(setDeletingId(null));
  };

  const { mutate: mutateDelete, isLoading: deleteLoading } = useMutation(deleteTax, {
    onSuccess: () => {
      client.invalidateQueries('/tax/list');
      message.success('Delete successfully!');
      handleClose();
    },
    onError: (error: IError) => {
      message.error(error?.meta.message[0]);
    },
  });

  const onDelete = () => {
    mutateDelete(deletingId as string);
  };

  if (isLoading) {
    return null;
  }

  return (
    <>
      <Modal
        visible={!!deletingId}
        title={`Delete Tax: ${data?.id}`}
        okButtonProps={{
          loading: deleteLoading,
        }}
        cancelButtonProps={{
          disabled: deleteLoading,
        }}
        onCancel={handleClose}
        onOk={onDelete}
        okType="danger"
        okText="Confirm"
      >
        Are you sure you want to delete this Tax?
      </Modal>
    </>
  );
};

export default Deletor;
