import { Form } from 'antd';
import { useGetTaxDetail } from 'api/tax';
import { createTax, updateTax } from 'api/tax/request';
import { IError } from 'api/types';
import { useGetAllCountries } from 'api/user';
// import { useGetAllCountries } from 'api/user';
// import { ICountryResponse } from 'api/user/types';
import { Drawer } from 'components/Drawer';
import { Input, InputTextArea } from 'components/Input';
import { Select } from 'components/Select';
// import { Select } from 'components/Select';
import { useAppDispatch, useAppSelector } from 'hooks/reduxHook';
import { FC, useEffect, useMemo } from 'react';
import { useMutation, useQueryClient } from 'react-query';
import { getEditingId, setEditingId } from 'store/ducks/tax/slice';
import { rules } from 'utils/form';
import { message } from 'utils/message';
import { IListType } from './type';
import { useGetListTaxType } from 'api/taxType';

const Editor: FC<IListType> = ({ isUpdate: canUpdate }) => {
  const dispatch = useAppDispatch();
  const editingId = useAppSelector(getEditingId);
  const client = useQueryClient();
  // const { data: countries } = useGetAllCountries();
  const [form] = Form.useForm();

  const isUpdate = useMemo(() => {
    return (canUpdate && editingId !== '') || editingId === '';
  }, []);

  const action = editingId ? 'edit' : 'add';

  const { data, status: getLoading } = useGetTaxDetail(editingId as string, { enabled: !!editingId });

  const { mutate: update, isLoading: loadingUpdate } = useMutation(updateTax, {
    onSuccess: () => {
      client.invalidateQueries('/tax/list');
      message.success('Update successfully!');
      handleClose();
    },
    onError: (error: IError) => {
      message.error(error?.meta.message[0]);
    },
  });

  const { mutate: create, isLoading: loadingCreate } = useMutation(createTax, {
    onSuccess: () => {
      client.invalidateQueries('/tax/list');
      message.success('Create successfully!');
      handleClose();
    },
    onError: (error: IError) => {
      message.error(error?.meta.message[0]);
    },
  });

  useEffect(() => {
    form.resetFields();
    if (editingId !== null) {
      form.setFieldsValue(data);
    }
  }, [data]);

  const handleClose = () => {
    dispatch(setEditingId(null));
  };

  const onFinish = (value: any) => {
    const mutate = action === 'edit' ? update : create;
    const params = action === 'edit' ? { id: editingId, ...value } : value;
    mutate(params);
  };

  const { data: countries } = useGetAllCountries({ page: 1, limit: 300 });
  const { data: taxTypes } = useGetListTaxType({ page: 1, limit: 300 });

  const dataCountries = countries?.list.map((item) => {
    const temp = { value: item.id, label: item.name, filterSearch: String(item.name) };
    return temp;
  });

  const dataTaxType = taxTypes?.list.map((item) => {
    const temp = { value: item.id, label: item.taxTypeName, filterSearch: String(item.taxTypeName) };
    return temp;
  });

  return (
    <Drawer
      name="Tax"
      editingId={editingId}
      handleClose={handleClose}
      form={form}
      getLoading={getLoading === 'loading' || status === 'loading'}
      submitLoading={loadingCreate || loadingUpdate}
      viewOnly={!isUpdate}
    >
      <Form form={form} onFinish={onFinish}>
        <Form.Item
          label="Country"
          name="countryId"
          labelCol={{ span: 3 }}
          wrapperCol={{ span: 18 }}
          rules={[{ required: true }]}
        >
          <Select
            showSearch
            disabled={!isUpdate}
            placeholder="Select country"
            options={dataCountries}
            filterOption={(input, option) => option?.filterSearch.toLowerCase().includes(input)}
          ></Select>
        </Form.Item>
        <Form.Item label="Tax Name" name="taxName" labelCol={{ span: 3 }} wrapperCol={{ span: 18 }} rules={[rules]}>
          <Input disabled={!isUpdate} placeholder={'Enter your tax name'} maxLength={50} />
        </Form.Item>
        <Form.Item
          label="Description"
          name="description"
          labelCol={{ span: 3 }}
          wrapperCol={{ span: 18 }}
          rules={[rules]}
        >
          <InputTextArea disabled={!isUpdate} placeholder={'Enter your description'} maxLength={1000} />
        </Form.Item>
        <Form.Item
          label="Tax Type"
          name="taxTypeId"
          labelCol={{ span: 3 }}
          wrapperCol={{ span: 18 }}
          rules={[{ required: true }]}
        >
          <Select
            showSearch
            disabled={!isUpdate}
            placeholder="Select Tax Type"
            options={dataTaxType}
            filterOption={(input, option) => option?.filterSearch.toLowerCase().includes(input)}
          ></Select>
        </Form.Item>
      </Form>
    </Drawer>
  );
};

export default Editor;
