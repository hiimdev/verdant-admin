import { useGetListTax } from 'api/tax';
import { PaginatedTableAndSearch } from 'components/PaginatedTableAndSearch';
import { useAppDispatch } from 'hooks/reduxHook';
import { FC, useMemo, useState } from 'react';
import { setEditingId } from 'store/ducks/tax/slice';
import { debounce } from 'utils/js';
import { Permission } from 'utils/permission';
import { getColumns } from './Column';
import { IListType } from './type';

const List: FC<IListType> = ({ isUpdate }) => {
  const dispatch = useAppDispatch();
  const [params, setParams] = useState<any>({ sort: [] });
  const debouncedSetParams = useMemo(() => debounce(setParams, 500), []);
  const submitParams = (value: any) => {
    debouncedSetParams((prevParams: any) => ({
      ...prevParams,
      ...value,
      sort: value.sort ? [value.sort] : [],
    }));
  };

  const columns = useMemo(() => getColumns(dispatch, submitParams, isUpdate), [getColumns, isUpdate]);

  return (
    <>
      <PaginatedTableAndSearch
        permission={Permission.add_tax_type}
        title="Tax"
        setEditingId={setEditingId}
        columns={columns}
        useQuery={useGetListTax}
        requestParams={params}
        handleReset={() => setParams({ sort: [] })}
      />
    </>
  );
};

export default List;
