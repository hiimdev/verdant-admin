import { useAppSelector } from 'hooks/reduxHook';
import { FC, useMemo } from 'react';
import { getEditingId, getDeletingId } from 'store/ducks/tax/slice';
import { getPermission } from 'store/ducks/user/slice';
import { Permission } from 'utils/permission';
import Deletor from './components/Deletor';
import Editor from './components/Editor';
import List from './components/List';

const Tax: FC = () => {
  const permissions = useAppSelector(getPermission);
  const editingId = useAppSelector(getEditingId);
  const deletingId = useAppSelector(getDeletingId);

  const isUpdate = useMemo(() => {
    return permissions.includes(Permission.edit_delete_tax_type);
  }, [permissions]);

  return (
    <>
      <List isUpdate={isUpdate} />
      {editingId !== null && <Editor isUpdate={isUpdate} />}
      {deletingId && isUpdate && <Deletor />}
    </>
  );
};

export default Tax;
