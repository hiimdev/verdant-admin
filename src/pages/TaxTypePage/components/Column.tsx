import { AnyAction, Dispatch } from '@reduxjs/toolkit';
import { Button as AntdButton, Space } from 'antd';
import { TableTitle } from 'components/Table/component/TableTitle';
import { AiFillEdit, AiFillEye, AiOutlineDelete } from 'react-icons/ai';
import { setDeletingId, setEditingId } from 'store/ducks/taxType/slice';
import { timeFormatter } from 'utils/date';
import styles from './styles.module.less';

export const getColumns = (dispatch: Dispatch<AnyAction>, submitParams?: any, isUpdate?: boolean) => {
  return [
    {
      title: 'No',
      dataIndex: 'index',
      key: 'index',
      align: 'center',
      width: '80px',
    },
    {
      title: <TableTitle name="taxTypeName" label="Tax Type Name" onSubmit={submitParams} />,
      dataIndex: 'taxTypeName',
      key: 'taxTypeName',
      render(data: any) {
        return <>{data || '-'}</>;
      },
      align: 'center',
    },
    {
      title: <TableTitle label="Description" onSubmit={submitParams} name="description" />,
      dataIndex: 'description',
      key: 'description',
      align: 'center',
    },
    {
      title: <TableTitle label="Is Gts" onSubmit={submitParams} name="isGts" />,
      dataIndex: 'isGts',
      key: 'isGts',
      render(data: number) {
        return <>{data && data === 1 ? 'true' : 'false' || '-'}</>;
      },
      align: 'center',
    },
    {
      title: <TableTitle type={'none'} label="Created By" onSubmit={submitParams} name="createdBy" />,
      dataIndex: 'createdBy',
      key: 'createdBy',
      render(data: string) {
        return <>{data || '-'}</>;
      },
      align: 'center',
    },
    {
      title: <TableTitle type={'none'} label="Created At" onSubmit={submitParams} name="createdAt" />,
      dataIndex: 'createdAt',
      key: 'createdAt',
      render(data: string) {
        return timeFormatter(Number(data));
      },
      align: 'center',
    },
    {
      title: <TableTitle type={'none'} label="Updated At" onSubmit={submitParams} name="updatedAt" />,
      dataIndex: 'updatedAt',
      key: 'updatedAt',
      render(data: string) {
        return timeFormatter(Number(data));
      },
      align: 'center',
    },
    {
      title: <TableTitle label="Updated By" onSubmit={submitParams} name="updatedBy" />,
      dataIndex: 'updatedBy',
      key: 'updatedBy',
      render(data: any, record: any) {
        return <>{data || record?.created_by || '-'}</>;
      },
      align: 'center',
    },
    {
      title: 'Action',
      align: 'center',
      dataIndex: 'id',
      render(id: string) {
        return (
          <Space>
            <AntdButton
              className={styles.btnAction}
              icon={isUpdate ? <AiFillEdit /> : <AiFillEye />}
              type="primary"
              onClick={() => dispatch(setEditingId(id))}
              title={isUpdate ? 'Edit' : 'View'}
            />
            {isUpdate && (
              <AntdButton
                className={styles.btnAction}
                icon={<AiOutlineDelete />}
                danger
                onClick={() => dispatch(setDeletingId(id))}
                title="Delete"
              />
            )}
          </Space>
        );
      },
      fixed: 'right',
    },
  ];
};
