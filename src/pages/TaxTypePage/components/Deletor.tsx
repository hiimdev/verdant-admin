import { Modal } from 'antd';
import { useAppDispatch, useAppSelector } from 'hooks/reduxHook';
import { FC } from 'react';
import { useMutation, useQueryClient } from 'react-query';
import { message } from 'utils/message';
import { IError } from 'api/types';
import { getDeletingId, setDeletingId } from 'store/ducks/taxType/slice';
import { useGetTaxTypeDetail } from 'api/taxType';
import { deleteTaxType } from 'api/taxType/request';
const Deletor: FC = () => {
  const dispatch = useAppDispatch();
  const deletingId = useAppSelector(getDeletingId);
  const client = useQueryClient();

  const { data, isLoading } = useGetTaxTypeDetail(deletingId as string, { enabled: !!deletingId });

  const handleClose = () => {
    dispatch(setDeletingId(null));
  };

  const { mutate: mutateDelete, isLoading: deleteLoading } = useMutation(deleteTaxType, {
    onSuccess: () => {
      client.invalidateQueries('/tax/list-tax-type');
      message.success('Delete successfully!');
      handleClose();
    },
    onError: (error: IError) => {
      message.error(error?.meta.message[0]);
    },
  });

  const onDelete = () => {
    mutateDelete(deletingId as string);
  };

  if (isLoading) {
    return null;
  }

  return (
    <>
      <Modal
        visible={!!deletingId}
        title={`Delete Tax Type: ${data?.id}`}
        okButtonProps={{
          loading: deleteLoading,
        }}
        cancelButtonProps={{
          disabled: deleteLoading,
        }}
        onCancel={handleClose}
        onOk={onDelete}
        okType="danger"
        okText="Confirm"
      >
        Are you sure you want to delete this Tax Type?
      </Modal>
    </>
  );
};

export default Deletor;
