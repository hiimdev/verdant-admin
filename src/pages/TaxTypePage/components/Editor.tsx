import { Form, Checkbox } from 'antd';
import { useGetTaxTypeDetail } from 'api/taxType';
import { createTaxType, updateTaxType } from 'api/taxType/request';
import { IError } from 'api/types';
import { Drawer } from 'components/Drawer';
import { Input, InputTextArea } from 'components/Input';
import { useAppDispatch, useAppSelector } from 'hooks/reduxHook';
import { FC, useEffect, useMemo, useState } from 'react';
import { useMutation, useQueryClient } from 'react-query';
import { getEditingId, setEditingId } from 'store/ducks/taxType/slice';
import { rules } from 'utils/form';
import { message } from 'utils/message';
import { IListType } from './type';
import type { CheckboxChangeEvent } from 'antd/es/checkbox';

const Editor: FC<IListType> = ({ isUpdate: canUpdate }) => {
  const dispatch = useAppDispatch();
  const editingId = useAppSelector(getEditingId);
  const client = useQueryClient();
  const [form] = Form.useForm();

  const isUpdate = useMemo(() => {
    return (canUpdate && editingId !== '') || editingId === '';
  }, []);

  const action = editingId ? 'edit' : 'add';

  const { data, status: getLoading } = useGetTaxTypeDetail(editingId as string, { enabled: !!editingId });

  const { mutate: update, isLoading: loadingUpdate } = useMutation(updateTaxType, {
    onSuccess: () => {
      client.invalidateQueries('/tax/list-tax-type');
      message.success('Update successfully!');
      handleClose();
    },
    onError: (error: IError) => {
      message.error(error?.meta.message[0]);
    },
  });

  const { mutate: create, isLoading: loadingCreate } = useMutation(createTaxType, {
    onSuccess: () => {
      client.invalidateQueries('/tax/list-tax-type');
      message.success('Create successfully!');
      handleClose();
    },
    onError: (error: IError) => {
      message.error(error?.meta.message[0]);
    },
  });

  useEffect(() => {
    form.resetFields();
    if (editingId !== null) {
      form.setFieldsValue(data);
    }
  }, [data]);

  const handleClose = () => {
    dispatch(setEditingId(null));
  };

  const onFinish = (value: any) => {
    const mutate = action === 'edit' ? update : create;
    const params =
      action === 'edit' ? { id: editingId, ...value, isGts: valueCheckbox } : { ...value, isGts: valueCheckbox };
    mutate(params);
  };

  const [valueCheckbox, setValueCheckbox] = useState<boolean>(false);

  const handleChecked = (e: CheckboxChangeEvent) => {
    setValueCheckbox(e.target.checked);
  };

  return (
    <Drawer
      name="Tax Type"
      editingId={editingId}
      handleClose={handleClose}
      form={form}
      getLoading={getLoading === 'loading' || status === 'loading'}
      submitLoading={loadingCreate || loadingUpdate}
      viewOnly={!isUpdate}
    >
      <Form form={form} onFinish={onFinish}>
        <Form.Item
          label="Tax Type Name"
          name="taxTypeName"
          labelCol={{ span: 3 }}
          wrapperCol={{ span: 18 }}
          rules={[rules]}
        >
          <Input disabled={!isUpdate} placeholder={'Enter your tax type name'} maxLength={50} />
        </Form.Item>
        <Form.Item
          label="Description"
          name="description"
          labelCol={{ span: 3 }}
          wrapperCol={{ span: 18 }}
          rules={[rules]}
        >
          <InputTextArea disabled={!isUpdate} placeholder={'Enter your description'} maxLength={1000} />
        </Form.Item>
        <Form.Item label="Is GTS" name="isGts" labelCol={{ span: 3 }}>
          <Checkbox
            value={valueCheckbox}
            defaultChecked={action === 'edit' ? data?.isGts : valueCheckbox}
            onChange={handleChecked}
            style={{ marginTop: 7 }}
          ></Checkbox>
        </Form.Item>
      </Form>
    </Drawer>
  );
};

export default Editor;
