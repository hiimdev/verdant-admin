import { AnyAction, Dispatch } from '@reduxjs/toolkit';
import { TableTitle } from 'components/Table/component/TableTitle';
import { timeFormatter } from 'utils/date';
import BigNumber from 'bignumber.js';
import { convertPriceEther, convertDecimalNumber } from 'utils/common';

export const getColumns = (dispatch: Dispatch<AnyAction>, submitParams?: any) => {
  return [
    {
      title: 'No',
      dataIndex: 'index',
      key: 'index',
      align: 'center',
      width: '80px',
    },
    {
      title: <TableTitle label="Txid" onSubmit={submitParams} name="txid" />,
      dataIndex: 'txid',
      key: 'txid',
      align: 'center',
    },
    {
      title: <TableTitle label="Name" onSubmit={submitParams} name="name" />,
      dataIndex: 'name',
      key: 'name',
      align: 'center',
    },
    {
      title: <TableTitle label="Token ID" onSubmit={submitParams} name="tokenId" />,
      dataIndex: 'tokenId',
      key: 'tokenId',
      align: 'center',
    },
    {
      title: <TableTitle label="Category Name" onSubmit={submitParams} name="categoryName" />,
      dataIndex: 'categoryName',
      key: 'categoryName',
      align: 'center',
    },
    {
      title: <TableTitle label="Collection Name" onSubmit={submitParams} name="collectionName" />,
      dataIndex: 'collectionName',
      key: 'collectionName',
      align: 'center',
    },
    {
      title: <TableTitle label="Brand Name" onSubmit={submitParams} name="brandName" />,
      dataIndex: 'brandName',
      key: 'brandName',
      align: 'center',
    },
    {
      title: <TableTitle label="Vendor Name" onSubmit={submitParams} name="vendorName" />,
      dataIndex: 'vendorName',
      key: 'vendorName',
      align: 'center',
      render(data: any) {
        return <>{data}</>;
      },
    },
    {
      title: <TableTitle label="Trade Time" onSubmit={submitParams} name="createdAt" type="date" />,
      dataIndex: 'createdAt',
      key: 'createdAt',
      align: 'center',
      render(value: string) {
        return timeFormatter(Number(value));
      },
    },
    {
      title: (
        <TableTitle
          label="Price"
          onSubmit={(values: any) =>
            submitParams({
              ...values,
              price: values.price ? Number(new BigNumber(values.price).multipliedBy(10 ** 18)) : undefined,
            })
          }
          name="price"
        />
      ),
      dataIndex: 'price',
      key: 'price',
      align: 'center',
      render(price: string) {
        return price ? convertPriceEther(price) : '-';
      },
    },
    {
      title: <TableTitle label="Buyer Paid" onSubmit={submitParams} name="buyerPaid" />,
      key: 'buyerPaid',
      dataIndex: 'buyerPaid',
      align: 'center' as const,
      render(buyerPaid: string) {
        return buyerPaid ? convertPriceEther(buyerPaid) : '-';
      },
    },
    {
      title: <TableTitle label="Buyer Email" onSubmit={submitParams} name="buyerEmail" />,
      dataIndex: 'buyerEmail',
      key: 'buyerEmail',
      align: 'center' as const,
    },
    {
      title: <TableTitle label="Seller Received" onSubmit={submitParams} name="sellerReceive" />,
      dataIndex: 'sellerReceive',
      key: 'sellerReceive',
      render(data: string) {
        return data ? convertPriceEther(data) : '-';
      },
      align: 'center',
    },
    {
      title: <TableTitle label="Seller Email" onSubmit={submitParams} name="sellerEmail" />,
      dataIndex: 'sellerEmail',
      key: 'sellerEmail',
      align: 'center',
      render(data: any) {
        return <>{data}</>;
      },
    },
    {
      title: <TableTitle label="Goods Tax Rate" onSubmit={submitParams} name="goodsTaxRate" />,
      dataIndex: 'goodsTaxRate',
      key: 'goodsTaxRate',
      align: 'center',
      render(data: any) {
        return data ? `${convertDecimalNumber(data)}%` : '-';
      },
    },
    {
      title: <TableTitle label="Goods Tax Fee" onSubmit={submitParams} name="goodsTaxFee" />,
      dataIndex: 'goodsTaxFee',
      key: 'goodsTaxFee',
      align: 'center',
      render(data: string) {
        return data ? convertPriceEther(data) : '-';
      },
    },
    {
      title: <TableTitle label="Goods Tax Address" onSubmit={submitParams} name="goodsTaxAddress" />,
      dataIndex: 'goodsTaxAddress',
      key: 'goodsTaxAddress',
      align: 'center',
      render(data: any) {
        return <>{data}</>;
      },
    },
    {
      title: <TableTitle label="Service Fee Rate" onSubmit={submitParams} name="serviceFeeRate" />,
      dataIndex: 'serviceFeeRate',
      key: 'serviceFeeRate',
      align: 'center',
      render(data: any) {
        return data ? `${convertDecimalNumber(data)}%` : '-';
      },
    },
    {
      title: <TableTitle label="Service Fee Tax Rate" onSubmit={submitParams} name="serviceFeeTaxRate" />,
      dataIndex: 'serviceFeeTaxRate',
      key: 'serviceFeeTaxRate',
      align: 'center',
      render(data: any) {
        return data ? `${convertDecimalNumber(data)}%` : '-';
      },
    },
    {
      title: <TableTitle label="Service Fee for Verdant" onSubmit={submitParams} name="serviceFeeForVerdant" />,
      dataIndex: 'serviceFeeForVerdant',
      key: 'serviceFeeForVerdant',
      align: 'center',
      render(data: string) {
        return data ? convertPriceEther(data) : '-';
      },
    },
    {
      title: <TableTitle label="Service fee GST for Verdant" onSubmit={submitParams} name="gstForVerdant" />,
      dataIndex: 'gstForVerdant',
      key: 'gstForVerdant',
      align: 'center',
      render(data: string) {
        return data ? convertPriceEther(data) : '-';
      },
    },
    {
      title: <TableTitle label="Service fee for vendor" onSubmit={submitParams} name="serviceFeeForVendor" />,
      dataIndex: 'serviceFeeForVendor',
      key: 'serviceFeeForVendor',
      align: 'center',
      render(data: string) {
        return data ? convertPriceEther(data) : '-';
      },
    },
    {
      title: <TableTitle label="Service fee GST for Vendor" onSubmit={submitParams} name="gstForVendor" />,
      dataIndex: 'gstForVendor',
      key: 'gstForVendor',
      align: 'center',
      render(data: string) {
        return data ? convertPriceEther(data) : '-';
      },
    },
    {
      title: <TableTitle label="Revenue for Verdant" onSubmit={submitParams} name="revenueForVerdant" />,
      key: 'revenueForVerdant',
      dataIndex: 'revenueForVerdant',
      align: 'center',
      render(data: string) {
        return data ? convertPriceEther(data) : '-';
      },
    },
    {
      title: <TableTitle label="Revenue GST" onSubmit={submitParams} name="revenueGst" />,
      key: 'revenueGst',
      dataIndex: 'revenueGst',
      align: 'center',
      render(data: string) {
        return data ? convertPriceEther(data) : '-';
      },
    },
  ];
};
