import { PaginatedTableAndSearch } from 'components/PaginatedTableAndSearch';
import { FC, useMemo, useState } from 'react';
import { downloadTransactionTraded, useGetTradedTransaction } from 'api/statistical';
import { debounce, onDownload } from 'utils/js';
import { useAppDispatch } from 'hooks/reduxHook';
import useBreakpoint from 'antd/lib/grid/hooks/useBreakpoint';
import { getColumns } from './Column';
import { useMutation } from 'react-query';
import { message } from 'utils/message';
import { IError } from 'api/types';

const List: FC = () => {
  const dispatch = useAppDispatch();
  const [params, setParams] = useState<any>({ sort: [] });
  const debouncedSetParams = useMemo(() => debounce(setParams, 500), []);
  const submitParams = (value: any) => {
    debouncedSetParams((prevParams: any) => ({
      ...prevParams,
      ...value,
      page: 1,
      sort: value.sort ? [value.sort] : [],
    }));
  };
  const columns = useMemo(() => getColumns(dispatch, submitParams), [getColumns]);

  const screen = useBreakpoint();

  const { mutate } = useMutation(downloadTransactionTraded, {
    onSuccess: (res) => {
      onDownload(res, 'transaction-traded-data');
    },
    onError: (error: IError) => {
      message.error(error?.meta.message[0]);
    },
  });

  return (
    <>
      <PaginatedTableAndSearch
        title="Transaction Traded"
        columns={columns}
        useQuery={useGetTradedTransaction}
        requestParams={params}
        scroll={screen.xxl ? {} : { x: 1400 }}
        handleReset={() => setParams({ sort: [] })}
        mutateDownload={mutate}
      />
    </>
  );
};

export default List;
