import { AnyAction, Dispatch } from '@reduxjs/toolkit';
import { TableTitle } from 'components/Table/component/TableTitle';
import { TRANSACTION_SORT } from 'utils/constant';
import { timeFormatter } from 'utils/date';
import BigNumber from 'bignumber.js';
import { convertPriceEther } from 'utils/common';
import { shortenAddress } from '@usedapp/core';

export const getColumns = (dispatch: Dispatch<AnyAction>, submitParams?: any) => {
  return [
    {
      title: 'No',
      dataIndex: 'index',
      key: 'index',
      align: 'center',
      width: '80px',
    },
    {
      title: (
        <TableTitle
          type={'none'}
          label="Date"
          onSubmit={submitParams}
          name="updatedAt"
          sort={[TRANSACTION_SORT.DATE_DESC, TRANSACTION_SORT.DATE_ASC]}
        />
      ),
      dataIndex: 'updatedAt',
      key: 'updatedAt',
      align: 'center',
      render(data: string) {
        return timeFormatter(Number(data));
      },
    },
    {
      title: (
        <TableTitle
          label="NFT Name"
          onSubmit={submitParams}
          name="name"
          sort={[TRANSACTION_SORT.NAMENFT_DESC, TRANSACTION_SORT.NAMENFT_ASC]}
        />
      ),
      dataIndex: 'name',
      key: 'name',
      align: 'center',
    },
    {
      title: (
        <TableTitle
          label="Token Id"
          onSubmit={submitParams}
          name="tokenId"
          sort={[TRANSACTION_SORT.TOKENID_DESC, TRANSACTION_SORT.TOKENID_ASC]}
        />
      ),
      dataIndex: 'tokenId',
      key: 'tokenId',
      align: 'center',
    },
    {
      title: (
        <TableTitle
          label="Transaction Type"
          onSubmit={submitParams}
          name="transactionType"
          sort={[TRANSACTION_SORT.TRANSACTIONTYPE_DESC, TRANSACTION_SORT.TRANSACTIONTYPE_ASC]}
        />
      ),
      dataIndex: 'transactionType',
      key: 'transactionType',
      align: 'center',
      render(data: any) {
        return <>{data}</>;
      },
    },
    {
      title: (
        <TableTitle
          label="Price"
          onSubmit={(values: any) =>
            submitParams({
              ...values,
              price: values.price ? Number(new BigNumber(values.price).multipliedBy(10 ** 18)) : undefined,
            })
          }
          name="price"
          sort={[TRANSACTION_SORT.PRICE_DESC, TRANSACTION_SORT.PRICE_ASC]}
        />
      ),
      dataIndex: 'price',
      key: 'price',
      align: 'center',
      render(price: string) {
        return price ? convertPriceEther(price) : '-';
      },
    },
    {
      title: (
        <TableTitle
          label="From"
          onSubmit={submitParams}
          name="fromWallet"
          sort={[TRANSACTION_SORT.NAMENFT_DESC, TRANSACTION_SORT.NAMENFT_ASC]}
        />
      ),
      key: 'fromWallet',
      dataIndex: 'fromWallet',
      align: 'center' as const,
      render: (data: any) => <>{shortenAddress(data)}</>,
    },
    {
      title: (
        <TableTitle
          label="To"
          onSubmit={submitParams}
          name="toWallet"
          sort={[TRANSACTION_SORT.NAMENFT_DESC, TRANSACTION_SORT.NAMENFT_ASC]}
        />
      ),
      dataIndex: 'toWallet',
      key: 'toWallet',
      align: 'center' as const,
      render: (data: any) => <>{shortenAddress(data)}</>,
    },
    {
      title: (
        <TableTitle
          label="Last action by"
          onSubmit={submitParams}
          name="username"
          sort={[TRANSACTION_SORT.NAMENFT_DESC, TRANSACTION_SORT.NAMENFT_ASC]}
        />
      ),
      dataIndex: 'username',
      key: 'username',
      render(value: string) {
        return <>{value || '-'}</>;
      },
      align: 'center',
    },
  ];
};
