import useBreakpoint from 'antd/lib/grid/hooks/useBreakpoint';
import { downloadTransaction, useGetTransaction } from 'api/statistical';
import { IError } from 'api/types';
import { PaginatedTableAndSearch } from 'components/PaginatedTableAndSearch';
import { useAppDispatch } from 'hooks/reduxHook';
import { FC, useMemo, useState } from 'react';
import { useMutation } from 'react-query';
import { debounce, onDownload } from 'utils/js';
import { message } from 'utils/message';
import { getColumns } from './Column';

const List: FC = () => {
  const dispatch = useAppDispatch();
  const [params, setParams] = useState<any>({ sort: [] });
  const debouncedSetParams = useMemo(() => debounce(setParams, 500), []);
  const submitParams = (value: any) => {
    debouncedSetParams((prevParams: any) => ({
      ...prevParams,
      ...value,
      page: 1,
      sort: value.sort ? [value.sort] : [],
    }));
  };
  const columns = useMemo(() => getColumns(dispatch, submitParams), [getColumns]);

  const screen = useBreakpoint();

  const { mutate } = useMutation(downloadTransaction, {
    onSuccess: (res) => {
      onDownload(res, 'transaction-data');
    },
    onError: (error: IError) => {
      message.error(error?.meta.message[0]);
    },
  });

  return (
    <>
      <PaginatedTableAndSearch
        title="Transaction"
        columns={columns}
        useQuery={useGetTransaction}
        requestParams={params}
        scroll={screen.xxl ? {} : { x: 1400 }}
        handleReset={() => setParams({ sort: [] })}
        mutateDownload={mutate}
      />
    </>
  );
};

export default List;
