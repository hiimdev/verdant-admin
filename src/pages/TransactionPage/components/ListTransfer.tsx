import { Header } from 'components/Header';
import { PaginatedTableAndSearch } from 'components/PaginatedTableAndSearch';
import { FC } from 'react';
import { setEditingId } from 'store/ducks/whiteList/slice';
import { timeFormatter } from 'utils/date';
import { useGetTransactionTransfer } from 'api/statistical';
import { WalletAddress } from 'components/WalletAddress/WalletAddress';
import { Tooltip } from 'antd';

const getColumns = () => {
  return [
    {
      title: 'No',
      dataIndex: 'index',
      key: 'index',
      align: 'center',
    },
    {
      title: 'Update at',
      dataIndex: 'createdAt',
      key: 'createdAt',
      render(createdAt: string) {
        return timeFormatter(Number(createdAt));
      },
      align: 'center',
    },
    {
      title: 'NFT Name',
      dataIndex: 'nameNft',
      key: 'nameNft',
      align: 'center',
      render(data: any) {
        return <>{data || '-'} </>;
      },
    },
    {
      title: 'Token ID',
      dataIndex: 'tokenId',
      key: 'tokenId',
      align: 'center',
      render(data: any) {
        return <>{data || '-'} </>;
      },
    },
    {
      title: 'User Type',
      dataIndex: 'isVendor',
      key: 'isVendor',
      align: 'center',
      render(data: any) {
        return <>{data === '0' ? 'Platform User' : 'Vendor'}</>;
      },
    },
    {
      title: (
        <div>
          Transfer from <br />
          (Username)
        </div>
      ),
      dataIndex: 'usernameFrom',
      key: 'usernameFrom',
      align: 'center',
      render(data: any) {
        return (
          <>
            <Tooltip title={data} placement="top">
              {`${data?.slice(0, 15)}...` || '-'}
            </Tooltip>{' '}
          </>
        );
      },
    },
    {
      title: (
        <div>
          Transfer from <br />
          (User wallet Address)
        </div>
      ),
      dataIndex: 'walletFrom',
      key: 'walletFrom',
      align: 'center',
      render(data: string) {
        return <WalletAddress text={data} />;
      },
      width: '145px',
    },
    {
      title: (
        <div>
          Transfer to <br /> (Username)
        </div>
      ),
      dataIndex: 'usernameTo',
      key: 'usernameTo',
      align: 'center',
      render(data: any) {
        return (
          <>
            <Tooltip title={data} placement="top">
              {`${data?.slice(0, 15)}...` || '-'}
            </Tooltip>{' '}
          </>
        );
      },
    },
    {
      title: (
        <div>
          Transfer to <br />
          (User wallet Address)
        </div>
      ),
      dataIndex: 'walletTo',
      key: 'walletTo',
      align: 'center',
      render(data: string) {
        return <WalletAddress text={data} />;
      },
      width: '145px',
    },
    {
      title: 'Total transfers for this NFT Token ID',
      dataIndex: 'totalTransfers',
      key: 'totalTransfers',
      align: 'center' as const,
    },
  ];
};

const ListTransfer: FC = () => {
  const columns = getColumns();

  return (
    <>
      <Header title="" setEditingId={setEditingId} viewOnly></Header>
      <PaginatedTableAndSearch columns={columns} useQuery={useGetTransactionTransfer} />
    </>
  );
};

export default ListTransfer;
