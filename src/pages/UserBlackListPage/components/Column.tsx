import { AnyAction, Dispatch } from '@reduxjs/toolkit';
import { Button, Space } from 'antd';
import { TableTitle } from 'components/Table/component/TableTitle';
import { AiFillDelete } from 'react-icons/ai';
import { setDeletingId } from 'store/ducks/blackList/slice';
import { BLACK_LIST_SORT } from 'utils/constant';
import { timeFormatter } from 'utils/date';
import { WalletAddress } from './WalletAddress';

export const getColumns = (dispatch: Dispatch<AnyAction>, submitParams?: any, isUpdate?: boolean) => {
  return [
    {
      title: 'No',
      dataIndex: 'index',
      key: 'index',
      align: 'center',
      width: '80px',
    },
    {
      title: (
        <TableTitle
          label="User type"
          onSubmit={submitParams}
          name="minter"
          sort={[BLACK_LIST_SORT.USERNAME_DESC, BLACK_LIST_SORT.USERNAME_ASC]}
        />
      ),
      dataIndex: 'minter',
      key: 'minter',
      render(data: any) {
        return <>{data ? 'Vendor' : 'Platform User'}</>;
      },
      align: 'center',
    },
    {
      title: (
        <TableTitle
          label="Vendor"
          onSubmit={submitParams}
          name="vendorName"
          sort={[BLACK_LIST_SORT.VENDOR_DESC, BLACK_LIST_SORT.VENDOR_ASC]}
        />
      ),
      dataIndex: 'vendorName',
      key: 'vendorName',
    },
    {
      title: (
        <TableTitle
          label="Username"
          onSubmit={submitParams}
          name="username"
          sort={[BLACK_LIST_SORT.USERNAME_DESC, BLACK_LIST_SORT.USERNAME_ASC]}
        />
      ),
      dataIndex: 'username',
      key: 'username',
    },
    {
      title: (
        <TableTitle
          label="Email"
          onSubmit={submitParams}
          name="email"
          sort={[BLACK_LIST_SORT.EMAIL_DESC, BLACK_LIST_SORT.EMAIL_ASC]}
        />
      ),
      dataIndex: 'email',
      key: 'email',
    },
    {
      title: (
        <TableTitle
          label="Wallet"
          onSubmit={submitParams}
          name="wallet"
          sort={[BLACK_LIST_SORT.WALLET_DESC, BLACK_LIST_SORT.WALLET_ASC]}
        />
      ),
      dataIndex: 'wallet',
      key: 'wallet',
      render(wallet: any) {
        return wallet ? <WalletAddress text={wallet} /> : '-';
      },
    },
    {
      title: (
        <TableTitle
          type={'none'}
          label="Update at"
          onSubmit={submitParams}
          name="updatedAt"
          sort={[BLACK_LIST_SORT.UPDATEAT_DESC, BLACK_LIST_SORT.UPDATEAT_ASC]}
        />
      ),
      dataIndex: 'updatedAt',
      key: 'updatedAt',
      render(created_at: string) {
        return timeFormatter(Number(created_at));
      },
    },
    {
      title: (
        <TableTitle
          label="Update by"
          onSubmit={submitParams}
          name="updatedBy"
          sort={[BLACK_LIST_SORT.UPDATED_BY_DESC, BLACK_LIST_SORT.UPDATED_BY_ASC]}
        />
      ),
      dataIndex: 'updatedBy',
      key: 'updatedBy',
    },
    {
      title: 'Action',
      align: 'center',
      dataIndex: 'id',
      fixed: 'right',
      render(id: string) {
        return (
          <Space>
            {isUpdate && (
              <Button
                icon={<AiFillDelete />}
                title="Remove from blacklist"
                danger
                onClick={() => dispatch(setDeletingId(id))}
              ></Button>
            )}
          </Space>
        );
      },
    },
  ];
};
