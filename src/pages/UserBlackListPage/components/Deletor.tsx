import { Modal } from 'antd';
import { deleteWhiteBlackList } from 'api/search/request';
import { IError } from 'api/types';
import { useUserQuery } from 'api/user';
import { useAppDispatch, useAppSelector } from 'hooks/reduxHook';
import { FC } from 'react';
import { useMutation, useQueryClient } from 'react-query';
import { getDeletingId, setDeletingId } from 'store/ducks/blackList/slice';
import { message } from 'utils/message';

const Deletor: FC = () => {
  const dispatch = useAppDispatch();
  const deletingId = useAppSelector(getDeletingId);
  const client = useQueryClient();
  const { data } = useUserQuery(deletingId as string, { enabled: !!deletingId });

  const handleClose = () => {
    dispatch(setDeletingId(null));
  };

  const { mutate, isLoading: deleteLoading } = useMutation(deleteWhiteBlackList, {
    onSuccess: () => {
      client.invalidateQueries('/user-admin/black-list');
      message.success('Removed from the blacklist successfully!');
      handleClose();
    },
    onError: (error: IError) => {
      message.error(error?.meta.message[0]);
    },
  });

  const onDelete = () => {
    mutate({ email: data?.email });
  };

  return (
    <>
      <Modal
        visible={!!deletingId}
        title={`User: ${data?.username}`}
        // icon={<AiFillExclamationCircle />}
        okButtonProps={{
          loading: deleteLoading,
        }}
        cancelButtonProps={{
          disabled: deleteLoading,
        }}
        onCancel={handleClose}
        onOk={onDelete}
        okType="danger"
        okText="Confirm"
      >
        Are you sure you want to remove this account from blacklist?
      </Modal>
    </>
  );
};

export default Deletor;
