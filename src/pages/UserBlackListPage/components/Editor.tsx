import { useContractFunction } from '@usedapp/core';
import { Form } from 'antd';
import { useGetBlackListToAdd } from 'api/search';
import { addBlackList } from 'api/search/request';
import { IError } from 'api/types';
import { Drawer } from 'components/Drawer';
import { Select } from 'components/Select';
import NFTABI from 'contracts/NFTABI.json';
import { useAppDispatch, useAppSelector } from 'hooks/reduxHook';
import { useConnectABI } from 'hooks/useConnectABI';
import { useWallet } from 'hooks/useWallet';
import { FC, useEffect } from 'react';
import { useMutation, useQueryClient } from 'react-query';
import { getEditingId, setEditingId } from 'store/ducks/blackList/slice';
import { CONTRACT_ADDRESS, MARKET_ADDRESS } from 'utils/constant';
import { convertFormValue, rules } from 'utils/form';
import { message } from 'utils/message';

const Editor: FC = () => {
  const dispatch = useAppDispatch();
  const editingId = useAppSelector(getEditingId);
  const client = useQueryClient();
  const { account } = useWallet();
  const [form] = Form.useForm();
  const { data } = useGetBlackListToAdd({ page: 1, limit: 100 });

  const { state: statusRemoveMinter, send: removeMinter } = useContractFunction(
    useConnectABI(NFTABI, CONTRACT_ADDRESS.NFT),
    'removeMinter'
  );

  const { mutate, isLoading } = useMutation(addBlackList, {
    onSuccess: (res) => {
      removeMinter(res?.wallet);
      // client.invalidateQueries('/user-admin/black-list');
      // message.success('Add successfully!');
      // handleClose();
    },
    onError: (error: IError) => {
      message.error(error?.meta.message[0]);
    },
  });

  const handleClose = () => {
    dispatch(setEditingId(null));
    form.resetFields();
  };

  const onFinish = (value: any) => {
    if (account === MARKET_ADDRESS) {
      mutate(convertFormValue(value));
    } else {
      message.info('Please login correctly Digicap account');
    }
  };

  useEffect(() => {
    if (statusRemoveMinter.status === 'Success') {
      client.invalidateQueries('/user-admin/black-list');
      message.success('Add to black list successfully!');
      handleClose();
    } else if (statusRemoveMinter.status === 'Fail' || statusRemoveMinter.status === 'Exception') {
      message.error(statusRemoveMinter.errorMessage || '');
    }
  }, [statusRemoveMinter]);

  return (
    <Drawer
      name="Black List"
      editingId={editingId}
      handleClose={handleClose}
      form={form}
      getLoading={false}
      submitLoading={isLoading}
    >
      <Form form={form} onFinish={onFinish}>
        <Form.Item label="Email" name="email" labelCol={{ span: 3 }} rules={[rules]}>
          <Select placeholder="Select email">
            {data?.list.map((item: any) => (
              <Select.Option key={item.email} value={item.email}>
                {item.username}
              </Select.Option>
            ))}
          </Select>
        </Form.Item>
      </Form>
    </Drawer>
  );
};

export default Editor;
