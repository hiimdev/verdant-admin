import useBreakpoint from 'antd/lib/grid/hooks/useBreakpoint';
import { useGetBlackList } from 'api/search';
import { PaginatedTableAndSearch } from 'components/PaginatedTableAndSearch';
import { useAppDispatch } from 'hooks/reduxHook';
import { FC, useMemo, useState } from 'react';
import { setEditingId } from 'store/ducks/blackList/slice';
import { debounce } from 'utils/js';
import { Permission } from 'utils/permission';
import { getColumns } from './Column';
import { IListType } from './type';

const List: FC<IListType> = ({ isUpdate }) => {
  const dispatch = useAppDispatch();
  const [params, setParams] = useState<any>({ sort: [] });
  const debouncedSetParams = useMemo(() => debounce(setParams, 500), []);
  const submitParams = (value: any) => {
    debouncedSetParams((prevParams: any) => ({
      ...prevParams,
      ...value,
      sort: value.sort ? [value.sort] : [],
    }));
  };
  const columns = useMemo(() => getColumns(dispatch, submitParams, isUpdate), [getColumns, isUpdate]);
  // const requestParams = useAppSelector(getRequestParams);
  // useEffect(() => {
  //   dispatch(setRequestParams(params));
  // }, [params]);

  const screen = useBreakpoint();
  return (
    <>
      <PaginatedTableAndSearch
        permission={Permission.add_blacklist}
        title="Black List"
        setEditingId={setEditingId}
        columns={columns}
        useQuery={useGetBlackList}
        requestParams={params}
        scroll={screen.xxl ? undefined : { x: 1400 }}
        handleReset={() => setParams({ sort: [] })}
      />
    </>
  );
};

export default List;
