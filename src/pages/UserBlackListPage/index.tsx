import Deletor from './components/Deletor';
import Editor from './components/Editor';
import List from './components/List';
import { useAppSelector } from 'hooks/reduxHook';
import { FC, useMemo } from 'react';
import { getDeletingId, getEditingId } from 'store/ducks/blackList/slice';
import { getPermission } from 'store/ducks/user/slice';
import { Permission } from 'utils/permission';

const BlackList: FC = () => {
  const permissions = useAppSelector(getPermission);
  const editingId = useAppSelector(getEditingId);
  const deletingId = useAppSelector(getDeletingId);

  const isUpdate = useMemo(() => {
    return permissions.includes(Permission.edit_black_list);
  }, [permissions]);

  return (
    <>
      <List isUpdate={isUpdate} />
      {editingId && <Editor />}
      {deletingId && isUpdate && <Deletor />}
    </>
  );
};

export default BlackList;
