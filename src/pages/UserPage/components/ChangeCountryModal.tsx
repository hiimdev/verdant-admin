import { Form, Modal, Typography } from 'antd';
import { IError } from 'api/types';
import { updateUserCountry, useGetAllCountries } from 'api/user';
import { IUpdateCountryRequest } from 'api/user/types';
import { Select } from 'components/Select';
import { FC } from 'react';
import { useMutation } from 'react-query';
import { message } from 'utils/message';

interface Props {
  visible: number | null;
  setVisible: (data: number | null) => void;
}

const { Title, Paragraph } = Typography;

export const ChangeCountryModal: FC<Props> = ({ visible, setVisible }) => {
  const { data: countries } = useGetAllCountries({ page: 1, limit: 300 });

  const dataCountries = countries?.list.map((item) => {
    const temp = { value: item.id, label: item.name, filterSearch: String(item.name) };
    return temp;
  });
  const [form] = Form.useForm();

  const { mutate } = useMutation(updateUserCountry, {
    onSuccess: () => {
      message.success('Update successfully!');
      setVisible(null);
    },
    onError: (error: IError) => {
      message.error(error?.meta.message[0]);
    },
  });

  const onFinish = (e: any) => {
    const request: IUpdateCountryRequest = {
      userId: visible,
      ...e,
    };
    mutate(request);
  };
  return (
    <Modal visible={!!visible} onCancel={() => setVisible(null)} width={400} onOk={() => form.submit()}>
      <div>
        <Title level={3} style={{ textAlign: 'center' }}>
          Notification
        </Title>
        <Paragraph>The Country default is Singapore. Do you want to change it ?</Paragraph>
        <Form onFinish={onFinish} form={form}>
          <Form.Item name="countryId" rules={[{ required: true }]} style={{ marginBottom: 0, marginTop: 20 }}>
            <Select
              showSearch
              placeholder="Select country"
              options={dataCountries}
              filterOption={(input, option) => option?.filterSearch.toLowerCase().includes(input)}
            ></Select>
          </Form.Item>
        </Form>
      </div>
    </Modal>
  );
};
