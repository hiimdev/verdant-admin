import { CheckCircleOutlined } from '@ant-design/icons';
import { AnyAction, Dispatch } from '@reduxjs/toolkit';
import { Button, Popconfirm, Space } from 'antd';
import { TableTitle } from 'components/Table/component/TableTitle';
import { useWallet } from 'hooks/useWallet';
import { AiFillEye, AiOutlineCloseCircle, AiOutlineEdit } from 'react-icons/ai';
import { setEditingId, setType } from 'store/ducks/user/slice';
import { KYC_STATUS, MARKET_ADDRESS, USER_ENUMS, USER_SORT, USER_STATUS_ENUM } from 'utils/constant';
import { timeFormatter } from 'utils/date';
import { message } from 'utils/message';
import styles from './styles.module.less';

export const getColumns = (
  dispatch: Dispatch<AnyAction>,
  mutate: any,
  handleKYC,
  submitParams?: any,
  isUpdate?: boolean,
  isAddBlackList?: boolean
) => {
  return [
    {
      title: 'No',
      key: 'index',
      dataIndex: 'index',
      align: 'center',
      width: '80px',
    },
    {
      title: (
        <TableTitle
          type="select"
          name="user"
          label="User type"
          onSubmit={(values) => submitParams({ ...values, user: Number(values.user) })}
          obj={USER_ENUMS}
        />
      ),
      dataIndex: 'minter',
      key: 'minter',
      render(data: any) {
        return <>{data ? 'Vendor' : 'Platform User'}</>;
      },
      align: 'center',
    },
    {
      title: (
        <TableTitle
          name="vendor"
          label="Vendor"
          onSubmit={submitParams}
          sort={[USER_SORT.VENDOR_DESC, USER_SORT.VENDOR_ASC]}
        />
      ),
      dataIndex: 'vendorName',
      key: 'vendorName',
      render(data: any, record: any) {
        return <>{record.minter ? data : '-'}</>;
      },
      align: 'center',
    },
    {
      title: (
        <TableTitle
          name="username"
          label="Username"
          onSubmit={submitParams}
          sort={[USER_SORT.USERNAME_DESC, USER_SORT.USERNAME_ASC]}
        />
      ),
      dataIndex: 'username',
      key: 'username',
    },
    {
      title: (
        <TableTitle
          name="email"
          label="Email"
          onSubmit={submitParams}
          sort={[USER_SORT.EMAIL_DESC, USER_SORT.EMAIL_ASC]}
        />
      ),
      dataIndex: 'email',
      key: 'email',
    },
    {
      title: (
        <TableTitle
          name="NFTsTraded"
          label="Total NFTs Traded"
          onSubmit={(values: any) =>
            submitParams({ ...values, isActive: values.isActive ? Number(values.isActive) : undefined })
          }
          sort={[USER_SORT.NFTsTraded_DESC, USER_SORT.NFTsTraded_ASC]}
        />
      ),
      dataIndex: 'NFTsTraded',
      key: 'NFTsTraded',
      align: 'center',
    },
    {
      title: (
        <TableTitle
          type="select"
          name="KYCStatus"
          label="Successful KYC"
          onSubmit={(values: any) =>
            submitParams({ ...values, KYCStatus: values.KYCStatus ? Number(values.KYCStatus) : undefined })
          }
          obj={KYC_STATUS}
        />
      ),
      dataIndex: 'KYCStatus',
      key: 'KYCStatus',
      align: 'center',
      render(data: number, record: any) {
        return (
          <Popconfirm
            title={
              <div style={{ width: '300px' }}>
                Are you sure to change the KYC status for this account from
                {data === 0 ? ' Unsuccessful' : ' Successful'} to {data !== 0 ? 'Unsuccessful' : 'Successful'}?
              </div>
            }
            onConfirm={() => handleKYC(record.id)}
            okText="Yes"
            cancelText="No"
          >
            {data === 1 ? (
              <CheckCircleOutlined style={{ fontSize: '14px' }} />
            ) : (
              <div
                style={{
                  borderRadius: '50%',
                  width: '14px',
                  height: '14px',
                  border: '1px solid #888',
                  margin: 'auto',
                  cursor: 'pointer',
                }}
              ></div>
            )}
          </Popconfirm>
        );
      },
    },
    {
      title: (
        <TableTitle
          type="none"
          name="createdAt"
          label="Created at"
          onSubmit={submitParams}
          sort={[USER_SORT.CREATEDAT_DESC, USER_SORT.CREATEDAT_ASC]}
        />
      ),
      dataIndex: 'createdAt',
      key: 'createdAt',
      render(createdAt: string) {
        return timeFormatter(Number(createdAt));
      },
      align: 'center',
    },
    {
      title: (
        <TableTitle
          name="createdBy"
          label="Created By"
          onSubmit={submitParams}
          sort={[USER_SORT.CREATEDBY_DESC, USER_SORT.CREATEDBY_ASC]}
        />
      ),
      dataIndex: 'createdBy',
      key: 'createdBy',
      align: 'center',
      render(data: any) {
        return <>{data || 'Self-registered'}</>;
      },
    },
    {
      title: (
        <TableTitle
          type="none"
          name="updatedAt"
          label="Update at"
          onSubmit={submitParams}
          sort={[USER_SORT.UPDATEAT_DESC, USER_SORT.UPDATEAT_ASC]}
        />
      ),
      dataIndex: 'updatedAt',
      key: 'updatedAt',
      render(updatedAt: string) {
        return timeFormatter(Number(updatedAt));
      },
      align: 'center',
    },
    {
      title: (
        <TableTitle
          name="updatedBy"
          label="Update By"
          onSubmit={submitParams}
          sort={[USER_SORT.UPDATEDBY_DESC, USER_SORT.UPDATEDBY_ASC]}
        />
      ),
      dataIndex: 'updatedBy',
      key: 'updatedBy',
      align: 'center',
      render(data: any) {
        return <>{data || '-'}</>;
      },
    },
    {
      title: <TableTitle type="select" name="status" label="Status" onSubmit={submitParams} obj={USER_STATUS_ENUM} />,
      dataIndex: 'status',
      key: 'status',
      render(status: 'request' | 'active') {
        return USER_STATUS_ENUM[status];
      },
    },
    {
      title: 'Action',
      align: 'center',
      dataIndex: 'id',
      fixed: 'right',
      render(id: any, record: any) {
        const { account } = useWallet();
        return (
          <Space>
            {isAddBlackList && (
              <Popconfirm
                title="Are you sure add this account to blacklist？"
                okText="Yes"
                cancelText="No"
                onConfirm={() => {
                  if (account === MARKET_ADDRESS) {
                    mutate({ email: record.email });
                  } else {
                    message.info('Please login correctly Digicap account');
                  }
                }}
              >
                <Button className={styles.btnAction} icon={<AiOutlineCloseCircle />} title="Add to black list" />
              </Popconfirm>
            )}
            <Button
              className={styles.btnAction}
              icon={isUpdate ? <AiOutlineEdit /> : <AiFillEye />}
              type="primary"
              onClick={() => {
                dispatch(setEditingId(String(id)));
                dispatch(setType(String('edit')));
              }}
              title={isUpdate ? 'Edit' : 'View'}
            />
          </Space>
        );
      },
    },
  ];
};
