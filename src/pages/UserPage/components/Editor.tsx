import { useContractFunction } from '@usedapp/core';
import { Form, Modal, Space, Typography } from 'antd';
import { addAdminTransfer, useGetMasterWallet, useGetSignatureExpTime } from 'api/statistical';
import { IError } from 'api/types';
import { useGetListVendorFilter, useUserQuery } from 'api/user';
import { updateBrandUser } from 'api/user/requet';
import { IVendorResponse } from 'api/user/types';
import BigNumber from 'bignumber.js';
import { Button } from 'components/Button';
import { Drawer } from 'components/Drawer';
import { Input, InputNumber } from 'components/Input';
import { Select } from 'components/Select';
import NFTABI from 'contracts/NFTABI.json';
import TokenABI from 'contracts/TokenABI.json';
import { formatUnits } from 'ethers/lib/utils';
import { useAppDispatch, useAppSelector } from 'hooks/reduxHook';
import { useConnectABI } from 'hooks/useConnectABI';
import { useGetTokenBalanceToken } from 'hooks/useGetBalance';
import { useGetMinter } from 'hooks/useGetMinter';
import { useWallet } from 'hooks/useWallet';
import { FC, useEffect, useMemo, useState } from 'react';
import { useMutation, useQueryClient } from 'react-query';
import { getEditingId, setEditingId } from 'store/ducks/user/slice';
import { convertPriceEther, getLoadingButtonCallSMCT } from 'utils/common';
import { CONTRACT_ADDRESS, MARKET_ADDRESS } from 'utils/constant';
import { typeNumber } from 'utils/form';
import { message } from 'utils/message';
import { IListType } from './type';

const { Title } = Typography;

const Editor: FC<IListType> = ({ isUpdate: canUpdate }) => {
  const dispatch = useAppDispatch();
  const editingId = useAppSelector(getEditingId);
  const { account } = useWallet();
  const { data: user } = useUserQuery(editingId as string, { enabled: !!editingId });
  const [callBackGetMinter, setCallBackGetMinter] = useState(false);
  const [transferModal, setTransferModal] = useState(false);
  const { data: listVendor } = useGetListVendorFilter({ page: 1, limit: 50 }, { enabled: !!editingId });

  const IsMinter = useGetMinter(user?.wallet, callBackGetMinter);
  const balance = useGetTokenBalanceToken(user?.wallet || '', CONTRACT_ADDRESS.TOKEN, TokenABI, transferModal);

  const isUpdate = useMemo(() => {
    return (canUpdate && editingId !== '') || editingId === '';
  }, []);

  const client = useQueryClient();
  const [form] = Form.useForm();

  const { data: master } = useGetMasterWallet();
  useEffect(() => {
    form.resetFields();
    if (editingId !== null) {
      form.setFieldsValue({
        ...user,
        // brandIds: data?.listBrand.map((item) => item.id),
        avatarUrl: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSa9wrgEqvctXHKvLGLkap8aVGp_KY6WFF2FnvQFYxa&s',
        vendorId: user?.isVendor ? user?.vendorId : '',
        totalTokens: convertPriceEther(user?.totalTokens || 0),
      });
      if (IsMinter) {
        form.setFieldsValue({
          vendorId: user?.vendorId || '',
        });
      }
    }
  }, [user, IsMinter]);

  useEffect(() => {
    form.setFieldsValue({
      remainNfts: balance ? formatUnits(balance) : '0',
    });
  }, [balance]);

  const { mutate, isLoading } = useMutation(updateBrandUser, {
    onSuccess: () => {
      client.invalidateQueries('/user-admin/paging_filter');
      message.success('Successfully');
      handleClose();
    },
    onError: (error: IError) => {
      message.error(error?.meta.message);
    },
  });

  const handleClose = () => {
    client.invalidateQueries('/user-admin/paging_filter');
    setCallBackGetMinter(false);
    dispatch(setEditingId(null));
  };

  const {
    resetState: resetAdd,
    state: statusAddMinter,
    send: addMinter,
  } = useContractFunction(useConnectABI(NFTABI, CONTRACT_ADDRESS.NFT), 'addMinter');

  const { resetState: resetRemove, state: statusRemoveMinter } = useContractFunction(
    useConnectABI(NFTABI, CONTRACT_ADDRESS.NFT),
    'removeMinter'
  );

  const {
    resetState: resetTransfer,
    state: statusTransfer,
    send: transfer,
  } = useContractFunction(useConnectABI(TokenABI, CONTRACT_ADDRESS.TOKEN), 'transfer');

  const { mutate: mutateGetSignature } = useMutation(useGetSignatureExpTime, {
    onSuccess: (data) => {
      addMinter(data?._minters, data.signature, data._expTime);
      console.log([user?.wallet], `${data.signature}`, data._expTime);
      console.log(master, 'sdsdsds');
    },
    onError: (error: IError) => {
      message.error(error?.meta.message);
    },
  });

  const onFinish = async (e: any) => {
    // const temp = Array.from(new Set(e.brandIds));

    mutate({ id: editingId as string, referralName: e.referralName, vendorId: e.vendorId });
  };

  const handleClickAddWhiteList = () => {
    if (!user?.wallet) {
      message.info('You need to connect to wallet address on the website');
      return;
    }
    console.log(1, account === MARKET_ADDRESS, '1' === '1');
    if (account === MARKET_ADDRESS) {
      const request = { _sender: account, _minters: `['${user.wallet}']` };
      mutateGetSignature(request);
    } else {
      message.info('You need to login with Market account');
    }
  };
  const { mutate: mutateAddAdminTransfer } = useMutation(addAdminTransfer, {
    onSuccess: () => {},
    onError: (error: IError) => {
      message.error(error?.meta.message);
    },
  });

  useEffect(() => {
    if (statusTransfer.status === 'Success') {
      mutateAddAdminTransfer({ txid: statusTransfer?.transaction?.hash || '' });
      resetTransfer();
      setCallBackGetMinter(!callBackGetMinter);
      message.success('Transfer Successfully');
      setTransferModal(false);
    } else if (statusTransfer.status === 'Exception' || statusTransfer.status === 'Fail') {
      message.error(statusTransfer.errorMessage || '');
    }
  }, [statusTransfer]);

  useEffect(() => {
    if (statusRemoveMinter.status === 'Success') {
      resetRemove();
      setCallBackGetMinter(!callBackGetMinter);
      message.success('Remove Vendor Successfully');
      setEditingId(null);
    }
  }, [statusRemoveMinter]);

  useEffect(() => {
    if (statusAddMinter.status === 'Success') {
      resetAdd();
      let request = {
        id: editingId as string,
        vendorId: user?.vendorId ? Number(user?.vendorId) : '',
        referralName: user?.referralName,
        brandIds: user?.listBrand.map((item) => item.id),
      };
      if (form.getFieldValue('vendorId')) {
        request = {
          id: editingId as string,
          referralName: user?.referralName,
          brandIds: user?.listBrand.map((item) => item.id),
          vendorId: form.getFieldValue('vendorId'),
        };
      }
      mutate(request);
      // dispatch(setEditingId(null));
      setCallBackGetMinter(!callBackGetMinter);
      message.success('Add Vendor Successfully');
    }
  }, [statusAddMinter]);

  const onFinishTransfer = (value: any) => {
    if (account === MARKET_ADDRESS) {
      const bigNumber = new BigNumber(value.price).multipliedBy(10 ** 18);
      transfer(user?.wallet, `${bigNumber.toPrecision(bigNumber.e ? bigNumber.e + 1 : 0)}`);
    } else {
      message.info('You need to login with Market account');
    }
  };

  const handleTransfer = () => {
    if (user?.wallet) {
      setTransferModal(true);
    } else {
      message.info('This account is not connect to the wallet on the website');
    }
  };

  return (
    <Drawer
      name="user"
      editingId={editingId}
      handleClose={handleClose}
      form={form}
      submitLoading={isLoading}
      getLoading={(!user && !!editingId) || (IsMinter !== true && IsMinter !== false)}
      disabledSubmit={!user?.wallet}
      viewOnly={!isUpdate}
    >
      <Form form={form} onFinish={onFinish}>
        <Form.Item label="Username" name="username" labelCol={{ span: 6 }} wrapperCol={{ span: 15 }}>
          <Input disabled />
        </Form.Item>
        <Form.Item label="Email" name="email" labelCol={{ span: 6 }} wrapperCol={{ span: 15 }}>
          <Input disabled />
        </Form.Item>
        <Form.Item label="Wallet" name="wallet" labelCol={{ span: 6 }} wrapperCol={{ span: 15 }}>
          <Input disabled />
        </Form.Item>
        <Form.Item label="Number of owned NFTs" name="countNFT" labelCol={{ span: 6 }} wrapperCol={{ span: 15 }}>
          <Input disabled />
        </Form.Item>
        <Form.Item
          label="Vendor name"
          name="vendorId"
          labelCol={{ span: 6 }}
          wrapperCol={{ span: 15 }}
          rules={[
            () => ({
              validator() {
                if (user?.isActiveKyc === 1) {
                  return Promise.resolve();
                } else
                  return Promise.reject(new Error(`Cannot add this user as a Vendor because hasn't KYC successful`));
              },
            }),
          ]}
        >
          <Select disabled={!isUpdate} placeholder="Select vendor" style={{ width: '100%' }}>
            {listVendor?.list?.map((item: IVendorResponse) => (
              <Select.Option key={item.id} value={item.id}>
                {item.vendorName}
              </Select.Option>
            ))}
          </Select>
        </Form.Item>
        {!editingId && (
          <Form.Item label="Referral name" name="referralName" labelCol={{ span: 6 }} wrapperCol={{ span: 15 }}>
            <Input disabled={!isUpdate} placeholder={'Enter your neferral name'} />
          </Form.Item>
        )}
        <Form.Item label="Remaining mint VMTs" name="remainNfts" labelCol={{ span: 6 }} wrapperCol={{ span: 15 }}>
          <Input disabled />
        </Form.Item>
        <Form.Item
          label="Total Issued Tokens by Verdant"
          name="totalTokens"
          labelCol={{ span: 6 }}
          wrapperCol={{ span: 15 }}
        >
          <Input disabled />
        </Form.Item>
        <Form.Item label="Action" labelCol={{ span: 6 }} wrapperCol={{ span: 15 }}>
          <Space>
            <Button disabled={!isUpdate} onClick={handleTransfer}>
              Add More VMT
            </Button>
            {!IsMinter ? (
              <Button
                disabled={!isUpdate}
                loading={getLoadingButtonCallSMCT(statusAddMinter)}
                onClick={handleClickAddWhiteList}
              >
                Add Vendor
              </Button>
            ) : (
              <></>
              // <Button loading={getLoadingButtonCallSMCT(statusRemoveMinter)} onClick={handleClickRemoveWhiteList}>
              //   Remove Vendor
              // </Button>
            )}
          </Space>
        </Form.Item>
      </Form>
      <Modal footer={null} visible={transferModal} onCancel={() => setTransferModal(false)} width={400}>
        <div>
          <Title level={3} style={{ textAlign: 'center' }}>
            TRANSFER
          </Title>
          <Form onFinish={onFinishTransfer}>
            <Form.Item name="price" rules={[{ required: true }]}>
              <InputNumber
                onKeyDown={(evt) => {
                  typeNumber(evt);
                }}
                type="number"
                placeholder="Number of token"
              />
            </Form.Item>
            <Form.Item style={{ marginBottom: 0, textAlign: 'center' }}>
              <Button loading={getLoadingButtonCallSMCT(statusTransfer)} htmlType="submit">
                Transfer
              </Button>
            </Form.Item>
          </Form>
        </div>
      </Modal>
    </Drawer>
  );
};

export default Editor;
