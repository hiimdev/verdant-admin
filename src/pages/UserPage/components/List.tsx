import { useContractFunction } from '@usedapp/core';
import useBreakpoint from 'antd/lib/grid/hooks/useBreakpoint';
import { addBlackList } from 'api/search/request';
import { IError } from 'api/types';
import { useUsersQuery } from 'api/user';
import { updateKYC } from 'api/user/requet';
import { PaginatedTableAndSearch } from 'components/PaginatedTableAndSearch';
import NFTABI from 'contracts/NFTABI.json';
import { useAppDispatch } from 'hooks/reduxHook';
import { useConnectABI } from 'hooks/useConnectABI';
import { FC, useEffect, useMemo, useState } from 'react';
import { useMutation, useQueryClient } from 'react-query';
import { CONTRACT_ADDRESS } from 'utils/constant';
import { debounce } from 'utils/js';
import { message } from 'utils/message';
import { ChangeCountryModal } from './ChangeCountryModal';
import { getColumns } from './Column';
import { IListType } from './type';

const List: FC<IListType> = ({ isUpdate, isAddBlackList }) => {
  const [visible, setVisible] = useState<number | null>(null);

  const { state: statusRemoveMinter, send: removeMinter } = useContractFunction(
    useConnectABI(NFTABI, CONTRACT_ADDRESS.NFT),
    'removeMinter'
  );

  const { mutate } = useMutation(addBlackList, {
    onSuccess: (data) => {
      removeMinter(data?.wallet);
    },
    onError: (error: IError) => {
      message.error(error?.meta.message[0]);
    },
  });

  const dispatch = useAppDispatch();
  const [params, setParams] = useState({ sort: [] });
  const client = useQueryClient();
  const debouncedSetParams = useMemo(() => debounce(setParams, 500), []);

  const submitParams = (value: any) => {
    debouncedSetParams((prevParams: any) => ({
      ...prevParams,
      ...value,
      sort: value.sort ? [value.sort] : [],
    }));
  };
  const { mutate: update } = useMutation(updateKYC, {
    onSuccess: (res) => {
      if (res.isDefaultCountry) {
        setVisible(res.id);
      }
      client.invalidateQueries('/user-admin/paging_filter');
      message.success('KYC update successfully');
    },
    onError: (error: IError) => {
      message.error(error?.meta.message[0]);
    },
  });

  const handleKYC = (val) => {
    const temp = { id: val };
    update(temp);
  };

  const columns = useMemo(
    () => getColumns(dispatch, mutate, handleKYC, submitParams, isUpdate, isAddBlackList),
    [getColumns, isUpdate, isAddBlackList]
  );
  const screen = useBreakpoint();

  useEffect(() => {
    if (statusRemoveMinter.status === 'Success') {
      client.invalidateQueries('/user-admin/black-list');
      client.invalidateQueries('/user-admin/paging_filter');
      message.success('Add to black list successfully!');
    } else if (statusRemoveMinter.status === 'Fail' || statusRemoveMinter.status === 'Exception') {
      message.error(statusRemoveMinter.errorMessage || '');
    }
  }, [statusRemoveMinter]);

  return (
    <>
      <PaginatedTableAndSearch
        title="User"
        columns={columns}
        useQuery={useUsersQuery}
        requestParams={params}
        scroll={screen.xxl ? undefined : { x: 1900 }}
        handleReset={() => setParams({ sort: [] })}
      />
      <ChangeCountryModal visible={visible} setVisible={setVisible} />
    </>
  );
};

export default List;
