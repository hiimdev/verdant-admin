import { Button, Tooltip } from 'antd';
import { FC } from 'react';
import { shortenAddress } from '@usedapp/core';
import { useCopy } from 'hooks/useCopy';
import { AiOutlineCheck, AiOutlineCopy } from 'react-icons/ai';

type IWalletAddress = {
  text: string;
};

export const WalletAddress: FC<IWalletAddress> = ({ text }) => {
  const [copied, copy] = useCopy();

  return (
    <>
      <Tooltip title={text}>{text ? shortenAddress(text) : '-'}</Tooltip>
      <Button
        type="link"
        onClick={() => copy(text || '')}
        icon={copied ? <AiOutlineCheck /> : <AiOutlineCopy />}
      ></Button>
    </>
  );
};
