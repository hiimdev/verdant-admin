import { useAppSelector } from 'hooks/reduxHook';
import { FC, useMemo } from 'react';
import { getEditingId, getPermission } from 'store/ducks/user/slice';
import { Permission } from 'utils/permission';
import Editor from './components/Editor';
import List from './components/List';

const User: FC = () => {
  const permissions = useAppSelector(getPermission);
  const editingId = useAppSelector(getEditingId);

  const isUpdate = useMemo(() => {
    return permissions.includes(Permission.edit_user);
  }, [permissions]);

  const isAddBlackList = useMemo(() => {
    return permissions.includes(Permission.add_blacklist);
  }, [permissions]);
  return (
    <>
      <List isUpdate={isUpdate} isAddBlackList={isAddBlackList} />
      {editingId && <Editor isUpdate={isUpdate} />}
    </>
  );
};

export default User;
