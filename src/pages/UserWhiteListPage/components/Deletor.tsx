import { Modal } from 'antd';
import { useAppDispatch, useAppSelector } from 'hooks/reduxHook';
import { FC, useEffect } from 'react';
import { getDeletingId, setDeletingId } from 'store/ducks/whiteList/slice';
import { useQueryClient } from 'react-query';
import { message } from 'utils/message';
import { useUserQuery } from 'api/user';
import { useWallet } from 'hooks/useWallet';
import { CONTRACT_ADDRESS, MARKET_ADDRESS } from 'utils/constant';
import { useConnectABI } from 'hooks/useConnectABI';
import { useContractFunction } from '@usedapp/core';
import NFTABI from 'contracts/NFTABI.json';
import { getLoadingButtonCallSMCT } from 'utils/common';

const Deletor: FC = () => {
  const dispatch = useAppDispatch();
  const deletingId = useAppSelector(getDeletingId);
  const client = useQueryClient();
  const { data } = useUserQuery(deletingId as string, { enabled: !!deletingId });
  const { account } = useWallet();

  const handleClose = () => {
    dispatch(setDeletingId(null));
  };

  const {
    resetState: resetRemove,
    state: statusRemoveMinter,
    send: removeMinter,
  } = useContractFunction(useConnectABI(NFTABI, CONTRACT_ADDRESS.NFT), 'removeMinter');

  const onDelete = () => {
    if (account === MARKET_ADDRESS) removeMinter(data?.wallet);
    else {
      message.info('You need to login with Market account');
    }
  };

  useEffect(() => {
    if (statusRemoveMinter.status === 'Success') {
      resetRemove();
      message.success('Remove Minter Successfully');
      client.invalidateQueries('/whitelist/minter');
    }
  }, [statusRemoveMinter]);

  return (
    <>
      <Modal
        visible={!!deletingId}
        title={`Delete user: ${data?.username}`}
        // icon={<AiFillExclamationCircle />}
        okButtonProps={{
          loading: getLoadingButtonCallSMCT(statusRemoveMinter),
        }}
        onCancel={handleClose}
        onOk={onDelete}
        okType="danger"
        okText="Confirm"
      >
        Are you sure you want to remove this account from whiteList?
      </Modal>
    </>
  );
};

export default Deletor;
