import { useContractFunction } from '@usedapp/core';
import { Form, Input, Spin } from 'antd';
import { Drawer } from 'components/Drawer';
import { useAppDispatch, useAppSelector } from 'hooks/reduxHook';
import { useConnectABI } from 'hooks/useConnectABI';
import { FC, useEffect } from 'react';
import { useQueryClient } from 'react-query';
import { getEditingId, setEditingId } from 'store/ducks/whiteList/slice';
import { CONTRACT_ADDRESS, MARKET_ADDRESS } from 'utils/constant';
import { rules } from 'utils/form';
import { message } from 'utils/message';
import NFTABI from 'contracts/NFTABI.json';
import { getLoadingButtonCallSMCT } from 'utils/common';
import { useWallet } from 'hooks/useWallet';

const Editor: FC = () => {
  const dispatch = useAppDispatch();
  const editingId = useAppSelector(getEditingId);
  const client = useQueryClient();
  const { account } = useWallet();

  const [form] = Form.useForm();

  // const { mutate, isLoading } = useMutation(addWhiteList, {
  //   onSuccess: () => {
  //     client.invalidateQueries('/user-admin/white-list');
  //     message.success('Add successfully!');
  //     handleClose();
  //   },
  //   onError: (error: IError) => {
  //     message.error(error?.meta.message[0]);
  //   },
  // });

  const { state, send } = useContractFunction(useConnectABI(NFTABI, CONTRACT_ADDRESS.NFT), 'addMinter');

  useEffect(() => {
    if (state.status === 'Exception') {
      message.error(state.errorMessage as string);
    } else if (state.status === 'Success') {
      client.invalidateQueries('/whitelist/minter');
      message.success('Add successfully!');
      handleClose();
    }
  }, [state]);

  const handleClose = () => {
    dispatch(setEditingId(null));
    form.resetFields();
  };

  const onFinish = (value: any) => {
    if (account === MARKET_ADDRESS) send(value.wallet);
    else {
      message.info('Please login correctly Digicap account');
    }
  };

  return (
    <Drawer
      name="Minter"
      editingId={editingId}
      handleClose={handleClose}
      form={form}
      getLoading={false}
      submitLoading={getLoadingButtonCallSMCT(state)}
    >
      <Spin tip="Loading..." spinning={getLoadingButtonCallSMCT(state)}>
        <Form form={form} onFinish={onFinish}>
          <Form.Item label="Wallet address" name="wallet" labelCol={{ span: 3 }} rules={[rules]}>
            <Input placeholder="Enter your wallet address" />
          </Form.Item>
        </Form>
      </Spin>
    </Drawer>
  );
};

export default Editor;
