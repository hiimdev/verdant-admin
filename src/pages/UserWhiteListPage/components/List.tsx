import { AnyAction, Dispatch } from '@reduxjs/toolkit';
import { useGetWhiteList } from 'api/search';
import { Header } from 'components/Header';
import { PaginatedTableAndSearch } from 'components/PaginatedTableAndSearch';
import { useAppDispatch } from 'hooks/reduxHook';
import { FC } from 'react';
import { AiOutlineDelete } from 'react-icons/ai';
import { setDeletingId, setEditingId } from 'store/ducks/whiteList/slice';
import styles from './styles.module.less';
import { WalletAddress } from './WalletAddress';
import { useWallet } from 'hooks/useWallet';
import { Button } from 'components/Button';
import { requestSwitchNetwork } from 'lib/requestSwitchNetwork';
import { CURRENCY } from 'lib/config';
import { timeFormatter } from 'utils/date';
import { Tooltip } from 'antd';

const getColumns = (dispatch: Dispatch<AnyAction>) => {
  return [
    {
      title: 'No',
      dataIndex: 'index',
      key: 'index',
      align: 'center',
    },
    {
      title: 'Super Admin/Vendor',
      dataIndex: 'isVendor',
      key: 'isVendor',
      align: 'center',
      render(data: any) {
        return <>{data === 1 ? 'Vendor' : 'Super Admin'}</>;
      },
    },
    {
      title: 'Status',
      dataIndex: 'isVendor',
      key: 'isVendor',
      align: 'center',
      render(data: any) {
        return <>{data === 1 ? 'Active' : 'InActive'}</>;
      },
    },
    {
      title: 'Vendor',
      dataIndex: 'vendorName',
      key: 'vendorName',
      align: 'center',
      render(data: any) {
        return <>{data || '-'}</>;
      },
    },
    {
      title: 'Total NFTs minted',
      dataIndex: 'NFTsMinted',
      key: 'NFTsMinted',
      align: 'center',
    },
    {
      title: 'Username',
      dataIndex: 'username',
      key: 'username',
      align: 'center',
      render(data: any) {
        return (
          <>
            <Tooltip title={data} placement="top">
              {`${data?.slice(0, 15)}...` || '-'}
            </Tooltip>{' '}
          </>
        );
      },
    },
    {
      title: 'Email',
      dataIndex: 'email',
      key: 'email',
      align: 'center',
      render(data: any) {
        return (
          <>
            <Tooltip title={data} placement="top">
              {`${data?.slice(0, 15)}...` || '-'}
            </Tooltip>{' '}
          </>
        );
      },
    },
    {
      title: 'Wallet',
      dataIndex: 'wallet',
      key: 'wallet',
      align: 'center',
      render(wallet: any) {
        return wallet ? <WalletAddress text={wallet} /> : '-';
      },
      width: '150px',
    },
    {
      title: 'Last mint date',
      dataIndex: 'lastMintDate',
      key: 'lastMintDate',
      render(data: string) {
        return timeFormatter(Number(data));
      },
      align: 'center',
    },
    {
      title: 'Action',
      align: 'center',
      dataIndex: 'id',
      render(id: string) {
        return (
          <Button
            className={styles.btnAction}
            icon={<AiOutlineDelete />}
            danger
            onClick={() => dispatch(setDeletingId(id))}
            title="Delete"
          />
        );
      },
    },
  ];
};

const List: FC = () => {
  const dispatch = useAppDispatch();
  const columns = getColumns(dispatch);
  const { chainId, account, error, connect } = useWallet();

  const handleConnectWallet = async () => {
    await connect('injected');
  };

  const handleSwitchNetwork = async () => {
    await requestSwitchNetwork(Number(Object.keys(CURRENCY)[0]));
  };

  return (
    <>
      {((account && chainId !== 80001) ||
        String(error?.name) === 'UnsupportedChainIdError' ||
        String(error?.name) === 't') && (
        <Button style={{ marginBottom: '30px' }} onClick={handleSwitchNetwork}>
          Wrong wallet
        </Button>
      )}
      {(!account || chainId !== 80001) && !error && (
        <Button style={{ marginBottom: '30px' }} onClick={handleConnectWallet}>
          Connect wallet
        </Button>
      )}
      {account && <Header title="Minter" setEditingId={setEditingId}></Header>}
      <PaginatedTableAndSearch
        columns={columns}
        useQuery={useGetWhiteList}
        // requestParams={requestParams}
      />
    </>
  );
};

export default List;
