import { FC } from 'react';
import Deletor from './components/Deletor';
import Editor from './components/Editor';
import List from './components/List';

const WhiteList: FC = () => {
  return (
    <>
      <List />
      <Editor />
      <Deletor />
    </>
  );
};

export default WhiteList;
