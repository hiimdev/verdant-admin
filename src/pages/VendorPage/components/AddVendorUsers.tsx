import { Modal, Typography } from 'antd';
import { Form } from 'antd';
import { FC, useEffect, useState } from 'react';
import { Input, InputPassword } from 'components/Input';
import { rules } from 'utils/form';
import { createUserVendor, useAddUserVendor, useGetDetailUserVendor } from 'api/user/requet';
import { IError } from 'api/types';
import { useMutation, useQueryClient } from 'react-query';
import { message } from 'utils/message';
import { useAppSelector } from 'hooks/reduxHook';
import { getLogId } from 'store/ducks/vendor/slice';
import { useGetDetailVendor } from 'api/user';
import { IVendorResponse } from 'api/user/types';
import { useContractFunction } from '@usedapp/core';
import { useConnectABI } from 'hooks/useConnectABI';
import { CONTRACT_ADDRESS } from 'utils/constant';
import NFTABI from 'contracts/NFTABI.json';
import { useUser } from 'hooks/useUser';
import { useWallet } from 'hooks/useWallet';

export const AddVendorUsers: FC<{
  handleClose: () => void;
  show: boolean;
}> = ({ handleClose, show }) => {
  const [step, setStep] = useState(0);
  const [valueEmail, setValueEmai] = useState('');
  const [detailUser, setDetailUser] = useState({});
  const [view, setView] = useState(false);
  const client = useQueryClient();
  const logId = useAppSelector(getLogId);
  const { data: vendorDetail } = useGetDetailVendor(logId as string, { enabled: !!logId });
  const { user } = useUser();
  const { account } = useWallet();

  const { state: statusAddMinter, send: addMinter } = useContractFunction(
    useConnectABI(NFTABI, CONTRACT_ADDRESS.NFT),
    'addMinter'
  );

  const { mutate: create, status: statusCreateUserVendor } = useMutation(createUserVendor, {
    onSuccess: (data: any) => {
      if (user?.dataReturn?.roleId === 1) {
        if (account) {
          const { paramAddMinter } = data;
          addMinter(paramAddMinter.minters, paramAddMinter.signature, paramAddMinter.expTime);
          return;
        }
      }
      client.invalidateQueries('/vendor/list-user-admin');
      message.success('Create successfully!');
      handleClose();
    },
    onError: (error: IError) => {
      message.error(error?.meta.message[0]);
    },
  });

  const { mutate: get, status: statusCheckEmail } = useMutation(useGetDetailUserVendor, {
    onSuccess: (data) => {
      setDetailUser(data.data);
      setView(data.data?.email ? true : false);
      setStep(step + 1);
    },
    onError: (error: IError) => {
      message.error(error?.meta.message[0]);
    },
  });

  const { mutate: add, status: statusAddUserVendor } = useMutation(useAddUserVendor, {
    onSuccess: () => {
      client.invalidateQueries('/vendor/list-user-admin');
      message.success('Successfully');
      handleClose();
    },
    onError: (error: IError) => {
      message.error(error?.meta.message[0]);
    },
  });

  const handleCreateUserVendor = (value) => {
    const request = { ...value, email: valueEmail, vendorId: logId };
    delete request.confirmPassword;
    if (user?.dataReturn?.roleId === 1 && !account) {
      message.info('You need to log in the metamask');
    } else {
      create(request);
    }
  };

  const handleAddUserVendor = (value) => {
    const request = { ...value, vendorId: logId };
    delete request.username;
    add(request);
  };

  useEffect(() => {
    if (statusAddMinter.status === 'Success') {
      client.invalidateQueries('/vendor/list-user-admin');
      message.success('Create successfully!');
      handleClose();
    }
  }, [statusAddMinter.status]);

  const onNext = (value) => {
    if (step === 0) {
      setValueEmai(value.email);
      get(value.email);
    }
    if (step === 1 && view) {
      handleAddUserVendor(value);
    }
    if (step === 1 && !view) {
      handleCreateUserVendor(value);
    }
  };
  const onBack = () => {
    setStep(step - 1);
  };
  if (step === 1 && view)
    return (
      <Step1
        vendorDetail={vendorDetail}
        onBack={onBack}
        onNext={onNext}
        data={detailUser}
        loadCheck={statusAddUserVendor === 'loading'}
      />
    );
  if (step === 1 && !view)
    return (
      <Step2
        email={valueEmail}
        vendorDetail={vendorDetail}
        onBack={onBack}
        onNext={onNext}
        loadCheck={
          statusCreateUserVendor === 'loading' ||
          statusAddMinter.status === 'PendingSignature' ||
          statusAddMinter.status === 'Mining'
        }
      />
    );
  return (
    <Step0
      vendorDetail={vendorDetail}
      show={show}
      onNext={onNext}
      onClose={handleClose}
      loadCheck={statusCheckEmail === 'loading'}
    />
  );
};
const Step0: FC<{
  onClose?: () => void;
  onNext?: (value) => void;
  show: boolean;
  loadCheck: boolean;
  vendorDetail?: IVendorResponse;
}> = ({ onClose, onNext, show, loadCheck, vendorDetail }) => {
  const [form] = Form.useForm();
  const visible = show;
  return (
    <Modal
      confirmLoading={loadCheck}
      okText={'Confirm'}
      title="Add Vendor Users"
      onOk={form.submit}
      onCancel={onClose}
      visible={visible}
    >
      <Form form={form} layout="vertical" onFinish={onNext}>
        <Form.Item
          label={`Input the email address to create Vendor user for ${vendorDetail?.vendorName || ''}`}
          name="email"
          rules={[{ required: true }, { type: 'email' }]}
        >
          <Input />
        </Form.Item>
      </Form>
    </Modal>
  );
};

const Step1: FC<{
  data?: any;
  onNext?: (value) => void;
  onBack?: () => void;
  vendorDetail?: IVendorResponse;
  loadCheck: boolean;
}> = ({ data, onNext, onBack, vendorDetail, loadCheck }) => {
  const [form] = Form.useForm();
  useEffect(() => {
    form.resetFields();
    form.setFieldsValue(data);
  }, [data]);
  return (
    <Modal
      confirmLoading={loadCheck}
      visible
      okText={'Confirm'}
      width={1000}
      title="Add Vendor Users"
      onOk={form.submit}
      onCancel={onBack}
    >
      <Typography.Title level={5}>
        {data.email} is used to creating an account as platform user, including:
      </Typography.Title>
      <Form onFinish={onNext} form={form} layout="vertical">
        <Form.Item name="email" label="User Name">
          <Input disabled />
        </Form.Item>
        <Form.Item name="username" label="Full Name">
          <Input disabled />
        </Form.Item>
      </Form>
      <Typography.Title level={5}>
        Are you sure to add this account as Vendor Use of {vendorDetail?.vendorName}?
      </Typography.Title>
    </Modal>
  );
};

const Step2: FC<{
  onNext?: (value) => void;
  onBack?: () => void;
  loadCheck: boolean;
  vendorDetail?: IVendorResponse;
  email: string;
}> = ({ onNext, onBack, loadCheck, vendorDetail, email }) => {
  const [form] = Form.useForm();
  return (
    <Modal
      confirmLoading={loadCheck}
      okText={'Confirm'}
      width={1000}
      title="Add Vendor Users"
      onOk={form.submit}
      onCancel={onBack}
      visible
    >
      <Typography.Title level={5}>
        To add {email} becom Vendor User of {vendorDetail?.vendorName}, please input informations below:
      </Typography.Title>
      <Form form={form} layout="vertical" onFinish={onNext}>
        <Form.Item
          label="Username"
          name="username"
          labelCol={{ span: 3 }}
          wrapperCol={{ span: 18 }}
          rules={[{ ...rules }]}
        >
          <Input placeholder={'Enter your user name'} defaultValue="" />
        </Form.Item>
        <Form.Item
          label="Password"
          name="password"
          labelCol={{ span: 3 }}
          wrapperCol={{ span: 18 }}
          rules={[{ ...rules }]}
        >
          <InputPassword autoComplete="new-password" placeholder={'Enter your password'} defaultValue="" />
        </Form.Item>
        <Form.Item
          label="Confirm Password"
          name="confirmPassword"
          labelCol={{ span: 3 }}
          wrapperCol={{ span: 18 }}
          rules={[
            { required: true },
            ({ getFieldValue }) => ({
              validator(_, value) {
                if (!value || getFieldValue('password') === value) {
                  return Promise.resolve();
                }
                return Promise.reject(new Error('Should match Password'));
              },
            }),
          ]}
        >
          <InputPassword placeholder={'Confirm your password'} />
        </Form.Item>
      </Form>
    </Modal>
  );
};
