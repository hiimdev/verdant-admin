import { EditOutlined, EyeOutlined, UsergroupAddOutlined } from '@ant-design/icons';
import { AnyAction } from '@reduxjs/toolkit';
import { Button, Space } from 'antd';
import { TableTitle } from 'components/Table/component/TableTitle';
import { Dispatch } from 'react';
import { AiFillEye, AiOutlineInfo } from 'react-icons/ai';
import { setEditingId, setLogId, setReceiveId } from 'store/ducks/vendor/slice';
import { VENDOR_SORT } from 'utils/constant';
import { timeFormatter } from 'utils/date';
import styles from './styles.module.less';

export const getColumns = (dispatch: Dispatch<AnyAction>, submitParams?: any, isUpdate?: boolean) => {
  return [
    {
      title: 'No',
      dataIndex: 'index',
      key: 'index',
      width: '80px',
      align: 'center',
    },
    {
      title: (
        <TableTitle
          name="vendorName"
          label="Name"
          onSubmit={submitParams}
          sort={[VENDOR_SORT.VENDORNAME_DESC, VENDOR_SORT.VENDORNAME_ASC]}
        />
      ),
      dataIndex: 'vendorName',
      key: 'vendorName',
      render(data: string) {
        return <>{data || '-'}</>;
      },
    },
    {
      title: (
        <TableTitle
          name="description"
          label="Description"
          onSubmit={submitParams}
          sort={[VENDOR_SORT.DESCRIPTION_DESC, VENDOR_SORT.DESCRIPTION_ASC]}
        />
      ),
      dataIndex: 'description',
      key: 'description',
      render(data: string) {
        return <div className={styles.description}>{data ? data : 'No description'}</div>;
      },
      width: '200px',
    },
    {
      title: (
        <TableTitle
          name="countryName"
          label="Country"
          onSubmit={submitParams}
          sort={[VENDOR_SORT.DESCRIPTION_DESC, VENDOR_SORT.DESCRIPTION_ASC]}
        />
      ),
      dataIndex: 'countryName',
      key: 'countryName',
      width: '200px',
    },
    {
      title: (
        <TableTitle
          name="totalUser"
          label="Total Users"
          onSubmit={(values: any) =>
            submitParams({ ...values, totalUser: values.totalUser ? Number(values.totalUser) : undefined })
          }
          sort={[VENDOR_SORT.TOTALUSER_DESC, VENDOR_SORT.TOTALUSER_ASC]}
        />
      ),
      dataIndex: 'totalUser',
      key: 'totalUser',
      render(data: string) {
        return <>{data || '0'}</>;
      },
      align: 'center',
    },
    {
      title: (
        <TableTitle
          type="none"
          name="createdAt"
          label="Create At"
          onSubmit={submitParams}
          sort={[VENDOR_SORT.CREATEDAT_DESC, VENDOR_SORT.CREATEDAT_ASC]}
        />
      ),
      dataIndex: 'createdAt',
      key: 'createdAt',
      align: 'center',
      render(createdAt: string) {
        return <>{timeFormatter(Number(createdAt)) || '-'}</>;
      },
    },
    {
      title: (
        <TableTitle
          name="createdBy"
          label="Created By"
          onSubmit={submitParams}
          sort={[VENDOR_SORT.CREATEDBY_DESC, VENDOR_SORT.CREATEDBY_ASC]}
        />
      ),
      dataIndex: 'createdBy',
      key: 'createdBy',
      align: 'center',
      render(data: string) {
        return <>{data || '-'}</>;
      },
    },
    {
      title: (
        <TableTitle
          type="none"
          name="updatedAt"
          label="Update at"
          onSubmit={submitParams}
          sort={[VENDOR_SORT.UPDATEDAT_DESC, VENDOR_SORT.UPDATEDAT_ASC]}
        />
      ),
      dataIndex: 'updatedAt',
      key: 'updatedAt',
      render(updatedAt: string) {
        return <>{timeFormatter(Number(updatedAt)) || '-'}</>;
      },
    },
    {
      title: (
        <TableTitle
          name="updatedBy"
          label="Update By"
          onSubmit={submitParams}
          sort={[VENDOR_SORT.UPDATEDBY_DESC, VENDOR_SORT.UPDATEDBY_ASC]}
        />
      ),
      dataIndex: 'updatedBy',
      key: 'updatedBy',
      align: 'center',
      render(data: string) {
        return <>{data || '-'}</>;
      },
    },
    {
      title: 'Action',
      align: 'center',
      dataIndex: 'id',
      fixed: 'right',
      render(id: any) {
        return (
          <Space>
            <Button icon={<EyeOutlined />} onClick={() => dispatch(setLogId(String(id)))} title="View" />
            <Button
              icon={<EditOutlined />}
              type="primary"
              onClick={() => dispatch(setEditingId(String(id)))}
              title={isUpdate ? 'Edit' : 'View'}
            />
            <Button
              icon={<UsergroupAddOutlined />}
              onClick={() => dispatch(setReceiveId(String(id)))}
              title={'receive'}
            />
          </Space>
        );
      },
    },
  ];
};
