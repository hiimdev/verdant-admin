import { TransactionOutlined, UnorderedListOutlined } from '@ant-design/icons';
import { Button, Popover, Space, Typography } from 'antd';
import { TableTitle } from 'components/Table/component/TableTitle';
import { setViewDetailId } from 'store/ducks/vendor/slice';
import { USER_ENUMS, USER_SORT } from 'utils/constant';
import { timeFormatter } from 'utils/date';

export const getColumns = (
  dispatch,
  showId: string,
  open: boolean,
  submitParams?: any,
  email?: string,
  transfer?: any,
  setShowId?: any,
  handleOpenChange?: any
) => {
  return [
    {
      title: 'No',
      key: 'index',
      dataIndex: 'index',
      align: 'center',
    },
    {
      title: (
        <TableTitle
          label="Status"
          type="select"
          onSubmit={(values) => submitParams({ ...values, user: Number(values.user) })}
          name="user"
          obj={USER_ENUMS}
        />
      ),
      dataIndex: 'status',
      key: 'status',
      render(data: any) {
        return <>{data}</>;
      },
      align: 'center',
    },
    {
      title: (
        <TableTitle
          name="username"
          label="Username"
          onSubmit={submitParams}
          sort={[USER_SORT.USERNAME_DESC, USER_SORT.USERNAME_ASC]}
        />
      ),
      dataIndex: 'username',
      key: 'username',
    },
    {
      title: (
        <TableTitle
          name="email"
          label="Email"
          onSubmit={submitParams}
          sort={[USER_SORT.EMAIL_DESC, USER_SORT.EMAIL_ASC]}
        />
      ),
      dataIndex: 'email',
      key: 'email',
    },
    {
      title: (
        <TableTitle
          name=""
          label="VMTs Remaining"
          onSubmit={submitParams}
          // sort={[USER_SORT.EMAIL_DESC, USER_SORT.EMAIL_ASC]}
        />
      ),
      dataIndex: 'VMTs',
      key: 'VMTs',
      align: 'center',
      render(data: string) {
        return data || '-';
      },
    },
    {
      title: (
        <TableTitle
          name="wallet"
          label="Vedant Wallet Address"
          onSubmit={submitParams}
          // sort={[USER_SORT.EMAIL_DESC, USER_SORT.EMAIL_ASC]}
        />
      ),
      dataIndex: 'wallet',
      align: 'center',
      key: 'wallet',
      render(wallet: string) {
        return wallet || '-';
      },
    },
    {
      title: (
        <TableTitle
          type="none"
          name="createdAt"
          label="Created At"
          onSubmit={submitParams}
          sort={[USER_SORT.UPDATEAT_DESC, USER_SORT.UPDATEAT_ASC]}
        />
      ),
      dataIndex: 'createdAt',
      key: 'createdAt',
      render(createdAt: string) {
        return timeFormatter(Number(createdAt)) || '-';
      },
      align: 'center',
    },
    {
      title: (
        <TableTitle
          type="none"
          name="updatedAt"
          label="Update At"
          onSubmit={submitParams}
          sort={[USER_SORT.UPDATEAT_DESC, USER_SORT.UPDATEAT_ASC]}
        />
      ),
      dataIndex: 'updatedAt',
      key: 'updatedAt',
      render(updatedAt: string) {
        return timeFormatter(Number(updatedAt));
      },
      align: 'center',
    },
    {
      title: 'Action',
      align: 'center',
      dataIndex: 'id',
      fixed: 'right',
      render(id: any, record: any) {
        return (
          <Space style={{ position: 'relative' }}>
            <Button
              icon={<UnorderedListOutlined />}
              onClick={() => dispatch(setViewDetailId(String(id)))}
              title="View Vendor Detail"
            />
            {record.email != email ? (
              <Popover
                placement="top"
                visible={showId == record.id && open}
                title={
                  <div style={{ maxWidth: 300 }}>
                    <Typography.Paragraph
                      strong
                    >{`You are doing a VMT transfer from your wallet to ${record.email} wallet.`}</Typography.Paragraph>
                  </div>
                }
                content={transfer(record.wallet)}
                trigger="click"
                onVisibleChange={handleOpenChange}
              >
                <Button
                  icon={<TransactionOutlined />}
                  type="primary"
                  onClick={() => {
                    setShowId(record.id);
                  }}
                  title={'Transfer VMT'}
                  style={{ position: 'relative' }}
                />
              </Popover>
            ) : (
              <></>
            )}
          </Space>
        );
      },
    },
  ];
};
