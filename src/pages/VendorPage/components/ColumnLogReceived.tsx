import { UnorderedListOutlined } from '@ant-design/icons';
import { Button, Space, Button as AntdButton } from 'antd';
import { TableTitle } from 'components/Table/component/TableTitle';
import { AiFillEdit, AiOutlineDelete } from 'react-icons/ai';
import { setViewVendorReceivedDetailId, setDeletingId, setEditingIdVendorReceived } from 'store/ducks/vendor/slice';
import { USER_SORT } from 'utils/constant';
import { timeFormatter } from 'utils/date';
import styles from './styles.module.less';

export const getColumnsReceived = (
  dispatch,
  showId?: string,
  open?: boolean,
  submitParams?: any,
  email?: string,
  transfer?: any,
  setShowId?: any,
  handleOpenChange?: any
) => {
  return [
    {
      title: 'No',
      key: 'index',
      dataIndex: 'index',
      align: 'center',
    },
    {
      title: <TableTitle name="addressReceived" label="Address Received" onSubmit={submitParams} />,
      dataIndex: 'addressReceived',
      key: 'addressReceived',
      align: 'center',
    },
    {
      title: <TableTitle name="percentageReceived" label="Percentage Received" onSubmit={submitParams} />,
      dataIndex: 'percentageReceived',
      key: 'percentageReceived',
      render(data: number) {
        return <>{(data && data / 100) || '-'}%</>;
      },
      align: 'center',
    },
    {
      title: <TableTitle name="isGts" label="Is Gts" onSubmit={submitParams} />,
      dataIndex: 'isGts',
      key: 'isGts',
      align: 'center',
      render(data: number) {
        return <>{data && data === 1 ? 'true' : 'false' || '-'}</>;
      },
    },
    {
      title: <TableTitle type="none" name="createdAt" label="Created At" onSubmit={submitParams} />,
      dataIndex: 'createdAt',
      key: 'createdAt',
      render(createdAt: string) {
        return timeFormatter(Number(createdAt)) || '-';
      },
      align: 'center',
    },
    {
      title: <TableTitle type={'none'} label="Created By" onSubmit={submitParams} name="createdBy" />,
      dataIndex: 'createdBy',
      key: 'createdBy',
      render(data: string) {
        return <>{data || '-'}</>;
      },
      align: 'center',
    },
    {
      title: <TableTitle type="none" name="updatedAt" label="Update At" onSubmit={submitParams} />,
      dataIndex: 'updatedAt',
      key: 'updatedAt',
      render(updatedAt: string) {
        return timeFormatter(Number(updatedAt));
      },
      align: 'center',
    },
    {
      title: <TableTitle label="Updated By" onSubmit={submitParams} name="updatedBy" />,
      dataIndex: 'updatedBy',
      key: 'updatedBy',
      render(data: any, record: any) {
        return <>{data || record?.created_by || '-'}</>;
      },
      align: 'center',
    },
    {
      title: 'Action',
      align: 'center',
      dataIndex: 'id',
      fixed: 'right',
      render(id: any) {
        return (
          <Space style={{ position: 'relative' }}>
            <Button
              icon={<UnorderedListOutlined />}
              onClick={() => dispatch(setViewVendorReceivedDetailId(String(id)))}
              title="View Vendor Received Detail"
            />
            <AntdButton
              className={styles.btnAction}
              icon={<AiFillEdit />}
              type="primary"
              onClick={() => dispatch(setEditingIdVendorReceived(id))}
              title={'Edit'}
            />
            <AntdButton
              className={styles.btnAction}
              icon={<AiOutlineDelete />}
              danger
              onClick={() => dispatch(setDeletingId(String(id)))}
              title="Delete"
            />
          </Space>
        );
      },
    },
  ];
};
