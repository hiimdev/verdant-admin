import { MinusCircleOutlined, PlusOutlined } from '@ant-design/icons';
import { Form } from 'antd';
import { useCategoriesQuery } from 'api/category';
import { IError } from 'api/types';
import { useGetAllCountries, useGetDetailVendor } from 'api/user';
import { createVendor, updateVendor } from 'api/user/requet';
import { Button } from 'components/Button';
import { Drawer } from 'components/Drawer';
import { Input } from 'components/Input';
import { Select } from 'components/Select';
import { useAppDispatch, useAppSelector } from 'hooks/reduxHook';
import { FC, useEffect, useMemo } from 'react';
import { useMutation, useQueryClient } from 'react-query';
import { getEditingId, setEditingId } from 'store/ducks/vendor/slice';
import { message } from 'utils/message';
import styles from './styles.module.less';
import { IListType } from './type';

const Editor: FC<IListType> = ({ isUpdate: canUpdate }) => {
  const dispatch = useAppDispatch();
  const editingId = useAppSelector(getEditingId);
  const client = useQueryClient();

  const [form] = Form.useForm();

  const action = editingId ? 'edit' : 'add';

  const isUpdate = useMemo(() => {
    return (canUpdate && editingId !== '') || editingId === '';
  }, []);

  const { data, status: getLoading } = useGetDetailVendor(editingId as string, { enabled: !!editingId });

  const { data: categories } = useCategoriesQuery({ page: 1, limit: 100 });

  useEffect(() => {
    form.resetFields();
    if (editingId !== null) {
      form.setFieldsValue({
        ...data,
        categoryCommissionIds: data?.listCategory?.map((item) => item.categoryCommissionId),
      });
    }
  }, [data]);

  const { mutate: create, isLoading: loadingCreate } = useMutation(createVendor, {
    onSuccess: () => {
      client.invalidateQueries('/vendor/paging_filter');
      message.success('Create successfully!');
      handleClose();
    },
    onError: (error: IError) => {
      message.error(error?.meta.message[0]);
    },
  });

  const { mutate: update, isLoading: loadingUpdate } = useMutation(updateVendor, {
    onSuccess: () => {
      client.invalidateQueries('/vendor/paging_filter');
      message.success('Update successfully!');
      handleClose();
    },
    onError: (error: IError) => {
      message.error(error?.meta.message[0]);
    },
  });

  const handleClose = () => {
    dispatch(setEditingId(null));
  };
  const onFinish = (value: any) => {
    const temp = Array.from(new Set(value.categoryCommissionIds));

    const mutate = action === 'edit' ? update : create;
    const params =
      action === 'add'
        ? { ...value, categoryCommissionIds: temp }
        : {
            ...value,
            id: editingId,
            categoryCommissionIds: temp,
          };
    mutate(params);
  };
  const { data: countries } = useGetAllCountries({ page: 1, limit: 300 });

  const dataCountries = countries?.list.map((item) => {
    const temp = { value: item.id, label: item.name, filterSearch: String(item.name) };
    return temp;
  });

  return (
    <Drawer
      name="Vendor"
      editingId={editingId}
      handleClose={handleClose}
      form={form}
      submitLoading={loadingCreate || loadingUpdate}
      getLoading={getLoading === 'loading'}
      viewOnly={!isUpdate}
    >
      <Form form={form} onFinish={onFinish}>
        <Form.Item
          label="Vendor Name"
          name="vendorName"
          labelCol={{ span: 3 }}
          wrapperCol={{ span: 18 }}
          rules={[{ required: true }]}
        >
          <Input disabled={!isUpdate} placeholder={'Enter your vendor name'} />
        </Form.Item>
        <Form.Item
          label="Description"
          name="description"
          labelCol={{ span: 3 }}
          wrapperCol={{ span: 18 }}
          rules={[{ required: true }]}
        >
          <Input disabled={!isUpdate} placeholder={'Enter vendor description'} />
        </Form.Item>
        <Form.Item label="Category" name="categoryCommissionIds" labelCol={{ span: 3 }} wrapperCol={{ span: 18 }}>
          {/* <Select placeholder="Select Category">
            <Select.Option disabled={true}>--- none ---</Select.Option>
            {categories?.list.map((item) => (
              <Select.Option key={item.id} value={item.id}>
                {item.categoryName}
              </Select.Option>
            ))}
          </Select> */}
          <Form.List name="categoryCommissionIds">
            {(fields, { add, remove }) => (
              <>
                {fields.map((field, index) => (
                  <div className={styles.listBrands} key={index}>
                    <Form.Item
                      {...field}
                      style={{ width: '90%', marginBottom: 0 }}
                      rules={[{ required: true, message: 'Category is Required' }]}
                    >
                      <Select disabled={!isUpdate} placeholder="Select Category">
                        <Select.Option disabled={true}>--- none ---</Select.Option>
                        {categories?.list.map((item) => (
                          <Select.Option key={item.id} value={item.id}>
                            {item.categoryName}
                          </Select.Option>
                        ))}
                      </Select>
                    </Form.Item>
                    <MinusCircleOutlined
                      onClick={() => {
                        remove(field.name);
                      }}
                    />
                  </div>
                ))}
                <Button
                  onClick={() => {
                    add();
                  }}
                  block
                >
                  <PlusOutlined /> Add Category
                </Button>
              </>
            )}
          </Form.List>
        </Form.Item>
        <Form.Item
          label="Country"
          name="countryId"
          labelCol={{ span: 3 }}
          wrapperCol={{ span: 18 }}
          rules={[{ required: true }]}
        >
          <Select
            showSearch
            disabled={!isUpdate}
            placeholder="Select country"
            options={dataCountries}
            filterOption={(input, option) => option?.filterSearch.toLowerCase().includes(input)}
          ></Select>
          {/* <InfiniteSelect
            hasMore={hasNextCountry as boolean}
            loading={isFetchingMoreCountry || isCountryLoading}
            loadMore={fetchMoreCountry}
            placeholder="Select country"
          >
            {contriesData?.map((item) => (
              <InfiniteSelect.Option key={item.id} value={item.id}>
                {item.name}
              </InfiniteSelect.Option>
            ))}
          </InfiniteSelect> */}
        </Form.Item>
      </Form>
    </Drawer>
  );
};

export default Editor;
