import { Space, Input, Modal } from 'antd';
import { PaginatedTableAndSearch } from 'components/PaginatedTableAndSearch';
import { useAppDispatch, useAppSelector } from 'hooks/reduxHook';
import { FC, useMemo, useState } from 'react';
import { getLogId, setLogId } from 'store/ducks/vendor/slice';
import { debounce } from 'utils/js';
import { postMethod, useGetListUserFromVendor } from 'api/user';
import { Button } from 'antd';
import { useUser } from 'hooks/useUser';
import { getColumns } from './ColumnLog';
import { message } from 'utils/message';
import { CONTRACT_ADDRESS } from 'utils/constant';
import TOKEN from 'contracts/TokenABI.json';
import { useGetTokenBalanceToken_, useGetTokenBalance_ } from 'hooks/useGetBalance';
import BigNumber from 'bignumber.js';
import { useMutation } from 'react-query';
import { IError } from 'api/types';

const Log: FC<{
  handleShowModal: () => void;
}> = ({ handleShowModal }) => {
  const dispatch = useAppDispatch();
  const logId = useAppSelector(getLogId);
  const submitParams = (value: any) => {
    debouncedSetParams((prevParams: any) => ({
      ...prevParams,
      ...value,
      sort: value.sort ? [value.sort] : [],
    }));
  };
  const { user } = useUser();
  const email = user.dataReturn.email;
  const balanceToken = useGetTokenBalanceToken_(user.wallet || '', CONTRACT_ADDRESS.TOKEN, TOKEN);
  const handleClick = () => {
    handleShowModal();
  };

  const { mutate: mutatePostWalletSystem } = useMutation(postMethod, {
    onSuccess: () => {
      setValue('0');
      setShowId('');
    },

    onError: (error: IError) => {
      message.error(error.meta.message);
    },
  });

  const handleConfirm = (value) => {
    if (!value.value || value.value === '0') {
      return message.info('value must be great than 0');
    }
    if (value.value > balanceToken.value) {
      return message.info('This account is not enough TOKEN');
    }
    const bigNumber = new BigNumber(Number(value.value) || '0').multipliedBy(10 ** 18);
    const request = {
      method: 'withDrawToken',
      currency: 'polygon',
      data: [CONTRACT_ADDRESS.TOKEN, `${bigNumber.toPrecision(bigNumber.e ? bigNumber.e + 1 : 0)}`, value.wallet],
    };
    mutatePostWalletSystem(request);
  };

  const [value, setValue] = useState('0');
  const [open, setOpen] = useState(false);
  const [showId, setShowId] = useState('');
  const hide = () => {
    setShowId('');
  };

  const handleOpenChange = (newOpen: boolean) => {
    setOpen(newOpen);
    setValue('0');
  };

  const transfer = (wallet) =>
    useMemo(() => {
      return (
        <Space direction="vertical" size={'small'} style={{ display: 'flex' }}>
          <Input
            defaultValue={0}
            min={0}
            value={value}
            onChange={(e) => {
              setValue(e.target.value);
            }}
            type="number"
          />
          <div
            style={{
              textAlign: 'right',
            }}
          >
            <Button
              size="large"
              type="default"
              onClick={() => {
                hide();
                setShowId('');
                setValue('0');
              }}
              style={{ marginRight: 10 }}
            >
              Cancel
            </Button>
            <Button
              size="large"
              type="primary"
              onClick={() => {
                handleConfirm({ value, wallet });
              }}
            >
              Confirm
            </Button>
          </div>
        </Space>
      );
    }, [value, showId]);
  const columns = useMemo(
    () => getColumns(dispatch, showId, open, submitParams, email, transfer, setShowId, handleOpenChange),
    [getColumns, value, showId, open]
  );
  const [params, setParams] = useState<any>({});
  const visible = logId !== null;
  const handleClose = () => {
    dispatch(setLogId(null));
    setParams({});
  };
  const debouncedSetParams = useMemo(() => debounce(setParams, 500), []);

  return (
    <Modal
      width={1150}
      visible={visible}
      title={`List User From ID: ${logId}`}
      onOk={handleClose}
      onCancel={handleClose}
      cancelButtonProps={{ hidden: true }}
      maskClosable={false}
    >
      <PaginatedTableAndSearch
        title="List of Vendor User"
        columns={columns}
        useQuery={useGetListUserFromVendor}
        requestParams={{ ...params, vendorId: logId }}
        othersExtra={
          <Button size="large" type="primary" onClick={handleClick}>
            Add Vendor Users
          </Button>
        }
      />
    </Modal>
  );
};

export default Log;
