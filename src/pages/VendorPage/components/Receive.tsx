import { Checkbox, Form } from 'antd';
import { IError } from 'api/types';
import { createVendorReceived, updateVendorReceived } from 'api/user/requet';
import { Drawer } from 'components/Drawer';
import { Input } from 'components/Input';
import { useAppDispatch, useAppSelector } from 'hooks/reduxHook';
import { FC, useEffect, useMemo, useState } from 'react';
import { useMutation, useQueryClient } from 'react-query';
import {
  getReceiveId,
  setReceiveId,
  getEditingIdVendorReceived,
  setEditingIdVendorReceived,
} from 'store/ducks/vendor/slice';
import { message } from 'utils/message';
import { PaginatedTableAndSearch } from 'components/PaginatedTableAndSearch';
import { getColumns } from './ColumnLog';
import { debounce } from 'utils/js';
import { useGetListVendorReceived, useGetDetailVendorReceived } from 'api/user';
import { getColumnsReceived } from './ColumnLogReceived';
import { CheckboxChangeEvent } from 'antd/lib/checkbox';

const Receive: FC = () => {
  const dispatch = useAppDispatch();
  const client = useQueryClient();
  const receiveId = useAppSelector(getReceiveId);
  const editingIdVendorReceived = useAppSelector(getEditingIdVendorReceived);

  const [form] = Form.useForm();

  const { data } = useGetDetailVendorReceived(editingIdVendorReceived as string, {
    enabled: !!editingIdVendorReceived,
  });

  useEffect(() => {
    form.resetFields();
    if (editingIdVendorReceived !== null) {
      form.setFieldsValue({
        ...data,
        percentageReceived: (data?.percentageReceived ?? 0) / 100,
      });
    }
  }, [data, editingIdVendorReceived]);

  const isReceived = useMemo(() => {
    return receiveId === '';
  }, []);

  const { mutate: create, isLoading: loadingCreate } = useMutation(createVendorReceived, {
    onSuccess: () => {
      client.invalidateQueries('/vendor/list-vendor-received');
      message.success('Create successfully!');
    },
    onError: (error: IError) => {
      message.error(error?.meta.message[0]);
    },
  });

  const { mutate: update, isLoading: loadingUpdate } = useMutation(updateVendorReceived, {
    onSuccess: () => {
      client.invalidateQueries('/vendor/list-vendor-received');
      message.success('Update successfully!');
    },
    onError: (error: IError) => {
      message.error(error?.meta.message[0]);
    },
  });

  const handleClose = () => {
    dispatch(setReceiveId(null));
    dispatch(setEditingIdVendorReceived(null));
  };

  const onFinish = (value: any) => {
    if (editingIdVendorReceived) {
      const params = {
        vendorId: Number(data?.vendorId),
        addressReceived: value?.addressReceived,
        percentageReceived: Number(value?.percentageReceived) * 100,
        isGts: valueCheckbox,
        id: Number(editingIdVendorReceived),
      };
      update(params);
    } else {
      const params = {
        vendorId: Number(receiveId),
        addressReceived: value?.addressReceived,
        percentageReceived: Number(value?.percentageReceived) * 100,
        isGts: valueCheckbox,
      };
      create(params);
    }
  };

  const submitParams = (value: any) => {
    debouncedSetParams((prevParams: any) => ({
      ...prevParams,
      ...value,
      sort: value.sort ? [value.sort] : [],
    }));
  };

  const [params, setParams] = useState<any>({});

  const debouncedSetParams = useMemo(() => debounce(setParams, 500), []);
  const [open] = useState(false);
  const [showId] = useState('');
  const [valueCheckbox, setValueCheckbox] = useState<boolean>(false);

  const handleChecked = (e: CheckboxChangeEvent) => {
    setValueCheckbox(e.target.checked);
  };

  const columns = useMemo(() => getColumnsReceived(dispatch, showId, open, submitParams), [getColumns, showId, open]);

  return (
    <>
      <Drawer
        name="Vendor Received"
        handleClose={handleClose}
        form={form}
        submitLoading={loadingCreate || loadingUpdate}
        viewOnly={isReceived}
        editingId={editingIdVendorReceived ?? ''}
      >
        <Form form={form} onFinish={onFinish}>
          <Form.Item
            label="Address Received"
            name="addressReceived"
            labelCol={{ span: 3 }}
            wrapperCol={{ span: 18 }}
            rules={[{ required: true }]}
          >
            <Input placeholder={'Enter Address Received'} />
          </Form.Item>
          <Form.Item
            label="Percentage Received"
            name="percentageReceived"
            labelCol={{ span: 3 }}
            wrapperCol={{ span: 18 }}
            rules={[{ required: true }]}
          >
            <Input placeholder={'Enter Percentage Received'} />
          </Form.Item>
          <Form.Item label="Is GTS" name="isGts" labelCol={{ span: 3 }}>
            <Checkbox
              value={editingIdVendorReceived ? Boolean(data?.isGts) : valueCheckbox}
              defaultChecked={editingIdVendorReceived ? Boolean(data?.isGts) : valueCheckbox}
              onChange={handleChecked}
              style={{ marginTop: 7 }}
            ></Checkbox>
          </Form.Item>
        </Form>
        <PaginatedTableAndSearch
          title="List of Vendor Received"
          columns={columns}
          useQuery={useGetListVendorReceived}
          requestParams={{ ...params, vendorId: receiveId }}
        />
      </Drawer>
    </>
  );
};

export default Receive;
