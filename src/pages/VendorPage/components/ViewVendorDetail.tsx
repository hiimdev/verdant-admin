import { Modal } from 'antd';
import { useGetExternalWallet } from 'api/user';
import { PaginatedTableAndSearch } from 'components/PaginatedTableAndSearch';
import { useAppDispatch, useAppSelector } from 'hooks/reduxHook';
import { FC, useMemo } from 'react';
import { getViewDetailId, setViewDetailId } from 'store/ducks/vendor/slice';
import { timeFormatter } from 'utils/date';

const ViewVendorDetail: FC = () => {
  const getColumns = () => {
    return [
      {
        title: 'No',
        dataIndex: 'index',
        key: 'index',
        align: 'center',
        width: '80px',
      },

      {
        title: 'External Wallet Address',
        dataIndex: 'wallet',
        key: 'wallet',
        align: 'center',
        render(data: any) {
          return <>{data || '-'}</>;
        },
      },
      {
        title: 'Status',
        dataIndex: 'status',
        key: 'status',
        align: 'center',
        render(data: any) {
          return <>{data || '-'}</>;
        },
      },
      {
        title: 'Last time connect',
        dataIndex: 'updatedAt',
        key: 'updatedAt',
        align: 'center',
        render(data: any) {
          return <>{timeFormatter(Number(data)) || '-'}</>;
        },
      },
    ];
  };

  const columns = useMemo(() => getColumns(), [getColumns]);
  const userId = useAppSelector(getViewDetailId);
  const visible = userId !== null;
  const dispatch = useAppDispatch();

  const handleClose = () => {
    dispatch(setViewDetailId(null));
  };

  return (
    <Modal
      title={`List Wallet From ID: ${userId}`}
      width={1150}
      visible={visible}
      onOk={handleClose}
      onCancel={handleClose}
      cancelButtonProps={{ hidden: true }}
    >
      <PaginatedTableAndSearch
        requestParams={{ userId }}
        title="View Vendor Detail"
        columns={columns}
        useQuery={useGetExternalWallet}
      />
    </Modal>
  );
};

export default ViewVendorDetail;
