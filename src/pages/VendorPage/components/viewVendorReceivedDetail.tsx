/* eslint-disable no-constant-condition */
import { Modal, Table } from 'antd';
import { useGetDetailVendorReceived } from 'api/user';
import { useAppDispatch, useAppSelector } from 'hooks/reduxHook';
import { FC } from 'react';
import { getViewVendorReceivedDetailId, setViewVendorReceivedDetailId } from 'store/ducks/vendor/slice';
import { timeFormatter } from 'utils/date';
import type { ColumnsType } from 'antd/es/table';

interface DataType {
  addressReceived: string;
  percentageReceived: number;
  isGts: number;
  createdAt?: string;
  updatedAt?: string;
  createdBy?: string;
  updatedBy?: string;
}

const COLUMNS: ColumnsType<DataType> = [
  {
    title: 'Address Received',
    dataIndex: 'addressReceived',
    key: 'addressReceived',
    align: 'center',
  },
  {
    title: 'Percentage Received',
    dataIndex: 'percentageReceived',
    key: 'percentageReceived',
    align: 'center',
    render(data: number) {
      return <>{(data && data / 100) || '-'}</>;
    },
  },
  {
    title: 'Is Gts',
    dataIndex: 'isGts',
    key: 'isGts',
    align: 'center',
    render(data: number) {
      return <>{data && data === 1 ? 'true' : 'false' || '-'}</>;
    },
  },
  {
    title: 'Created At',
    dataIndex: 'createdAt',
    key: 'createdAt',
    align: 'center',
  },
  {
    title: 'Updated At',
    dataIndex: 'updatedAt',
    key: 'updatedAt',
    align: 'center',
  },
  {
    title: 'Created By',
    dataIndex: 'createdBy',
    key: 'createdBy',
    align: 'center',
  },
];

const ViewVendorReceiveDetail: FC = () => {
  const receivedDetailId = useAppSelector(getViewVendorReceivedDetailId);
  const visible = receivedDetailId !== null;
  const dispatch = useAppDispatch();

  const { data: dataDetail, isLoading } = useGetDetailVendorReceived(String(receivedDetailId), {
    enabled: !!receivedDetailId,
  });

  const data: DataType[] = [
    {
      addressReceived: String(dataDetail?.addressReceived) === 'undefined' ? '-' : String(dataDetail?.addressReceived),
      percentageReceived: dataDetail?.percentageReceived ?? 0,
      isGts: dataDetail?.isGts ?? 0,
      createdAt: timeFormatter(Number(dataDetail?.createdAt)),
      updatedAt: timeFormatter(Number(dataDetail?.updatedAt)),
      createdBy: String(dataDetail?.createdBy) === 'undefined' ? '-' : String(dataDetail?.createdBy),
    },
  ];

  const handleClose = () => {
    dispatch(setViewVendorReceivedDetailId(null));
  };

  return (
    <Modal
      title={`Vender Received Detail From ID: ${receivedDetailId}`}
      width={1200}
      visible={visible}
      onOk={handleClose}
      onCancel={handleClose}
      cancelButtonProps={{ hidden: true }}
    >
      <Table columns={COLUMNS} dataSource={data} loading={isLoading} />
    </Modal>
  );
};

export default ViewVendorReceiveDetail;
