import { useAppSelector } from 'hooks/reduxHook';
import { FC, useMemo, useState } from 'react';
import { getPermission } from 'store/ducks/user/slice';
import { Permission } from 'utils/permission';
import Editor from './components/Editor';
import List from './components/List';
import Log from './components/Log';
import {
  getEditingId,
  getLogId,
  getViewDetailId,
  getReceiveId,
  getViewVendorReceivedDetailId,
  getDeletingId,
} from 'store/ducks/vendor/slice';
import { AddVendorUsers } from './components/AddVendorUsers';
import ViewVendorDetail from './components/ViewVendorDetail';
import ViewVendorReceiveDetail from './components/viewVendorReceivedDetail';
import Receive from './components/Receive';
import Deletor from './components/DeletorVendorReceived';

const Vendor: FC = () => {
  const permissions = useAppSelector(getPermission);
  const editingId = useAppSelector(getEditingId);
  const receiveId = useAppSelector(getReceiveId);
  const receiveDetaiId = useAppSelector(getViewVendorReceivedDetailId);
  const logId = useAppSelector(getLogId);
  const viewVendorId = useAppSelector(getViewDetailId);
  const deletingId = useAppSelector(getDeletingId);

  const isUpdate = useMemo(() => {
    return permissions.includes(Permission.edit_vendor);
  }, [permissions]);
  const [show, setShow] = useState(false);
  const handleShowModal = () => {
    setShow(true);
  };
  const handleClose = () => {
    setShow(false);
  };

  return (
    <>
      <List isUpdate={isUpdate} />
      {editingId !== null && <Editor isUpdate={isUpdate} />}
      {logId && <Log handleShowModal={handleShowModal} />}
      {show ? <AddVendorUsers show={show} handleClose={handleClose} /> : <></>}
      {viewVendorId !== null && <ViewVendorDetail />}
      {receiveDetaiId !== null && <ViewVendorReceiveDetail />}
      {receiveId !== null && <Receive />}
      {deletingId !== null && <Deletor />}
    </>
  );
};

export default Vendor;
