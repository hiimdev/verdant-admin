import { AnyAction, Dispatch } from '@reduxjs/toolkit';
import { TableTitle } from 'components/Table/component/TableTitle';
import { timeFormatter } from 'utils/date';
import { convertPriceEther } from 'utils/common';
import { SearchFilter, SelectFilter } from 'components/Popover';
import { Tooltip } from 'antd';
import { setAddress } from 'store/ducks/statical/slice';

export const getColumns = (dispatch: Dispatch<AnyAction>, submitParams?: any) => {
  return [
    {
      title: 'No',
      dataIndex: 'index',
      key: 'index',
      align: 'center',
    },
    {
      title: <TableTitle label="Vendor" onSubmit={submitParams} name="vendor_name" sort={[1, 2]} />,
      dataIndex: 'vendor_name',
      key: 'vendor_name',
      align: 'center',
      render(data: any) {
        return <>{data || '-'}</>;
      },
    },
    {
      title: (
        <div className="flex-center-between">
          Status
          <SelectFilter
            label=""
            name="status"
            onSubmit={(values: any) =>
              submitParams({ ...values, status: values.status ? Number(values.status) : undefined })
            }
            obj={{ 1: 'Active', 2: 'Inactive' }}
          />
        </div>
      ),
      dataIndex: 'isVendor',
      key: 'isVendor',
      align: 'center',
      render(data: any) {
        return <>{data === 1 ? 'Active' : 'InActive'}</>;
      },
    },
    {
      title: (
        <div className="flex-center-between">
          Email
          <SearchFilter label="Email" onSubmit={submitParams} name="email" sort={[1, 2]} />
        </div>
      ),
      dataIndex: 'email',
      key: 'email',
      align: 'center',
      render(data: any) {
        return (
          <>
            <Tooltip title={data} placement="top">
              {`${data?.slice(0, 15)}...` || '-'}
            </Tooltip>{' '}
          </>
        );
      },
    },
    {
      title: (
        <div className="flex-center-between">
          Username
          <SearchFilter label="Username" onSubmit={submitParams} name="username" sort={[1, 2]} />
        </div>
      ),
      dataIndex: 'username',
      key: 'username',
      align: 'center',
      render(data: any) {
        return (
          <>
            <Tooltip title={data} placement="top">
              {`${data?.slice(0, 15)}...` || '-'}
            </Tooltip>{' '}
          </>
        );
      },
    },
    {
      title: (
        <div className="flex-center-between">
          <> Total Issued Tokens by Verdant</>
          <SearchFilter label="" onSubmit={submitParams} name="number_dmt" sort={[1, 2]} />
        </div>
      ),
      dataIndex: 'number_dmt',
      key: 'number_dmt',
      align: 'center',
      width: '130px',
    },
    {
      title: (
        <div className="flex-center-between">
          <> Total NFT minted</>
          <SearchFilter label="Total NFT minted" onSubmit={submitParams} name="number_minted" sort={[1, 2]} />
        </div>
      ),
      dataIndex: 'number_minted',
      key: 'number_minted',
      align: 'center',
      render(data: any, record: any) {
        return (
          <div
            style={{ cursor: 'pointer', color: '#4192ff', textDecoration: 'underline' }}
            onClick={() => dispatch(setAddress(record?.address))}
          >
            {data}
          </div>
        );
      },
      width: '110px',
    },
    {
      title: (
        <div className="flex-center-between">
          <>Total NFT traded</>
          <SearchFilter label="" onSubmit={submitParams} name="number_sold" sort={[1, 2]} />
        </div>
      ),
      dataIndex: 'number_sold',
      key: 'number_sold',
      align: 'center',
      width: '110px',
      render(data: any) {
        return data || 0;
      },
    },
    {
      title: (
        <div className="flex-center-between">
          <>Average Price of traded NFTs</>
          <SearchFilter label="Vendor" onSubmit={submitParams} name="vendor_name" sort={[1, 2]} />
        </div>
      ),
      dataIndex: 'number_price',
      key: 'number_price',
      align: 'center',
      width: '140px',
      render(data: any) {
        return data || 0;
      },
    },
    {
      title: (
        <div className="flex-center-between">
          <>Last Issued Token at</>
          <SelectFilter label="" onSubmit={submitParams} sort={[1, 2]} />
        </div>
      ),
      dataIndex: 'last_issued_at',
      key: 'last_issued_at',
      align: 'center',
      render(data: string) {
        return timeFormatter(Number(data));
      },
    },
    {
      title: (
        <div className="flex-center-between">
          Last Issued Token by
          <SearchFilter label="" onSubmit={submitParams} name="last_issued_by" sort={[1, 2]} />
        </div>
      ),
      dataIndex: 'last_issued_by',
      key: 'last_issued_by',
      align: 'center',
    },
    {
      title: (
        <div className="flex-center-between">
          Last minted NFT at
          <SelectFilter label="" onSubmit={submitParams} sort={[1, 2]} />
        </div>
      ),
      dataIndex: 'last_minted_at',
      key: 'last_minted_at',
      align: 'center',
      render(data: string) {
        return timeFormatter(Number(data));
      },
    },
    {
      title: (
        <div className="flex-center-between">
          Last minted NFT by
          <SearchFilter label="" onSubmit={submitParams} name="last_minted_by" sort={[1, 2]} />
        </div>
      ),
      dataIndex: 'last_minted_by',
      key: 'last_minted_by',
      align: 'center',
      render(data: any) {
        return (
          <>
            <Tooltip title={data} placement="top">
              {`${data?.slice(0, 15)}...` || '-'}
            </Tooltip>{' '}
          </>
        );
      },
    },
  ];
};
