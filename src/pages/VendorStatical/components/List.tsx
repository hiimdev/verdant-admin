import { DatePicker, Table as TableAntd, Typography } from 'antd';
import { useGetVendorStatical } from 'api/statistical';
import { Header } from 'components/Header';
import { Input } from 'components/Input';
import { PaginatedTableAndSearch } from 'components/PaginatedTableAndSearch';
import { useAppSelector } from 'hooks/reduxHook';
import { FC, useMemo, useState } from 'react';
import { useDispatch } from 'react-redux';
import { getAddress } from 'store/ducks/statical/slice';
import { convertPriceEther } from 'utils/common';
import { debounce } from 'utils/js';
import { getColumns } from './Column';
import ListOfVendor from './ListOfVendor';
import styles from './styles.module.less';

const { Paragraph } = Typography;

const { RangePicker } = DatePicker;

const List: FC = () => {
  const dispatch = useDispatch();

  const [params, setParams] = useState<any>({ sort: [] });
  const debouncedSetParams = useMemo(() => debounce(setParams, 500), []);
  const submitParams = (value: any) => {
    debouncedSetParams((prevParams: any) => ({
      ...prevParams,
      ...value,
      sort: value.sort ? [value.sort] : [],
    }));
  };

  const columns = getColumns(dispatch, submitParams);
  const address = useAppSelector(getAddress);

  const { data } = useGetVendorStatical({ page: 1, limit: 10 });

  const Summary = useMemo(() => {
    return (
      <>
        <TableAntd.Summary.Row>
          <TableAntd.Summary.Cell className={styles.cellTd} index={0} colSpan={5}>
            <Paragraph style={{ textAlign: 'center', margin: 0 }}>Total</Paragraph>
          </TableAntd.Summary.Cell>
          <TableAntd.Summary.Cell className={styles.cellTd} index={1}>
            <Paragraph style={{ textAlign: 'center', margin: 0 }}>{data?.summary?.total_number_dmt || 0}</Paragraph>
          </TableAntd.Summary.Cell>
          <TableAntd.Summary.Cell className={styles.cellTd} index={2}>
            <Paragraph style={{ textAlign: 'center', margin: 0 }}>{data?.summary?.total_number_minted || 0}</Paragraph>
          </TableAntd.Summary.Cell>
          <TableAntd.Summary.Cell className={styles.cellTd} index={3}>
            <Paragraph style={{ textAlign: 'center', margin: 0 }}>{data?.summary?.total_number_sold || 0}</Paragraph>
          </TableAntd.Summary.Cell>
          <TableAntd.Summary.Cell className={styles.cellTd} index={4}>
            <Paragraph style={{ textAlign: 'center', margin: 0 }}>
              {data?.summary?.total_number_price ? convertPriceEther(data?.summary?.total_number_price) : 0}
            </Paragraph>
          </TableAntd.Summary.Cell>
        </TableAntd.Summary.Row>
      </>
    );
  }, [data]);

  return (
    <div className={styles.root}>
      <Header title="" viewOnly>
        <Input
          placeholder="Vendor Name"
          onChange={(ev: any) =>
            debouncedSetParams((prevParams: any) => ({
              ...prevParams,
              vender: ev.target.value,
            }))
          }
        />
        <RangePicker
          onChange={(ev: any) =>
            debouncedSetParams((prevParams: any) => ({
              ...prevParams,
              from: ev && ev[0] ? new Date(ev[0]).getTime() : '',
              to: ev && ev[1] ? new Date(ev[1]).getTime() : '',
            }))
          }
        />
      </Header>
      <PaginatedTableAndSearch
        title="Vendor Distribution"
        summary={Summary}
        columns={columns}
        useQuery={useGetVendorStatical}
        requestParams={params}
        handleReset={() => setParams({ sort: [] })}
      />

      {address && <ListOfVendor params={params} />}
    </div>
  );
};

export default List;
