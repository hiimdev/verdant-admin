import { PaginatedTableAndSearch } from 'components/PaginatedTableAndSearch';
import { FC } from 'react';
import { useGetNFTMitedVendor } from 'api/statistical';
import { Modal } from 'antd';
import { convertPriceEther } from 'utils/common';
import { useAppDispatch, useAppSelector } from 'hooks/reduxHook';
import { getAddress, setAddress } from 'store/ducks/statical/slice';

const getColumns = () => {
  return [
    {
      title: 'No',
      dataIndex: 'index',
      key: 'index',
    },
    {
      title: 'Token ID',
      dataIndex: 'token_id',
      key: 'tokenId',
      align: 'center',
    },
    {
      title: 'Name',
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: 'Status',
      dataIndex: 'is_onsale',
      key: 'is_onsale',
      render(is_onsale: any) {
        return <>{is_onsale ? 'On Sale' : 'Not On Sale'}</>;
      },
    },
    {
      title: 'Price',
      dataIndex: 'price',
      key: 'price',
      align: 'center',
      render(price: any) {
        return <>{price ? convertPriceEther(price) : '-'}</>;
      },
    },
    {
      title: 'Collection',
      dataIndex: 'collection_name',
      key: 'collectionName',
    },
    {
      title: 'Owner',
      dataIndex: 'owner_name',
      key: 'ownerName',
    },
  ];
};

const ListOfVendor: FC<{ params: any }> = ({ params }) => {
  const dispatch = useAppDispatch();
  const columns = getColumns();
  const address = useAppSelector(getAddress);
  const visible = address !== null;

  const handleClose = () => {
    dispatch(setAddress(null));
  };

  return (
    <Modal
      width={1150}
      visible={visible}
      title={`ADDRESS: ${address}`}
      onOk={handleClose}
      onCancel={handleClose}
      cancelButtonProps={{ hidden: true }}
    >
      <PaginatedTableAndSearch
        columns={columns}
        useQuery={useGetNFTMitedVendor}
        requestParams={{ ...params, address: address }}
      />
    </Modal>
  );
};

export default ListOfVendor;
