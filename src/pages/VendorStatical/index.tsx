import { FC } from 'react';
import List from './components/List';

const Vendor: FC = () => {
  return (
    <>
      <List />
    </>
  );
};

export default Vendor;
