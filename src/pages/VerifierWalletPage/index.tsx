import { useContractFunction } from '@usedapp/core';
import { Button, Form, Row, Typography } from 'antd';
import { useGetVerifierWallet } from 'api/statistical';
import clsx from 'clsx';
import { Input } from 'components/Input';
import MarketABI from 'contracts/MarketABI.json';
import NFTABI from 'contracts/NFTABI.json';
import { useConnectABI } from 'hooks/useConnectABI';
import { useEffect } from 'react';
import { CONTRACT_ADDRESS } from 'utils/constant';
import { message } from 'utils/message';
import styles from './styles.module.less';

function SmartContractSetting() {
  const { data: master } = useGetVerifierWallet();

  const { state, send } = useContractFunction(useConnectABI(MarketABI, CONTRACT_ADDRESS.MARKET), 'setVerifier');
  const { state: nftState, send: nftSend } = useContractFunction(
    useConnectABI(NFTABI, CONTRACT_ADDRESS.NFT),
    'setVerifier'
  );
  const onFinish = (values) => {
    console.log(values, 'values');
    send(values.wallet);
  };

  const onFinishNFT = (values) => {
    nftSend(values.wallet);
  };

  useEffect(() => {
    if (state.status === 'Success') {
      message.success('Succcessfully');
    } else if (['Exception', 'Fail'].includes(state.status)) {
      message.error(state.errorMessage || 'Exception');
    }
  }, [state]);

  useEffect(() => {
    if (nftState.status === 'Success') {
      message.success('Succcessfully');
    } else if (['Exception', 'Fail'].includes(nftState.status)) {
      message.error(nftState.errorMessage || 'Exception');
    }
  }, [nftState]);

  if (!master) {
    return null;
  }

  return (
    <>
      <div className={clsx(styles.root)}>
        <div className={clsx(styles.container)}>
          <Typography.Title className={clsx(styles.title)} level={3}>
            Smart contract setting (Market)
          </Typography.Title>
          <Form onFinish={onFinish} layout="vertical" initialValues={{ wallet: master }}>
            <Form.Item label="Set verifier" name="wallet" rules={[{ required: true }]}>
              <Input />
            </Form.Item>
            <Row justify="center">
              <Button htmlType="submit" type="primary">
                Save
              </Button>
            </Row>
          </Form>
        </div>
      </div>

      <div className={clsx(styles.root)}>
        <div className={clsx(styles.container)}>
          <Typography.Title className={clsx(styles.title)} level={3}>
            Smart contract setting (NFT)
          </Typography.Title>
          <Form onFinish={onFinishNFT} layout="vertical" initialValues={{ wallet: master }}>
            <Form.Item label="Set verifier" name="wallet" rules={[{ required: true }]}>
              <Input />
            </Form.Item>
            <Row justify="center">
              <Button htmlType="submit" type="primary">
                Save
              </Button>
            </Row>
          </Form>
        </div>
      </div>
    </>
  );
}

export default SmartContractSetting;
