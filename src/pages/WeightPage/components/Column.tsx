import { AnyAction, Dispatch } from '@reduxjs/toolkit';
import { Button as AntdButton, Space } from 'antd';
import { TableTitle } from 'components/Table/component/TableTitle';
import { AiFillEdit, AiFillEye } from 'react-icons/ai';
import { setWeightId } from 'store/ducks/system/slice';
import { timeFormatter } from 'utils/date';
import styles from './styles.module.less';

export const getColumns = (dispatch: Dispatch<AnyAction>, submitParams?: any, isUpdate?: boolean) => {
  return [
    {
      title: 'No',
      dataIndex: 'index',
      key: 'index',
      align: 'center',
      width: '80px',
    },
    {
      title: <TableTitle name="gram" label="Gram" onSubmit={submitParams} />,
      dataIndex: 'gram',
      key: 'gram',
      align: 'center',
      render(data: number) {
        return <>{Number(Number(data).toFixed(2))}</>;
      },
    },
    {
      title: <TableTitle name="ounce" label="Ounce" onSubmit={submitParams} />,
      dataIndex: 'ounce',
      key: 'ounce',
      align: 'center',
      render(data: number) {
        return <>{Number(Number(data).toFixed(2))}</>;
      },
    },
    {
      title: <TableTitle name="status" label="Status" onSubmit={submitParams} />,
      dataIndex: 'status',
      key: 'status',
      align: 'center',
      render(status: number) {
        return <>{status ? 'Active' : 'InActive'}</>;
      },
    },
    {
      title: <TableTitle type="none" label="Update at" onSubmit={submitParams} name="updated_at" />,
      dataIndex: 'updated_at',
      key: 'updated_at',
      render(created_at: string) {
        return timeFormatter(Number(created_at));
      },
      align: 'center',
    },
    {
      title: 'Action',
      align: 'center',
      dataIndex: 'id',
      fixed: 'right',
      render(id: string) {
        return (
          <Space>
            <AntdButton
              className={styles.btnAction}
              icon={isUpdate ? <AiFillEdit /> : <AiFillEye />}
              type="primary"
              onClick={() => dispatch(setWeightId(id))}
              title={isUpdate ? 'Edit' : 'View'}
            />
          </Space>
        );
      },
    },
  ];
};
