import { Form } from 'antd';
import { useGetWeightDetail } from 'api/admin';
import { addWeight, updateWeight } from 'api/admin/request';
import { IError } from 'api/types';
import { Drawer } from 'components/Drawer';
import { Input } from 'components/Input';
import { Select } from 'components/Select';
import { useAppDispatch, useAppSelector } from 'hooks/reduxHook';
import { FC, useEffect, useMemo } from 'react';
import { useMutation, useQueryClient } from 'react-query';
import { getWeightId, setWeightId } from 'store/ducks/system/slice';
import { WEIGHT_PERCENT } from 'utils/constant';
import { message } from 'utils/message';
import { IListType } from './type';

const Editor: FC<IListType> = ({ isUpdate: canUpdate }) => {
  const dispatch = useAppDispatch();
  const editingId = useAppSelector(getWeightId);
  const client = useQueryClient();

  const isUpdate = useMemo(() => {
    return (canUpdate && editingId !== '') || editingId === '';
  }, []);

  const [form] = Form.useForm();

  const action = editingId ? 'edit' : 'add';

  const { data, status: getLoading } = useGetWeightDetail(editingId as string, { enabled: !!editingId });

  const { mutate: update, isLoading: loadingUpdate } = useMutation(updateWeight, {
    onSuccess: () => {
      client.invalidateQueries('/config/admin/list-weight-data');
      message.success('Successfully!');
      handleClose();
    },
    onError: (error: IError) => {
      message.error(error?.meta.message[0]);
    },
  });

  const { mutate: create, isLoading: loadingCreate } = useMutation(addWeight, {
    onSuccess: () => {
      client.invalidateQueries('/config/admin/list-weight-data');
      message.success('Successfully!');
      handleClose();
    },
    onError: (error: IError) => {
      message.error(error?.meta.message[0]);
    },
  });

  useEffect(() => {
    form.resetFields();
    if (data) {
      form.setFieldsValue({
        ...data,
        gram: Number(data?.gram),
      });
    }
  }, [data]);

  const handleClose = () => {
    dispatch(setWeightId(null));
  };

  const onFinish = (value: any) => {
    const request = {
      ...value,
      gram: Number(value.gram),
    };
    if (action === 'add') {
      create(request);
    } else {
      update({
        ...request,
        id: editingId,
      });
    }
  };

  return (
    <Drawer
      name="Weight"
      editingId={editingId}
      handleClose={handleClose}
      form={form}
      getLoading={getLoading === 'loading'}
      submitLoading={loadingUpdate || loadingCreate}
      viewOnly={false}
    >
      <Form form={form} onFinish={onFinish}>
        <Form.Item
          label="Gram"
          name="gram"
          labelCol={{ span: 3 }}
          wrapperCol={{ span: 18 }}
          rules={[{ required: true }]}
        >
          <Input
            type="number"
            placeholder={'Enter Gram'}
            onChange={(e) => {
              form.setFieldsValue({ ounce: Number(e.target.value) * WEIGHT_PERCENT });
            }}
          />
        </Form.Item>
        <Form.Item
          label="Ounce"
          name="ounce"
          labelCol={{ span: 3 }}
          wrapperCol={{ span: 18 }}
          rules={[{ required: true }]}
        >
          <Input
            type="number"
            placeholder={'Enter ounce'}
            onChange={(e) => {
              form.setFieldsValue({ gram: Number(e.target.value) / WEIGHT_PERCENT });
            }}
          />
        </Form.Item>
        <Form.Item
          label="Active"
          name="status"
          labelCol={{ span: 3 }}
          wrapperCol={{ span: 18 }}
          rules={[{ required: true }]}
        >
          <Select placeholder="Select status">
            {[
              { value: 1, label: 'Active' },
              { value: 0, label: 'Inactive' },
            ].map((item: any) => (
              <Select.Option key={item?.value} value={item?.value}>
                {item?.label}
              </Select.Option>
            ))}
          </Select>
        </Form.Item>
      </Form>
    </Drawer>
  );
};

export default Editor;
