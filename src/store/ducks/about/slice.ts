import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from 'store';

export interface IAboutStore {
  editingId: string | null;
  deletingId: string | null;
  requestParams: { name: string };
  edittingCategoryId: string | null;
}

const initialState: IAboutStore = {
  editingId: null,
  deletingId: null,
  requestParams: { name: '' },
  edittingCategoryId: null,
};

export const aboutSlice = createSlice({
  name: 'about',
  initialState,
  reducers: {
    setEditingId: (state, action: PayloadAction<string | null>) => {
      return {
        ...state,
        editingId: action.payload,
      };
    },
    setDeletingId: (state, action: PayloadAction<string | null>) => {
      return {
        ...state,
        deletingId: action.payload,
      };
    },
    setRequestParams: (state, action: PayloadAction<any>) => {
      return {
        ...state,
        requestParams: action.payload,
      };
    },
    setEditingCategoryId: (state, action: PayloadAction<string | null>) => {
      return {
        ...state,
        edittingCategoryId: action.payload,
      };
    },
  },
});

export const getEditingId = (state: RootState) => state.about.editingId;
export const getDeletingId = (state: RootState) => state.about.deletingId;
export const getRequestParams = (state: RootState) => state.about.requestParams;
export const getEditingCategoryId = (state: RootState) => state.about.edittingCategoryId;

export const { setEditingId, setDeletingId, setRequestParams, setEditingCategoryId } = aboutSlice.actions;

export default aboutSlice.reducer;
