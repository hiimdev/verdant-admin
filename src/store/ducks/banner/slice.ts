import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from 'store';

export interface IBannerStore {
  editingId: string | null;
  deletingId: string | null;
  requestParams: { name: string };
}

const initialState: IBannerStore = {
  editingId: null,
  deletingId: null,
  requestParams: { name: '' },
};

export const bannerSlice = createSlice({
  name: 'banner',
  initialState,
  reducers: {
    setEditingId: (state, action: PayloadAction<string | null>) => {
      return {
        ...state,
        editingId: action.payload,
      };
    },
    setDeletingId: (state, action: PayloadAction<string | null>) => {
      return {
        ...state,
        deletingId: action.payload,
      };
    },
    setRequestParams: (state, action: PayloadAction<any>) => {
      return {
        ...state,
        requestParams: action.payload,
      };
    },
  },
});

export const getEditingId = (state: RootState) => state.banner.editingId;
export const getDeletingId = (state: RootState) => state.banner.deletingId;
export const getRequestParams = (state: RootState) => state.banner.requestParams;

export const { setEditingId, setDeletingId, setRequestParams } = bannerSlice.actions;

export default bannerSlice.reducer;
