import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from 'store';

export interface IBlackListStore {
  editingId: string | null;
  deletingId: string | null;
  requestParams: { name: string };
}

const initialState: IBlackListStore = {
  editingId: null,
  deletingId: null,
  requestParams: { name: '' },
};

export const blackListSlice = createSlice({
  name: 'blackList',
  initialState,
  reducers: {
    setEditingId: (state, action: PayloadAction<string | null>) => {
      return {
        ...state,
        editingId: action.payload,
      };
    },
    setDeletingId: (state, action: PayloadAction<string | null>) => {
      return {
        ...state,
        deletingId: action.payload,
      };
    },
    setRequestParams: (state, action: PayloadAction<any>) => {
      return {
        ...state,
        requestParams: action.payload,
      };
    },
  },
});

export const getEditingId = (state: RootState) => state.blackList.editingId;
export const getDeletingId = (state: RootState) => state.blackList.deletingId;
export const getRequestParams = (state: RootState) => state.blackList.requestParams;

export const { setEditingId, setDeletingId, setRequestParams } = blackListSlice.actions;

export default blackListSlice.reducer;
