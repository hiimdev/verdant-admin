import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from 'store';

export interface IBrandStore {
  editingId: string | null;
  deletingId: string | null;
  requestParams: { name: string; status: string };
}

const initialState: IBrandStore = {
  editingId: null,
  deletingId: null,
  requestParams: { name: '', status: '' },
};

export const brandSlice = createSlice({
  name: 'brand',
  initialState,
  reducers: {
    setEditingId: (state, action: PayloadAction<string | null>) => {
      return {
        ...state,
        editingId: action.payload,
      };
    },
    setDeletingId: (state, action: PayloadAction<string | null>) => {
      return {
        ...state,
        deletingId: action.payload,
      };
    },
    setRequestParams: (state, action: PayloadAction<any>) => {
      return {
        ...state,
        requestParams: action.payload,
      };
    },
  },
});

export const getEditingId = (state: RootState) => state.brand.editingId;
export const getDeletingId = (state: RootState) => state.brand.deletingId;
export const getRequestParams = (state: RootState) => state.brand.requestParams;

export const { setEditingId, setDeletingId, setRequestParams } = brandSlice.actions;

export default brandSlice.reducer;
