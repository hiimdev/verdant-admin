import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from 'store';

export interface ICategoryStore {
  editingId: string | null;
  deletingId: string | null;
  requestParams: { name: string };
}

const initialState: ICategoryStore = {
  editingId: null,
  deletingId: null,
  requestParams: { name: '' },
};

export const categorySlice = createSlice({
  name: 'category',
  initialState,
  reducers: {
    setEditingId: (state, action: PayloadAction<string | null>) => {
      return {
        ...state,
        editingId: action.payload,
      };
    },
    setDeletingId: (state, action: PayloadAction<string | null>) => {
      return {
        ...state,
        deletingId: action.payload,
      };
    },
    setRequestParams: (state, action: PayloadAction<any>) => {
      return {
        ...state,
        requestParams: action.payload,
      };
    },
  },
});

export const getEditingId = (state: RootState) => state.category.editingId;
export const getDeletingId = (state: RootState) => state.category.deletingId;
export const getRequestParams = (state: RootState) => state.category.requestParams;

export const { setEditingId, setDeletingId, setRequestParams } = categorySlice.actions;

export default categorySlice.reducer;
