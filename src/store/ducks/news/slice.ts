import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from 'store';

export interface INewsStore {
  editingId: string | null;
  deletingId: string | null;
  requestParams: { name: string };
}

const initialState: INewsStore = {
  editingId: null,
  deletingId: null,
  requestParams: { name: '' },
};

export const newsSlice = createSlice({
  name: 'news',
  initialState,
  reducers: {
    setEditingId: (state, action: PayloadAction<string | null>) => {
      return {
        ...state,
        editingId: action.payload,
      };
    },
    setDeletingId: (state, action: PayloadAction<string | null>) => {
      return {
        ...state,
        deletingId: action.payload,
      };
    },
    setRequestParams: (state, action: PayloadAction<any>) => {
      return {
        ...state,
        requestParams: action.payload,
      };
    },
  },
});

export const getEditingId = (state: RootState) => state.news.editingId;
export const getDeletingId = (state: RootState) => state.news.deletingId;
export const getRequestParams = (state: RootState) => state.news.requestParams;

export const { setEditingId, setDeletingId, setRequestParams } = newsSlice.actions;

export default newsSlice.reducer;
