import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from 'store';

interface LogProps {
  id: string | null;
  type: string;
}
export interface INFTStore {
  editingId: string | null;
  rateId: string | null;
  logId: string | null;
  logRateId: LogProps | null;
}

const initialState: INFTStore = {
  editingId: null,
  logId: null,
  rateId: null,
  logRateId: null,
};

export const nftSlice = createSlice({
  name: 'nft',
  initialState,
  reducers: {
    setEditingId: (state, action: PayloadAction<string | null>) => {
      return {
        ...state,
        editingId: action.payload,
      };
    },
    setRateId: (state, action: PayloadAction<string | null>) => {
      return {
        ...state,
        rateId: action.payload,
      };
    },
    setLogId: (state, action: PayloadAction<string | null>) => {
      return {
        ...state,
        logId: action.payload,
      };
    },
    setLogRateId: (state, action: PayloadAction<LogProps | null>) => {
      return {
        ...state,
        logRateId: action.payload,
      };
    },
  },
});

export const getEditingId = (state: RootState) => state.nft.editingId;
export const getRateId = (state: RootState) => state.nft.rateId;
export const getLogId = (state: RootState) => state.nft.logId;
export const getLogRateId = (state: RootState) => state.nft.logRateId;

export const { setEditingId, setRateId, setLogId, setLogRateId } = nftSlice.actions;

export default nftSlice.reducer;
