import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from 'store';

export interface ITaxStore {
  editingId: string | null;
  deletingId: string | null;
  requestParams: { name: string };
}

const initialState: ITaxStore = {
  editingId: null,
  deletingId: null,
  requestParams: { name: '' },
};

export const taxSlice = createSlice({
  name: 'tax',
  initialState,
  reducers: {
    setEditingId: (state, action: PayloadAction<string | null>) => {
      return {
        ...state,
        editingId: action.payload,
      };
    },
    setDeletingId: (state, action: PayloadAction<string | null>) => {
      return {
        ...state,
        deletingId: action.payload,
      };
    },
    setRequestParams: (state, action: PayloadAction<any>) => {
      return {
        ...state,
        requestParams: action.payload,
      };
    },
  },
});

export const getEditingId = (state: RootState) => state.tax.editingId;
export const getDeletingId = (state: RootState) => state.tax.deletingId;
export const getRequestParams = (state: RootState) => state.tax.requestParams;

export const { setEditingId, setDeletingId, setRequestParams } = taxSlice.actions;

export default taxSlice.reducer;
