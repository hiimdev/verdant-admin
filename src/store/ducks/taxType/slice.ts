import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from 'store';

export interface ITaxTypeStore {
  editingId: string | null;
  deletingId: string | null;
  requestParams: { name: string };
}

const initialState: ITaxTypeStore = {
  editingId: null,
  deletingId: null,
  requestParams: { name: '' },
};

export const taxTypeSlice = createSlice({
  name: 'taxType',
  initialState,
  reducers: {
    setEditingId: (state, action: PayloadAction<string | null>) => {
      return {
        ...state,
        editingId: action.payload,
      };
    },
    setDeletingId: (state, action: PayloadAction<string | null>) => {
      return {
        ...state,
        deletingId: action.payload,
      };
    },
    setRequestParams: (state, action: PayloadAction<any>) => {
      return {
        ...state,
        requestParams: action.payload,
      };
    },
  },
});

export const getEditingId = (state: RootState) => state.taxType.editingId;
export const getDeletingId = (state: RootState) => state.taxType.deletingId;
export const getRequestParams = (state: RootState) => state.taxType.requestParams;

export const { setEditingId, setDeletingId, setRequestParams } = taxTypeSlice.actions;

export default taxTypeSlice.reducer;
