import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from 'store';

export interface IWhiteListStore {
  editingId: string | null;
  deletingId: string | null;
  requestParams: { name: string };
}

const initialState: IWhiteListStore = {
  editingId: null,
  deletingId: null,
  requestParams: { name: '' },
};

export const whiteListSlice = createSlice({
  name: 'whiteList',
  initialState,
  reducers: {
    setEditingId: (state, action: PayloadAction<string | null>) => {
      return {
        ...state,
        editingId: action.payload,
      };
    },
    setDeletingId: (state, action: PayloadAction<string | null>) => {
      return {
        ...state,
        deletingId: action.payload,
      };
    },
    setRequestParams: (state, action: PayloadAction<any>) => {
      return {
        ...state,
        requestParams: action.payload,
      };
    },
  },
});

export const getEditingId = (state: RootState) => state.whiteList.editingId;
export const getDeletingId = (state: RootState) => state.whiteList.deletingId;
export const getRequestParams = (state: RootState) => state.whiteList.requestParams;

export const { setEditingId, setDeletingId, setRequestParams } = whiteListSlice.actions;

export default whiteListSlice.reducer;
