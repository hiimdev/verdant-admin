import * as React from 'react';

declare global {
  namespace JSX {
    interface IntrinsicElements {
      'model-viewer': ModelViewerJSX & React.DetailedHTMLProps<React.HTMLAttributes<HTMLElement>, HTMLElement>;
    }
  }
}

interface ModelViewerJSX {
  src: string;
  alt?: string;
  poster?: string;
  'shadow-intensity'?: string;
  autoplay?: boolean;
  'auto-rotate'?: boolean;
  'camera-controls'?: boolean;
  ar?: boolean;
  'ar-modes'?: string;
  'interaction-prompt'?: string;
  'data-js-focus-visible'?: boolean;
  loading?: string;
  'ar-status'?: string;
  exposure?: string;
}

declare module 'quill-image-resize-module-react';
