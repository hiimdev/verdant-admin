import { shortenAddress } from '@usedapp/core';
import BigNumber from 'bignumber.js';
import { CONTRACT_ADDRESS } from './constant';

export const getLoadingButtonCallSMCT = (obj: any) => {
  return obj.status !== 'Exception' && obj.status !== 'None' && obj.status !== 'Success';
};

export const convertPriceEther = (price: any) => {
  return Number(new BigNumber(price || '0').dividedBy(10 ** 18).toFixed(3));
};

export const convertDecimalNumber = (number: any) => {
  return Number(Number.parseFloat(number).toFixed(3));
};

export const convertVendorName = (data: any) => {
  if (data?.owner_first_name || data?.owner_last_name)
    return `${data?.owner_first_name || ''} ${data?.owner_last_name || ''}`;
  return data?.owner_name || data?.owner_email || data?.human_owner;
};

export const convertHistoryFromName = (data: any) => {
  if (data?.user_from_first_name || data?.user_from_last_name)
    return `${data?.user_from_first_name || ''} ${data?.user_from_last_name || ''}`;
  return (
    data?.from_name ||
    data?.owner_from_name ||
    data?.owner_from_email ||
    ((data?.fromWallet || '').toLowerCase() === CONTRACT_ADDRESS.MARKET.toLowerCase()
      ? 'Digicap Market'
      : !data.from
      ? '-'
      : shortenAddress(data.from))
  );
};

export const convertHistoryToName = (data: any) => {
  if (data?.user_to_first_name || data?.user_to_last_name)
    return `${data?.user_to_first_name || ''} ${data?.user_to_last_name || ''}`;
  return (
    data?.to_name ||
    data?.owner_to_name ||
    data?.owner_to_email ||
    (data?.to.toLowerCase() === CONTRACT_ADDRESS.MARKET.toLowerCase()
      ? 'Digicap Market'
      : !data?.to
      ? '-'
      : shortenAddress(data.to))
  );
};

export const convertLogAction = (value: any) => {
  switch (value) {
    case 'mint':
      return 'Mint';
    case 'burn':
      return 'Burnt';
    case 'transfer':
      return 'Transfer';
    case 'OrderCreated':
      return 'Place Seller Offer';
    case 'Buy':
      return 'Trade';
    case 'OrderCancelled':
      return 'Cancel Seller Offer';
    case 'OrderUpdated':
      return 'Update Seller Offer';
    case 'BidCreated':
      return 'Place Buyer Bid';
    case 'AcceptBid':
      return 'Trade';
    case 'BidUpdated':
      return 'Update Buyer Bid';
    case 'BidCancelled':
      return 'Cancel Buyer Bid';
    default:
      return 'Mint';
  }
};

export const convertLogActionV2 = (value: any) => {
  switch (value) {
    case 'mint':
      return 'Mint';
    case 'burn':
      return 'Burn';
    case 'transfer':
      return 'Transfer';
    case 'OrderCreated':
      return 'List Selling Price';
    case 'Buy':
      return 'Last Traded Price';
    case 'OrderCancelled':
      return 'Cancel Sell';
    case 'OrderUpdated':
      return 'Update Sell';
    case 'BidCreated':
      return 'Create Offer';
    case 'AcceptBid':
      return 'Accept Offer';
    case 'BidUpdated':
      return 'Update Offer';
    case 'BidCancelled':
      return 'Cancel Offer';
    default:
      return 'Mint';
  }
};
