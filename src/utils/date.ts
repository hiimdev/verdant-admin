import moment from 'moment';

export const timeFormatter = (time: number) => {
  return moment(time).format('YYYY/MM/DD HH:mm:ss');
};

export const relativeTime = (date: string) => {
  var mom = moment(Number(date));
  return mom.fromNow();
};
