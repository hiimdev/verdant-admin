export function debounce(fn: any, wait: any) {
  let timeout: any;
  return (...args: any[]) => {
    window.clearTimeout(timeout);
    timeout = window.setTimeout(fn, wait, ...args);
  };
}

export const onDownload = (blob: any, fileName?: any) => {
  const url = window.URL.createObjectURL(new Blob([blob]));
  const link = document.createElement('a');
  const title = fileName === '' ? `Download` : fileName;

  link.href = url;
  link.setAttribute('download', `${title}.csv`);
  link.click();
  link.parentNode?.removeChild(link);
};
