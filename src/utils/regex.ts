export const REGEX = {
  password: /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/,
  userName: /^[^~`!_#@/\$%\^&\*\())\+=\?><,;:"\]\[\{\\|]+$/, //eslint-disable-line
  onlyLettersAndSpaces: /^[A-Za-z\s]*$/,
  typeEmail: /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/, //eslint-disable-line
  space: /<(.|\n)*?>/g,
};
